local _solutionName = "Aftermath"
local _ideName = "vs2013"
local _platform = "x64"


AM_PROJECT_ROOT = _WORKING_DIR .. "/../"
AM_SOURCE_ROOT = AM_PROJECT_ROOT .."src/"
AM_ENGINE_ROOT = AM_SOURCE_ROOT .. "Aftermath/"
AM_APP_ROOT = AM_SOURCE_ROOT .. "Apps/"
AM_SOLUTION_NAME = _solutionName .. "-" .. _ideName .. "-" .. _platform
AM_SOLUTION_DIR = AM_PROJECT_ROOT .. "../" .. _solutionName .. "-Build/" .. AM_SOLUTION_NAME
AM_TARGET_DIR = AM_SOLUTION_DIR .. "/bin"
AM_PLATFORM = _platform

-- A solution contains projects, and defines the available configurations
solution (AM_SOLUTION_NAME)
	configurations { "Debug", "Release" }
	platforms { (_platform) }

	local command = "copy " .. _WORKING_DIR .. "/libs/d3dcompiler_47.dll " .. AM_TARGET_DIR .. "/d3dcompiler_47.dll";
	command = command:gsub("/", "\\");

	includedirs {(AM_SOURCE_ROOT), (AM_ENGINE_ROOT)}
	
	location (AM_SOLUTION_DIR)
	targetdir(AM_TARGET_DIR)
	libdirs {(AM_TARGET_DIR)}

	vpaths 
	{ 
		["*"] = {"../src/Aftermath/*", "../src/Apps/*"}
	}

    configuration "windows"
        defines {"WIN32", "AM_PLATFORM_WINDOWS", "GLEW_STATIC", "_WIN32_WINNT_0x0501"}
        links {"gdi32", "winmm", "user32"}
        postbuildcommands { command}

   
   	--include (AM_SOURCE_ROOT .. "/libs/glew")
	include (AM_ENGINE_ROOT)
	include (AM_APP_ROOT)