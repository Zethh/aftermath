#!/bin/bash
echo $1
SOLUTION_NAME=$1
IDE_NAME=$2
ARCHITECTURE=$3
GENERATOR=$4
BUILD_DIR_NAME=${SOLUTION_NAME}-${IDE_NAME}-${ARCHITECTURE}
BUILD_FOLDER="${SOLUTION_NAME}-build"
echo "${SOLUTION_NAME}-${IDE_NAME}-${ARCHITECTURE}"
pushd ../..
if [ ! -d "${BUILD_FOLDER}" ]; then
    mkdir ${BUILD_FOLDER}
fi

pushd ${BUILD_FOLDER}
if [  ! -d "${BUILD_DIR_NAME}" ]; then
    mkdir ${BUILD_DIR_NAME}
fi

pushd ${BUILD_DIR_NAME}
if [ "$3" == "ios" ]; then
/Applications/CMake\ 2.8-12.app/Contents/bin/cmake -DCMAKE_TOOLCHAIN_FILE=/Users/CMac/Documents/aftermath/setup/iOS.cmake -DCMAKE_IOS_DEVELOPER_ROOT=/Applications/Xcode.app/Contents/Developer/Platforms/iPhoneOS.platform/Developer -G ${GENERATOR} -DSOLUTION_NAME=${SOLUTION_NAME} ../../${SOLUTION_NAME}/setup
else
/Applications/CMake\ 2.8-12.app/Contents/bin/cmake ../../${SOLUTION_NAME}/setup -G ${GENERATOR} -DSOLUTION_NAME=${SOLUTION_NAME}
fi
popd
popd

    

