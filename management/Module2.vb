Imports System
Imports EnvDTE
Imports EnvDTE80
Imports EnvDTE90
Imports EnvDTE90a
Imports EnvDTE100
Imports System.Diagnostics

Public Module Module2
    Sub FileSwitch()
        Try
            Dim CurrentPath As String = DTE.ActiveDocument.FullName
            Dim OtherPath As String

            If (IO.Path.HasExtension(CurrentPath)) Then
                Dim CurrentExtension As String = IO.Path.GetExtension(CurrentPath)

                Select Case CurrentExtension
                    Case ".h", ".hpp", ".hxx"
                        OtherPath = IO.Path.ChangeExtension(CurrentPath, ".cpp")
                        If (Not IO.File.Exists(OtherPath)) Then
                            OtherPath = IO.Path.ChangeExtension(CurrentPath, ".c")
                            If (Not IO.File.Exists(OtherPath)) Then
                                OtherPath = IO.Path.ChangeExtension(CurrentPath, ".cxx")
                            End If
                        End If
                    Case ".cpp", ".c", ".cxx"
                        OtherPath = IO.Path.ChangeExtension(CurrentPath, ".h")
                        If (Not IO.File.Exists(OtherPath)) Then
                            OtherPath = IO.Path.ChangeExtension(CurrentPath, ".hpp")
                            If (Not IO.File.Exists(OtherPath)) Then
                                OtherPath = IO.Path.ChangeExtension(CurrentPath, ".hxx")
                            End If
                        End If
                    Case Else
                End Select
                If (OtherPath <> Nothing) Then
                    DTE.ItemOperations.OpenFile(OtherPath)
                End If
            End If

        Catch ex As System.Exception
            MsgBox(ex.Message)
        End Try
    End Sub

End Module
