#pragma once

//#include <Aftermath/math/CommonTrig.h>
#include <iostream>
#include <stdio.h>
#include <cmath>



namespace am {

template <typename T> class TQuaternion;
template <typename T> class TVector4;
template <typename T> class TVector3;
template <class T>
class TMatrix4
{

public:
    TMatrix4()
    {
        value[0] = (T)0;
        value[1] = (T)0;
        value[2] = (T)0;
        value[3] = (T)0;
    }

	TMatrix4(T number)
	{
		value[0] = TVector4<T>( number,	0,	0,	0);
		value[1] = TVector4<T>( 0,	number,	0,	0);
		value[2] = TVector4<T>( 0,	0,	number,	0);
		value[3] = TVector4<T>( 0,	0,	0,	number);
	}

	TMatrix4(TVector4<T> v0, TVector4<T> v1, TVector4<T> v2, TVector4<T> v3)
	{
		value[0] = v0;
		value[1] = v1;
		value[2] = v2;
		value[3] = v3;
	}

    bool operator== (const TMatrix4<T>& m) const
    {
        return (value[0] == m[0] &&
                value[1] == m[1] &&
                value[2] == m[2] &&
                value[3] == m[3]);

    }

	unsigned int numComponents() { return 16; }

    void operator= (TMatrix4<T> m)
    {
        value[0] = m.value[0];
        value[1] = m.value[1];
        value[2] = m.value[2];
        value[3] = m.value[3];
    }

    TMatrix4<T> operator* (const TMatrix4<T>& m) const
	{
        TMatrix4<T> result;
		result[0] = value[0] * m[0][0] + value[1] * m[0][1] + value[2] * m[0][2] + value[3] * m[0][3];
		result[1] = value[0] * m[1][0] + value[1] * m[1][1] + value[2] * m[1][2] + value[3] * m[1][3];
		result[2] = value[0] * m[2][0] + value[1] * m[2][1] + value[2] * m[2][2] + value[3] * m[2][3];
		result[3] = value[0] * m[3][0] + value[1] * m[3][1] + value[2] * m[3][2] + value[3] * m[3][3];
		//return TMatrix4<T>(value[0] * m.value[0],
		//				  value[1] * m.value[1],
		//				  value[2] * m.value[2],
		//				  value[3] * m.value[3]);
		return result;

	}

    TMatrix4<T> operator/ (T scale) const
	{
		return TMatrix4<T>(value[0] / scale,
						  value[1] / scale,
						  value[2] / scale,
						  value[3] / scale);

	}

	TVector4<T>& operator[] (unsigned int i)
	{
		return value[i];
	}

	const TVector4<T>& operator[] (unsigned int i) const
	{
		return value[i];
	}

	TVector3<T> getRight() const
	{
		return std::move(TVector3<T>(value[0].x, value[0].y, value[0].z));
	}

	TVector3<T> getUp() const
	{
		return std::move(TVector3<T>(value[1].x, value[1].y, value[1].z));
	}

	TVector3<T> getForward() const
	{
		return std::move(TVector3<T>(-value[2].x, -value[2].y, -value[2].z));
	}

	TVector3<T> getBackward() const
	{
		return std::move(TVector3<T>(value[2].x, value[2].y, value[2].z));
	}

	TVector3<T> getTranslation() const
	{
		return std::move(TVector3<T>(value[3].x, value[3].y, value[3].z));
	}

    //friend TMatrix4 rotate( const TMatrix4<T>& m, T angle, const TVector3<T>& v)
    //{
    //    return rotate(m, angle, v);
    //}

    friend std::ostream& operator<< (std::ostream& out, const TMatrix4<T>& v)
    {
//        out << " v(" << v.x << ", " << v.y << ", " << v.z << ", " << v.w << ")";
        out << "m(" << std::endl << v[0] << std::endl << v[1] << std::endl << v[2] << std::endl << v[3] << std::endl << ")" << std::endl;
        return out;
    }

	struct { TVector4<T> value[4]; };

};


typedef TMatrix4<float> mat4;
typedef TMatrix4<float> Matrix4f;
typedef TMatrix4<double> Matrix4d;





template <typename T> 
TMatrix4<T> inverse(const TMatrix4<T>& m )
{

	T Coef00 = m[2][2] * m[3][3] - m[3][2] * m[2][3];
	T Coef02 = m[1][2] * m[3][3] - m[3][2] * m[1][3];
	T Coef03 = m[1][2] * m[2][3] - m[2][2] * m[1][3];

	T Coef04 = m[2][1] * m[3][3] - m[3][1] * m[2][3];
	T Coef06 = m[1][1] * m[3][3] - m[3][1] * m[1][3];
	T Coef07 = m[1][1] * m[2][3] - m[2][1] * m[1][3];

	T Coef08 = m[2][1] * m[3][2] - m[3][1] * m[2][2];
	T Coef10 = m[1][1] * m[3][2] - m[3][1] * m[1][2];
	T Coef11 = m[1][1] * m[2][2] - m[2][1] * m[1][2];

	T Coef12 = m[2][0] * m[3][3] - m[3][0] * m[2][3];
	T Coef14 = m[1][0] * m[3][3] - m[3][0] * m[1][3];
	T Coef15 = m[1][0] * m[2][3] - m[2][0] * m[1][3];

	T Coef16 = m[2][0] * m[3][2] - m[3][0] * m[2][2];
	T Coef18 = m[1][0] * m[3][2] - m[3][0] * m[1][2];
	T Coef19 = m[1][0] * m[2][2] - m[2][0] * m[1][2];

	T Coef20 = m[2][0] * m[3][1] - m[3][0] * m[2][1];
	T Coef22 = m[1][0] * m[3][1] - m[3][0] * m[1][1];
	T Coef23 = m[1][0] * m[2][1] - m[2][0] * m[1][1];
//
	TVector4<T> /*const */SignA(+1, -1, +1, -1);
	TVector4<T> /*const */SignB(-1, +1, -1, +1);

	TVector4<T> Fac0(Coef00, Coef00, Coef02, Coef03);
	TVector4<T> Fac1(Coef04, Coef04, Coef06, Coef07);
	TVector4<T> Fac2(Coef08, Coef08, Coef10, Coef11);
	TVector4<T> Fac3(Coef12, Coef12, Coef14, Coef15);
	TVector4<T> Fac4(Coef16, Coef16, Coef18, Coef19);
	TVector4<T> Fac5(Coef20, Coef20, Coef22, Coef23);
//
	TVector4<T> Vec0(m[1][0], m[0][0], m[0][0], m[0][0]);
	TVector4<T> Vec1(m[1][1], m[0][1], m[0][1], m[0][1]);
	TVector4<T> Vec2(m[1][2], m[0][2], m[0][2], m[0][2]);
	TVector4<T> Vec3(m[1][3], m[0][3], m[0][3], m[0][3]);
//
	TVector4<T> Inv0 = SignA * (Vec1 * Fac0 - Vec2 * Fac1 + Vec3 * Fac2);
	TVector4<T> Inv1 = SignB * (Vec0 * Fac0 - Vec2 * Fac3 + Vec3 * Fac4);
	TVector4<T> Inv2 = SignA * (Vec0 * Fac1 - Vec1 * Fac3 + Vec3 * Fac5);
	TVector4<T> Inv3 = SignB * (Vec0 * Fac2 - Vec1 * Fac4 + Vec2 * Fac5);
//
	TMatrix4<T> Inverse(Inv0, Inv1, Inv2, Inv3);
//
	TVector4<T> Row0(Inverse[0][0], Inverse[1][0], Inverse[2][0], Inverse[3][0]);
//
	T Determinant = dot(m[0], Row0);
//
	Inverse =  Inverse / Determinant;
//	    
	return Inverse;
}

template <typename T>
TMatrix4<T> transpose(const TMatrix4<T>& m)
{
        TMatrix4<T> result;
        result[0][0] = m[0][0];
        result[0][1] = m[1][0];
        result[0][2] = m[2][0];
        result[0][3] = m[3][0];

        result[1][0] = m[0][1];
        result[1][1] = m[1][1];
        result[1][2] = m[2][1];
        result[1][3] = m[3][1];

        result[2][0] = m[0][2];
        result[2][1] = m[1][2];
        result[2][2] = m[2][2];
        result[2][3] = m[3][2];

        result[3][0] = m[0][3];
        result[3][1] = m[1][3];
        result[3][2] = m[2][3];
        result[3][3] = m[3][3];
        return result;
}


template <typename T>
TMatrix4<T> translate(const TMatrix4<T>& m, const TVector3<T>& v)
{
    //return createTranslation(v) * createRotation(m);

	TMatrix4<T> result(m);

	result[3] = m[0] * v[0] + m[1] * v[1] + m[2] * v[2] + m[3];
	return result;
}

//
//

template <typename T>
TMatrix4<T> rotate( const TMatrix4<T>& m, T angle, const TVector3<T>& v)
{

    //TQuaternion<T> q0(m);
    //TQuaternion<T> q1(angle, v);

    //TMatrix4<T> rotation = (q0 * q1).getMatrix();
    //return (createTranslation(m) * createScale(m) * rotation ); 

    if(v == TVector3<T>(0)) 
        return m;

	T a = toRadians(angle);
	T c = std::cos(a);
	T s = std::sin(a);

	TVector3<T> axis = normalize(v);
	//TVector3<T> temp = axis*(T(1) - c);
	TVector3<T> temp = (T(1) - c) * axis;
	TMatrix4<T> rotation(T(0));

	rotation[0][0] = c + temp[0] * axis[0];
	rotation[0][1] = 0 + temp[0] * axis[1] + s * axis[2];
    rotation[0][2] = 0 + temp[0] * axis[2] - s * axis[1];

	rotation[1][0] = 0 + temp[1] * axis[0] - s * axis[2];
	rotation[1][1] = c + temp[1] * axis[1];
	rotation[1][2] = 0 + temp[1] * axis[2] + s * axis[0];

	rotation[2][0] = 0 + temp[2] * axis[0] + s * axis[1];
	rotation[2][1] = 0 + temp[2] * axis[1] - s * axis[0];
	rotation[2][2] = c + temp[2] * axis[2];

	TMatrix4<T> result(T(0));
	result[0] = m[0] * rotation[0][0] + m[1] * rotation[0][1] + m[2] * rotation[0][2];
	result[1] = m[0] * rotation[1][0] + m[1] * rotation[1][1] + m[2] * rotation[1][2];
	result[2] = m[0] * rotation[2][0] + m[1] * rotation[2][1] + m[2] * rotation[2][2];
	result[3] = m[3];

	return result;

}


template <typename T>
TMatrix4<T> createTranslation(const TVector3<T>& v)
{
    TMatrix4<T> translation(1);
    translation[3] = TVector4<T>(v.x, v.y, v.z, 1);
    return translation;
}

template <typename T>
TMatrix4<T> createRotation(const TMatrix4<T>& m)
{
    TMatrix4<T> rotation(m);
    rotation[3] = TVector4<T>(0, 0, 0, 1);
    return rotation;
}

template <typename T>
TMatrix4<T> createScale(const TVector3<T>& v)
{
    TMatrix4<T> scale(1.0);
    scale[0][0] = v.x;
    scale[1][1] = v.y;
    scale[2][2] = v.z;
    return scale;
}

template <typename T>
TMatrix4<T> createTranslation(const TMatrix4<T>& m)
{
    TMatrix4<T> translation(1.0);
    translation[3] = m[3];
    return translation;
}



template <typename T> 
TMatrix4<T> scale(const TMatrix4<T>& m,	const TVector3<T>& v)
{
    TMatrix4<T> result(T(0));
	result[0] = m[0] * v[0];
	result[1] = m[1] * v[1];
	result[2] = m[2] * v[2];
	result[3] = m[3];
	return result;
}


template <typename T>
TMatrix4<T> createPerspectiveMatrix( T fovy, T aspect, T zNear, T zFar)
{
	T range = std::tan(toRadians(fovy / T(2)) * zNear);
	T left = -range * aspect;
	T right = range * aspect;
	T bottom = -range;
	T top = range;

	TMatrix4<T> result(T(0));
	result[0][0] = (T(2) * zNear) / (right - left);
	result[1][1] = (T(2) * zNear) / (top - bottom);
	result[2][2] = - (zFar + zNear) / (zFar - zNear);
	result[2][3] = - T(1);
	result[3][2] = - (T(2) * zFar * zNear) / (zFar - zNear);
	return result;
}


template <typename T> 
TMatrix4<T> createOrthographicMatrix(T left, T right, T bottom, T top,	T zNear, T zFar)
{
	TMatrix4<T> result(1);
	result[0][0] = T(2) / (right - left);
	result[1][1] = T(2) / (top - bottom);
	result[2][2] = - T(2) / (zFar - zNear);
	result[3][0] = - (right + left) / (right - left);
	result[3][1] = - (top + bottom) / (top - bottom);
	result[3][2] = - (zFar + zNear) / (zFar - zNear);
	return result;
}

template <typename T>
TMatrix4<T> lookAt(const TVector3<T>& position, const TVector3<T>& targetPosition, const TVector3<T>& up)
{
    if(position == targetPosition)
        return createTranslation(position);

	TVector3<T> f = normalize(targetPosition - position);
	TVector3<T> u = normalize(up);
	TVector3<T> s = normalize(cross(f, u));
    u = normalize(cross(s, f));

	TMatrix4<T> rotation(1);
    rotation[0][0] = s.x;
    rotation[0][1] = s.y;
    rotation[0][2] = s.z;
    rotation[1][0] = u.x;
    rotation[1][1] = u.y;
    rotation[1][2] = u.z;
    rotation[2][0] = -f.x;
    rotation[2][1] = -f.y;
    rotation[2][2] = -f.z;

    return createTranslation(position) * rotation;

}

template <typename T>
TVector3<T> extractHeading(const TMatrix4<T>& matrix)
{
    return normalize(TVector3<T>(-matrix[2].x, -matrix[2].y, -matrix[2].z));
}

template <typename T>
TVector3<T> extractPitch(const TMatrix4<T>& matrix)
{
    return normalize(TVector3<T>(matrix[0].x, matrix[0].y, matrix[0].z));
}

template <typename T>
TVector3<T> extractRoll(const TMatrix4<T>& matrix)
{
    return normalize(TVector3<T>(matrix[1].x, matrix[1].y, matrix[1].z));
}

template <typename T>
TVector3<T> extractPosition(const TMatrix4<T>& matrix)
{
	return TVector3<T>(matrix[3].x, matrix[3].y, matrix[3].z);
}





} 