#pragma once

namespace am
{
	template <typename> class TVector3;
	template <class T>
	class TSphere
	{
	public:
		TSphere(const TVector3<T>& position, T radius)
			: _position(position)
			, _radius(radius)
		{

		}

		const TVector3<T>& getPosition() const
		{
			return _position;
		}

		const T getRadius() const
		{
			return _radius;
		}

		const T getRadiusSquared() const
		{
			return _radius * _radius;
		}

		friend std::ostream& operator<< (std::ostream& out, const TSphere<T>& v)
		{
			out << "Sphere Pos(" << v._position.x << ", " << v._position.y << ", " << v._position.z << ")" << " Radius(" << v._radius << ")";
			return out;
		}

	private:
		TVector3<T> _position;
		T _radius;
	};

	typedef TSphere<float> Sphere;
}