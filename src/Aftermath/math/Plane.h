#pragma once
#include <iostream>
namespace am
{
	template <typename> class TVector3;
	template <typename> class TVector4;
	template <class T>
	class TPlane
	{
	public:
		TPlane(const TVector3<T>& normal, T distance)
			: _normal(normal)
			, _distance(distance)
		{

		}

		TPlane(T a, T b, T c, T d)
			: _a(a)
			, _b(b)
			, _c(c)
			, _d(d)
		{

		}

		TPlane(const TVector4<T>& abcd)
			: _abcd(abcd)
		{

		}

		TPlane(const TVector3<T>& p0, const TVector3<T>& p1, const TVector3<T>& p2)
		{
			auto v = p1 - p0;
			auto u = p2 - p0;
			_normal = normalize(cross(v, u));
			_distance = -dot(_normal, p0);
		}

		TPlane(const TVector3<T>& point, const TVector3<T>& normal)
		{
			_normal = normalize(normal);
			_distance = -dot(point, _normal);
		}

		TVector3<T> getNormal() const
		{
			return _normal;
		}

		T getDistance() const
		{
			return _distance;
		}

		T distanceToPoint(const TVector3<T>& point) const
		{
			return dot(_normal, point) + _distance;
		}

		TVector3<T> project(const TVector3<T>& point) const
		{
			return (point - _normal * distanceToPoint(point));
		}

		friend std::ostream& operator<< (std::ostream& out, const TPlane<T>& v)
		{
			out << "Plane Normal(" << v._a<< ", " << v._b << ", " << v._c << ")" << " D(" << v._d << ")";
			return out;
		}

	private:
		union
		{
			struct { TVector3<T> _normal; T _distance; };
			struct { TVector4<T> _abcd; };
			struct { T _a; T _b; T _c; T _d; };
		};
	};

	typedef TPlane<float> Plane;
}