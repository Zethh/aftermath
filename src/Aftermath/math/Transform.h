#pragma once
#include "Quaternion.h"
#include "Matrix4.h"

namespace am
{
	template <typename> class TVector3;

	template <class T>
	class TTransform
	{
	public:
		__forceinline void setMatrix(const TMatrix4<T>& matrix) { _localMatrix = matrix; }
		__forceinline void setPosition(const TVector3<T>& position) { _translation = position; }
		__forceinline void setRotation(const TQuaternion<T>& rotation);
		__forceinline TVector3<T> getLocalPosition() const { return _translation; }
		__forceinline TMatrix4<T> getLocalMatrix() const { return _localMatrix; }
		TMatrix4<T> getWorldMatrix() const;

	private:
		union
		{
			struct { TMatrix4<T> _localMatrix; };
			struct { TVector3<T> _right;		T t0; 
					 TVector3<T> _up;			T t1;
					 TVector3<T> _forward;		T t2;
					 TVector3<T> _translation;	T t3;
			};
		};
		TTransform<T>* _parent = nullptr;
	};

	template <typename T> void TTransform<T>::setRotation(const TQuaternion<T>& rotation)
	{
		auto translation = _translation;
		_localMatrix = rotation.getMatrix();
		_translation = translation;
	}

	template <typename T> TMatrix4<T> TTransform<T>::getWorldMatrix() const
	{
		if (_parent != nullptr)
			return _parent->getWorldMatrix() * _localMatrix;
		return _localMatrix;
	}

	typedef TTransform<float> Transform;
}