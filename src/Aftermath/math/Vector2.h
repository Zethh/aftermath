#pragma once

#include <iostream>
#include <math.h>
#include <cmath>

namespace am {
template <class T>
class TVector2
{
public:

	union
	{
		struct {T x, y;};
		struct {T r, g;};
		struct {T element[2];};
	};

    TVector2()
    {
        x = (T)0;
        y = (T)0;
    }

    TVector2(const T& _x, const T& _y) { x = _x; y = _y;}

    TVector2(const T& value) { x = value; y = value;}

	unsigned int numComponents() { return 2; }

    TVector2<T> operator+ (const TVector2<T>& v) const
	{
		return TVector2<T>(x+v.x, y+v.y);
	}

    TVector2<T> operator- (const TVector2<T>& v) const
	{
        return TVector2<T>(x-v.x, y-v.y);
	}

    TVector2<T> operator/ (const T& value) const
	{
		return TVector2<T>(x/value, y/value);
	}

    void operator= (const T& value)
    {
        x = value;
        y = value;
    }

	TVector2<T> operator* (const TVector2<T>& v) const
	{
		return TVector2<T>(x*v.x, y*v.y);
	}

    TVector2<T> operator* (const T& s) const
	{
		return TVector2<T>(x*s, y*s);
	}

    T& operator[] (unsigned int i)
	{
		return element[i];
	}

	const T& operator[] (unsigned int i) const
	{
		return element[i];
	}

    bool operator == (const TVector2<T>& v) const
    {
        return (x == v.x && y == v.y);
    }

	friend std::ostream& operator<< (std::ostream& out, const TVector2<T>& v)
	{
		out << " v(" << v.x << ", " << v.y << ")";
		return out;
	}


	//T x;
	//T y;
};

typedef TVector2<float> Vector2f;
typedef TVector2<double> Vector2d;
typedef TVector2<unsigned int> Vector2u;
typedef TVector2<int> Vector2i;


//TVector2
template <typename T>
T dot(const TVector2<T>& lhs, const TVector2<T>& rhs)
{
	return lhs.x * rhs.x + lhs.y * rhs.y;
}

template <typename T>
T length( const TVector2<T>& v)
{
    T sqr = v.x * v.x + v.y * v.y;
    return std::sqrt(sqr);
}

template <typename T>
TVector2<T> normalize(const TVector2<T>& v)
{
	//Add functionality to support different square roots
    T magnitude = (T)(std::sqrt(v.x * v.x + v.y * v.y));
	TVector2<T> v2;
	v2 = v/magnitude;
	return v2;
}



template <typename T>
T distance( const TVector2<T>& lhs, const TVector2<T>& rhs)
{
    return length(rhs - lhs);
}

template <typename T>
const T* address(const TVector2<T>& v)
{
    return &(v.x);
}

}