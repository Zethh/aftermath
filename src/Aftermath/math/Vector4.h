#pragma once

#include <iostream>

namespace am {

template <typename> class TVector3;

template <class T>
class TVector4
{
public:

	union
	{
		struct {T x, y, z, w;};
		struct {T r, g, b, a;};
		struct {T element[4];};
	};

	union X
	{
		T x;
		T r;
		T element[1];
	};

	union Y
	{
		T y;
		T g;
		T element[1];
	};

    TVector4()
    {
        x = (T)0;
        y = (T)0;
        z = (T)0;
        w = (T)0;
    }

	//vec4(x, y, z, w)
	TVector4(T _x, T _y, T _z, T _w) { x = _x; y = _y; z = _z; w = _w;}

	//vec3(vec3, w)
    TVector4(const TVector3<T>& v, T _w)
	{ 
		x = v.x;
		y = v.y;
		z = v.z;
		w = _w;
	}

	//vec3(x, vec3)
    TVector4(T _x, const TVector3<T>& v) { x = _x; y = v.y; z = v.y; w = v.z;}

	TVector4(T value) { x = value; y = value; z = value; w = value; }

    unsigned int numComponents() const { return 4; }

    TVector4<T> operator+ (const TVector4<T>& v) const
	{
		TVector4<T> vec;
		vec.x = x + v.x;
		vec.y = y + v.y;
		vec.z = z + v.z;
		vec.w = w + v.w;
		return vec;
	}

    TVector4<T> operator- (const TVector4<T>& v) const
	{
		TVector4<T> vec;
		vec.x = x - v.x;
		vec.y = y - v.y;
		vec.z = z - v.z;
		vec.w = w - v.w;
		return vec;
	}

    TVector4<T> operator/ (T value) const
	{
		TVector4<T> vec;
		vec.x = x / value;
		vec.y = y / value;
		vec.z = z / value;
		vec.w = w / value;
		return vec;
	}

    TVector4<T> operator* (const TVector4<T>& v) const
	{
		TVector4<T> vec;
		vec.x = x * v.x;
		vec.y = y * v.y;
		vec.z = z * v.z;
		vec.w = w * v.w;
		return vec;
	}

	TVector4<T> operator* (T s) const
	{
		TVector4<T> vec;
		vec.x = x * s;
		vec.y = y * s;
		vec.z = z * s;
		vec.w = w * s;
		return vec;
	}

	T& operator[] (unsigned int i)
	{
		return element[i];
	}

	const T& operator[] (unsigned int i) const
	{
		return element[i];
	}

	friend std::ostream& operator<< (std::ostream& out, const TVector4<T>& v)
	{
		out << " v(" << v.x << ", " << v.y << ", " << v.z << ", " << v.w << ")";
		return out;
	}

    bool operator== (const TVector4<T>& v) const
    {
        return (x == v.x &&
                y == v.y &&
                z == v.z &&
                w == v.w);

    }

};

typedef TVector4<float> fvec4;
typedef TVector4<double> dvec4;
typedef TVector4<unsigned int> uvec4;
typedef TVector4<int> ivec4;

typedef TVector4<float> Vector4f;
typedef TVector4<double> Vector4d;
typedef TVector4<unsigned int> Vector4ui;
typedef TVector4<int> Vector4i;

typedef fvec4 vec4;
typedef Vector4f Vector4; 

//TVector4
template <typename T>
T dot(const TVector4<T>& lhs, const TVector4<T>& rhs)
{
    return (lhs.x * rhs.x + lhs.y * rhs.y + lhs.z * rhs.z + lhs.w * rhs.w);
}

template <typename T>
TVector4<T> normalize(const TVector4<T>& v)
{
	//Add functionality to support different square roots
    T magnitude = T(std::sqrt(v.x * v.x + v.y * v.y + v.z * v.z + v.w * v.w));
    TVector4<T> v2;
	v2 = v/magnitude;
	return v2;
}

template <typename T>
T length( const TVector4<T>& v)
{
	T sqr = v.x * v.x + v.y * v.y + v.z * v.z + v.w * v.w;
	return std::sqrt(sqr);
}

template <typename T>
T distance( const TVector4<T>& lhs, const TVector4<T>& rhs)
{
    return length(rhs-lhs);
}

template <typename T>
const T * address(const TVector4<T>& v)
{
	return &(v.x);
}


}