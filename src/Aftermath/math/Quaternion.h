#pragma once

namespace am {
	template <typename T> class TVector3;
	template <typename T> class TVector4;
	template <typename T> class TMatrix4;
    template <class T>
    class TQuaternion
    {
    public:
        union
        {
            struct { T w, x, y, z; };
        };

        TQuaternion()
        {
            x = (T)0;
            y = (T)0;
            z = (T)0;
            w = (T)1;
        }

        TQuaternion(T deg, const TVector3<T>& rotation)
        {
            const T pi = T(3.1415926535897932384626433832795);
            T theta =  deg / (T)360 * pi * (T)2;
            //T theta = toRadians<T>(deg);
            w = std::cos(theta/(T)2);
            x = rotation.x * std::sin(theta/(T)2);
            y = rotation.y * std::sin(theta/(T)2);
            z = rotation.z * std::sin(theta/(T)2);
        }

        TQuaternion(T _w, T _x, T _y, T _z)
        {
            w = _w;
            x = _x;
            y = _y;
            z = _z;
        }

        TQuaternion(const TVector3<T>& euler)
        {
            TVector3<T> eulerRad = toRadians(euler);
            TVector3<T> c(  std::cos(eulerRad.x * (T)0.5),
                            std::cos(eulerRad.y * (T)0.5),
                            std::cos(eulerRad.z * (T)0.5));
            
            TVector3<T> s(  std::sin(eulerRad.x * (T)0.5),
                            std::sin(eulerRad.y * (T)0.5),
                            std::sin(eulerRad.z * (T)0.5));

            w = c.x * c.y * c.z + s.x * s.y * s.z;
            x = s.x * c.y * c.z - c.x * s.y * s.z;
            y = c.x * s.y * c.z + s.x * c.y * s.z;
            z = c.x * c.y * s.z - s.x * s.y * c.z;


//                normalise();


        }

        TVector3<T> getForwardVector() const
        {
            return *this * TVector3<T>(0, 0, -1);
        }

        TVector3<T> getUpVector() const
        {
            return *this * TVector3<T>(0, 1, 0);
         /*   return TVector3<T>( T(2) * (x * y - w * z), 
                                T(1) - T(2) * (x * x + z * z),
                                (T(2) * (y * z + w * x)));*/
        }

        TVector3<T> getRightVector() const
        {
            return TVector3<T>( T(1) - T(2) * (y * y + z * z),
                                T(2) * (x * y + w * z),
                                T(2) * (x * z - w * y));
        }

        TQuaternion(const TMatrix4<T>& matrix)
        {
            //float  trace, s, q[4];
            //int    i, j, k;
            //int nxt[3] = {1, 2, 0};
            //trace = m[0][0] + m[1][1] + m[2][2];
            //// check the diagonal
            //if (trace > 0.0)
            //{
            //    s = std::sqrt(trace + 1.0);
            //    w = s / 2.0;
            //    s = 0.5 / s;
            //    x = (m[1][2] - m[2][1]) * s;
            //    y = (m[2][0] - m[0][2]) * s;
            //    z = (m[0][1] - m[1][0]) * s;
            //} else {		
            //// diagonal is negative
            //    i = 0;
            //    if (m[1][1] > m[0][0]) i = 1;
            //    if (m[2][2] > m[i][i]) i = 2;
            //    j = nxt[i];
            //    k = nxt[j];
            //    s = std::sqrt((m[i][i] - (m[j][j] + m[k][k])) + 1.0);
            //    q[i] = s * 0.5;
            //    if (s != 0.0) s = 0.5 / s;
            //    q[3] = (m[j][k] - m[k][j]) * s;
            //    q[j] = (m[i][j] + m[j][i]) * s;
            //    q[k] = (m[i][k] + m[k][i]) * s;
            //    x = q[0];
            //    y = q[1];
            //    z = q[2];
            //    w = q[3];
            //}




        TMatrix4<T> m = matrix;
        m[0] = normalize(m[0]);
        m[1] = normalize(m[1]);
        m[2] = normalize(m[2]);






        T fourXSquaredMinus1 = m[0][0] - m[1][1] - m[2][2];
        T fourYSquaredMinus1 = m[1][1] - m[0][0] - m[2][2];
        T fourZSquaredMinus1 = m[2][2] - m[0][0] - m[1][1];
        T fourWSquaredMinus1 = m[0][0] + m[1][1] + m[2][2];
        
        int biggestIndex = 0;
        T fourBiggestSquaredMinus1 = fourWSquaredMinus1;
        if(fourXSquaredMinus1 > fourBiggestSquaredMinus1)
        {
            fourBiggestSquaredMinus1 = fourXSquaredMinus1;
            biggestIndex = 1;
        }
        if(fourYSquaredMinus1 > fourBiggestSquaredMinus1)
        {
            fourBiggestSquaredMinus1 = fourYSquaredMinus1;
            biggestIndex = 2;
        }
        if(fourZSquaredMinus1 > fourBiggestSquaredMinus1)
        {
            fourBiggestSquaredMinus1 = fourZSquaredMinus1;
            biggestIndex = 3;
        }

        T biggestVal = std::sqrt(fourBiggestSquaredMinus1 + T(1)) * T(0.5);
        T mult = T(0.25) / biggestVal;

        //TQuaternion<T> Result;
        switch(biggestIndex)
        {
        case 0:
            w = biggestVal; 
            x = (m[1][2] - m[2][1]) * mult;
            y = (m[2][0] - m[0][2]) * mult;
            z = (m[0][1] - m[1][0]) * mult;
            break;
        case 1:
            w = (m[1][2] - m[2][1]) * mult;
            x = biggestVal;
            y = (m[0][1] + m[1][0]) * mult;
            z = (m[2][0] + m[0][2]) * mult;
            break;
        case 2:
            w = (m[2][0] - m[0][2]) * mult;
            x = (m[0][1] + m[1][0]) * mult;
            y = biggestVal;
            z = (m[1][2] + m[2][1]) * mult;
            break;
        case 3:
            w = (m[0][1] - m[1][0]) * mult;
            x = (m[2][0] + m[0][2]) * mult;
            y = (m[1][2] + m[2][1]) * mult;
            z = biggestVal;
            break;
        }


        

        /////////////////

       /* float trace = m[0][0] + m[1][1] + m[2][2];
        if(trace > 0)
        {
            float s = 0.5f / std::sqrt(trace + 1.0f);
            w = 0.25f / s;
            x = ( m[2][1] - m[1][2] ) * s;
            y = ( m[0][2] - m[2][0] ) * s;
            z = ( m[1][0] - m[0][1] ) * s;

        }
        else
        {
            std::cerr << "AOIJDF" << std::endl;
            if ( m[0][0] > m[1][1] && m[0][0] > m[2][2] )
            {
              float s = 2.0f * std::sqrt( 1.0f + m[0][0] - m[1][1] - m[2][2]);
              w = (m[2][1] - m[1][2] ) / s;
              x = 0.25f * s;
              y = (m[0][1] + m[1][0] ) / s;
              z = (m[0][2] + m[2][0] ) / s;
            } else if (m[1][1] > m[2][2]) {
              float s = 2.0f * std::sqrt( 1.0f + m[1][1] - m[0][0] - m[2][2]);
              w = (m[0][2] - m[2][0] ) / s;
              x = (m[0][1] + m[1][0] ) / s;
              y = 0.25f * s;
              z = (m[1][2] + m[2][1] ) / s;
            } else {
              float s = 2.0f * std::sqrt( 1.0f + m[2][2] - m[0][0] - m[1][1] );
              w = (m[1][0] - m[0][1] ) / s;
              x = (m[0][2] + m[2][0] ) / s;
              y = (m[1][2] + m[2][1] ) / s;
              z = 0.25f * s;
            }
        }*/


        /////////////////








            // Algorithm in Ken Shoemake's article in 1987 SIGGRAPH course notes
            // article "Quaternion Calculus and Fast Animation".

            //float flTrace = (m[0][0] + m[1][1] + m[2][2]);
            //float flRoot;

            //if ( flTrace > 0 )
            //{
            //    // |w| > 1/2, may as well choose w > 1/2
            //    flRoot = sqrt(flTrace + 1); // 2w

            //    w = flRoot/2;

            //    flRoot = 0.5f/flRoot; // 1/(4w)
            //    x = (m[2][1]-m[1][2])*flRoot;
            //    y = (m[1][0]-m[0][1])*flRoot;
            //    z = (m[0][2]-m[2][0])*flRoot;
            //}
            //else
            //{
            //    const int NEXT[3] = { 1, 2, 0 };

            //    // |w| <= 1/2
            //    int i = 0;
            //    if ( m[2][2] > m[0][0] )
            //        i = 1;
            //    if ( m[1][1] > m[i][i] )
            //        i = 2;
            //    int j = NEXT[i];
            //    int k = NEXT[j];

            //    flRoot = std::sqrt(m[i][i]-m[j][j]-m[k][k]+1);
            //    float* apflQuat[3] = { &x, &y, &z };
            //    *apflQuat[i] = flRoot/2;
            //    flRoot = 0.5f/flRoot;
            //    w = (m[j][k]-m[k][j])*flRoot;
            //    *apflQuat[j] = (m[i][j]+m[j][i])*flRoot;
            //    *apflQuat[k] = (m[i][k]+m[k][i])*flRoot;
            //}

        if(w > 1.0f)
            std::cerr << "ERROR" << std::endl;
        }

        TQuaternion<T> operator/ (const T& value)
        {
            return TQuaternion<T>(w/value, x/value, y/value, z/value);
        }

        TQuaternion<T> operator+ (const TQuaternion<T>& q)
        {
            return TQuaternion<T>(w + q.w, x + q.x, y + q.y, z + q.z);
        }

        TQuaternion<T> operator* (const T& value)
        {
            return TQuaternion<T>(w * value, x * value, y * value, z * value);
        }

        TQuaternion<T> operator* (const TQuaternion<T>& q)
	    { 
            return TQuaternion<T>(
                w * q.w - x * q.x - y * q.y - z * q.z,
	            w * q.x + x * q.w + y * q.z - z * q.y,
	            w * q.y + y * q.w + z * q.x - x * q.z,
	            w * q.z + z * q.w + x * q.y - y * q.x);
	    } 
      
        TVector3<T> operator*(const TVector3<T>& v)
	    {
            T two = (T)2;
            TVector3<T> uv, uuv;
            TVector3<T> vec(x, y, z);
            uv = cross(vec, v);
            uuv = cross(vec, uv);
            uv *= (two * w);
            uuv *= two;

            return (v + uv + uuv);
	    }

        TMatrix4<T> getMatrix() const
        {

            TMatrix4<T> result;
            result[0][0] = 1 - 2 * y * y - 2 * z * z;
            result[0][1] = 2 * x * y + 2 * w * z;
            result[0][2] = 2 * x * z - 2 * w * y;
            result[0][3] = 0.0;

            result[1][0] = 2 * x * y - 2 * w * z;
            result[1][1] = 1 - 2 * x * x - 2 * z * z;
            result[1][2] = 2 * y * z + 2 * w * x;
            result[1][3] = 0.0;

            result[2][0] = 2 * x * z + 2 * w * y;
            result[2][1] = 2 * y * z - 2 * w * x;
            result[2][2] = 1 - 2 * x * x - 2 * y * y;
            result[2][3] = 0.0;

            result[3][0] = 0.0;
            result[3][1] = 0.0;
            result[3][2] = 0.0;
            result[3][3] = 1.0;

            return result;







            /*float x2 = x * x;
            float y2 = y * y;
            float z2 = z * z;
            float xy = x * y;
            float xz = x * z;
            float yz = y * z;
            float wx = w * x;
            float wy = w * y;
            float wz = w * z;

            TMatrix4<T> m;
            m[0][0] = 1.0f - 2.0f * (y2 + z2);
            m[0][1] = 2.0f * (xy - wz);
            m[0][2] = 2.0f * (xz + wy);
            m[0][3] = 0.0f;

            m[1][0] = 2.0f * (xy + wz);
            m[1][1] = 1.0f - 2.0f * (x2 + z2);
            m[1][2] = 2.0f * (yz - wx);
            m[1][3] = 0.0f;

            m[2][0] = 2.0f * (xz - wy);
            m[2][1] = 2.0f * (yz + wx);
            m[2][2] = 1.0f - 2.0f * (x2 + y2);
            m[2][3] = 0.0f;

            m[3][0] = 0.0f;
            m[3][1] = 0.0f;
            m[3][2] = 0.0f;
            m[3][3] = 1.0f;

            return m;*/
        }

        TVector3<T> getEuler() const
        {
            float sqx = x*x;
	        float sqy = y*y;
	        float sqz = z*z;
	        float sqw = w*w;

	        TVector3<T> res;
	        res.pitch = toDegrees(atan2(2*(y*z + x*w), (-sqx-sqy+sqz+sqw)));
	        res.yaw = toDegrees(asin(-2*(x*z - y*w)));
	        res.roll = toDegrees(atan2(2*(x*y + z*w), (sqx-sqy-sqz+sqw)));
	        return res;
        }
    
    	friend std::ostream& operator<< (std::ostream& out, const TQuaternion<T>& q)
	    {
		    out << " q( w: " << q.w << ", " << q.x << ", " << q.y << ", " << q.z << ")";
		    return out;
	    }

    };

    template <typename T>
    TVector3<T> operator*(const TQuaternion<T>& q, const TVector3<T>& v)
	{
        T two = (T)2;
        TVector3<T> uv, uuv;
        TVector3<T> vec(q.x,q.y,q.z);

        uv = cross(vec, v);
        uuv = cross(vec, uv);
        uv *= (two * q.w);
        uuv *= two;

        return (v + uv + uuv);
	}

    template <typename T>
    TVector3<T> operator*(const TVector3<T>& v, const TQuaternion<T>& q)
	{
        return (inverse(q) * v);
	}

    template <typename T>
    TQuaternion<T> operator- (const TQuaternion<T>& q)
    {
        return TQuaternion<T>(-q.w, -q.x, -q.y, -q.z);
    }

    template <typename T>
    TQuaternion<T> operator*( const T& value, const TQuaternion<T>& q)
    {
        return q * value;
    }

    template <typename T>
    TQuaternion<T> operator*( const TQuaternion<T>& q, const T& value)
    {
        return TQuaternion<T>(q.w * value, q.x * value, q.y * value, q.z * value);
    }

    template <typename T>
    TQuaternion<T> operator*( const TQuaternion<T>& q0, const TQuaternion<T>& q1)
    {
        return TQuaternion<T>(
                q0.w * q1.w - q0.x * q1.x - q0.y * q1.y - q0.z * q1.z,
	            q0.w * q1.x + q0.x * q1.w + q0.y * q1.z - q0.z * q1.y,
	            q0.w * q1.y + q0.y * q1.w + q0.z * q1.x - q0.x * q1.z,
	            q0.w * q1.z + q0.z * q1.w + q0.x * q1.y - q0.y * q1.x);
    }

    template <typename T>
    TQuaternion<T> cross(const TQuaternion<T>& q0, const TQuaternion<T>& q1)
    {
        return TQuaternion<T>(
            q0.w * q1.w - q0.x * q1.x - q0.y * q1.y - q0.z * q1.z,
	        q0.w * q1.x + q0.x * q1.w + q0.y * q1.z - q0.z * q1.y,
	        q0.w * q1.y + q0.y * q1.w + q0.z * q1.x - q0.x * q1.z,
	        q0.w * q1.z + q0.z * q1.w + q0.x * q1.y - q0.y * q1.x);
    }

    template <typename T>
    T length(const TQuaternion<T>& q)
    {
        return std::sqrt(dot(q, q));
    }

    template <typename T>
    T dot(const TQuaternion<T>& q0, const TQuaternion<T>& q1)
    {
        return q0.x * q1.x + q0.y * q1.y + q0.z * q1.z + q0.w * q1.w;
    }

    template <typename T>
    TQuaternion<T> normalize(const TQuaternion<T>& q)
    {
        T len = length(q);
        if(len <=(T)0) 
            return TQuaternion<T>(1, 0, 0, 0);
        T oneOverLen = (T)1/len;
        return TQuaternion<T>(q.w * oneOverLen, q.x * oneOverLen, q.y * oneOverLen, q.z * oneOverLen);
    }

    template <typename T>
    TQuaternion<T> inverse(const TQuaternion<T>& q)
    {
        return TQuaternion<T>(q.w, -q.x, -q.y, -q.z);
    }

    template <typename T>
    TMatrix4<T> toMatrix(const TQuaternion<T>& q)
    {
        return q.getMatrix();
    }

    template <typename T>
    TQuaternion<T> lerp(T t, const TQuaternion<T>& from, const TQuaternion<T>& to)
    {
        TQuaternion<T> q0 = (from * ((T)1 - t));
        return normalize<T>((from * ((T)1 - t) + (to * t)));
    }

    template <typename T>
    TQuaternion<T> slerp(T t, const TQuaternion<T>& from, const TQuaternion<T>& to)
    {
        //from----p---------to
        // ---------d------->
        // p = x * q

        TQuaternion<T> q0(from);

        if(t <= 0.0f)
		    return from;
	    if(t >= 1.0f)
		    return to;

	    float cosHalfTheta = dot(from, to);
	    TQuaternion<T> q2(to);

	    if(cosHalfTheta < 0.0f)
	    {
		    cosHalfTheta = -cosHalfTheta;
		    q2 = -q2;
            /*q2.x = -q2.x;
            q2.y = -q2.y;
            q2.z = -q2.z;
            q2.w = -q2.w;*/
	    }

	    if(cosHalfTheta > 0.99f)
	    {
		    return lerp(t, q0,q2);
	    }

	    float halfTheta = std::acos(cosHalfTheta);
	    //float sinTheta = std::sin(halfTheta);

	    return (std::sin((1.0f-t) * halfTheta) * q0+std::sin(t * halfTheta) * q2) /std::sin(halfTheta);


        }

    typedef TQuaternion<float> Quaternionf;
    typedef TQuaternion<double> Quaderniond;

} 