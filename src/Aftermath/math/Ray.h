#pragma once
#include <iostream>
namespace am
{
	template <typename> class TVector2;
	template <typename> class TVector3;
	template <typename> class TMatrix4;
	template <typename> class TSphere;
	template <typename> class TPlane;
	template <class T>
	class TRay
	{
	public:
		TRay(const TVector3<T>& position, const TVector3<T>& direction)
			: _position(position)
			, _direction(direction)
		{

		}

		TRay(int x, int y, int width, int height, const TMatrix4<T> worldMatrix)
		{
			T normalizedX = ((T)x / (T)width) - 0.5f;
			T normalizedY = (((T)height - (T)y) / (T)height) - 0.5f;

			_direction = normalizedX * worldMatrix.getRight() +
				normalizedY * worldMatrix.getUp() +
				worldMatrix.getForward();
			
			_direction = normalize(_direction);
			_position = worldMatrix.getTranslation();
		}

		bool intersect(const TSphere<T>& sphere) const
		{
			auto c = sphere.getPosition();
			auto r = sphere.getRadius();
			auto v = _direction;
			auto u = c - _position;

			if (dot(u, v) < 0)
			{
				auto lenU = length(u);
				if (lenU > r)
					return false;
				else
					return true; // ray is on or inside the sphere. 1 intersection point only
			}
			

			auto q = _position + project(u, v);
			auto distanceCQ = distance(c, q);
			if (distanceCQ > r)
				return false;
			else
				return true;
		}

		bool intersect(const TSphere<T>& sphere, T& outDistance) const
		{
			outDistance = -1;
			auto c = sphere.getPosition();
			auto r = sphere.getRadius();
			auto p = _position;
			auto v = _direction;
			auto u = c - p;

			if (dot(u, v) < 0)
			{
				auto lenU = length(u);
				if (lenU > r)
					return false;
				else
				{
					if ((lenU - r) < (T)0.00001) // Intersection 1 found
					{
						outDistance = 0;
					}
					else
					{
						auto q = p + project(u, v);
						auto distanceCQ = distance(c, q);
						auto distanceIQ = std::sqrt(r*r - distanceCQ*distanceCQ);
						auto distancePQ = distance(p, q);
						outDistance = distancePQ + distanceIQ;
					}
					return true; // ray is on or inside the sphere. 1 intersection point only
				}
				
			}

			auto q = p + project(u, v);
			auto distanceCQ = distance(c, q);
			if (distanceCQ > r)
				return false;
			else
			{
				//Find real distance by first checking if we're intersecting with the front or back of the sphere
				auto distanceIQ = std::sqrt(r*r - distanceCQ*distanceCQ);
				auto distancePQ = distance(p, q);
				auto lenU = length(u);
				if (lenU > r)								// intersecting with front
					outDistance = distancePQ - distanceIQ;
				else										// intersecting with back
					outDistance = distancePQ + distanceIQ;
				return true;
			}
			
		}

		bool intersect(const TSphere<T>& sphere, TVector3<T>& intersection) const
		{
			auto c = sphere.getPosition();
			auto r = sphere.getRadius();
			auto p = _position;
			auto v = _direction;
			auto u = c - p;

			if (dot(u, v) < 0)
			{
				auto lenU = length(u);
				if (lenU > r)
					return false;
				else
				{
					if ((lenU - r) < (T)0.00001) // Intersection 1 found
					{
						intersection = p;
					}
					else
					{
						auto q = p + project(u, v);
						auto distanceCQ = distance(c, q);
						auto distanceIQ = std::sqrt(r*r - distanceCQ*distanceCQ);
						auto distancePQ = distance(p, q);
						auto dist = distancePQ + distanceIQ;
						intersection = p + dist * v;
					}
					return true; // ray is on or inside the sphere. 1 intersection point only
				}

			}

			auto q = p + project(u, v);
			auto distanceCQ = distance(c, q);
			if (distanceCQ > r)
				return false;
			else
			{
				//Find real distance by first checking if we're intersecting with the front or back of the sphere
				auto distanceIQ = std::sqrt(r*r - distanceCQ*distanceCQ);
				auto distancePQ = distance(p, q);
				auto lenU = length(u);
				if (lenU > r)								// intersecting with front
				{
					auto dist = distancePQ - distanceIQ;
					intersection = p + dist * v;
				}
				else										// intersecting with back
				{
					auto dist = distancePQ + distanceIQ;
					intersection = p + dist * v;
				}
				return true;
			}
		}

		bool intersect(const TSphere<T>& sphere, TVector3<T>& intersection1, TVector3<T>& intersection2) const
		{
			auto c = sphere.getPosition();
			auto r = sphere.getRadius();
			auto p = _position;
			auto v = _direction;
			auto u = c - p;

			if (dot(u, v) < 0)
			{
				auto lenU = length(u);
				if (lenU > r)
					return false;
				else
				{
					if ((lenU - r) < (T)0.00001) // Intersection 1 found
					{
						intersection1 = intersection2 = p;
					}
					else
					{
						auto q = p + project(u, v);
						auto distanceCQ = distance(c, q);
						auto distanceIQ = std::sqrt(r*r - distanceCQ*distanceCQ);
						auto distancePQ = distance(p, q);
						auto dist = distancePQ + distanceIQ;
						intersection1 = intersection2 = p + dist * v;
					}
					return true; // ray is on or inside the sphere. 1 intersection point only
				}

			}

			auto q = p + project(u, v);
			auto distanceCQ = distance(c, q);
			if (distanceCQ > r)
				return false;
			else
			{
				//Find real distance by first checking if we're intersecting with the front or back of the sphere
				auto distanceIQ = std::sqrt(r*r - distanceCQ*distanceCQ);
				auto distancePQ = distance(p, q);
				auto lenU = length(u);
				if (lenU > r)								// intersecting with front
				{
					auto dist = distancePQ - distanceIQ;
					intersection1 = p + dist * v;
					intersection2 = intersection1 + (distanceIQ * (T)2) * v;
				}
				else										// intersecting with back
				{
					auto dist = distancePQ + distanceIQ;
					intersection1 = intersection2 = p + dist * v;
				}
				return true;
			}
		}

		bool intersect(const TPlane<T>& plane, T& outDistance) const
		{
			T denom = dot(plane.getNormal(), _direction);
			if (std::abs(denom) > 0.00001)
			{
				TVector3<T> p0 = plane.project(am::TVector3<T>::Zero());
				auto u = p0 - _position;
				outDistance = dot(u, plane.getNormal()) / denom;
				return (outDistance >= 0);
			}
			return false;
		}

		bool intersect(const TPlane<T>& plane, TVector3<T>& intersection) const
		{
			T outDistance = -1;
			T denom = dot(plane.getNormal(), _direction);
			if (std::abs(denom) > 0.00001)
			{
				TVector3<T> p0 = plane.project(am::TVector3<T>::Zero());
				auto u = p0 - _position;
				outDistance = dot(u, plane.getNormal()) / denom;
				intersection = _position + _direction * outDistance;
				return (outDistance >= 0);
			}
			return false;
		}

		friend std::ostream& operator<< (std::ostream& out, const TRay<T>& v)
		{
			out << "Ray Pos(" << v._position.x << ", " << v._position.y << ", " << v._position.z << ")" << " Dir(" << v._direction.x << ", " << v._direction.y << ", " << v._direction.z << ")";
			return out;
		}

	private:
		TVector3<T> _position;
		TVector3<T> _direction;
	};

	typedef TRay<float> Ray;
}