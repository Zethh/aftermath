#pragma once
#undef near
#undef far
#include <Aftermath/math/Plane.h>
#include <array>
namespace am {

template <typename> class TVector3;
template <typename> class TVector4;
template <typename> class TMatrix4;
template <typename> class TPlane;
template <typename> class TSphere;

template <class T>
class TFrustum
{
public:
    TFrustum(const TMatrix4<T>& matrix)
		: left(normalize(matrix[3] + matrix[0]))
		, right(normalize(matrix[3] - matrix[0]))
		, bottom(normalize(matrix[3] + matrix[1]))
		, top(normalize(matrix[3] - matrix[1]))
		, near(normalize(matrix[3] + matrix[2]))
		, far(normalize(matrix[3] - matrix[2]))
    {
    }

    void getFrustumCorners(TVector3<T> *points)
    {
        points[0] = getIntersectionPoint(near, top, left);
        points[1] = getIntersectionPoint(near, top, right);
        points[2] = getIntersectionPoint(near, bottom, right);
        points[3] = getIntersectionPoint(near, bottom, left);
        points[4] = getIntersectionPoint(far, top, left);
        points[5] = getIntersectionPoint(far, top, right);
        points[6] = getIntersectionPoint(far, bottom, right);
        points[7] = getIntersectionPoint(far, bottom, left);
    }

	bool contains(const TVector3<T>& point)
	{
		TPlane<T> planes[] = { left, right, top, bottom, near, far };
		for (auto p : planes)
		{
			if (p.distanceToPoint(point) < 0.00001)
				return false;
		}
		return true;
	}

	bool contains(const TSphere<T>& sphere)
	{
		TPlane<T> planes[] = { left, right, top, bottom, near, far };
		for (auto p : planes)
		{
			if (p.distanceToPoint(sphere.getPosition()) < -sphere.getRadius())
				return false;
		}
		return true;
	}

private:
	TVector3<T> getIntersectionPoint(const TPlane<T>& p1, const TPlane<T>& p2, const TPlane<T>& p3)
	{

		TVector3<T> p1V = p1.getNormal();
		TVector3<T> p2V = p2.getNormal();
		TVector3<T> p3V = p3.getNormal();

		return TVector3<T>( p1.getDistance() * cross(p2V, p3V) +
							p2.getDistance() * cross(p3V, p1V) +
							p3.getDistance() * cross(p1V, p2V)) /
							dot(p1V, cross(p3V, p2V));
	}

private:
	TPlane<T> left;
	TPlane<T> right;
	TPlane<T> top;
	TPlane<T> bottom;
	TPlane<T> near;
	TPlane<T> far;
};

template <typename T>
TFrustum<T> createFrustum(const TMatrix4<T>& projectionMatrix, const TMatrix4<T>& viewMatrix)
{
    return TFrustum<T>(transpose(projectionMatrix * viewMatrix));

}

typedef TFrustum<float> Frustum;

} 