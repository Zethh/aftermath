#pragma once

#include <iostream>
#include <Aftermath/math/CommonTrig.h>
#include <math.h>
#include <cmath>



namespace am {
	template <typename> class TPlane;
	template <typename> class TVector2;
template <class T>
class TVector3
{
public:

	union
	{
		struct {T x, y, z;};
		struct {T r, g, b;};
		struct {T element[3];};
        struct {T pitch, yaw, roll;};
	};

    TVector3()
    {
        x = (T)0;
        y = (T)0;
        z = (T)0;
    }
	TVector3(T _x, T _y, T _z) { x = _x; y = _y; z = _z;}

    TVector3(const TVector2<T>& v, T _z)
    {
        x = v.x;
        y = v.y;
        z = _z;
    }

    TVector3(T _x, const TVector2<T>& _v)
    {
        x = _x;
        y = _v.y;
        z = _v.y;
    }

	TVector3(T v) { x = v; y = v; z = v;}

	

	static TVector3<T> Up() { return TVector3<T>(0, 1, 0); }
	static TVector3<T> Down() { return TVector3<T>(0, -1, 0); }
	static TVector3<T> Left() { return TVector3<T>(-1, 0, 0); }
	static TVector3<T> Right() { return TVector3<T>(1, 0, 0); }
	static TVector3<T> Forward() { return TVector3<T>(0, 0, -1); }
	static TVector3<T> Backward() { return TVector3<T>(0, 0, 1); }
	static TVector3<T> One() { return TVector3<T>(1, 1, 1); }
	static TVector3<T> Zero() { return TVector3<T>(0, 0, 0); }

    unsigned int numComponents() const { return 3; }

    TVector3<T> operator+ (const TVector3<T>& v) const
	{
		return TVector3<T>(x+v.x, y+v.y, z+v.z);
	}

    TVector3<T> operator- (const TVector3<T>& v) const
	{
		return TVector3<T>(x-v.x, y-v.y, z-v.z);
	}

	TVector3<T> operator/ (T value) const
	{
		return TVector3<T>(x/value, y/value, z/value);
	}

	TVector3<T> operator* (const TVector3<T>& v) const
	{
		return TVector3<T>(x*v.x, y*v.y, z*v.z);
	}

	TVector3<T> operator* (T s) const
	{
		return TVector3<T>(x*s, y*s, z*s);
	}

    TVector3<T> operator*= (T s)
	{
        x*=s;
        y*=s;
        z*=s;

        return *this;
	}

	TVector3<T> operator+= (const TVector3<T>& v)
	{
		x = x + v.x;
		y = y + v.y;
		z = z + v.z;

        return *this;
	}

    T& operator[] (unsigned int i)
	{
		return element[i];
	}

	const T& operator[] (unsigned int i) const
	{
		return element[i];
	}

    bool operator== (const TVector3<T>& v) const
    {
        return (x == v.x && y == v.y && z == v.z);
    }

	friend std::ostream& operator<< (std::ostream& out, const TVector3<T>& v)
	{
		out << " v(" << v.x << ", " << v.y << ", " << v.z << ")";
		return out;
	}

};

typedef TVector3<float> fvec3;
typedef TVector3<double> dvec3;
typedef TVector3<unsigned int> uvec3;
typedef TVector3<int> ivec3;

typedef TVector3<float> Vector3f;
typedef TVector3<double> Vector3d;
typedef TVector3<unsigned int> Vector3u;
typedef TVector3<int> Vector3i;

typedef fvec3 vec3;
typedef Vector3f Vector3;

template <typename T>
TVector3<T> operator* (T value, const TVector3<T>& vector)
{
	TVector3<T> vec;
	vec.x = vector.x * value;
	vec.y = vector.y * value;
	vec.z = vector.z * value;
	return vec;
}

template <typename T>
TVector3<T> cross(const TVector3<T>& lhs, const TVector3<T>& rhs)
{
	return TVector3<T>(
        lhs.y * rhs.z - rhs.y * lhs.z,
        lhs.z * rhs.x - rhs.z * lhs.x,
        lhs.x * rhs.y - rhs.x * lhs.y);
}

template <typename T>
T dot(const TVector3<T>& lhs, const TVector3<T>& rhs)
{
	return lhs.x * rhs.x + lhs.y * rhs.y + lhs.z * rhs.z;
}

template <typename T>
TVector3<T> normalize(const TVector3<T>& v)
{
	//Add functionality to support different square roots
    T magnitude = T(std::sqrt(v.x * v.x + v.y * v.y + v.z * v.z));
	TVector3<T> v2 = v;
    if(magnitude > 0.0)
    {
        v2 = v/magnitude;
    }
    return v2;
}

template <typename T>
T angleRadians(const TVector3<T>& lhs, const TVector3<T>& rhs)
{
	return std::acos(dot(normalize(lhs), normalize(rhs)));
}

template <typename T>
T angleDegrees(const TVector3<T>& lhs, const TVector3<T>& rhs)
{
	return toDegrees(angleRadians(lhs, rhs));
}

template <typename T>
TVector3<T> projectUnitV(const TVector3<T>& u, const TVector3<T>& v)
{
	return dot(v, u) * v;
}

template <typename T>
TVector3<T> project(const TVector3<T>& u, const TVector3<T>& v)
{
	return (dot(v, u) / lengthSquared(v)) * v;
}

template <typename T>
TVector3<T> project(const TVector3<T>& point, const TPlane<T>& plane)
{
	return plane.project(point);
}

template <typename T>
T length( const TVector3<T>& v)
{
	T sqr = v.x * v.x + v.y * v.y + v.z * v.z;
    return std::sqrt(sqr);
}

template <typename T>
T lengthSquared(const TVector3<T>& v)
{
	return v.x * v.x + v.y * v.y + v.z * v.z;
}

template <typename T>
T distance( const TVector3<T>& lhs, const TVector3<T>& rhs)
{
    return length(rhs - lhs);
}

template <typename T>
const T * address(const TVector3<T>& v)
{
	return &(v.x);
}

template <typename T>
TVector3<T> lerp(T dt, const TVector3<T>& from, const TVector3<T>& to)
{
    return (from + dt * (from - to));
}



}