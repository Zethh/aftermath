#pragma once

namespace am {

	template <typename T> class TVector2;
	template <typename T> class TVector3;
	template <typename T> class TVector4;
	template <typename T> class TQuaternion;
	template <typename T> class TMatrix4;

template <typename T>
T toRadians( T degrees)
{
	const T pi = T(3.1415926535897932384626433832795);
	return degrees * (pi / T(180));
}

template <typename T>
TVector3<T> toRadians(TVector3<T> degrees)
{
	return TVector3<T>(toRadians(degrees.x),
						toRadians(degrees.y),
						toRadians(degrees.z));
}

template <typename T>
TVector4<T> toRadians(TVector4<T> degrees)
{
	return TVector4<T>(toRadians(degrees.x),
						toRadians(degrees.y),
						toRadians(degrees.z),
						toRadians(degrees.w));
}

template <typename T>
T toDegrees( T radians)
{
	const T pi = T(3.1415926535897932384626433832795);
	return radians * (T(180) / pi);
}

} 