#pragma once
#include <iostream>

namespace am {
	class XMLAttribute
	{
	public:
		XMLAttribute();

		void setName(const std::string& name);

		const std::string& getName() const;

		bool hasName(const std::string& name) const;

		void setValue(const std::string& value);

		bool hasValue(const std::string& name) const;

		const std::string& asString() const;

		int asInt() const;

		unsigned int asUInt() const;

		float asFloat() const;

		double asDouble() const;

	private:
		std::string _name;
		std::string _value;
	};
}