#include "XMLAttribute.h"

#if defined(_MSC_VER) && (_MSC_VER >= 1400 )
#define AFTERMATH_SSCANF   sscanf_s
#else
#define AFTERMATH_SSCANF   sscanf
#endif
namespace am {
		XMLAttribute::XMLAttribute() 
		{
		}

		void XMLAttribute::setName(const std::string& name)
		{
			_name = name;
		}

		const std::string& XMLAttribute::getName() const 
		{
			return _name; 
		}

		bool XMLAttribute::hasName(const std::string& name) const
		{
			return (name.compare(_name) == 0);
		}

		void XMLAttribute::setValue(const std::string& value)
		{
			_value = value;
		}

		bool XMLAttribute::hasValue(const std::string& value) const
		{
			return (value.compare(_value) == 0);
		}

		const std::string& XMLAttribute::asString() const
		{
			return _value;
		}

		int XMLAttribute::asInt() const
		{
			int value;
			AFTERMATH_SSCANF(_value.c_str(), "%d", &value);
			return value;
		}

		unsigned int XMLAttribute::asUInt() const
		{
			unsigned int value;
			AFTERMATH_SSCANF(_value.c_str(), "%u", &value);
			return value;
		}

		float XMLAttribute::asFloat() const
		{
			float value;
			AFTERMATH_SSCANF(_value.c_str(), "%f", &value);
			return value;
		}

		double XMLAttribute::asDouble() const
		{
			double value;
			AFTERMATH_SSCANF(_value.c_str(), "%lf", &value);
			return value;
		}
	}