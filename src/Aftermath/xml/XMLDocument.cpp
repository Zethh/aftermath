#include "XMLDocument.h"
#include <Aftermath/external/tinyxml/TinyXml2.h>

namespace am {
	static void parseXMLTree(XMLNode& node, tinyxml2::XMLElement* element)
	{
		// Get the name of the current element
		node.setName(element->Name());

		const tinyxml2::XMLAttribute* attribute = element->FirstAttribute();
		while (attribute)
		{
			XMLAttribute& attr = node.generateAttribute();
			attr.setName(attribute->Name());
			attr.setValue(attribute->Value());

			attribute = attribute->Next();
		}

		// Check if the element contains data which isn't an element itself
		const char* text = element->GetText();
		if (text) node.setValue(text);

		// Proceed to the next child
		tinyxml2::XMLElement* child = element->FirstChildElement();
		while (child)
		{
			parseXMLTree(node.generateChild(), child);
			child = child->NextSiblingElement();
		}

	}

	XMLDoc::XMLDoc(const std::string& filename)
	{
		tinyxml2::XMLDocument txmlDocument;
		int error = txmlDocument.LoadFile(filename.c_str());
		if (error != 0)
			return;

		parseXMLTree(_root, txmlDocument.FirstChildElement());
	}

	XMLDoc::~XMLDoc()
	{

	}

	const XMLNode& XMLDoc::getRootNode() const
	{
		return _root;
	}
}