#include "XMLNode.h"

namespace am {
		static const XMLNode errorNode;
		static const XMLAttribute errorAttribute;

		XMLNode::XMLNode()
			: _name("ERROR")
			, _value("")
			, _valid(false)
		{
		}

		XMLNode::~XMLNode()
		{

		}

		void XMLNode::setName(const std::string& name) 
		{
			_name = name; 
			_valid = true;
		}

		const std::string& XMLNode::getName() const 
		{
			return _name; 
		}

		bool XMLNode::hasName(const std::string& name) const
		{
			return (name.compare(_name) == 0);
		}

		void XMLNode::setValue(const std::string& value)
		{ 
			_value = value; 
		}

		const std::string& XMLNode::getValue() const 
		{ 
			return _value; 
		}

		bool XMLNode::isValid() const
		{
			return _valid;
		}

		XMLAttribute& XMLNode::generateAttribute()
		{
			_attributes.push_back(XMLAttribute());
			return _attributes.back();
		}

		size_t XMLNode::numAttributes() const 
		{ 
			return _attributes.size(); 
		}

		const XMLAttribute& XMLNode::getAttribute(const std::string& name) const
		{
			for (size_t i = 0; i < _attributes.size(); ++i)
			{
				if (_attributes[i].hasName(name))
					return _attributes[i];
			}
			return errorAttribute;
		}

		const XMLAttribute& XMLNode::getAttribute(size_t index) const 
		{ 
			return _attributes[index];
		}

		XMLNode& XMLNode::generateChild()
		{
			_children.push_back(XMLNode());
			return _children.back();
		}

		size_t XMLNode::numChildren() const 
		{ 
			return _children.size();
		}

		const XMLNode& XMLNode::getChild(size_t index) const 
		{ 
			return _children[index];
		}

		const XMLNode& XMLNode::findNode(const std::string& name) const
		{
			for (size_t i = 0; i < _children.size(); ++i)
			{
				if (_children[i].hasName(name))
				{
					return _children[i];
				}
			}

			for (size_t i = 0; i < _children.size(); ++i)
			{
				const XMLNode& node = _children[i].findNode(name);
				if (node.hasName(name))
					return node;
			}

			return errorNode;
		}

		const XMLNode& XMLNode::findNode(const std::string& name, const std::string& attributeName, const std::string& attributeValue) const
		{
			for (size_t i = 0; i < _children.size(); ++i)
			{
				if (_children[i].hasName(name) && _children[i].getAttribute(attributeName).hasValue(attributeValue))
				{
					return _children[i];
				}
			}

			for (size_t i = 0; i < _children.size(); ++i)
			{
				const XMLNode& node = _children[i].findNode(name, attributeName, attributeValue);
				if (node.getAttribute(attributeName).hasValue(attributeValue))
					return node;
			}

			return errorNode;
		}

		const XMLNode& XMLNode::findNode(const std::string& attributeName, const std::string& attributeValue) const
		{
			for (size_t i = 0; i < _children.size(); ++i)
			{
				if (_children[i].getAttribute(attributeName).asString().compare(attributeValue) == 0)
				{
					return _children[i];
				}
			}

			for (size_t i = 0; i < _children.size(); ++i)
			{
				const XMLNode& node = _children[i].findNode(attributeName, attributeValue);
				if (node.getAttribute(attributeName).asString().compare(attributeValue) == 0)
					return node;
			}

			return errorNode;
		}
	}