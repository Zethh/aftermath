#pragma once
#include <vector>
#include <Aftermath/xml/XMLAttribute.h>

namespace am {
	class XMLNode
	{
	public:
		XMLNode();

		~XMLNode();

		void setName(const std::string& name);

		const std::string& getName() const;

		bool hasName(const std::string& name) const;

		void setValue(const std::string& value);

		const std::string& getValue() const;

		bool isValid() const;

		XMLAttribute& generateAttribute();

		size_t numAttributes() const;

		const XMLAttribute& getAttribute(const std::string& name) const;

		const XMLAttribute& getAttribute(size_t index) const;

		XMLNode& generateChild();

		size_t numChildren() const;

		const XMLNode& getChild(size_t index) const;

		const XMLNode& findNode(const std::string& name) const;

		const XMLNode& findNode(const std::string& name, const std::string& attributeName, const std::string& attributeValue) const;

		const XMLNode& findNode(const std::string& attributeName, const std::string& attributeValue) const;

	private:
		typedef std::vector<XMLNode> Children;
		typedef std::vector<XMLAttribute> Attributes;

		std::string _name;
		std::string _value;
		Attributes _attributes;
		Children _children;
		bool _valid;
	};
}