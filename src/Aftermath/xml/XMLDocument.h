#pragma once
#include <Aftermath/core/Referenced.h>
#include <Aftermath/xml/XMLNode.h>

namespace am {
	class XMLDoc : public Referenced
	{
		AFTERMATH_META(XMLDoc)
	public:
		XMLDoc(const std::string& filename);

		const XMLNode& getRootNode() const;

	protected:
		~XMLDoc();

	private:
		XMLNode _root;
	};
}