#ifndef BUFFER_H
#define BUFFER_H

#include <vector>

#include <Aftermath/core/Referenced.h>
#include <Aftermath/math/Vector2.h>
#include <Aftermath/math/Vector3.h>
#include <Aftermath/math/Vector4.h>
#include <Aftermath/render/RenderEnums.h>
#include <Aftermath/render/Renderer.h>
#include <iostream>
#include <map>
namespace am {
	class IRenderBuffer;
	class IVertexElement : public Referenced
	{
		AFTERMATH_META(IVertexElement)
	public:

		virtual void setName(const std::string& name) = 0;
		virtual void resize(size_t size) = 0;
		virtual void* getData() = 0;
		virtual void* getData() const = 0;
		virtual Usage getUsage() const = 0;
		virtual VertexElementType getType() const = 0;
		virtual size_t arraySize() const = 0;
		virtual size_t byteSize() const = 0;
		virtual size_t typeSize() const = 0;
		virtual size_t numComponents() const = 0;
		virtual IRenderBuffer* getOwner() const = 0;
		virtual void setOwner(IRenderBuffer* owner) = 0;
	};

	template <typename T, const unsigned int components, Usage usage, VertexElementType type>
	class TVertexElement : public IVertexElement
	{
		typedef TVertexElement<T, components, usage, type> Element_Type;
	public:
		TVertexElement(std::initializer_list<T> initList)
			: _data(initList)
		{

		}

		TVertexElement()
		{

		}

		virtual void setName(const std::string& name) override { _name = name; }
		virtual void resize(size_t size) override { _data.resize(size); }
		virtual void* getData() override { return (void*)_data.data(); }
		virtual void* getData() const override { return (void*)_data.data(); }
		virtual Usage getUsage() const override	{ return usage; }
		virtual VertexElementType getType() const override { return type; }
		virtual size_t arraySize() const override { return _data.size(); }
		virtual size_t byteSize() const override { return _data.size() * sizeof(T); }
		virtual size_t typeSize() const override { return sizeof(T); }
		std::vector<T>& getVector() { return _data; }
		const std::vector<T>& getVector() const { return _data; }
		size_t numComponents() const { return components; }
		virtual IRenderBuffer* getOwner() const override { return _owner; };
		virtual void setOwner(IRenderBuffer* owner) override { _owner = owner; }

	private:
		std::vector<T> _data;
		IRenderBuffer::Ptr _owner;
		std::string _name;
	};

	typedef TVertexElement<unsigned int, 1, Usage_Static, VertexElementType_StaticUInt> StaticUInt;
	typedef TVertexElement<unsigned int, 1, Usage_Dynamic, VertexElementType_DynamicUInt> DynamicUInt;
	typedef TVertexElement<float, 1, Usage_Static, VertexElementType_StaticFloat> StaticFloat;
	typedef TVertexElement<float, 1, Usage_Dynamic, VertexElementType_DynamicFloat> DynamicFloat;
	typedef TVertexElement<Vector2f, 2, Usage_Static, VertexElementType_StaticFloat2> StaticFloat2;
	typedef TVertexElement<Vector2f, 2, Usage_Dynamic, VertexElementType_DynamicFloat2> DynamicFloat2;
	typedef TVertexElement<Vector3f, 3, Usage_Static, VertexElementType_StaticFloat3> StaticFloat3;
	typedef TVertexElement<Vector3f, 3, Usage_Dynamic, VertexElementType_DynamicFloat3> DynamicFloat3;
	typedef TVertexElement<Vector4f, 4, Usage_Static, VertexElementType_StaticFloat4> StaticFloat4;
	typedef TVertexElement<Vector4f, 4, Usage_Dynamic, VertexElementType_DynamicFloat4> DynamicFloat4;

}
#endif
