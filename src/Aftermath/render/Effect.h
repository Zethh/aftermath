#pragma once

#include <Aftermath/core/Referenced.h>
#include <Aftermath/utils/Resource.h>
#include <map>
#include <string>

#include <Aftermath/utils/ShaderProgram.h>


namespace am {

class Effect: public Resource
{
    AFTERMATH_META(Effect)

public:
    Effect() { }

    virtual ~Effect() { }

	class Technique: public Referenced
	{
        AFTERMATH_META(Technique)
	public:
		Technique() { }

		virtual  ~Technique() { }

		void setName(const std::string& name) { _name = name; }

		const std::string& getName() const { return _name; }

		void setProgram(const std::string& name, ShaderProgram* program)
		{
			program->setName(name);
			_program = program;
		}

		ShaderProgram* getProgram() const
		{
            return _program;
		}

        ShaderProgram::Ptr _program;
		std::string _name;
	};

	void addTechnique(const std::string& name, Effect::Technique* group)
	{
		_techniqueMap[name] = group;
	}

	Effect::Technique* getTechnique(const std::string& name) const
	{
		return _techniqueMap.find(name)->second;
	}

	void clearTechniques()
	{
		_techniqueMap.clear();
	}

	typedef std::map<std::string, Effect::Technique::Ptr> TechniqueMap;

private:
	TechniqueMap _techniqueMap;

};


}