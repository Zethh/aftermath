#pragma once
#include <Aftermath/render/Renderer.h>
#include <wrl/client.h>
using Microsoft::WRL::ComPtr;
namespace am
{
	class DxTexture : public ITexture
	{
		AFTERMATH_META(DxTexture)
	public:
		virtual int getWidth() const override { return _desc.width; }

		virtual int getHeight() const override { return _desc.height; }

		virtual const TextureDesc& getDesc() const { return _desc; }
	public:
		ComPtr<ID3D11Texture2D> dxTexture = nullptr;
		ComPtr<ID3D11ShaderResourceView> resourceView = nullptr;
		ComPtr<ID3D11RenderTargetView> renderTargetView = nullptr;
		ComPtr<ID3D11SamplerState> samplerState = nullptr;
		TextureDesc _desc;
	};
}