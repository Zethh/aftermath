#pragma once
#include <Aftermath/render/Renderer.h>
#include <vector>
#include <d3d11.h>
#include <map>
#include <wrl/client.h>
using Microsoft::WRL::ComPtr;
namespace am
{
	class DxRenderTarget : public IRenderTarget
	{
		AFTERMATH_META(DxRenderTarget)
	public:
		virtual unsigned int getWidth() const override { return _desc.width; }

		virtual unsigned int getHeight() const override { return _desc.height; }

		virtual ITexture* getTarget(RenderTargetSlot slot) const override { return _desc.slots.find(slot)->second; }

		std::vector<ComPtr<ID3D11RenderTargetView> > _renderTargetSlots;
		ComPtr<ID3D11DepthStencilView> _depthStencilView = nullptr;
		ComPtr<ID3D11DepthStencilState> _depthStencilState = nullptr;
		ComPtr<ID3D11Texture2D> depthTexture = nullptr;
		RenderTargetDesc _desc;
	};
}
