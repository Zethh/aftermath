#pragma once
#include <Aftermath/render/Buffer.h>
#include <d3d11.h>
#include <Windows.h>
#include <wrl/client.h>
using Microsoft::WRL::ComPtr;
namespace am
{
	class DxRenderBuffer : public IRenderBuffer
	{
		AFTERMATH_META(DxRenderBuffer)
	public:
		ComPtr<ID3D11Buffer> _id = nullptr;
		RenderBufferDesc _desc;
	};
}