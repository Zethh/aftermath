#include "DxRenderer.h"
#include <Aftermath/render/DX11/DxTexture.h>
#include <Aftermath/render/DX11/DxBuffer.h>
#include <Aftermath/render/DX11/DxRenderTarget.h>
#include <Aftermath/utils/ShaderProgram.h>
#include <Aftermath/utils/StringUtils.h>
#include <assert.h>
#include <Aftermath/render/Window.h>
#include <Aftermath/utils/Image.h>

#pragma comment(lib, "d3dcompiler.lib")
#pragma comment(lib, "dxgi.lib")
#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "dxguid.lib")

#define INITGUID
#include <d3d11shader.h>
#include <Windows.h>
#include <dxgi.h>
#include <d3dcommon.h>
#include <d3d11.h>
#include <d3d11_1.h>
#include <d3dcompiler.h>


namespace am
{
#pragma region staticfunctions
	static HRESULT compileShaderFromFile(ID3D11Device* device, ID3D11DeviceContext* deviceContext, const WCHAR* szFileName, LPCSTR szEntryPoint, LPCSTR szShaderModel, ID3DBlob** ppBlobOut)
	{
		HRESULT hr = S_OK;

		DWORD dwShaderFlags = D3DCOMPILE_ENABLE_STRICTNESS;
#if defined( DEBUG ) || defined( _DEBUG )
		// Set the D3DCOMPILE_DEBUG flag to embed debug information in the shaders.
		// Setting this flag improves the shader debugging experience, but still allows 
		// the shaders to be optimized and to run exactly the way they will run in 
		// the release configuration of this program.
		dwShaderFlags |= D3DCOMPILE_DEBUG;
#endif

		ID3DBlob* pErrorBlob = nullptr;
		hr = D3DCompileFromFile(szFileName, nullptr, nullptr, szEntryPoint, szShaderModel,
			dwShaderFlags, 0, ppBlobOut, &pErrorBlob);
		if (FAILED(hr))
		{
			if (pErrorBlob)
			{
				OutputDebugStringA(reinterpret_cast<const char*>(pErrorBlob->GetBufferPointer()));
				pErrorBlob->Release();
			}
			return hr;
		}
		if (pErrorBlob) pErrorBlob->Release();

		return S_OK;
	}

	unsigned int strToSlot(const char* str)
	{
		if (strcmp(str, "POSITION") == 0)
			return Vertex_Position;
		else if (strcmp(str, "TEXCOORD") == 0)
			return Vertex_TexCoord;
		else if (strcmp(str, "NORMAL") == 0)
			return Vertex_Normal;
		else if (strcmp(str, "TANGENT") == 0)
			return Vertex_Tangent;
		else if (strcmp(str, "BITANGENT") == 0)
			return Vertex_BiTangent;
		else if (strcmp(str, "COLOR") == 0)
			return Vertex_Color;
		else
			return 0xFFFFFFFF;
	}

	static HRESULT CreateConstantBufferDesc(ID3DBlob* pShaderBlob, ID3D11Device* pD3DDevice, IRenderer* renderer, Shader::Type shaderType, ShaderProgram* program)
	{

		// Reflect shader info
		ID3D11ShaderReflection* pVertexShaderReflection = NULL;
		if (FAILED(D3DReflect(pShaderBlob->GetBufferPointer(), pShaderBlob->GetBufferSize(), IID_ID3D11ShaderReflection, (void**)&pVertexShaderReflection)))
		{
			return S_FALSE;
		}
		
		D3D11_SHADER_DESC shaderDesc;
		pVertexShaderReflection->GetDesc(&shaderDesc);

		for (size_t i = 0; i < shaderDesc.BoundResources; ++i)
		{
			D3D11_SHADER_INPUT_BIND_DESC bindDesc;
			pVertexShaderReflection->GetResourceBindingDesc(i, &bindDesc);
			if (bindDesc.Type == D3D_SHADER_INPUT_TYPE::D3D_SIT_TEXTURE)
			{
				std::cerr << bindDesc.Name << std::endl;
				program->insertUniformPair(bindDesc.Name, bindDesc.BindPoint);
			}
		}

		

		for (unsigned int i = 0; i< shaderDesc.ConstantBuffers; i++)
		{
			ShaderProgram::ConstantBufferDesc cbs;
			D3D11_SHADER_BUFFER_DESC cbufferDesc;
			ID3D11ShaderReflectionConstantBuffer* constantBuffer = pVertexShaderReflection->GetConstantBufferByIndex(i);
			constantBuffer->GetDesc(&cbufferDesc);
			cbs.name = std::string(cbufferDesc.Name).c_str();
			cbs.size = cbufferDesc.Size;
			
			for (size_t j = 0; j < cbufferDesc.Variables; ++j)
			{
				ShaderProgram::ConstantBufferDesc::Variable cbv;
				auto* variable = constantBuffer->GetVariableByIndex(j);
				D3D11_SHADER_VARIABLE_DESC variableDesc;
				variable->GetDesc(&variableDesc);
				std::cerr << variableDesc.Name << std::endl;
				auto* type = variable->GetType();
				D3D11_SHADER_TYPE_DESC typeDesc;
				type->GetDesc(&typeDesc);

				cbv.name = variableDesc.Name;
				cbv.size = variableDesc.Size;
				cbv.offset = variableDesc.StartOffset;
				cbv.id = StringUtils::stringToHash(cbv.name);
				cbs.variables.push_back(cbv);
			}

			RenderBufferDesc desc;
			desc.bindFlag = BindFlag_ConstantBuffer;
			desc.bytes = cbs.size;
			desc.usage = Usage_Dynamic;
			cbs.renderBuffer = renderer->createRenderBuffer(desc);
			program->insertConstantBuffer(shaderType, std::move(cbs));
		}


		//Free allocation shader reflection memory
		pVertexShaderReflection->Release();
		return S_OK;
	}


	static HRESULT CreateInputLayoutDesc(ID3DBlob* pShaderBlob, ID3D11Device* pD3DDevice, bool interleaved, ID3D11InputLayout** pInputLayout)
	{
		// Reflect shader info
		ID3D11ShaderReflection* pVertexShaderReflection = NULL;
		if (FAILED(D3DReflect(pShaderBlob->GetBufferPointer(), pShaderBlob->GetBufferSize(), IID_ID3D11ShaderReflection, (void**)&pVertexShaderReflection)))
		{
			return S_FALSE;
		}

		// Get shader info
		D3D11_SHADER_DESC shaderDesc;
		pVertexShaderReflection->GetDesc(&shaderDesc);

		// Read input layout description from shader info
		std::vector<D3D11_INPUT_ELEMENT_DESC> inputLayoutDesc;
		for (unsigned int i = 0; i< shaderDesc.InputParameters; i++)
		{
			D3D11_SIGNATURE_PARAMETER_DESC paramDesc;
			pVertexShaderReflection->GetInputParameterDesc(i, &paramDesc);

			// fill out input element desc
			D3D11_INPUT_ELEMENT_DESC elementDesc;
			elementDesc.SemanticName = paramDesc.SemanticName;
			elementDesc.SemanticIndex = paramDesc.SemanticIndex;
			elementDesc.InputSlot = interleaved ? 0 : strToSlot(paramDesc.SemanticName);
			elementDesc.AlignedByteOffset = 0;//(i == 0) ? 0 : D3D11_APPEND_ALIGNED_ELEMENT;
			elementDesc.InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
			elementDesc.InstanceDataStepRate = 0;

			// determine DXGI format
			if (paramDesc.Mask == 1)
			{
				if (paramDesc.ComponentType == D3D_REGISTER_COMPONENT_UINT32) elementDesc.Format = DXGI_FORMAT_R32_UINT;
				else if (paramDesc.ComponentType == D3D_REGISTER_COMPONENT_SINT32) elementDesc.Format = DXGI_FORMAT_R32_SINT;
				else if (paramDesc.ComponentType == D3D_REGISTER_COMPONENT_FLOAT32) elementDesc.Format = DXGI_FORMAT_R32_FLOAT;
			}
			else if (paramDesc.Mask <= 3)
			{
				if (paramDesc.ComponentType == D3D_REGISTER_COMPONENT_UINT32) elementDesc.Format = DXGI_FORMAT_R32G32_UINT;
				else if (paramDesc.ComponentType == D3D_REGISTER_COMPONENT_SINT32) elementDesc.Format = DXGI_FORMAT_R32G32_SINT;
				else if (paramDesc.ComponentType == D3D_REGISTER_COMPONENT_FLOAT32) elementDesc.Format = DXGI_FORMAT_R32G32_FLOAT;
			}
			else if (paramDesc.Mask <= 7)
			{
				if (paramDesc.ComponentType == D3D_REGISTER_COMPONENT_UINT32) elementDesc.Format = DXGI_FORMAT_R32G32B32_UINT;
				else if (paramDesc.ComponentType == D3D_REGISTER_COMPONENT_SINT32) elementDesc.Format = DXGI_FORMAT_R32G32B32_SINT;
				else if (paramDesc.ComponentType == D3D_REGISTER_COMPONENT_FLOAT32) elementDesc.Format = DXGI_FORMAT_R32G32B32_FLOAT;
			}
			else if (paramDesc.Mask <= 15)
			{
				if (paramDesc.ComponentType == D3D_REGISTER_COMPONENT_UINT32) elementDesc.Format = DXGI_FORMAT_R32G32B32A32_UINT;
				else if (paramDesc.ComponentType == D3D_REGISTER_COMPONENT_SINT32) elementDesc.Format = DXGI_FORMAT_R32G32B32A32_SINT;
				else if (paramDesc.ComponentType == D3D_REGISTER_COMPONENT_FLOAT32) elementDesc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
			}

			//save element desc
			inputLayoutDesc.push_back(elementDesc);
		}

		// Try to create Input Layout
		HRESULT hr = pD3DDevice->CreateInputLayout(&inputLayoutDesc[0], inputLayoutDesc.size(), pShaderBlob->GetBufferPointer(), pShaderBlob->GetBufferSize(), pInputLayout);

		//Free allocation shader reflection memory
		pVertexShaderReflection->Release();
		return hr;
	}

	static bool compileShader(ID3D11Device* device, ID3D11DeviceContext* deviceContext, ID3D11InputLayout* shaderLayout, Shader* shader, ShaderProgram* program, IRenderer* renderer)
	{
		std::cerr << "shader build: " << shader->getFileName().c_str() << std::endl;
		std::wstring filename = StringUtils::stringToWString(shader->getFileName());
		const WCHAR* wstr = filename.c_str();
		ID3D11DeviceChild* compiled = nullptr;
		ID3DBlob* blob = nullptr;
		HRESULT hres;
		switch (shader->getType())
		{
		case Shader::Vertex:
		{
			ID3D11VertexShader* vertexShader;
			hres = compileShaderFromFile(device, deviceContext, wstr, "VShader", "vs_4_0", &blob);
			assert(SUCCEEDED(hres) && blob);
			hres = device->CreateVertexShader(blob->GetBufferPointer(), blob->GetBufferSize(), nullptr, &vertexShader);
			assert(SUCCEEDED(hres) && vertexShader);
			compiled = vertexShader;
			/*D3D11_INPUT_ELEMENT_DESC layout[] =
			{
				{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, (unsigned int)VertexSlot::Position, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
				{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_UINT, (unsigned int)VertexSlot::TexCoord, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
				{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, (unsigned int)VertexSlot::Normal, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
				{ "TANGENT", 0, DXGI_FORMAT_R32G32B32_FLOAT, (unsigned int)VertexSlot::Tangent, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
				{ "BITANGENT", 0, DXGI_FORMAT_R32G32B32_FLOAT, (unsigned int)VertexSlot::BiTangent, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
				{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, (unsigned int)VertexSlot::Color, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			};

			UINT numElements = ARRAYSIZE(layout);
			hres = device->CreateInputLayout(layout, numElements, blob->GetBufferPointer(), blob->GetBufferSize(), &shaderLayout);*/
			

			hres = CreateInputLayoutDesc(blob, device, false, &shaderLayout);
			assert(SUCCEEDED(hres) && shaderLayout);
			hres = CreateConstantBufferDesc(blob, device, renderer, shader->getType(), program);
			assert(SUCCEEDED(hres));
			program->setInputLayout(shaderLayout);
			break;
		}
		case Shader::Fragment:
			ID3D11PixelShader* pixelShader;
			hres = compileShaderFromFile(device, deviceContext, wstr, "PShader", "ps_4_0", &blob);
			assert(SUCCEEDED(hres) && blob);
			hres = device->CreatePixelShader(blob->GetBufferPointer(), blob->GetBufferSize(), nullptr, &pixelShader);
			assert(SUCCEEDED(hres) && pixelShader);
			hres =CreateConstantBufferDesc(blob, device, renderer, shader->getType(), program);
			assert(SUCCEEDED(hres));
			compiled = pixelShader;
			break;
		case Shader::Geometry:
			hres = compileShaderFromFile(device, deviceContext, wstr, "GShader", "gs_4_0", &blob);
			assert(SUCCEEDED(hres) && blob);
			ID3D11GeometryShader* geometryShader;
			hres = device->CreateGeometryShader(blob->GetBufferPointer(), blob->GetBufferSize(), nullptr, &geometryShader);
			assert(SUCCEEDED(hres) && geometryShader);
			compiled = geometryShader;
			break;
		}

		if (compiled != nullptr)
		{
			shader->setId((Shader::Handle)compiled);
		}

		return false;
	}

	DXGI_FORMAT pixelFormatToDx(PixelFormat format)
	{
		switch (format)
		{
		case PixelFormat_Undefined: return DXGI_FORMAT_UNKNOWN;
		case PixelFormat_Red: return DXGI_FORMAT_R8_UNORM;
		case PixelFormat_Green: return DXGI_FORMAT_R8_UNORM;
		case PixelFormat_Blue: return DXGI_FORMAT_R8_UNORM;
		case PixelFormat_Alpha: return DXGI_FORMAT_A8_UNORM;
		case PixelFormat_R16F: return DXGI_FORMAT_R16_FLOAT;
		case PixelFormat_RG16F: return DXGI_FORMAT_R16G16_FLOAT;
		case PixelFormat_RGB16F: return DXGI_FORMAT_R16G16B16A16_FLOAT;
		case PixelFormat_RGBA16F: return DXGI_FORMAT_R16G16B16A16_FLOAT;
		case PixelFormat_RGB: return DXGI_FORMAT_B8G8R8X8_UNORM; // <- wtf?
		case PixelFormat_RGBA: return DXGI_FORMAT_R8G8B8A8_UNORM;
		case PixelFormat_R32F: return DXGI_FORMAT_R32_FLOAT;
		case PixelFormat_RG32F: return DXGI_FORMAT_R32G32_FLOAT;
		case PixelFormat_RGB32F: return DXGI_FORMAT_R32G32B32_FLOAT;
		case PixelFormat_RGBA32F: return DXGI_FORMAT_R32G32B32A32_FLOAT;
		case PixelFormat_Luminance: return DXGI_FORMAT_UNKNOWN;
		case PixelFormat_LuminanceAlpha: return DXGI_FORMAT_UNKNOWN;
		default:
			return DXGI_FORMAT_UNKNOWN;
 		};
	}

	D3D11_TEXTURE_ADDRESS_MODE wrapModeToDx(WrapMode mode)
	{
		switch (mode)
		{
		case WrapMode_Clamp: return D3D11_TEXTURE_ADDRESS_CLAMP;
		case WrapMode_ClampToBorder: return D3D11_TEXTURE_ADDRESS_BORDER;
		case WrapMode_ClampToEdge: return D3D11_TEXTURE_ADDRESS_CLAMP;
		case WrapMode_Mirror: return D3D11_TEXTURE_ADDRESS_MIRROR;
		case WrapMode_Repeat: return D3D11_TEXTURE_ADDRESS_WRAP;
		default: return D3D11_TEXTURE_ADDRESS_WRAP;
		}
	}

	D3D11_CULL_MODE cullFaceToDx(CullFace cullface)
	{
		switch (cullface) 
		{
		case CullFace_Back: return D3D11_CULL_BACK;
		case CullFace_Front: return D3D11_CULL_FRONT;
		default: return D3D11_CULL_NONE;
		}
	}

	D3D11_PRIMITIVE_TOPOLOGY primitiveSetToDx(PrimitiveSet p)
	{
		switch (p)
		{
		case PrimitiveSet_Lines: return D3D11_PRIMITIVE_TOPOLOGY_LINELIST;
		case PrimitiveSet_LineStrip: return D3D11_PRIMITIVE_TOPOLOGY_LINESTRIP;
		case PrimitiveSet_Points: return D3D11_PRIMITIVE_TOPOLOGY_POINTLIST;
		case PrimitiveSet_Triangles: return D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
		case PrimitiveSet_TriangleStrip: return D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP;
		default: return D3D11_PRIMITIVE_TOPOLOGY_UNDEFINED;
		}
	}

#pragma endregion

	DxRenderer* g_dxRenderer;

	DxRenderer::DxRenderer()
	{
		g_dxRenderer = this;
	}

	RenderType DxRenderer::getRenderType() const
	{
		return RenderType_DX11;
	}

	void DxRenderer::initWindow(Window* window)
	{
		_currentWindow = window;
		

		PIXELFORMATDESCRIPTOR pixelFormatDescriptor =
		{
			sizeof(PIXELFORMATDESCRIPTOR),
			1,
			PFD_DRAW_TO_WINDOW | PFD_DOUBLEBUFFER,    //Flags
			PFD_TYPE_RGBA,            //The kind of framebuffer. RGBA or palette.
			32,                        //Colordepth of the framebuffer.
			0, 0, 0, 0, 0, 0,
			0,
			0,
			0,
			0, 0, 0, 0,
			24,                        //Number of bits for the depthbuffer
			8,                        //Number of bits for the stencilbuffer
			0,                        //Number of Aux buffers in the framebuffer.
			PFD_MAIN_PLANE,
			0,
			0, 0, 0
		};

		HWND windowHandle = window ? static_cast<HWND>(window->getHandle()) : NULL;
		HDC deviceContextHandle = GetDC(windowHandle);
		int pixelFormat = ChoosePixelFormat(deviceContextHandle, &pixelFormatDescriptor);
		SetPixelFormat(deviceContextHandle, pixelFormat, &pixelFormatDescriptor);

		setupSwapChain();

		HRESULT hres;
		DxTexture* bb = new DxTexture();
		_swapchain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&bb->dxTexture);
		hres = _device->CreateRenderTargetView(bb->dxTexture.Get(), NULL, &bb->renderTargetView);
		assert(SUCCEEDED(hres) && bb->renderTargetView);
		RenderTargetDesc rtDesc;
		rtDesc.width = window->getDesc().width;
		rtDesc.height = window->getDesc().height;
		rtDesc.useHardwareDepth = true;
		rtDesc.slots[RenderTargetSlot_Color0] = bb;
		_defaultRenderTarget = static_cast<DxRenderTarget*>(createRenderTarget(rtDesc));

		setupRasterizer();

		setRenderTarget(_defaultRenderTarget);
	}

	void DxRenderer::setupSwapChain()
	{
		// Set up swap chain
		DXGI_SWAP_CHAIN_DESC scd;
		ZeroMemory(&scd, sizeof(DXGI_SWAP_CHAIN_DESC));
		scd.BufferCount = 1;
		scd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		scd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
		scd.OutputWindow = static_cast<HWND>(_currentWindow->getHandle());
		scd.SampleDesc.Count = 1;
		scd.Windowed = TRUE;

		HRESULT hres;
		hres = D3D11CreateDeviceAndSwapChain(NULL,
			D3D_DRIVER_TYPE_HARDWARE,
			NULL,
			NULL,
			NULL,
			NULL,
			D3D11_SDK_VERSION,
			&scd,
			&_swapchain,
			&_device,
			NULL,
			&_deviceContext);

		assert(SUCCEEDED(hres) && _deviceContext);
	}

	void DxRenderer::setupDepthStencil(ID3D11Texture2D** texture, ID3D11DepthStencilState** state, ID3D11DepthStencilView** view)
	{
		// Set up Depth Texture
		D3D11_TEXTURE2D_DESC depthDesc = {};
		depthDesc.Width = _currentWindow->getDesc().width;
		depthDesc.Height = _currentWindow->getDesc().height;
		depthDesc.MipLevels = 1;
		depthDesc.ArraySize = 1;
		depthDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
		depthDesc.SampleDesc.Count = 1;
		depthDesc.SampleDesc.Quality = 0;
		depthDesc.Usage = D3D11_USAGE_DEFAULT;
		depthDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
		depthDesc.CPUAccessFlags = 0;
		depthDesc.MiscFlags = 0;

		HRESULT hres;
		hres = _device->CreateTexture2D(&depthDesc, nullptr, texture);
		assert(SUCCEEDED(hres) && texture);
		//////////////////////////////////////////////////////////////////////////

		// Set up depth stencil state
		D3D11_DEPTH_STENCIL_DESC depthStencilDesc = {};
		//depthStencilDesc.DepthEnable = false;
		depthStencilDesc.DepthEnable = true;
		depthStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
		depthStencilDesc.DepthFunc = D3D11_COMPARISON_LESS;
		depthStencilDesc.StencilEnable = true;
		depthStencilDesc.StencilReadMask = 0xFF;
		depthStencilDesc.StencilWriteMask = 0xFF;
		depthStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
		depthStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
		depthStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
		depthStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
		depthStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
		depthStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
		depthStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
		depthStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

		hres = _device->CreateDepthStencilState(&depthStencilDesc, state);
		assert(SUCCEEDED(hres) && state);

		/*_deviceContext->OMSetDepthStencilState(_depthStencilState.Get(), 1);*/
		_deviceContext->OMSetDepthStencilState(*state, 1);
		//////////////////////////////////////////////////////////////////////////

		// Set up Depth Stencil View
		D3D11_DEPTH_STENCIL_VIEW_DESC depthViewDesc = {};
		depthViewDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;//depthDesc.Format;
		depthViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
		depthViewDesc.Texture2D.MipSlice = 0;

		
		hres = _device->CreateDepthStencilView(*texture, &depthViewDesc, view);
		assert(SUCCEEDED(hres) && view);
	}

	void DxRenderer::setupRasterizer()
	{
		D3D11_RASTERIZER_DESC rDesc;
		rDesc.FillMode = D3D11_FILL_SOLID;
		rDesc.CullMode = D3D11_CULL_BACK;
		rDesc.FrontCounterClockwise = true;
		rDesc.DepthBias = 0;
		rDesc.SlopeScaledDepthBias = 0.0f;
		rDesc.DepthBiasClamp = 0.0f;
		rDesc.DepthClipEnable = true;
		rDesc.ScissorEnable = false;
		rDesc.MultisampleEnable = false;
		rDesc.AntialiasedLineEnable = false;

		HRESULT hres;
		hres = _device->CreateRasterizerState(&rDesc, &_backfaceRasterizerState);
		assert(SUCCEEDED(hres) && _backfaceRasterizerState);

		rDesc.CullMode = D3D11_CULL_FRONT;
		hres = _device->CreateRasterizerState(&rDesc, &_frontfaceRasterizerState);
		assert(SUCCEEDED(hres) && _frontfaceRasterizerState);

		rDesc.CullMode = D3D11_CULL_NONE;
		hres = _device->CreateRasterizerState(&rDesc, &_defaultRasterizerState);
		assert(SUCCEEDED(hres) && _defaultRasterizerState);


		//_deviceContext->RSSetState(_backfaceRasterizerState.Get());
	}

	void DxRenderer::deinitWindow(Window* window)
	{

	}

	const Window* DxRenderer::getWindow() const
	{
		return _currentWindow;
	}

	void DxRenderer::setShaderProgram(ShaderProgram* program)
	{
		if (program == nullptr)
		{
			_currentProgram = nullptr;
			return;
		}

		unsigned int id = program->getId();
		auto& shaderMap = program->getShaderMap();

		if (program->isDirty())
		{
			if (id == 0xFFFFFFFF)
			{
				// set program value?
			}

			for (auto itr : shaderMap)
			{
				ID3D11DeviceChild* shader = (ID3D11DeviceChild*)(itr.second->getId());
				if (shader != nullptr)
					shader->Release();
			}

			for (auto itr : shaderMap)
			{
				compileShader(_device.Get(), _deviceContext.Get(), _shaderLayout.Get(), itr.second, program, this);
			}

			program->setDirty(false);
		}

		_deviceContext->IASetInputLayout(static_cast<ID3D11InputLayout*>(program->getInputLayout()));

		for (auto itr : shaderMap)
		{
			ID3D11DeviceChild* shader = reinterpret_cast<ID3D11DeviceChild*>(itr.second->getId());
			if (shader == nullptr)
				continue;

			switch (itr.second->getType())
			{
			case Shader::Vertex:
				_deviceContext->VSSetShader(static_cast<ID3D11VertexShader*>(shader), 0, 0);
				break;
			case Shader::Fragment:
				_deviceContext->PSSetShader(static_cast<ID3D11PixelShader*>(shader), 0, 0);
				break;
			case Shader::Geometry:
				_deviceContext->GSSetShader(static_cast<ID3D11GeometryShader*>(shader), 0, 0);
				break;

			default:
				break;
			}
		}
		_currentProgram = program;
	}

	static std::vector<ID3D11Buffer*> updateConstantBuffers(ID3D11DeviceContext* deviceContext, UniformBuffer* uniformBuffer, std::vector<ShaderProgram::ConstantBufferDesc>& cbs)
	{
		const auto& uniforms = uniformBuffer->getUniforms();

		std::vector<ID3D11Buffer*> buffers;
		int index = 0;
		for (auto c : cbs)
		{
			D3D11_MAPPED_SUBRESOURCE mappedResource = {};
			auto* dxBuffer = static_cast<DxRenderBuffer*>(c.renderBuffer.get());
			ID3D11Buffer* buffer = dxBuffer->_id.Get();
			HRESULT hres;
			hres = deviceContext->Map(buffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
			assert(SUCCEEDED(hres));
			for (const auto& v : c.variables)
			{
				for (const auto& u : uniforms)
				{
					if (v.id == u.first)
					{

						memcpy((char*)mappedResource.pData + v.offset, u.second._data, v.size);
					}
				}
			}
			deviceContext->Unmap(buffer, 0);
			buffers.push_back(buffer);
		}

		return buffers;
	}

	void DxRenderer::setUniforms(UniformBuffer* uniformBuffer)
	{
		if (_currentProgram == nullptr || uniformBuffer == nullptr)
			return;

		{
			auto& cbs = _currentProgram->getConstantBuffers(Shader::Vertex);
			std::vector<ID3D11Buffer*> vsBuffers = updateConstantBuffers(_deviceContext.Get(), uniformBuffer, cbs);
			_deviceContext->VSSetConstantBuffers(0, vsBuffers.size(), vsBuffers.data());
		}
		{
			auto& cbs = _currentProgram->getConstantBuffers(Shader::Fragment);
			std::vector<ID3D11Buffer*> psBuffers = updateConstantBuffers(_deviceContext.Get(), uniformBuffer, cbs);
			_deviceContext->PSSetConstantBuffers(0, psBuffers.size(), psBuffers.data());
		}

		//ID3D11ShaderResourceView* resourceViews[10];
		//ID3D11SamplerState* samplerStates[10];
		//std::vector<ID3D11ShaderResourceView*> resourceViews;
		//std::vector<ID3D11SamplerState*> samplerStates;
		//unsigned int count = 0;
		auto& uniforms = uniformBuffer->getUniforms();
		for (auto u : uniforms)
		{
			if (u.second.getType() == Uniform::Type_Texture)
			{
				const auto& uniformMap = _currentProgram->getUniformMap();
				for (auto um : uniformMap)
				{
					if (u.first == StringUtils::stringToHash(um.first))
					{
						DxTexture* dxTexture = static_cast<DxTexture*>(u.second._itexture);
						/*resourceViews[um.second] = (dxTexture->resourceView.Get());
						samplerStates[um.second] = (dxTexture->samplerState.Get());
						count++;*/
						std::vector<ID3D11ShaderResourceView*> resourceViews;
						std::vector<ID3D11SamplerState*> samplerStates;
						resourceViews.push_back(dxTexture->resourceView.Get());
						samplerStates.push_back(dxTexture->samplerState.Get());
						_deviceContext->PSSetShaderResources(um.second, 1, resourceViews.data());
						_deviceContext->PSSetSamplers(um.second, 1, samplerStates.data());
					}
				}
				/*DxTexture* dxTexture = static_cast<DxTexture*>(u.second._itexture);
				resourceViews.push_back(dxTexture->resourceView.Get());
				samplerStates.push_back(dxTexture->samplerState.Get());*/
			}
		}
		/*_deviceContext->PSSetShaderResources(0, count, resourceViews);
		_deviceContext->PSSetSamplers(0, count, samplerStates);*/

		/*_deviceContext->PSSetShaderResources(0, resourceViews.size(), resourceViews.data());
		_deviceContext->PSSetSamplers(0, samplerStates.size(), samplerStates.data());*/
	}

	ITexture* DxRenderer::createTexture(const TextureDesc& desc)
	{
		DxTexture* dxTexture = new DxTexture();
		dxTexture->_desc = desc;
		HRESULT hres;
		D3D11_TEXTURE2D_DESC tdesc = {};
		tdesc.Width = desc.width;
		tdesc.Height = desc.height;
		tdesc.MipLevels = 1;
		tdesc.ArraySize = 1;
		tdesc.SampleDesc.Count = 1;
		tdesc.SampleDesc.Quality = 0;
		tdesc.Usage = D3D11_USAGE_DEFAULT;
		tdesc.Format = pixelFormatToDx(desc.pixelFormat);
		tdesc.BindFlags = D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET;
		tdesc.CPUAccessFlags = 0;
		tdesc.MiscFlags = 0;
		if (desc.data != nullptr)
		{
			D3D11_SUBRESOURCE_DATA sd = {};
			sd.pSysMem = desc.data->getPixels();
			sd.SysMemPitch = desc.data->getWidth() * desc.data->getNumComponents();
			sd.SysMemSlicePitch = 0;

			dxTexture->_desc.width = tdesc.Width = desc.data->getWidth();
			dxTexture->_desc.height = tdesc.Height = desc.data->getHeight();
			hres = _device->CreateTexture2D(&tdesc, &sd, &dxTexture->dxTexture);
		}
		else
		{
			hres = _device->CreateTexture2D(&tdesc, nullptr, &dxTexture->dxTexture);
		}

		assert(SUCCEEDED(hres) && dxTexture->dxTexture);

		// Create Shader Resource View
		D3D11_SHADER_RESOURCE_VIEW_DESC vdesc = {};
		vdesc.Format = tdesc.Format;
		vdesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
		vdesc.Texture2D.MipLevels = tdesc.MipLevels;
		hres = _device->CreateShaderResourceView(dxTexture->dxTexture.Get(), &vdesc, &dxTexture->resourceView);
		assert(SUCCEEDED(hres) && dxTexture->resourceView);

		// Create Render target view
		D3D11_RENDER_TARGET_VIEW_DESC rtDesc = {};
		rtDesc.Format = tdesc.Format;
		rtDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D; // change to array ?
		rtDesc.Texture2D.MipSlice = 0;

		hres = _device->CreateRenderTargetView(dxTexture->dxTexture.Get(), &rtDesc, &dxTexture->renderTargetView);
		assert(SUCCEEDED(hres) && dxTexture->renderTargetView);

		// Create Sampler State
		D3D11_SAMPLER_DESC sdesc = {};
		sdesc.Filter = D3D11_FILTER_ANISOTROPIC;
		sdesc.AddressU = wrapModeToDx(desc.s);
		sdesc.AddressV = wrapModeToDx(desc.r);
		sdesc.AddressW = wrapModeToDx(desc.t);
		sdesc.MipLODBias = 0.0f;
		sdesc.MaxAnisotropy = 16;
		sdesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
		sdesc.MinLOD = 0;
		sdesc.MaxLOD = D3D11_FLOAT32_MAX;
		hres = _device->CreateSamplerState(&sdesc, &dxTexture->samplerState);
		assert(SUCCEEDED(hres) && dxTexture->samplerState);

		return dxTexture;
	}

	void DxRenderer::destroyTexture(ITexture* texture)
	{

	}

	IRenderBuffer* DxRenderer::createRenderBuffer(const RenderBufferDesc& desc)
	{
		DxRenderBuffer* renderBuffer = new DxRenderBuffer();
		renderBuffer->_desc = desc;

		ID3D11Buffer *buffer = nullptr;
		D3D11_BUFFER_DESC bd;
		ZeroMemory(&bd, sizeof(D3D11_BUFFER_DESC));
		bd.Usage = D3D11_USAGE_DYNAMIC;
		bd.ByteWidth = desc.bytes;
		switch (desc.bindFlag)
		{
		case BindFlag_VertexBuffer:
			bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
			break;
		case BindFlag_IndexBuffer:
			bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
			break;
		case BindFlag_ConstantBuffer:
			bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
			break;
		}

		bd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		HRESULT hres;
		if (desc.initialData != nullptr)
		{
			D3D11_SUBRESOURCE_DATA sd;
			ZeroMemory(&sd, sizeof(D3D11_SUBRESOURCE_DATA));
			sd.pSysMem = desc.initialData;
			sd.SysMemPitch = 0;
			sd.SysMemSlicePitch = 0;
			hres = _device->CreateBuffer(&bd, &sd, &buffer);
		}
		else
		{
			hres = _device->CreateBuffer(&bd, nullptr, &buffer);
		}

		assert(SUCCEEDED(hres) && buffer);
		renderBuffer->_id = buffer;

		return renderBuffer;
	}

	void DxRenderer::destroyRenderBuffer(IRenderBuffer* buffer)
	{

	}

	IRenderTarget* DxRenderer::createRenderTarget(const RenderTargetDesc& desc)
	{
		DxRenderTarget* renderTarget = new DxRenderTarget();
		renderTarget->_desc = desc;
		setupDepthStencil(&renderTarget->depthTexture, &renderTarget->_depthStencilState, &renderTarget->_depthStencilView);

		for (auto s : desc.slots)
		{
			auto* dxTexture = static_cast<DxTexture*>(s.second);
			renderTarget->_renderTargetSlots.push_back(dxTexture->renderTargetView);
		}

		return renderTarget;
	}

	void DxRenderer::destroyRenderTarget(IRenderTarget* renderTarget)
	{

	}

	void DxRenderer::setRenderTarget(IRenderTarget* renderTarget)
	{
		_currentRenderTarget = static_cast<DxRenderTarget*>(renderTarget);
		if (renderTarget == nullptr)
		{
			setRenderTarget(_defaultRenderTarget);
			return;
		}
		else
		{
			ID3D11RenderTargetView* rtViews[RenderTargetSlot_Count];
			DxRenderTarget* dxRt = static_cast<DxRenderTarget*>(renderTarget);
			for (size_t i = 0; i < dxRt->_renderTargetSlots.size(); ++i)
			{
				rtViews[i] = dxRt->_renderTargetSlots[i].Get();
			}

			_deviceContext->OMSetRenderTargets(dxRt->_renderTargetSlots.size(), rtViews, dxRt->_desc.useHardwareDepth ? dxRt->_depthStencilView.Get() : nullptr);
			_deviceContext->OMSetDepthStencilState(dxRt->_depthStencilState.Get(), 1);
			Vector4f color{ 0.0f, 0.0f, 0.0f, 1.0f };
			for (auto rt : dxRt->_renderTargetSlots)
			{
				_deviceContext->ClearDepthStencilView(dxRt->_depthStencilView.Get(), D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0.0f);
				_deviceContext->ClearRenderTargetView(rt.Get(), &color[0]);

			}
		}
	}

	void DxRenderer::setViewport(const Viewport& viewport)
	{
		D3D11_VIEWPORT vp;
		ZeroMemory(&vp, sizeof(D3D11_VIEWPORT));
		vp.TopLeftX = (float)viewport.x;
		vp.TopLeftY = (float)viewport.y;
		vp.Width = (float)viewport.width;
		vp.Height = (float)viewport.height;
		vp.MinDepth = 0.0f;
		vp.MaxDepth = 1.0f;
		_deviceContext->RSSetViewports(1, &vp);
	}

	void DxRenderer::setCullFace(const CullFace& cullFace)
	{
		if (cullFace == CullFace_Back)
			_deviceContext->RSSetState(_backfaceRasterizerState.Get());
		else if (cullFace == CullFace_Front)
			_deviceContext->RSSetState(_frontfaceRasterizerState.Get());
		else
			_deviceContext->RSSetState(_defaultRasterizerState.Get());

	}

	void DxRenderer::setClearFlag(const ClearFlag& clearFlag, const Vector4f& color, float depth, float stencil)
	{
		unsigned int clearFlags = clearFlag;
		unsigned int flags = 0;
		flags |= ((clearFlags & ClearFlag_Depth) ? D3D11_CLEAR_DEPTH : flags);
		flags |= ((clearFlags & ClearFlag_Stencil) ? D3D11_CLEAR_STENCIL : flags);

		if (flags > 0)
			_deviceContext->ClearDepthStencilView(_currentRenderTarget->_depthStencilView.Get(), flags, depth, stencil);

		if (clearFlags & ClearFlag_Color)
		{
			for (auto r : _currentRenderTarget->_renderTargetSlots)
			{
				_deviceContext->ClearRenderTargetView(r.Get(), &color[0]);
			}
			
		}
	}

	void DxRenderer::startFrame()
	{
	
	}

	void DxRenderer::endFrame()
	{
		_swapchain->Present(0, 0);
	}

	void DxRenderer::renderMesh(Mesh* mesh)
	{
		if (mesh->getHandle() == 0xFFFFFFFF)
		{
			for (auto& l : mesh->getVertexLayout())
			{
				am::RenderBufferDesc desc;
				desc.usage = l.second->getUsage();
				desc.bindFlag = BindFlag_VertexBuffer;
				desc.bytes = l.second->byteSize();
				desc.initialData = l.second->getData();
				l.second->setOwner(createRenderBuffer(desc));
			}

			for (auto& ind : mesh->getIndexBuffers())
			{
				am::RenderBufferDesc desc;
				desc.usage = ind.second->getUsage();
				desc.bindFlag = BindFlag_IndexBuffer;
				desc.bytes = ind.second->byteSize();
				desc.initialData = ind.second->getData();
				ind.second->setOwner(createRenderBuffer(desc));
			}

			mesh->setHandle(1);
		}

		for (auto l : mesh->getVertexLayout())
		{
			auto* dxBuffer = static_cast<DxRenderBuffer*>(l.second->getOwner());
			UINT stride = l.second->typeSize();
			UINT offset = 0;
			ID3D11Buffer* buffer = dxBuffer->_id.Get();
			_deviceContext->IASetVertexBuffers((unsigned int)l.first, 1, &buffer, &stride, &offset);
		}

		auto& indices = mesh->getIndexBuffers();

		if (!indices.empty())
		{
			for (auto ind : indices)
			{
				auto* dxBuffer = static_cast<DxRenderBuffer*>(ind.second->getOwner());
				ID3D11Buffer* buffer = dxBuffer->_id.Get();
				_deviceContext->IASetIndexBuffer(buffer, DXGI_FORMAT_R32_UINT, 0);
				_deviceContext->IASetPrimitiveTopology(primitiveSetToDx(ind.first));
				_deviceContext->DrawIndexed(ind.second->arraySize(), 0, 0);
			}
		}
		else
			_deviceContext->Draw(mesh->arraySize(), 0);
	}




	//void DxRenderer::renderMesh(IMesh* mesh)
	//{
	//	if (mesh->handle == 0xFFFFFFFF)
	//	{
	//		am::IRenderBuffer::Desc desc;
	//		desc.usage = Usage::Static;
	//		desc.bindFlag = BindFlag::VertexBuffer;
	//		desc.bytes = mesh->byteSize();
	//		desc.initialData = mesh->getData();
	//		mesh->_renderBuffer = createRenderBuffer(desc);
	//	}

	//	auto* dxBuffer = static_cast<DxRenderBuffer*>(mesh->_renderBuffer.get());
	//	ID3D11Buffer* buffer = dxBuffer->_id.Get();
	//	UINT stride = mesh->vertexSize();
	//	UINT offset = 0;
	//	_deviceContext->IASetVertexBuffers(0, 1, &buffer, &stride, &offset);
	//	_deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	//	_deviceContext->Draw(3, 0);
	//}

	//struct Vertex
	//{
	//	Vector4f position;
	//	Vector4f color;
	//};

	//struct VertexArray
	//{
	//	Vector4f positions[3];
	//	Vector4f colors[3];
	//};

	//void DxRenderer::renderMesh(IMesh* mesh)
	//{
	//	static Vertex vData[]
	//	{
	//		{ { 0.0f, 0.5f, 0.5f, 1.0f }, { 1.0f, 0.0f, 0.0f, 1.0f } },
	//		{ { 0.5f, -0.5f, 0.5f, 1.0f }, { 0.0f, 1.0f, 0.0f, 1.0f } },
	//		{ { -0.5f, -0.5f, 0.5f, 1.0f }, { 0.0f, 0.0f, 1.0f, 1.0f } }
	//	};

	//	//static VertexArray vData
	//	//{
	//	//	{ { 0.0f, 0.5f, 0.5f, 1.0f }, { 0.5f, -0.5f, 0.5f, 1.0f }, {-0.5f, -0.5f, 0.5f, 1.0f } },
	//	//	{ { 1.0f, 0.0f, 0.0f, 1.0f }, { 0.0f, 1.0f, 0.0f, 1.0f }, { 0.0f, 0.0f, 1.0f, 1.0f } }
	//	//};

	//	am::IRenderBuffer::Desc vertex2Desc;
	//	vertex2Desc.usage = Usage::Static;
	//	vertex2Desc.bindFlag = BindFlag::VertexBuffer;
	//	vertex2Desc.bytes = sizeof(Vertex) * 3;
	//	vertex2Desc.initialData = &vData;
	//	DxRenderBuffer* renderBuffer = (DxRenderBuffer*)createRenderBuffer(vertex2Desc);
	//	ID3D11Buffer* buffer = renderBuffer->_id.Get();
	//	UINT stride = sizeof(Vertex);
	//	UINT offset = 0;
	//	_deviceContext->IASetVertexBuffers(0, 1, &buffer, &stride, &offset);
	//	_deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	//	_deviceContext->Draw(3, 0);
	//	delete renderBuffer;
	//}

	const char* DxRenderer::getName() const
	{
		return "DirectXDeferred";
	}

}