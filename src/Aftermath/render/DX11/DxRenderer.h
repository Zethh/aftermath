#pragma once

#include <Aftermath/render/Renderer.h>
#include <Aftermath/render/Mesh.h>
#include <Aftermath/render/DX11/DxBuffer.h>
#include <Aftermath/render/DX11/DxRenderTarget.h>
#include <Windows.h>
#include <dxgi.h>
#include <d3dcommon.h>
#include <d3d11.h>
#include <d3d11_1.h>
#include <d3dcompiler.h>
#include <memory.h>
#include <map>
#include <wrl/client.h>
using Microsoft::WRL::ComPtr;
namespace am
{
	class DxRenderer : public IRenderer
	{
		AFTERMATH_META(DxRenderer)
	public:
		DxRenderer();

		virtual RenderType getRenderType() const override;

		virtual void initWindow(Window* window) override;

		virtual void deinitWindow(Window* window) override;

		virtual const Window* getWindow() const override;

		virtual void setShaderProgram(ShaderProgram* program) override;

		virtual void setUniforms(UniformBuffer* uniformBuffer) override;

		virtual ITexture* createTexture(const TextureDesc& desc) override;

		virtual void destroyTexture(ITexture* texture) override;

		virtual IRenderBuffer* createRenderBuffer(const RenderBufferDesc& desc) override;

		virtual void destroyRenderBuffer(IRenderBuffer* buffer) override;

		virtual IRenderTarget* createRenderTarget(const RenderTargetDesc& desc) override;

		virtual void destroyRenderTarget(IRenderTarget* renderTarget) override;

		virtual void setRenderTarget(IRenderTarget* renderTarget) override;

		virtual void setViewport(const Viewport& viewport) override;

		virtual void setCullFace(const CullFace& cullFace) override;

		virtual void setClearFlag(const ClearFlag& clearFlag = ClearFlag_All, const Vector4f& color = { 0.0f, 0.0f, 0.0f, 1.0f }, float depth = 1.0f, float stencil = 0.0f) override;

		virtual void startFrame() override;

		virtual void endFrame() override;

		virtual void renderMesh(Mesh* mesh) override;

		virtual const char* getName() const override;

	private:
		void setupSwapChain();

		void setupDepthStencil(ID3D11Texture2D** texture, ID3D11DepthStencilState** state, ID3D11DepthStencilView** view);

		void setupRasterizer();

	private:
		Window* _currentWindow = nullptr;
		ComPtr<IDXGISwapChain> _swapchain = nullptr;             // the pointer to the swap chain interface
		ComPtr<ID3D11Device> _device = nullptr;                     // the pointer to our Direct3D device interface
		ComPtr<ID3D11DeviceContext> _deviceContext = nullptr;           // the pointer to our Direct3D device context
		ComPtr<ID3D11InputLayout> _shaderLayout = nullptr;
		ComPtr<ID3D11RasterizerState> _frontfaceRasterizerState = nullptr;
		ComPtr<ID3D11RasterizerState> _backfaceRasterizerState = nullptr;
		ComPtr<ID3D11RasterizerState> _defaultRasterizerState = nullptr;

		DxRenderTarget* _defaultRenderTarget = nullptr;
		DxRenderTarget* _currentRenderTarget = nullptr;
		ShaderProgram* _currentProgram = nullptr;
	};

	extern DxRenderer* g_dxRenderer;
}