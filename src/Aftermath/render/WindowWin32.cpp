#include "Window.h"
#ifdef AM_PLATFORM_WINDOWS
#include <Windows.h>
#include <windowsx.h>
#include <iostream>

namespace am {
	
	static MouseButton getMouseButton(short msg)
	{
		switch (msg)
		{
		case MK_LBUTTON:
		case WM_LBUTTONDOWN:
		case WM_LBUTTONUP:
			return MouseButton_Left;

		case MK_RBUTTON:
		case WM_RBUTTONDOWN:
		case WM_RBUTTONUP:
			return MouseButton_Right;

		case MK_MBUTTON:
		case WM_MBUTTONDOWN:
		case WM_MBUTTONUP:
			return MouseButton_Middle;

		default:
			return MouseButton_None;
		}

	}

	static ModifierKey getModifierKey(short msg)
	{
		switch (msg)
		{
		case MK_CONTROL: return Modifier_Ctrl;
		case MK_SHIFT: return Modifier_Shift;
		default: return Modifier_None;
		}
	}

	static std::vector<WindowEvent> g_events;
		static LRESULT CALLBACK WndProc(HWND windowHandle, UINT message, WPARAM wParam, LPARAM lParam)
		{
			switch (message)
			{
			case WM_MOUSEMOVE:
			{
				WindowEvent e;
				e.type = WindowEvent::Event_MouseMove;
				e.x = GET_X_LPARAM(lParam);
				e.y = GET_Y_LPARAM(lParam);
				e.windowHandle = windowHandle;
				g_events.push_back(e);
				break;
			}
			case WM_CLOSE:
			{
				WindowEvent e;
				e.type = WindowEvent::Event_Close;
				e.windowHandle = windowHandle;
				g_events.push_back(e);
				break;
			}
			case WM_LBUTTONDOWN:
			case WM_RBUTTONDOWN:
			case WM_MBUTTONDOWN:
			{
				WindowEvent e;
				e.type = WindowEvent::Event_MouseDown;
				e.windowHandle = windowHandle;
				e.x = GET_X_LPARAM(lParam);
				e.y = GET_Y_LPARAM(lParam);
				e.mouseButton = getMouseButton(message);
				g_events.push_back(e);
				break;
			}

			case WM_LBUTTONUP:
			case WM_RBUTTONUP:
			case WM_MBUTTONUP:
			{
				WindowEvent e;
				e.type = WindowEvent::Event_MouseUp;
				e.windowHandle = windowHandle;
				e.x = GET_X_LPARAM(lParam);
				e.y = GET_Y_LPARAM(lParam);
				e.mouseButton = getMouseButton(message);
				g_events.push_back(e);
				break;
			}

			case WM_MOUSEWHEEL:
			{
				WindowEvent e;
				short fwKeys = LOWORD(wParam);
				e.wheelDelta = HIWORD(wParam);
				e.x = GET_X_LPARAM(lParam);
				e.y = GET_Y_LPARAM(lParam);
				e.type = WindowEvent::Event_MouseWheel;
				e.mouseButton = getMouseButton(message);
				e.modifierKey = getModifierKey(message);
				e.windowHandle = windowHandle;
				g_events.push_back(e);
				break;
			}
			default:
				return DefWindowProc(windowHandle, message, wParam, lParam);
			}
			return DefWindowProc(windowHandle, message, wParam, lParam);
		}

		class WindowImpl
		{
		public:
			HWND _windowHandle;
		};

		Window::Window()
			: _p(new WindowImpl())
		{
			
		}

		Window::~Window()
		{
			//RenderDevice::destroyGraphicsContext(this);
		}

		Window* Window::create(const Desc& descriptor)
		{
			Window* window = new Window();
			window->_desc = descriptor;
			HINSTANCE hInstance = (HINSTANCE)GetModuleHandle(NULL);
			WNDCLASS windowClass = { 0 };
			windowClass.lpfnWndProc = WndProc;
			windowClass.hInstance = hInstance;
			windowClass.hbrBackground = (HBRUSH)(COLOR_BACKGROUND);
			windowClass.lpszClassName = "Christoffer Pettersson";
			windowClass.style = CS_OWNDC;
			if (!RegisterClass(&windowClass))
			{
				std::cerr << "Unable to register Window" << std::endl;
			}

			//Ensure client area is the size in the descriptor.
			RECT wr = { 0, 0, descriptor.width, descriptor.height };
			AdjustWindowRect(&wr, WS_OVERLAPPEDWINDOW, FALSE);

			window->_p->_windowHandle = CreateWindow(windowClass.lpszClassName, descriptor.title, WS_OVERLAPPEDWINDOW | WS_VISIBLE, descriptor.x, descriptor.y, wr.right - wr.left, wr.bottom - wr.top, 0, 0, hInstance, 0);
			return window;
		}

		Window::Handle Window::getHandle()
		{
			return _p->_windowHandle;
		}

		void Window::resize(int width, int height)
		{
			SetWindowPos(_p->_windowHandle, 0, 0, 0, width, height, SWP_NOMOVE | SWP_NOZORDER | SWP_NOACTIVATE);
		}

		const Window::Desc& Window::getDesc() const
		{
			return _desc;
		}

		std::vector<WindowEvent> Window::pollEvents()
		{
			std::vector<WindowEvent> events(g_events);
			g_events.clear();
			MSG msg;
			while (PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE))
			{
				if (!GetMessage(&msg, NULL, 0, 0))
					return std::move(events);

				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}

			return std::move(events);
		}

		void Window::setTitle(const char* title)
		{
			_desc.title = title;
			SetWindowText(_p->_windowHandle, title);
		}

	}
#endif