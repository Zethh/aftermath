#pragma once
#include <Aftermath/render/Buffer.h>

namespace am
{
	class GlRenderBuffer : public IRenderBuffer
	{
		AFTERMATH_META(GlRenderBuffer)
	public:
		unsigned int _id = 0xFFFFFFFF;
		RenderBufferDesc _desc;
	};
}