#include "GlRenderer.h"
#include <Aftermath/render/OpenGL/GlTexture.h>
#include <Aftermath/render/OpenGL/GlBuffer.h>
#include <Aftermath/render/OpenGL/GlRenderTarget.h>
#include <Aftermath/utils/ShaderProgram.h>
#include <Aftermath/render/Window.h>
#include <assert.h>
#include <Aftermath/utils/Image.h>
namespace am
{


#ifdef AM_PLATFORM_WINDOWS
	#pragma comment(lib, "OpenGL32.lib")
	#include <Windows.h>


	#include <Aftermath/render/OpenGL/external/glew/glew.h>
	#include <Aftermath/render/OpenGL/external/glew/wglew.h>

	static void createGraphicsContext(Window* window, HGLRC* outDevice, HDC* outDeviceContext)
	{
		PIXELFORMATDESCRIPTOR pixelFormatDescriptor =
		{
			sizeof(PIXELFORMATDESCRIPTOR),
			1,
			PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER,    //Flags
			PFD_TYPE_RGBA,            //The kind of framebuffer. RGBA or palette.
			32,                        //Colordepth of the framebuffer.
			0, 0, 0, 0, 0, 0,
			0,
			0,
			0,
			0, 0, 0, 0,
			24,                        //Number of bits for the depthbuffer
			8,                        //Number of bits for the stencilbuffer
			0,                        //Number of Aux buffers in the framebuffer.
			PFD_MAIN_PLANE,
			0,
			0, 0, 0
		};

		HWND windowHandle = window ? static_cast<HWND>(window->getHandle()) : NULL;
		HDC deviceContextHandle = GetDC(windowHandle);
		int pixelFormat = ChoosePixelFormat(deviceContextHandle, &pixelFormatDescriptor);
		SetPixelFormat(deviceContextHandle, pixelFormat, &pixelFormatDescriptor);

		HGLRC defaultGLRenderingContextHandle = wglCreateContext(deviceContextHandle);
		wglMakeCurrent(deviceContextHandle, defaultGLRenderingContextHandle);

		GLenum err = glewInit();
		if (GLEW_OK != err)
		{
			MessageBox(0, "Glew not initialized", "GLEW", 0);
		}

		int attribs[] =
		{
			WGL_CONTEXT_MAJOR_VERSION_ARB, 4,
			WGL_CONTEXT_MINOR_VERSION_ARB, 0,
			WGL_CONTEXT_FLAGS_ARB, 0,
			0
		};

		HGLRC coreGLRenderingContextHandle;

		if (wglewIsSupported("WGL_ARB_create_context") == 1)
		{
			coreGLRenderingContextHandle = wglCreateContextAttribsARB(deviceContextHandle, 0, attribs);
			wglMakeCurrent(NULL, NULL);
			wglDeleteContext(defaultGLRenderingContextHandle);
			wglMakeCurrent(deviceContextHandle, coreGLRenderingContextHandle);
			wglSwapIntervalEXT(1);
		}
		else
		{
			coreGLRenderingContextHandle = defaultGLRenderingContextHandle;
		}

		//Checking GL version
		int OpenGLVersion[2];
		glGetIntegerv(GL_MAJOR_VERSION, &OpenGLVersion[0]);
		glGetIntegerv(GL_MINOR_VERSION, &OpenGLVersion[1]);
		std::cerr << "Created Graphics Context(OpenGL) Version " << OpenGLVersion[0] << "." << OpenGLVersion[1] << std::endl;

		*outDevice = coreGLRenderingContextHandle;
		*outDeviceContext = deviceContextHandle;
	}
	#endif


#pragma region staticfunctions
	struct GLPixelFormat
	{
		unsigned int internal;
		unsigned int output;
		unsigned int type;
		bool valid;
	};
	static GLPixelFormat getGLFormat(PixelFormat format)
	{
		GLPixelFormat res;
		res.valid = true;
		switch (format)
		{
		case PixelFormat_Red:
			res.internal = GL_RED;
			res.output = GL_RED;
			res.type = GL_UNSIGNED_BYTE;
			break;
		case PixelFormat_Green:
			res.internal = GL_GREEN;
			res.output = GL_GREEN;
			res.type = GL_UNSIGNED_BYTE;
			break;
		case PixelFormat_Blue:
			res.internal = GL_BLUE;
			res.output = GL_BLUE;
			res.type = GL_UNSIGNED_BYTE;
			break;
		case PixelFormat_Alpha:
			res.internal = GL_ALPHA;
			res.output = GL_ALPHA;
			res.type = GL_UNSIGNED_BYTE;
			break;
		case PixelFormat_R16F:
			res.internal = GL_R16F;
			res.output = GL_RED;
			res.type = GL_HALF_FLOAT;
			break;
		case PixelFormat_RG16F:
			res.internal = GL_RG16F;
			res.output = GL_RG;
			res.type = GL_HALF_FLOAT;
			break;
		case PixelFormat_RGB16F:
			res.internal = GL_RGB16F;
			res.output = GL_RGB;
			res.type = GL_HALF_FLOAT;
			break;
		case PixelFormat_RGBA16F:
			res.internal = GL_RGBA16F;
			res.output = GL_RGBA;
			res.type = GL_HALF_FLOAT;
			break;
		case PixelFormat_RGB:
			res.internal = GL_RGB;
			res.output = GL_RGB;
			res.type = GL_UNSIGNED_BYTE;
			break;
		case PixelFormat_RGBA:
			res.internal = GL_RGBA;
			res.output = GL_RGBA;
			res.type = GL_UNSIGNED_BYTE;
			break;
		case PixelFormat_R32F:
			res.internal = GL_R32F;
			res.output = GL_RGB;
			res.type = GL_FLOAT;
			break;
		case PixelFormat_RG32F:
			res.internal = GL_RG32F;
			res.output = GL_RGB;
			res.type = GL_FLOAT;
			break;
		case PixelFormat_RGB32F:
			res.internal = GL_RGB32F;
			res.output = GL_RGB;
			res.type = GL_FLOAT;
			break;
		case PixelFormat_RGBA32F:
			res.internal = GL_RGBA32F;
			res.output = GL_RGBA;
			res.type = GL_FLOAT;
			break;
		default:
			res.valid = false;
			break;
		};

		return res;
	}

	static unsigned int getGLFilter(FilterMode filter)
	{
		switch (filter)
		{
		case FilterMode_Linear: return GL_LINEAR;
		case FilterMode_LinearMipmapLinear: return GL_LINEAR_MIPMAP_LINEAR;
		case FilterMode_LinearMipmapNearest: return GL_LINEAR_MIPMAP_NEAREST;
		case FilterMode_Nearest: return GL_NEAREST;
		case FilterMode_NearestMipmapLinear: return GL_NEAREST_MIPMAP_LINEAR;
		case FilterMode_NearestMipmapNearest: return GL_NEAREST_MIPMAP_NEAREST;
		default: return GL_NONE; break;
		};
	}

	static unsigned int getGLWrapMode(WrapMode wrap)
	{
		switch (wrap)
		{
		case WrapMode_Clamp: return GL_CLAMP;
		case WrapMode_ClampToEdge: return GL_CLAMP_TO_EDGE;
		case WrapMode_ClampToBorder: return GL_CLAMP_TO_BORDER;
		case WrapMode_Repeat: return GL_REPEAT;
		case WrapMode_Mirror: return GL_MIRRORED_REPEAT;
		default: return GL_NONE; break;
		};
	}

	static unsigned int getGLSlot(RenderTargetSlot s)
	{
		switch (s)
		{
		case RenderTargetSlot_Color0: return GL_COLOR_ATTACHMENT0;
		case RenderTargetSlot_Color1: return GL_COLOR_ATTACHMENT1;
		case RenderTargetSlot_Color2: return GL_COLOR_ATTACHMENT2;
		case RenderTargetSlot_Color3: return GL_COLOR_ATTACHMENT3;
		case RenderTargetSlot_Color4: return GL_COLOR_ATTACHMENT4;
		case RenderTargetSlot_Color5: return GL_COLOR_ATTACHMENT5;
		case RenderTargetSlot_Color6: return GL_COLOR_ATTACHMENT6;
		case RenderTargetSlot_Color7: return GL_COLOR_ATTACHMENT7;
		case RenderTargetSlot_Color8: return GL_COLOR_ATTACHMENT8;
		case RenderTargetSlot_Color9: return GL_COLOR_ATTACHMENT9;
		case RenderTargetSlot_Color10: return GL_COLOR_ATTACHMENT10;
		case RenderTargetSlot_Color11: return GL_COLOR_ATTACHMENT11;
		case RenderTargetSlot_Color12: return GL_COLOR_ATTACHMENT12;
		case RenderTargetSlot_Color13: return GL_COLOR_ATTACHMENT13;
		case RenderTargetSlot_Color14: return GL_COLOR_ATTACHMENT14;
		case RenderTargetSlot_Color15: return GL_COLOR_ATTACHMENT15;
		case RenderTargetSlot_HWDepth: return GL_DEPTH_ATTACHMENT;
		case RenderTargetSlot_HWStencil: return GL_STENCIL_ATTACHMENT;
		default: return GL_NONE; break;
		};
	}

	static unsigned int getGLType(RenderTargetSlot s)
	{
		switch (s)
		{
		case RenderTargetSlot_Color0:
		case RenderTargetSlot_Color1:
		case RenderTargetSlot_Color2:
		case RenderTargetSlot_Color3:
		case RenderTargetSlot_Color4:
		case RenderTargetSlot_Color5:
		case RenderTargetSlot_Color6:
		case RenderTargetSlot_Color7:
		case RenderTargetSlot_Color8:
		case RenderTargetSlot_Color9:
		case RenderTargetSlot_Color10:
		case RenderTargetSlot_Color11:
		case RenderTargetSlot_Color12:
		case RenderTargetSlot_Color13:
		case RenderTargetSlot_Color14:
		case RenderTargetSlot_Color15:
			return GL_COLOR;
			break;
		case RenderTargetSlot_HWDepth: return GL_DEPTH;
		case RenderTargetSlot_HWStencil: return GL_STENCIL;
		default: return GL_NONE; break;
		};
	}

	static void bindTexture(ITexture* texture, unsigned int bindLocation)
	{
		auto* glTexture = static_cast<GlTexture*>(texture);
		if (glTexture->_id != 0xFFFFFFFF)
		{
			glActiveTexture(GL_TEXTURE0 + bindLocation);
			glBindTexture(GL_TEXTURE_2D, glTexture->_id);
			glTexture->_bindLocation = bindLocation;
		}
	}

	static unsigned int usageToGL(Usage usage)
	{
		switch (usage)
		{
		case Usage_Static: return GL_STATIC_DRAW;
		case Usage_Dynamic: return GL_DYNAMIC_DRAW;
		default: return 0xFFFFFFFF;
		}
	}

	static unsigned int primitiveSetToGL(PrimitiveSet p)
	{
		switch (p)
		{
		case PrimitiveSet_Triangles: return GL_TRIANGLES; break;
		case PrimitiveSet_LineStrip: return GL_LINE_STRIP; break;
		case PrimitiveSet_Lines: return GL_LINES; break;
		case PrimitiveSet_TriangleStrip: return GL_TRIANGLE_STRIP; break;
		case PrimitiveSet_Points: return GL_POINTS; break;
		default: return GL_NONE; break;
		}
	}

	static void unbindTexture(ITexture* texture)
	{
		auto* glTexture = static_cast<GlTexture*>(texture);
		if (glTexture->_id != 0xFFFFFFFF && glTexture->_bindLocation != 0xFFFFFFFF)
		{
			glActiveTexture(GL_TEXTURE0 + glTexture->_bindLocation);
			glBindTexture(GL_TEXTURE_2D, 0);
			glTexture->_bindLocation = 0xFFFFFFFF;
		}
	}

	static void bindRenderTarget(IRenderTarget* renderTarget)
	{
		auto* glRt = static_cast<GlRenderTarget*>(renderTarget);
		glBindFramebuffer(GL_FRAMEBUFFER, glRt->_framebufferId);

		Vector4f clearColor(0.0f, 0.0f, 0.0f, 1.0f);
		unsigned int drawBuffers[18];
		unsigned int drawBuffersCount = 0;

		for (auto& s : glRt->_desc.slots)
		{
			glDrawBuffer(getGLSlot(s.first));
			glClearBufferfv(getGLType(s.first), 0, (const GLfloat *)(&(clearColor)));
			drawBuffers[drawBuffersCount++] = getGLSlot(s.first);
		}

		glDrawBuffers(drawBuffersCount, drawBuffers);
	}

	static void unbindRenderTarget()
	{
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}

	static bool linkShaderProgram(ShaderProgram* program)
	{
		int linked;
		unsigned int id = program->getId();
		glLinkProgram(id);
		glGetProgramiv(id, GL_LINK_STATUS, &linked);
		if (!linked)
		{
			std::cerr << "Unable to link program" << std::endl;
			int infoLogLen = 0;
			glGetProgramiv(id, GL_INFO_LOG_LENGTH, &infoLogLen);
			char* infoLog = new char[infoLogLen];
			glGetProgramInfoLog(id, infoLogLen, &infoLogLen, infoLog);
			std::cout << std::endl << infoLog << std::endl;
			delete[] infoLog;
			return false;
		}
		else
		{
			std::cerr << "Linked program" << std::endl;
		}

		program->clearUniformPairs();
		{
			int total = -1;
			glGetProgramiv(id, GL_ACTIVE_UNIFORMS, &total);
			for (int i = 0; i<total; ++i)  {
				int name_len = -1, num = -1;
				GLenum type = GL_ZERO;
				char name[100];
				glGetActiveUniform(id, GLuint(i), sizeof(name)-1,
					&name_len, &num, &type, name);
				name[name_len] = 0;
				GLuint location = glGetUniformLocation(id, name);
				std::cerr << "Uniform: " << name << " location: " << location << std::endl;
				program->insertUniformPair(name, location);
			}
		}
		return true;
	}

	static bool compileShader(Shader* shader)
	{
		std::cerr << "shader build: " << shader->getFileName().c_str() << std::endl;
		int compiled, id = (int)shader->getId();
		std::string str = shader->getSource();
		const char* buffer = str.c_str();
		glShaderSource(id, 1, &buffer, 0);
		glCompileShader(id);
		glGetShaderiv(id, GL_COMPILE_STATUS, &compiled);

		if (!compiled)
		{
			std::cerr << "Unable to compile shader: " << shader->getFileName().c_str() << std::endl;
			int infoLogLen = 0;
			int charsWritten = 0;

			glGetShaderiv(id, GL_INFO_LOG_LENGTH, &infoLogLen);
			if (infoLogLen > 0)
			{
				char* infoLog = new char[infoLogLen];
				glGetShaderInfoLog(id, infoLogLen, &charsWritten, infoLog);
				std::cerr << shader->getFileName().c_str() << ", " << std::endl << infoLog << std::endl;
				delete[] infoLog;
			}
			return false;

		}
		else
		{
			return true;
		}

	}

	static unsigned int getOpenGLType(Shader::Type type)
	{
		if (type == Shader::Vertex)
			return GL_VERTEX_SHADER;
		else if (type == Shader::Fragment)
			return GL_FRAGMENT_SHADER;
		else if (type == Shader::Geometry)
			return GL_GEOMETRY_SHADER;
		else
			return 0;
	}
	
#pragma endregion

	GlRenderer* g_glRenderer;
	GlRenderer::GlRenderer()
	{
		g_glRenderer = this;
	}

	RenderType GlRenderer::getRenderType() const
	{
		
		return RenderType_OGL;
	}

	void GlRenderer::initWindow(Window* window)
	{
		createGraphicsContext(window, &_device, &_deviceContext);
		_currentWindow = window;
		wglMakeCurrent(_deviceContext, _device);
		glEnable(GL_DEPTH_TEST);
	}

	void GlRenderer::deinitWindow(Window* window)
	{
		wglMakeCurrent(NULL, NULL);
	}

	const Window* GlRenderer::getWindow() const
	{
		return _currentWindow;
	}

	void GlRenderer::setShaderProgram(ShaderProgram* program)
	{
		if (program != _currentProgram && _currentProgram != nullptr)
		{
			ShaderProgram::TextureSlot* textureSlots = _currentProgram->getTextureSlots();
			for (unsigned int i = 0; i < ShaderProgram::MaxTextureSlots; ++i)
			{
				if (textureSlots[i].inUse)
				{
					glActiveTexture(GL_TEXTURE_2D + i);
					glBindTexture(GL_TEXTURE_2D, 0);
					textureSlots[i].inUse = false;
				}
			}
		}

		if (program == nullptr)
		{
			glUseProgram(0);
			//ogl::useProgram(0);
			return;
		}

		unsigned int id = program->getId();
		if (program->isDirty())
		{
			if (id == 0xFFFFFFFF)
			{
				id = glCreateProgram();
				program->setId(id);
			}

			auto& shaderMap = program->getShaderMap();
			for (auto itr : shaderMap)
			{
				if (glIsShader((unsigned int)itr.second->getId()))
				{
					glDeleteShader((unsigned int)itr.second->getId());
					glDetachShader(id, (unsigned int)itr.second->getId());
				}
			}

			for (auto itr : shaderMap)
			{
				itr.second->setId((Shader::Handle)glCreateShader(getOpenGLType(itr.second->getType())));
				if (glIsShader((unsigned int)itr.second->getId()))
				{
					if (compileShader(itr.second))
					{
						glAttachShader(id, (unsigned int)itr.second->getId());
					}
				}
			}

			linkShaderProgram(program);
			program->setDirty(false);
		}
		_currentProgram = program;
		glUseProgram(id);
	}

	void GlRenderer::setUniforms(UniformBuffer* uniformBuffer)
	{
		if (uniformBuffer == nullptr && _currentProgram != nullptr)
		{
			ShaderProgram::TextureSlot* textureSlots = _currentProgram->getTextureSlots();
			for (unsigned int i = 0; i < ShaderProgram::MaxTextureSlots; ++i)
			{
				if (textureSlots[i].inUse)
				{
					glActiveTexture(GL_TEXTURE_2D + i);
					glBindTexture(GL_TEXTURE_2D, 0);
					textureSlots[i].inUse = false;
				}
			}
			return;
		}

		const auto& bufferUniforms = uniformBuffer->getUniforms();
		for (auto bufferItr : bufferUniforms)
		{
			const Uniform& bufferUniform = bufferItr.second;
			const auto& uniformMap = _currentProgram->getUniformMap();
			for (auto shaderUniformItr : uniformMap)
			{
				hash shaderUniformHash = StringUtils::stringToHash(shaderUniformItr.first);
				int uniformLocation = shaderUniformItr.second;
				if (shaderUniformHash == bufferItr.first)
				{
					if (shaderUniformItr.second != -1)
					{
						switch (bufferUniform.getType())
						{
						case Uniform::Type_Int:
						{
												  glUniform1iv(uniformLocation, 1, bufferUniform._ints);
												  break;
						}

						case Uniform::Type_Vector2i:
						{
													   glUniform2iv(uniformLocation, 1, bufferUniform._ints);
													   break;
						}

						case Uniform::Type_Vector3i:
						{
													   glUniform3iv(uniformLocation, 1, bufferUniform._ints);
													   break;
						}

						case Uniform::Type_Vector4i:
						{
													   glUniform4iv(uniformLocation, 1, bufferUniform._ints);
													   break;
						}

						case Uniform::Type_Float:
						{
													glUniform1fv(uniformLocation, 1, bufferUniform._floats);
													break;
						}

						case Uniform::Type_Vector2f:
						{
													   glUniform2fv(uniformLocation, 1, bufferUniform._floats);
													   break;
						}

						case Uniform::Type_Vector3f:
						{
													   glUniform3fv(uniformLocation, 1, bufferUniform._floats);
													   break;
						}

						case Uniform::Type_Vector4f:
						{
													   glUniform4fv(uniformLocation, 1, bufferUniform._floats);
													   break;
						}

						case Uniform::Type_Matrix4f:
						{
													   glUniformMatrix4fv(uniformLocation, 1, GL_FALSE, bufferUniform._floats);
													   break;
						}

						case Uniform::Type_Texture:
						{
													  ShaderProgram::TextureSlot* textureSlots = _currentProgram->getTextureSlots();
													  for (unsigned int i = 0; i < ShaderProgram::MaxTextureSlots; ++i)
													  {

														  if (!textureSlots[i].inUse)
														  {
															  if (bufferUniform._itexture)
															  {
																  bindTexture(bufferUniform._itexture, i);
																  //bufferUniform._texture->bind(i);
																  textureSlots[i].inUse = true;
																  textureSlots[i].slot = i;
																  //textureSlots[i].textureId = bufferUniform._itexture;
																  glUniform1i(shaderUniformItr.second, i);
																  break;
															  }
														  }
													  }
													  break;
						}

						};
					}
				}
			}
		}
	}

	ITexture* GlRenderer::createTexture(const TextureDesc& desc)
	{
		GlTexture* texture = new GlTexture();
		texture->_desc = desc;
		auto& texDesc = texture->_desc;
		glGenTextures(1, &texture->_id);

		if (desc.data != nullptr)
		{
			texDesc.width = desc.data->getWidth();
			texDesc.height = desc.data->getHeight();
		}

		GLPixelFormat glFormat = getGLFormat(texDesc.pixelFormat);
		assert(glFormat.valid && "Invalid pixel format");

		glBindTexture(GL_TEXTURE_2D, texture->_id);
		glTexImage2D(GL_TEXTURE_2D, 0, glFormat.internal, texDesc.width, texDesc.height, 0, glFormat.output, glFormat.type, texDesc.data ? texDesc.data->getPixels() : nullptr);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, getGLWrapMode(texDesc.s));
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, getGLWrapMode(texDesc.t));
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, getGLWrapMode(texDesc.r));
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, getGLFilter(texDesc.min));
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, getGLFilter(texDesc.mag));
		glGenerateMipmap(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, 0);

		return texture;
	}

	void GlRenderer::destroyTexture(ITexture* texture)
	{
		auto* glTexture = static_cast<GlTexture*>(texture);
		if (glTexture->_id != 0xFFFFFFFF)
		{
			if (glTexture->_bindLocation != 0xFFFFFFFF)
				unbindTexture(texture);
			glDeleteTextures(1, &glTexture->_id);
		}
	}

	IRenderBuffer* GlRenderer::createRenderBuffer(const RenderBufferDesc& desc)
	{
		GlRenderBuffer* buffer = new GlRenderBuffer();
		buffer->_desc = desc;
		auto& bufferDesc = buffer->_desc;
		glGenBuffers(1, &buffer->_id);
		unsigned int bufferType = 0;

		switch (desc.bindFlag)
		{
		case BindFlag_VertexBuffer:
			bufferType = GL_ARRAY_BUFFER;
			break;
		case BindFlag_IndexBuffer:
			bufferType = GL_ELEMENT_ARRAY_BUFFER;
			break;
		case BindFlag_ConstantBuffer:
			bufferType = GL_UNIFORM_BUFFER;
			break;
		}

		if (bufferDesc.initialData != nullptr)
		{
			glBindBuffer(bufferType, buffer->_id);
			glBufferData(bufferType, bufferDesc.bytes, bufferDesc.initialData, usageToGL(bufferDesc.usage));
			glBindBuffer(bufferType, 0);
		}
		return buffer;
	}

	void GlRenderer::destroyRenderBuffer(IRenderBuffer* buffer)
	{
		auto* glBuffer = static_cast<GlRenderBuffer*>(buffer);
		if (glBuffer->_id != 0xFFFFFFFF)
		{
			glDeleteBuffers(1, &glBuffer->_id);
			glBuffer->_id = 0xFFFFFFFF;
		}
	}

	IRenderTarget* GlRenderer::createRenderTarget(const RenderTargetDesc& desc)
	{
		GlRenderTarget* renderTarget = new GlRenderTarget();
		renderTarget->_desc = desc;
		auto& rtDesc = renderTarget->_desc;
		glGenFramebuffers(1, &renderTarget->_framebufferId);
		if (rtDesc.useHardwareDepth)
			glGenRenderbuffers(1, &renderTarget->_renderbufferId);

		glBindFramebuffer(GL_FRAMEBUFFER, renderTarget->_framebufferId);

		for (auto s : rtDesc.slots)
		{
			bindTexture(s.second, 0);
			glFramebufferTexture2D(GL_FRAMEBUFFER, getGLSlot(s.first), GL_TEXTURE_2D, static_cast<GlTexture*>(s.second)->_id, 0);
			unbindTexture(s.second);
		}

		if (rtDesc.useHardwareDepth)
		{
			glBindRenderbuffer(GL_RENDERBUFFER, renderTarget->_renderbufferId);
			glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, rtDesc.width, rtDesc.height);
			glBindRenderbuffer(GL_RENDERBUFFER, 0);

			glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, renderTarget->_renderbufferId);
		}
		glBindFramebuffer(GL_FRAMEBUFFER, 0);

		return renderTarget;
	}

	void GlRenderer::destroyRenderTarget(IRenderTarget* renderTarget)
	{
		auto* glRt = static_cast<GlRenderTarget*>(renderTarget);
		if (glRt->_renderbufferId != 0xFFFFFFFF)
		{
			glDeleteRenderbuffers(1, &glRt->_renderbufferId);
			glRt->_renderbufferId = 0xFFFFFFFF;
		}

		if (glRt->_framebufferId != 0xFFFFFFFF)
		{
			glDeleteFramebuffers(1, &glRt->_framebufferId);
			glRt->_framebufferId = 0xFFFFFFFF;
		}
	}

	void GlRenderer::setRenderTarget(IRenderTarget* renderTarget)
	{
		if (renderTarget == nullptr)
		{
			glBindFramebuffer(GL_FRAMEBUFFER, 0);
			return;
		}

		auto* glRt = static_cast<GlRenderTarget*>(renderTarget);
		glBindFramebuffer(GL_FRAMEBUFFER, glRt->_framebufferId);

		Vector4f clearColor(0.0f, 0.0f, 0.0f, 1.0f);
		unsigned int drawBuffers[18];
		unsigned int drawBuffersCount = 0;

		for (auto& s : glRt->_desc.slots)
		{
			glDrawBuffer(getGLSlot(s.first));
			glClearBufferfv(getGLType(s.first), 0, (const GLfloat *)(&(clearColor)));
			drawBuffers[drawBuffersCount++] = getGLSlot(s.first);
		}

		glDrawBuffers(drawBuffersCount, drawBuffers);
	}

	void GlRenderer::setViewport(const Viewport& viewport)
	{
		glViewport(viewport.x, viewport.y, viewport.width, viewport.height);
		glScissor(viewport.x, viewport.y, viewport.width, viewport.height);
		glEnable(GL_SCISSOR_TEST);
	}

	void GlRenderer::setCullFace(const CullFace& cullFace)
	{
		switch (cullFace)
		{
		case CullFace_Front:
			glEnable(GL_CULL_FACE);
			glCullFace(GL_FRONT);
			break;
		case CullFace_Back:
			glEnable(GL_CULL_FACE);
			glCullFace(GL_BACK);
			break;
		default:
			glDisable(GL_CULL_FACE);
			break;
		}
	}

	void GlRenderer::setClearFlag(const ClearFlag& clearFlag, const Vector4f& color, float depth, float stencil)
	{
		glClearColor(color.r, color.g, color.b, color.a);
		glClearDepth(depth);
		glClearStencil(stencil);
		unsigned int clearFlags = (int)clearFlag;
		unsigned int flags = 0;
		flags |= ((clearFlags & ClearFlag_Color) ? GL_COLOR_BUFFER_BIT : flags);
		flags |= ((clearFlags & ClearFlag_Depth) ? GL_DEPTH_BUFFER_BIT : flags);
		flags |= ((clearFlags & ClearFlag_Stencil) ? GL_STENCIL_BUFFER_BIT : flags);
		glClear(flags);
	}

	void GlRenderer::startFrame()
	{
		
	}

	void GlRenderer::endFrame()
	{
		SwapBuffers(_deviceContext);
		
	}

	void GlRenderer::renderMesh(Mesh* mesh)
	{
		if (mesh->getHandle() == 0xFFFFFFFF)
		{
			unsigned int handle;
			glGenVertexArrays(1, &handle);
			mesh->setHandle(handle);
			for (auto& l : mesh->getVertexLayout())
			{
				am::RenderBufferDesc desc;
				desc.usage = l.second->getUsage();
				desc.bindFlag = BindFlag_VertexBuffer;
				desc.bytes = l.second->byteSize();
				desc.initialData = l.second->getData();
				l.second->setOwner(createRenderBuffer(desc));
			}

			auto& indices = mesh->getIndexBuffers();
			for (auto& i : indices)
			{
				am::RenderBufferDesc desc;
				desc.usage = i.second->getUsage();
				desc.bindFlag = BindFlag_IndexBuffer;
				desc.bytes = i.second->byteSize();
				desc.initialData = i.second->getData();
				i.second->setOwner(createRenderBuffer(desc));
			}
		}
		glBindVertexArray(mesh->getHandle());

		for (auto& l : mesh->getVertexLayout())
		{
			auto* glBuffer = static_cast<GlRenderBuffer*>(l.second->getOwner());
			glBindBuffer(GL_ARRAY_BUFFER, glBuffer->_id);
			glVertexAttribPointer((unsigned int)l.first, l.second->numComponents(), GL_FLOAT, GL_FALSE, 0, 0);
			glEnableVertexAttribArray((unsigned int)l.first);
		}

		auto& indices = mesh->getIndexBuffers();
		if (!indices.empty())
		{
			for (auto& ind : indices)
			{
				auto* glBuffer = static_cast<GlRenderBuffer*>(ind.second->getOwner());
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, glBuffer->_id);
				glDrawElements(primitiveSetToGL(ind.first), ind.second->arraySize(), GL_UNSIGNED_INT, 0);
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
			}

		}
		else
			glDrawArrays(GL_TRIANGLES, 0, mesh->arraySize());

		glBindVertexArray(0);
	}

	const char* GlRenderer::getName() const
	{
		return "OpenGLDeferred";
	}

}