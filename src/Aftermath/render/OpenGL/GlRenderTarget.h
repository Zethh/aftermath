#pragma once
#include <Aftermath/render/Renderer.h>

namespace am
{
	class GlRenderTarget : public IRenderTarget
	{
		AFTERMATH_META(GlRenderTarget)
	public:
		virtual unsigned int getWidth() const override { return _desc.width; }

		virtual unsigned int getHeight() const override { return _desc.height; }

		virtual ITexture* getTarget(RenderTargetSlot slot) const override { return _desc.slots.find(slot)->second; }

		unsigned int _framebufferId = 0xFFFFFFFF;
		unsigned int _renderbufferId = 0xFFFFFFFF;
		RenderTargetDesc _desc;
	};
}
