#pragma once

#include <Aftermath/render/Renderer.h>
#include <Windows.h>
namespace am
{
	class GlRenderer : public IRenderer
	{
		AFTERMATH_META(GlRenderer)
	public:
		GlRenderer();

		virtual RenderType getRenderType() const override;

		virtual void initWindow(Window* window) override;

		virtual void deinitWindow(Window* window) override;

		virtual const Window* getWindow() const override;

		virtual void setShaderProgram(ShaderProgram* program) override;

		virtual void setUniforms(UniformBuffer* uniformBuffer) override;

		virtual ITexture* createTexture(const TextureDesc& desc) override;

		virtual void destroyTexture(ITexture* texture) override;

		virtual IRenderBuffer* createRenderBuffer(const RenderBufferDesc& desc) override;

		virtual void destroyRenderBuffer(IRenderBuffer* buffer) override;

		virtual IRenderTarget* createRenderTarget(const RenderTargetDesc& desc) override;

		virtual void destroyRenderTarget(IRenderTarget* renderTarget) override;

		virtual void setRenderTarget(IRenderTarget* renderTarget) override;

		virtual void setViewport(const Viewport& viewport) override;

		virtual void setCullFace(const CullFace& cullFace) override;

		virtual void setClearFlag(const ClearFlag& clearFlag = ClearFlag_All, const Vector4f& color = { 0.0f, 0.0f, 0.0f, 1.0f }, float depth = 1.0f, float stencil = 0.0f) override;

		virtual void startFrame() override;

		virtual void endFrame() override;

		virtual void renderMesh(Mesh* mesh) override;

		const char* getName() const override;

	private:
		Window* _currentWindow = nullptr;
		HGLRC _device = nullptr;
		HDC _deviceContext = nullptr;
		ShaderProgram* _currentProgram = nullptr;
	};

	extern GlRenderer* g_glRenderer;
}