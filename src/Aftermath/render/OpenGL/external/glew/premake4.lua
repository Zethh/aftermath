project "glew"
	kind "StaticLib"
	language "C++"
	defines {"GLEW_STATIC"}
	local glew_root = AM_SOURCE_ROOT .. "/libs/glew/"

	files {glew_root .. "GL/*.h", glew_root .. "*.c"}
	--local _vpath = AM_APP_ROOT .. _libName .. "/*"
	local _vpath = {glew_root .. "**"}
	includedirs { glew_root .. "GL" }
	vpaths { ["*"] = _vpath}

	configuration "Debug"
        defines { "DEBUG" }
        flags { "Symbols" }
 
    configuration "Release"
        defines { "NDEBUG" }
        flags { "Optimize" }  
