#pragma once
#include <Aftermath/render/Renderer.h>
namespace am
{
	class GlTexture: public ITexture
	{
		AFTERMATH_META(GlTexture)
	public:
		virtual int getWidth() const override { return _desc.width; }

		virtual int getHeight() const override { return _desc.height; }

		virtual const TextureDesc& getDesc() const { return _desc; }
	public:
		unsigned int _id = 0xFFFFFFFF;
		unsigned int _bindLocation = 0xFFFFFFFF;
		TextureDesc _desc;
	};
}