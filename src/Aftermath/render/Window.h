#pragma once
#include <string>
#include <Aftermath/core/Referenced.h>
#include <vector>


enum MouseButton
{
	MouseButton_None = 0,
	MouseButton_Left = (1<<0),
	MouseButton_Right = (1<<1),
	MouseButton_Middle = (1<<2)
};

enum ModifierKey
{
	Modifier_None = 0,
	Modifier_Ctrl = (1<<0),
	Modifier_Shift = (1<<1)
};

enum KeyboardKey
{
	Key_A,
	Key_B,
};

struct WindowEvent
{
	enum Event
	{
		Event_MouseMove,
		Event_MouseDown,
		Event_MouseUp,
		Event_MouseWheel,
		Event_KeyDown,
		Event_KeyUp,
		Event_Resize,
		Event_Close,
		Event_None
	};

	union
	{
		struct { float dx, dy; };
		struct { MouseButton mouseButton; KeyboardKey keyboardKey; ModifierKey modifierKey;  short wheelDelta; };
	};
	unsigned int x, y;
	void* windowHandle;
	Event type = Event_None;
};

struct MouseDownEvent
{
	MouseDownEvent(int x, int y, MouseButton mouseButton, ModifierKey modifierKey)
	: _x(x), _y(y), _mouseButton(mouseButton), _modifiers(modifierKey){ }
	const MouseButton _mouseButton;
	const ModifierKey _modifiers;
	const int _x;
	const int _y;
};

struct MouseUpEvent
{
	MouseUpEvent(int x, int y, MouseButton mouseButton, ModifierKey modifierKey)
	: _x(x), _y(y), _mouseButton(mouseButton), _modifiers(modifierKey){ }
	const MouseButton _mouseButton;
	const ModifierKey _modifiers;
	const int _x;
	const int _y;
};

struct MouseMoveEvent
{
	MouseMoveEvent(int x, int y)
	: _x(x), _y(y) { }
	const int _x;
	const int _y;
};




namespace am {
		class WindowImpl;
		class Window: public Referenced
		{
			AFTERMATH_META(Window)
		public:
			Window();

			virtual ~Window();

			struct Desc
			{
				int x;
				int y;
				int width;
				int height;
				const char* title;
			};

			static Window* create(const Desc& descriptor);

			void resize(int width, int height);

			typedef void* Handle;

			Handle getHandle();

			static std::vector<WindowEvent> pollEvents();

			void setTitle(const char* title);

			void setResizable(bool resizable);

			const Desc& getDesc() const;

		private:
			WindowImpl* _p;
			Desc _desc;
			bool _resizable;
		};
	}