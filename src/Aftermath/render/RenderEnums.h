#pragma once

namespace am
{
	enum RenderType
	{
		RenderType_OGL,
		RenderType_DX11
	};

	enum PixelFormat
	{
		PixelFormat_Undefined,
		PixelFormat_Red,
		PixelFormat_Green,
		PixelFormat_Blue,
		PixelFormat_Alpha,
		PixelFormat_R16F,
		PixelFormat_RG16F,
		PixelFormat_RGB16F,
		PixelFormat_RGBA16F,
		PixelFormat_RGB,
		PixelFormat_RGBA,
		PixelFormat_R32F,
		PixelFormat_RG32F,
		PixelFormat_RGB32F,
		PixelFormat_RGBA32F,
		PixelFormat_Luminance,
		PixelFormat_LuminanceAlpha
	};

	enum VertexSlot
	{
		Vertex_Position,
		Vertex_TexCoord,
		Vertex_Normal,
		Vertex_Tangent,
		Vertex_BiTangent,
		Vertex_Color,
		Vertex_Count
	};

	enum VertexElementType
	{
		VertexElementType_StaticUInt,
		VertexElementType_StaticFloat,
		VertexElementType_StaticFloat2,
		VertexElementType_StaticFloat3,
		VertexElementType_StaticFloat4,
		VertexElementType_DynamicUInt,
		VertexElementType_DynamicFloat,
		VertexElementType_DynamicFloat2,
		VertexElementType_DynamicFloat3,
		VertexElementType_DynamicFloat4
	};

	enum PrimitiveSet
	{
		PrimitiveSet_Points,
		PrimitiveSet_Lines,
		PrimitiveSet_LineStrip,
		PrimitiveSet_Triangles,
		PrimitiveSet_TriangleStrip
	};

	enum BindFlag
	{
		BindFlag_VertexBuffer,
		BindFlag_IndexBuffer,
		BindFlag_ConstantBuffer
	};

	enum Usage
	{
		Usage_Static,
		Usage_Dynamic
	};

	enum CullFace
	{
		CullFace_None,
		CullFace_Front,
		CullFace_Back,
	};

	enum ClearFlag
	{
		ClearFlag_Color = 1 << 0,
		ClearFlag_Depth = 1 << 1,
		ClearFlag_Stencil = 1 << 2,
		ClearFlag_All = ClearFlag_Color | ClearFlag_Depth | ClearFlag_Stencil
	};

	enum WrapMode
	{
		WrapMode_Clamp,
		WrapMode_ClampToEdge,
		WrapMode_ClampToBorder,
		WrapMode_Repeat,
		WrapMode_Mirror
	};

	enum FilterMode
	{
		FilterMode_Linear,
		FilterMode_LinearMipmapLinear,
		FilterMode_LinearMipmapNearest,
		FilterMode_Nearest,
		FilterMode_NearestMipmapLinear,
		FilterMode_NearestMipmapNearest
	};

	enum RenderTargetSlot
	{
		RenderTargetSlot_Color0 = 0,
		RenderTargetSlot_Color1,
		RenderTargetSlot_Color2,
		RenderTargetSlot_Color3,
		RenderTargetSlot_Color4,
		RenderTargetSlot_Color5,
		RenderTargetSlot_Color6,
		RenderTargetSlot_Color7,
		RenderTargetSlot_Color8,
		RenderTargetSlot_Color9,
		RenderTargetSlot_Color10,
		RenderTargetSlot_Color11,
		RenderTargetSlot_Color12,
		RenderTargetSlot_Color13,
		RenderTargetSlot_Color14,
		RenderTargetSlot_Color15,
		RenderTargetSlot_HWDepth,
		RenderTargetSlot_HWStencil,
		RenderTargetSlot_Count
	};
}