#include "Mesh.h"

namespace am {

Mesh::Mesh()
: _handle(0xFFFFFFFF)
{

}

size_t Mesh::arraySize()
{
	size_t size = 0;
	for (auto l : _layout)
	{
		size = l.second->arraySize();
		break;
	}
	return size;
}

size_t Mesh::byteSize()
{
	return _layout.size() * vertexSize();
}

size_t Mesh::vertexSize()
{
	size_t layoutSize = 0;
	for (auto l : _layout)
		layoutSize += (size_t)l.second->typeSize();

	return layoutSize;
}

void Mesh::set(VertexSlot s, IVertexElement*  b)
{
	if (b)
		_layout[s] = b;
}

IVertexElement* Mesh::getVertexElement(VertexSlot s)
{
	return _layout[s];
}

Mesh::VertexLayout& Mesh::getVertexLayout()
{
	return _layout;
}

void Mesh::addIndexBuffer(IVertexElement* b, PrimitiveSet p)
{
	if (b)
		_indices.push_back(std::pair<PrimitiveSet, IVertexElement::Ptr>(p, b));
}

Mesh::Indices& Mesh::getIndexBuffers()
{
	return _indices;
}

void Mesh::setHandle(unsigned int id)
{
	_handle = id;
}

unsigned int Mesh::getHandle() const
{
	return _handle;
}

Mesh Mesh::createFullScreenQuad()
{
	Mesh mesh;
	StaticUInt* indices = new StaticUInt({ 0, 1, 2, 0, 2, 3 });
	StaticFloat3* vertices = new StaticFloat3(
	{
		{ -1, -1, 0 },
		{  1, -1, 0 },
		{  1,  1, 0 },
		{ -1,  1, 0}
	});

	mesh.set(Vertex_Position, vertices);
	mesh.addIndexBuffer(indices);
	return std::move(mesh);
}

Mesh Mesh::createCube(float width, float height, float depth)
{
	Vector3f min(-width, -height, -depth);
	Vector3f max(width, height, depth);
	Mesh mesh;
	
	StaticFloat3* vertices = new StaticFloat3();
	StaticFloat3* normals = new StaticFloat3();
	StaticFloat2* uvs = new StaticFloat2
	{
		{ 0.0f, 0.0f},
		{ 1.0f, 0.0f},
		{ 1.0f, 1.0f},
		{ 0.0f, 1.0f},
		{ 0.0f, 0.0f},
		{ 1.0f, 0.0f},
		{ 1.0f, 1.0f},
		{ 0.0f, 1.0f},

	};

	StaticUInt* indices = new StaticUInt
	{
		0, 1, 2,
		0, 2, 3,
		0, 3, 7,
		0, 7, 4,
		4, 7, 6,
		4, 6, 5,
		5, 6, 2,
		5, 2, 1,
		3, 2, 6,
		3, 6, 7,
		0, 4, 5,
		0, 5, 1
	};
		
	
	vertices->getVector().push_back(Vector3f(min.x, min.y, max.z));
	vertices->getVector().push_back(Vector3f(max.x, min.y, max.z)); //1
	vertices->getVector().push_back(Vector3f(max.x, max.y, max.z)); //2
	vertices->getVector().push_back(Vector3f(min.x, max.y, max.z)); //3
	vertices->getVector().push_back(Vector3f(min.x, min.y, min.z)); //4
	vertices->getVector().push_back(Vector3f(max.x, min.y, min.z)); //5
	vertices->getVector().push_back(Vector3f(max.x, max.y, min.z)); //6
	vertices->getVector().push_back(Vector3f(min.x, max.y, min.z)); //7
	
	normals->getVector().push_back(Vector3f(min.x, min.y, max.z));
	normals->getVector().push_back(Vector3f(max.x, min.y, max.z)); //1
	normals->getVector().push_back(Vector3f(max.x, max.y, max.z)); //2
	normals->getVector().push_back(Vector3f(min.x, max.y, max.z)); //3
	normals->getVector().push_back(Vector3f(min.x, min.y, min.z)); //4
	normals->getVector().push_back(Vector3f(max.x, min.y, min.z)); //5
	normals->getVector().push_back(Vector3f(max.x, max.y, min.z)); //6
	normals->getVector().push_back(Vector3f(min.x, max.y, min.z)); //7
	
		
		//mesh->setIndexBuffer(&_indices);
	mesh.set(Vertex_Position, vertices);
	mesh.set(Vertex_Normal, normals);
	mesh.set(Vertex_TexCoord, uvs);
	mesh.addIndexBuffer(indices, PrimitiveSet_Triangles);

	return std::move(mesh);
	}


}