#pragma once

#include <Aftermath/math/Matrix4.h>
#include <Aftermath/math/Vector2.h>
#include <Aftermath/math/Vector3.h>
#include <Aftermath/math/Vector4.h>
#include <Aftermath/render/Renderer.h>
#include <Aftermath/core/Meta.h>
#include <iostream>

#include <memory>
#include <string.h>
#include <Aftermath/utils/StringUtils.h>
#include <map>

namespace am {

class Uniform
{
public:
    Uniform()
    : _itexture(nullptr)
    { }

    union
    {
        struct { unsigned int _uints[4]; };
        struct { int _ints[4]; };
        struct { float _floats[16]; };
		struct { ITexture* _itexture; };
    };
	void* _data = nullptr;

    enum Type
    {
        Type_Unknown,
		Type_Int,
		Type_Vector2i,
		Type_Vector3i,
		Type_Vector4i,
		Type_Float,
		Type_Vector2f,
		Type_Vector3f,
		Type_Vector4f,
		Type_Matrix4f,
		Type_Texture
    };

    size_t getSize()
    {
        switch(_type)
        {
		case Type_Int: return 4;
		case Type_Vector2i: return 8;
		case Type_Vector3i: return 12;
		case Type_Vector4i: return 16;
		case Type_Float: return 4;
		case Type_Vector2f: return 8;
		case Type_Vector3f: return 12;
		case Type_Vector4f: return 16;
		case Type_Matrix4f: return 64;
		case Type_Texture: return sizeof(size_t);
        default: return 0;
        };
    }

    Type getType() const { return _type; }

    Uniform &operator=(int value)
    {
		_type = Type_Int;
        memcpy(_ints, &value, sizeof(int));
		_data = _ints;
        return *this;
    }

    Uniform &operator=(const Vector2i& value)
    {
        _type = Type_Vector2i;
        memcpy(_ints, &value, sizeof(Vector2i));
		_data = _ints;
        return *this;
    }

    Uniform &operator=(const Vector3i& value)
    {
		_type = Type_Vector3i;
        memcpy(_ints, &value, sizeof(Vector3i));
		_data = _ints;
        return *this;
    }

    Uniform &operator=(const Vector4i& value)
    {
		_type = Type_Vector4i;
        memcpy(_ints, &value, sizeof(Vector4i));
		_data = _ints;
        return *this;
    }

    Uniform &operator=(float value)
    {
		_type = Type_Float;
        memcpy(_floats, &value, sizeof(float));
		_data = _floats;
        return *this;
    }

    Uniform &operator=(const Vector2f& value)
    {
		_type = Type_Vector2f;
        memcpy(_floats, &value, sizeof(Vector2f));
		_data = _floats;
        return *this;
    }

    Uniform &operator=(const Vector3f& value)
    {
		_type = Type_Vector3f;
        memcpy(_floats, &value, sizeof(Vector3f));
		_data = _floats;
        return *this;
    }

    Uniform &operator=(const Vector4f& value)
    {
		_type = Type_Vector4f;
        memcpy(_floats, &value, sizeof(Vector4f));
		_data = _floats;
        return *this;
    }

    Uniform &operator=(const Matrix4f& value)
    {
		_type = Type_Matrix4f;
        memcpy(_floats, &value, sizeof(Matrix4f));
		_data = _floats;
        return *this;
    }

	Uniform &operator=(ITexture* value)
	{
		//if(_texture)
		//    _texture->unref();

		_type = Type_Texture;
		_itexture = value;
		_data = _itexture;
		return *this;
	}

    bool get(int& value)
    {
		if (_type != Type_Int) return false;

        memcpy(&value, _ints, sizeof(int));
        return true;
    }

    bool get(Vector2i& value)
    {
		if (_type != Type_Vector2i) return false;

        memcpy(&value, _ints, sizeof(Vector2i));
        return true;
    }

    bool get(Vector3i& value)
    {
		if (_type != Type_Vector3i) return false;

        memcpy(&value, _ints, sizeof(Vector3i));
        return true;
    }

    bool get(Vector4i& value)
    {
		if (_type != Type_Vector4i) return false;

        memcpy(&value, _ints, sizeof(Vector4i));
        return true;
    }

    bool get(float& value)
    {
		if (_type != Type_Float) return false;

        memcpy(&value, _floats, sizeof(float));
        return true;
    }

    bool get(Vector2f& value)
    {
		if (_type != Type_Vector2f) return false;

        memcpy(&value, _floats, sizeof(Vector2f));
        return true;
    }

    bool get(Vector3f& value)
    {
		if (_type != Type_Vector3f) return false;

        memcpy(&value, _floats, sizeof(Vector3f));
        return true;
    }

    bool get(Vector4f& value)
    {
		if (_type != Type_Vector4f) return false;

        memcpy(&value, _floats, sizeof(Vector4f));
        return true;
    }

    bool get(Matrix4f& value)
    {
		if (_type != Type_Matrix4f) return false;

        memcpy(&value, _floats, sizeof(Matrix4f));
        return true;
    }

private:
    Type _type;
};

class UniformBuffer: public Referenced
{
    AFTERMATH_META(UniformBuffer)
public:
    UniformBuffer()
    {
    }
    typedef std::map<hash, Uniform> Uniforms;

    // Setters
    void set(const std::string& name, int value) { _uniforms[StringUtils::stringToHash(name)] = value; }

	void set(const std::string& name, const Vector2i& value) { _uniforms[StringUtils::stringToHash(name)] = value; }

	void set(const std::string& name, const Vector3i& value) { _uniforms[StringUtils::stringToHash(name)] = value; }

    void set(const std::string& name, const Vector4i& value) { _uniforms[StringUtils::stringToHash(name)] = value; }

    void set(const std::string& name, float value) { _uniforms[StringUtils::stringToHash(name)] = value; }

    void set(const std::string& name, const Vector2f& value) { _uniforms[StringUtils::stringToHash(name)] = value; }

    void set(const std::string& name, const Vector3f& value) { _uniforms[StringUtils::stringToHash(name)] = value; }

    void set(const std::string& name, const Vector4f& value) { _uniforms[StringUtils::stringToHash(name)] = value; }

    void set(const std::string& name, const Matrix4f& value) { _uniforms[StringUtils::stringToHash(name)] = value; }

	void set(const std::string& name, ITexture* value) { _uniforms[StringUtils::stringToHash(name)] = value; }

    bool get(const std::string& name, Uniform& value)
    {
        hash nameHash = StringUtils::stringToHash(name);
        return get(nameHash, value);
    }

    bool get(const hash& name, Uniform& value)
    {
        for(Uniforms::const_iterator itr = _uniforms.begin(); itr != _uniforms.end(); ++itr)
        {
            if((*itr).first == name)
            {
                value = itr->second;
                return true;
            }
        }
        return false;
    }



    const Uniforms& getUniforms() const { return _uniforms; }

protected:
    Uniforms _uniforms;
};

}