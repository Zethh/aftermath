#pragma once

#include <Aftermath/core/Referenced.h>
#include <Aftermath/render/RenderEnums.h>
#include <Aftermath/math/Vector4.h>
#include <map>
namespace am
{
	class Window;
	class ShaderProgram;
	class UniformBuffer;
	class Mesh;
	class Image;

	struct Viewport
	{
		int x, y, width, height;
	};

	struct TextureDesc
	{
		WrapMode s, r, t;
		FilterMode min, mag;
		unsigned int width, height;
		PixelFormat pixelFormat;
		Image* data = nullptr;
	};

	class ITexture : public Referenced
	{
		AFTERMATH_META(ITexture);
	public:
		virtual int getWidth() const = 0;

		virtual int getHeight() const = 0;

		virtual const TextureDesc& getDesc() const = 0;
	};

	struct RenderTargetDesc
	{
		std::map<RenderTargetSlot, ITexture*> slots;
		bool useHardwareDepth;
		unsigned int width, height;
	};

	class IRenderTarget : public Referenced
	{
		AFTERMATH_META(IRenderTarget)
	public:
		virtual unsigned int getWidth() const = 0;

		virtual unsigned int getHeight() const = 0;

		virtual ITexture* getTarget(RenderTargetSlot slot) const = 0;
	};

	struct RenderBufferDesc
	{
		Usage usage;
		BindFlag bindFlag;
		unsigned int bytes;
		const void* initialData = nullptr;
	};

	class IRenderBuffer : public Referenced
	{
		AFTERMATH_META(IRenderBuffer)
	};

	class IRenderer
	{
		AFTERMATH_META(IRenderer)
	public:
		virtual RenderType getRenderType() const = 0;

		virtual void initWindow(Window* window) = 0;

		virtual void deinitWindow(Window* window) = 0;

		virtual const Window* getWindow() const = 0;

		virtual void setShaderProgram(ShaderProgram* program) = 0;

		virtual void setUniforms(UniformBuffer* uniformBuffer) = 0;

		virtual ITexture* createTexture(const TextureDesc& desc) = 0;

		virtual void destroyTexture(ITexture* texture) = 0;

		virtual IRenderBuffer* createRenderBuffer(const RenderBufferDesc& desc) = 0;

		virtual void destroyRenderBuffer(IRenderBuffer* buffer) = 0;

		virtual IRenderTarget* createRenderTarget(const RenderTargetDesc& desc) = 0;

		virtual void destroyRenderTarget(IRenderTarget* renderTarget) = 0;

		virtual void setRenderTarget(IRenderTarget* renderTarget) = 0;

		virtual void setViewport(const Viewport& viewport) = 0;

		virtual void setCullFace(const CullFace& cullFace) = 0;

		virtual void setClearFlag(const ClearFlag& clearFlag = ClearFlag_All, const Vector4f& color = { 0.0f, 0.0f, 0.0f, 1.0f }, float depth = 1.0f, float stencil = 0.0f) = 0;

		virtual void startFrame() = 0;

		virtual void endFrame() = 0;

		virtual void renderMesh(Mesh* mesh) = 0;

		virtual const char* getName() const = 0;
	};

	extern IRenderer* g_renderer;
}
