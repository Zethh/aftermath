#pragma once 

#include <iostream>
#include <vector>
#include <map>
#include <Aftermath/core/Referenced.h>
#include <Aftermath/render/Buffer.h>
#include <Aftermath/render/RenderEnums.h>
namespace am {
	class Mesh
	{
	public:
		Mesh();

		typedef std::map<VertexSlot, IVertexElement::Ptr> VertexLayout;

		typedef std::vector<std::pair<PrimitiveSet, IVertexElement::Ptr> > Indices;

		size_t arraySize();

		size_t byteSize();

		size_t vertexSize();

		void set(VertexSlot s, IVertexElement*  b);

		IVertexElement* getVertexElement(VertexSlot s);

		VertexLayout& getVertexLayout();

		void addIndexBuffer(IVertexElement* b, PrimitiveSet p = PrimitiveSet_Triangles);

		Indices& getIndexBuffers();

		void setHandle(unsigned int id);

		unsigned int getHandle() const;

		static Mesh createFullScreenQuad();

		static Mesh createCube(float width = 1.0f, float height = 1.0f, float depth = 1.0f);

	private:
		VertexLayout _layout;
		Indices _indices;
		unsigned int _handle;
	};
}
