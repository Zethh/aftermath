#pragma once
#include <string>
#include <vector>

#include <Aftermath/core/Defines.h>
namespace am {

class StringUtils
{
public:
	static std::string getFileName(const std::string& path);

	static std::string getFileExtension(const std::string& file);

	static std::string getDirectory(const std::string& file);

	static std::vector<std::string> getLinesContainingWord(const std::string& word, const std::string& text);

	static hash stringToHash(const std::string& str);

	static hash generateHash(void* key, unsigned int length);

    static std::wstring stringToWString(const std::string& str);

    static std::string wstringToString(const std::wstring& wstr);

	static std::string toForwardSlash(const std::string& str);
};

} 