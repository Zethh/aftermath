#include "EffectReader.h"

#include <Aftermath/core/Log.h>
#include <Aftermath/utils/ShaderProgram.h>
#include <Aftermath/utils/Shader.h>
#include <Aftermath/core/FileSystem.h>
#include <Aftermath/xml/XMLDocument.h>

namespace am {

EffectReader::EffectReader()
{
	_supportedExtensions.push_back("fx");
}

EffectReader::~EffectReader()
{
}

Resource* EffectReader::read(const std::string& filename, Resource* existingResosource)
{
	Effect* effect = dynamic_cast<Effect*>(existingResosource);
	
	if(!existingResosource)
	{
		effect = new Effect();
		effect->setResourceReader(this);
	}else
		effect->clearTechniques();

	am::XMLDoc::Ptr xmlDocument = new am::XMLDoc(filename);
	const XMLNode& root = xmlDocument->getRootNode();
	for (size_t i = 0; i < root.numChildren(); ++i)
	{
		const XMLNode& child = root.getChild(i);
		if (child.hasName("group"))
		{
			if (child.getAttribute("id").asString().compare(_groupId) == 0 || _groupId.length() == 0)
				parseGroup(child, effect);
		}
	}

	return effect;
}

void EffectReader::parseGroup(const XMLNode& node, Effect* effect)
{
	for (size_t i = 0; i < node.numChildren(); ++i)
	{
		const XMLNode& child = node.getChild(i);
		if (child.hasName("technique"))
		{
			Effect::Technique::Ptr technique = new Effect::Technique();
			std::string techniqueName = child.getAttribute("id").asString();
			technique->setName(techniqueName);
			LOG_WARNING("Loading Technique: %s\n", techniqueName.c_str());
			parseTechnique(child, technique);
			effect->addTechnique(techniqueName, technique);
		}
	}
}

void EffectReader::parseTechnique(const XMLNode& node, Effect::Technique* technique)
{
	ShaderProgram::Ptr shaderProgram = new ShaderProgram();
	for (size_t i = 0; i < node.numChildren(); ++i)
	{
		const XMLNode& child = node.getChild(i);
		if (child.hasName("vertexshader"))
		{
			std::string name = FileSystem::getFilePath(child.getValue());
			LOG_WARNING("Vertex Shader: %s\n", name.c_str());
			Shader::Ptr shader = ResourceManager::load<Shader>(name);
			shaderProgram->addShader(shader);
		}
		else if (child.hasName("fragmentshader"))
		{
			std::string name = FileSystem::getFilePath(child.getValue());
			LOG_WARNING("Fragment Shader: %s\n", name.c_str());
			Shader::Ptr shader = ResourceManager::load<Shader>(name);
			shaderProgram->addShader(shader);
		}
	}

	technique->setProgram(technique->getName(), shaderProgram);
}

void EffectReader::setGroupId(const std::string& groupId)
{
	_groupId = groupId;
}

} 