#include "Resource.h"
#include <Aftermath/utils/ResourceReader.h>
#include <mutex>
#include <atomic>
namespace am {

class ResourceImpl
{
public:
    ResourceImpl()
    {
        _status = Resource::Unloaded;
    }

    std::string _fileName;
	ResourceReader::Ptr _reader;
    std::atomic<Resource::Status> _status;
};

Resource::Resource()
	: _p(new ResourceImpl())
{
}

Resource::~Resource()
{
}

void Resource::setStatus(Status s)
{
    _p->_status = s;
}

Resource::Status Resource::getStatus() const
{
    return _p->_status;
}

void Resource::setFileName(const std::string& fileName)
{
	_p->_fileName = fileName;
}

const std::string& Resource::getFileName() const
{
    return _p->_fileName;
}

void Resource::setResourceReader(ResourceReader* reader)
{
	_p->_reader = reader;
}

bool Resource::reload()
{
	if(_p->_reader)
	{
		_p->_reader->read(this->getFileName(), this);
	}

	return false;
}

} 