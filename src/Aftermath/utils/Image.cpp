#include "Image.h"
#include <atomic>


#include <memory.h>
#include <memory>

namespace am {


class ImageImpl
{
public:
	ImageImpl()
		: _pixels(NULL)
		
	{
		_flags = 0;
		_currentFlags = 0;
	}

	std::atomic<unsigned int> _width;
	std::atomic<unsigned int> _height;
	std::atomic<unsigned int> _numBytes;
	std::atomic<unsigned int> _numComponents;
	std::atomic<unsigned int> _flags;
	std::atomic<unsigned int> _currentFlags;
	unsigned char* _pixels;
};

Image::Image()
	: _p(new ImageImpl())
{
	
}

Image::~Image()
{
	delete [] _p->_pixels;
	delete _p;
}

void Image::setFlags(unsigned int flags)
{
	_p->_flags = flags; 
	applyFlags();
}

unsigned int Image::getFlags() const
{
	return _p->_flags;
}

void Image::setData(unsigned int width, unsigned int height, unsigned int numComponents, unsigned char* pixels)
{
	_p->_currentFlags = 0;

	_p->_width = width;
	_p->_height = height;
	_p->_numComponents = numComponents;
	_p->_numBytes = width * height * numComponents;
	
    if(pixels)
    {
        if(_p->_pixels)
        delete [] _p->_pixels;
        unsigned int numBytes = _p->_numBytes;
        _p->_pixels = new unsigned char[numBytes];
        memcpy(_p->_pixels, pixels, _p->_numBytes);
    }

	applyFlags();

	AFTERMATH_NOTIFY_OBSERVERS(IImageObserver, onImageModified, this);
}

unsigned char* Image::getPixels() const
{
	return _p->_pixels;
}

unsigned int Image::getWidth() const
{
	return _p->_width;
}

unsigned int Image::getHeight() const
{
	return _p->_height;
}

unsigned int Image::getNumComponents() const
{
	return _p->_numComponents;
}

void Image::flipX()
{
	_p->_currentFlags = _p->_currentFlags ^ FLIP_PIXELS_X_AXIS;

	unsigned int width = _p->_width;
	unsigned int height = _p->_height;

	for(unsigned int y = 0; y < height; ++y)
	{
		for(unsigned int x = 0; x < width/2; ++x)
		{
			unsigned int lhs = (x + y * width) * getNumComponents();
			unsigned int rhs = (((width-1)-x) + y * width) * getNumComponents();
			unsigned char b1, b2, b3;
			b1 = _p->_pixels[lhs];
			b2 = _p->_pixels[lhs+1];
			b3 = _p->_pixels[lhs+2];

			_p->_pixels[lhs]   = _p->_pixels[rhs];
			_p->_pixels[lhs+1] = _p->_pixels[rhs+1];
			_p->_pixels[lhs+2] = _p->_pixels[rhs+2];

			_p->_pixels[rhs]   = b1;
			_p->_pixels[rhs+1] = b2;
			_p->_pixels[rhs+2] = b3;
		}
	}
}

void Image::flipY()
{
	_p->_currentFlags = _p->_currentFlags ^ FLIP_PIXELS_Y_AXIS;

	unsigned int width = _p->_width;
	unsigned int height = _p->_height;

	for(unsigned int y = 0; y < height/2; ++y)
	{
		for(unsigned int x = 0; x < width; ++x)
		{
			unsigned int lhs = (x + y * width) * getNumComponents();
			unsigned int rhs = (x + (height-1-y) * width) * getNumComponents();
			unsigned char b1, b2, b3;
			b1 = _p->_pixels[lhs];
			b2 = _p->_pixels[lhs+1];
			b3 = _p->_pixels[lhs+2];

			_p->_pixels[lhs]   = _p->_pixels[rhs];
			_p->_pixels[lhs+1] = _p->_pixels[rhs+1];
			_p->_pixels[lhs+2] = _p->_pixels[rhs+2];

			_p->_pixels[rhs]   = b1;
			_p->_pixels[rhs+1] = b2;
			_p->_pixels[rhs+2] = b3;
		}
	}
}

void Image::applyFlags()
{
	if(_p->_flags & FLIP_PIXELS_X_AXIS)
	{
		if(_p->_currentFlags & FLIP_PIXELS_X_AXIS)
		{
		}
		else
			flipX();
	}
	else
	{
		if(_p->_currentFlags & FLIP_PIXELS_X_AXIS)
		{
			flipX();
		}
	}

	if(_p->_flags & FLIP_PIXELS_Y_AXIS)
	{
		if(_p->_currentFlags & FLIP_PIXELS_Y_AXIS)
		{
		}
		else
			flipY();
	}
	else
		if(_p->_currentFlags & FLIP_PIXELS_Y_AXIS)
		{
			flipY();
		}
}

} 