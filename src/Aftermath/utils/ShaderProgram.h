#pragma once
#include <map>
#include <string>

#include <Aftermath/core/Referenced.h>
#include <Aftermath/utils/Shader.h>
#include <Aftermath/core/ResourceManager.h>
#include <Aftermath/render/Uniform.h>
#include <Aftermath/render/Mesh.h>

namespace am {

    class ShaderProgram: public Referenced, public Shader::IShaderObserver
    {
        AFTERMATH_META(ShaderProgram)
    public:
        ShaderProgram();

        void setName(const std::string& name);

        void addShader(Shader* shader);

        void setDirty(bool state = true);

        bool isDirty() const;

		void setId(unsigned int id) { _id = id; }

		unsigned int getId() const { return _id; }

		struct ConstantBufferDesc
		{
			struct Variable
			{
				hash id;
				std::string name;
				size_t offset;
				size_t size;
			};

			std::string name;
			size_t size;
			std::vector<Variable> variables;
			IRenderBuffer::Ptr renderBuffer;
		};

		void insertConstantBuffer(Shader::Type type, ConstantBufferDesc&& desc)
		{
			_constantBuffers[type].push_back(desc);
		}

		void setInputLayout(void* layout)
		{
			_layout = layout;
		}

		void* getInputLayout()
		{
			return _layout;
		}
		void* _layout = nullptr;

		std::vector<ConstantBufferDesc>& getConstantBuffers(Shader::Type type)
		{
			return _constantBuffers[type];
		}

		void insertUniformPair(const std::string& key, int value)
		{
			_uniformMap[key] = value;
		}

		void clearUniformPairs()
		{
			_uniformMap.clear();
		}

		typedef std::map<Shader::Type, Shader::Ptr> ShaderMap;

		typedef std::map<std::string, int> UniformMap;

		struct TextureSlot
		{
			TextureSlot()
			: inUse(false)
			{
			}

			unsigned int slot;
			unsigned int textureId;
			bool inUse;
		};

		static const unsigned int MaxTextureSlots = 16;

		ShaderMap& getShaderMap() { return _shaderMap; }

		const UniformMap& getUniformMap() const { return _uniformMap; }

		TextureSlot* getTextureSlots() { return _textureSlots; }

    protected:
        ~ShaderProgram();

        void onShaderModified(Shader* shader);

		std::map<Shader::Type, std::vector<ConstantBufferDesc>> _constantBuffers;

        ShaderMap _shaderMap;

        UniformMap _uniformMap;

        TextureSlot _textureSlots[MaxTextureSlots];

        std::string _name;

        bool _dirty;

		unsigned int _id;
    };
    
} 