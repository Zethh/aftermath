#include "ImageReader.h"

#include <Aftermath/utils/Image.h>
#include <Aftermath/core/Log.h>

#include <Aftermath/utils/Image.h>
#include <Aftermath/utils/StringUtils.h>
#include <Aftermath/external/stb_image/stb_image.h>

namespace am {

ImageReader::ImageReader()
{
	_supportedExtensions.push_back("bmp");
	_supportedExtensions.push_back("png");
	_supportedExtensions.push_back("tga");
	_supportedExtensions.push_back("jpg");
	_supportedExtensions.push_back("jpeg");

}

ImageReader::~ImageReader()
{

}

Resource* ImageReader::read(const std::string& fileName, Resource* existingResource)
{
	Image* resource = dynamic_cast<Image*>(existingResource);
	if (!resource)
	{
		resource = new Image();
		resource->setResourceReader(this);
	}
	else
	{
		std::cerr << "Using existing resource!\n" << std::endl;
	}

	int x, y, n;
	unsigned char* data = stbi_load(fileName.c_str(), &x, &y, &n, 0);
	if (data)
	{
		resource->setData(x, y, n, data);
		delete [] data;
		data = NULL;
	}
	
	else
		std::cerr << "FAILED TO LOAD IMAGE" << std::endl;

	return resource;
}

} 