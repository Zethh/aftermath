#pragma once

#include <Aftermath/core/Referenced.h>
#include <Aftermath/utils/ResourceReader.h>


namespace am {
	class Resource;
class ShaderReader: public ResourceReader
{
    AFTERMATH_META(ShaderReader)
public:
	ShaderReader();

	virtual ~ShaderReader() { }

	Resource* read(const std::string& fileName, Resource* existingResource = NULL);

protected:

};

} 