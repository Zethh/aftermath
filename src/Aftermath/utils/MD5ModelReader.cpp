//#include "MD5ModelReader.h"
//#include <Aftermath/utils/SkinnedModel.h>
//#include <fstream>
//#include <sstream>
//#include <Aftermath/math/Quaternion.h>
//#include <Aftermath/math/Vector3.h>
//#include <Aftermath/utils/StringUtils.h>
//
//#include <functional>
//#include <thread>
//#include <assert.h>
//namespace am {
//
//#define IGNORELINE ignore(std::numeric_limits<std::streamsize>::max(), '\n')
//
//    /*
//Calculates the w component of a quaternion.
//*/
//void calcQuatW(Quaternionf& q)
//{
//	float t = 1.0f - (q.x * q.x) - (q.y * q.y) - (q.z * q.z);
//		if(t < 0.0f)
//			q.w = 0.0f;
//		else
//            q.w = -std::sqrt(t);
//}
//
//
//// Removes quotes from a string.
//void removeQuotes(std::string& s) 
//{
//	s.erase(std::remove(s.begin(), s.end(), '\"'), s.end());
//}
//
//MD5ModelReader::MD5ModelReader()
//{
//    _supportedExtensions.push_back("md5mesh");
//}
//
//MD5ModelReader::~MD5ModelReader()
//{
//}
//
//Resource* MD5ModelReader::read(const std::string& fileName, Resource* existingResource)
//{
//    SkinnedModel* resource = dynamic_cast<SkinnedModel*>(existingResource);
//    if(!resource)
//        resource = new SkinnedModel();
//
//    if(resource->getStatus() != Resource::Loading)
//    {
//        resource->setStatus(Resource::Loading);
//		std::thread t(std::bind(&MD5ModelReader::internalRead, this, fileName, resource));
//		t.detach();
//    }
////    internalRead(fileName, resource);
//
//    return resource;
//}
//
//void MD5ModelReader::internalRead(const std::string &fileName, SkinnedModel *resource)
//{
//    unsigned int numJoints = 0;
//    unsigned int numMeshes = 0;
//
//    SkinnedModel::Skeleton& skeleton = resource->getSkeleton();
//    std::vector<SkinnedModel::Mesh>& meshes = resource->getMeshes();
//
//    unsigned int currMesh = 0;
//	std::string buffer, junk;
//    std::ifstream filebuff(fileName.c_str());
//	assert(filebuff.is_open() && "Unable to open file");
//	std::stringstream file;
//	file << filebuff.rdbuf();
//	filebuff.close();
//	file >> buffer;
//	while(!file.eof())
//	{
//		if(buffer == "MD5Version")
//		{
//			int version;
//			file >> version;
//			assert(version == 10);
//		}
//		else if(buffer == "numJoints")
//		{
//			file >> numJoints;
//			//We know how many joints this model contains, so we can resize our vector of joints.
//			skeleton.joints.reserve(numJoints);
//		}
//
//		else if(buffer == "numMeshes")
//		{
//			file >> numMeshes;
//			//We know how many meshes this model contains, so we can resice our vector of meshes.
//			meshes.resize(numMeshes);
//		}
//
//		else if(buffer == "joints")
//		{
//			file >> junk;
//			for(unsigned int i = 0; i < numJoints; ++i)
//			{
//				SkinnedModel::Joint joint;
//				file >> joint.name >> joint.parentId >> junk;
//				file >> joint.position.x >> joint.position.y >> joint.position.z >> junk >> junk;
//				file >> joint.rotation.x >> joint.rotation.y >> joint.rotation.z >> junk;
//				removeQuotes(joint.name);
//				calcQuatW(joint.rotation);
//				skeleton.joints.push_back(joint);
//				file.IGNORELINE;
//			}
//			assert(skeleton.joints.size() == numJoints && "Joint said size differs from actual size");
//		}
//
//		else if(buffer == "mesh")
//		{
//            //meshes.push_back(SkinnedModel::Mesh());
//			SkinnedModel::Mesh& mesh = meshes[currMesh];
//			//Mesh mesh;
//			file >> junk >> buffer;
//			while(buffer != "}")
//			{
//				////Shader is the handle for the texture to be used for the given mesh
//				//if(buffer == "shader")
//				//{
//				//	//Here we process the data to remove all / and \. and add the prefix .bmp at the end
//				//	file >> mesh.shader;
//				//	removeQuotes(mesh.shader);
//				//	int pos = mesh.shader.find_last_of('/');
//				//	if(pos < 0)
//				//		pos = mesh.shader.find_last_of('\\');
//				//	std::string tmp = mesh.shader.substr(pos+1, mesh.shader.npos);
//				//	int extension = tmp.find_last_of('.');
//				//	if(extension < 0)
//				//		tmp +=".tga";
//				//	else
//				//		tmp=(tmp.substr(0, extension)+".tga");
//				//	mesh.shader = tmp;
//				//	
//				//	//Once the texture data has been stripped and we know which texture to use
//				//	//we call the texture manager to have it return the id of the texture.
//				//	//mesh.texId = TextureManager::GetInstance()->GetTexture(tmp);
//				//	file.IGNORELINE;
//				//}
//
//				if(buffer == "numverts")
//				{
//					file >> mesh.numVertices;
//					file.IGNORELINE;
//					mesh.vertices.resize(mesh.numVertices);
//                    mesh.positions.resize(mesh.numVertices);
//                    mesh.texCoords.resize(mesh.numVertices);
//                    Vector2f tex;
//					for(unsigned int i = 0; i < mesh.numVertices; ++i)
//					{
//						SkinnedModel::Vertex vert;
//						file >> buffer >> buffer >> buffer;
//						file >> tex.x >> tex.y >> junk;
//						file >> buffer >> vert.numWeights;
//						file.IGNORELINE;
//
//						mesh.vertices[i] = vert;
//                        mesh.texCoords[i] = Vector2u((unsigned int)tex.x, (unsigned int)tex.y);
//                        mesh.vertices[i].weights.resize(vert.numWeights);
//					}
//				}
//
//				if(buffer == "numtris")
//				{
//					file >> mesh.numTriangles;
//					file.IGNORELINE;
//                    mesh.indices.resize(mesh.numTriangles * 3);
//					for(unsigned int i = 0; i < mesh.numTriangles; ++i)
//					{
//                        int index = i * 3;
//						file >> junk >> junk;
//                        file >> mesh.indices[index + 0] >> mesh.indices[index + 1] >> mesh.indices[index + 2];
//						file.IGNORELINE;
//					}
//				}
//
//				else if(buffer == "numweights")
//				{
//					file >> mesh.numWeights;
//					file.IGNORELINE;
//                    for(unsigned int j = 0; j < mesh.vertices.size(); ++j)
//                    {
//                        for(unsigned int i = 0; i < mesh.vertices[j].numWeights; ++i)
//					    {
//                            SkinnedModel::Weight& weight = mesh.vertices[j].weights[i];
//						    file >> junk >> junk;
//						    file >> weight.jointIndex >> weight.bias >> junk;
//						    file >> weight.position.x >> weight.position.y >> weight.position.z >> junk;
//						    file.IGNORELINE;
//					    }
//                    }
//
//				}
//				else
//				{
//					file.IGNORELINE;
//				}
//				file >> buffer;
//			} //while more data in current mesh
//			currMesh++;
//		} //Mesh
//		file >> buffer;
//	} //while !eof
//
//    computeBindPoseVertices(resource);
//    computeBindPoseNormals(resource);
//    computeJointsLocalSpaceNormals(resource);
//
//
//    std::cerr << "RESOURCE LOADED: " << meshes.size() << std::endl;
//    resource->setStatus(Resource::Loaded);
////    return resource;
//}
//
//
//void MD5ModelReader::computeBindPoseVertices(SkinnedModel* resource)
//{
//    //Iterate through all the meshes
//	for(unsigned int m = 0; m < resource->numMeshes(); ++m)
//	{
//		//For each mesh, we iterate through all of the vertices
//        SkinnedModel::Meshes& meshes = resource->getMeshes();
//		for(unsigned int i = 0; i < meshes[m].numVertices; ++i)
//		{
//			//We now want to calculate the final vertex position based
//			//on the weights affecting the joint.
//			Vector3f finalVertex(0.0f);
//
//			//We iterate through all the weights that will affect the final vertex position.
//            for(unsigned int j = 0; j < meshes[m].vertices[i].numWeights; ++j)
//			{
//				//Get weight j which is affecting vertex i on mesh m
//                const SkinnedModel::Weight& weight = meshes[m].vertices[i].weights[j];
//
//				//Joints were picked up from the .md5mesh file. the weight struct has information on 
//				//which joint it affects. We then retrieve this joint from the vector of joints called 
//				//baseSkeleton.
//				const SkinnedModel::Joint& joint = resource->getSkeleton().joints[weight.jointIndex];	
//
//
//				//We now need to apply this weight to our vertex and transform it.
//                Vector3f wv(joint.rotation * weight.position);
//
//				//We now know what the weighted vertex for this weight is. and we can then add this weights
//				//contribution to the final vertex. We must also apply the weight factor (weight.bias).
//				//The sum of all weight factors for a vertex must be equal to 1.0. This is known as vertex skinning.
//				finalVertex += (joint.position + wv) *weight.bias;
//			}
//
//			//Once we have iterated through all the weights, we know position of this vertex in bind-pose.
//			//add this vertex to our position buffer.
//            meshes[m].positions[i] = Vector3f(finalVertex.x, finalVertex.y, -finalVertex.z); //This is just so that we can render the mesh even if we dont have an animation for it.
//		}
//		
//	}
//}
//
//void MD5ModelReader::computeBindPoseNormals(SkinnedModel* resource)
//{
//    //Iterate through all the meshes
//	for(unsigned int m = 0; m < resource->numMeshes(); ++m)
//	{
//		//We now need to calculate the vertex normal for each triangle in the model.
//		//we do this mesh by mesh.
//        SkinnedModel::Meshes& meshes = resource->getMeshes();
//        meshes[m].normals.resize(meshes[m].numVertices);
//		for(unsigned int i = 0; i < meshes[m].numTriangles; ++i)
//		{
//			//to calculate the normal we need to find 3 points. create two vectors,
//			//and then do the cross product between these two. Each mesh has a list of
//			//triangles which contains the index value for the three vertices building the triangle.
//			//That means, we can create three points by using the vertices that are being pointed to by
//			//the index list for each triangle.
//            int index = i * 3;
//            Vector3f p1(meshes[m].positions[meshes[m].indices[index + 0]]);
//			Vector3f p2(meshes[m].positions[meshes[m].indices[index + 1]]);
//			Vector3f p3(meshes[m].positions[meshes[m].indices[index + 2]]);
//
//            //MD5 stores vertices with inverted z values. these values were inverted in the computeBindPoseVertices, but now
//            //need to be inverted back to compute normals.
//            p1.z = -p1.z;
//            p2.z = -p2.z;
//            p3.z = -p3.z;
//
//			//Now that we have three points, we can create two vectors by subtracting two of the points.
//			Vector3f v1(p2 - p1);
//			Vector3f v2(p3 - p1);
//
//			//To find the normal now we need to take the cross product of these two vectors.
//			Vector3f normal(cross(v1,v2));
//            normal = normal * -1.0f;
//
//            meshes[m].normals[meshes[m].indices[index + 0]] += normal;
//			meshes[m].normals[meshes[m].indices[index + 1]] += normal;
//			meshes[m].normals[meshes[m].indices[index + 2]] += normal;
//		}
//
//		//Each vertex of this mesh now has a normal. We take this normal and add it 
//		//to our buffer of normals which will be used for rendering.
//		for(unsigned int i = 0; i < meshes[m].numVertices; ++i)
//		{
//			meshes[m].normals[i] = normalize(meshes[m].normals[i]);
//		}
//	}
//}
//
//void MD5ModelReader::computeJointsLocalSpaceNormals(SkinnedModel* resource)
//{
//    //Iterate through all the meshes
//	for(unsigned int m = 0; m < resource->numMeshes(); ++m)
//	{
//        SkinnedModel::Meshes& meshes = resource->getMeshes();
//		
//        for(unsigned int i = 0; i < meshes[m].numVertices; ++i)
//		{
//			Vector3f finalWeightedNormal(0.0f);
//
//			for(unsigned int j = 0; j < meshes[m].vertices[i].numWeights; ++j)
//			{
//                SkinnedModel::Weight& weight = meshes[m].vertices[i].weights[j];
//				const SkinnedModel::Joint& joint = resource->getSkeleton().joints[weight.jointIndex];
//
//                Vector3f wn(meshes[m].normals[i]); //bind pose normal
//
//				wn = wn * joint.rotation; //compute inverse quaternion rotation.
//
//				weight.normal += wn; //apply this to the weighted normal.
//
//				finalWeightedNormal += wn;
//
//
//			}
//			meshes[m].normals[i] = finalWeightedNormal;
//
//
//		}
//
//		for(unsigned int i = 0; i < meshes[m].numVertices; ++i)
//		{
//			for(unsigned int j = 0; j < meshes[m].vertices[i].numWeights; ++j)
//			{
//                SkinnedModel::Weight& weight = meshes[m].vertices[i].weights[j];
//				weight.normal = normalize(weight.normal);
//			}
//		}
//	}
//}
//
//} 