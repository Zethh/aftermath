#include "Shader.h"
#include <atomic>

namespace am {
class ShaderImpl
{
public:
    std::string _source;
	std::atomic<Shader::Type> _type;
	Shader::Handle _id = nullptr;
};

Shader::Shader()
	: _p(new ShaderImpl())
{
}

Shader::~Shader()
{
	delete _p;
}

void Shader::setSource(const std::string& source)
{
	_p->_source = source;
	AFTERMATH_NOTIFY_OBSERVERS(IShaderObserver, onShaderModified, this);
}

std::string Shader::getSource() const
{
	return _p->_source;
}

void Shader::setType(const Type& type)
{
	_p->_type = type;
}

Shader::Type Shader::getType() const
{
	return _p->_type;
}

void Shader::setId(Handle id) 
{
	_p->_id = id;
}

Shader::Handle Shader::getId() const
{
	return _p->_id;
}




} 