#pragma once

#include <Aftermath/core/Referenced.h>
#include <Aftermath/utils/ResourceReader.h>

namespace am {
	class Resource;
	class SkinnedAnimation;
class MD5AnimationReader: public ResourceReader
{
    AFTERMATH_META(MD5AnimationReader)
public:
	MD5AnimationReader();

	Resource* read(const std::string& fileName, Resource* existingResource = NULL);

protected:
    virtual ~MD5AnimationReader();

private:
    void internalRead(const std::string& fileName, SkinnedAnimation* resource);
};


} 