#include "ShaderProgram.h"

#include <fstream>

namespace am {

ShaderProgram::ShaderProgram()
    : _dirty(false)
	, _id(0xFFFFFFFF)
{
}

ShaderProgram::~ShaderProgram()
{
}

void ShaderProgram::setName(const std::string& name)
{
    _name = name;
}

void ShaderProgram::addShader(Shader* shader)
{
    if(shader)
    {
        _shaderMap[shader->getType()] = shader;
        shader->addObserver(this);
        setDirty();
    }
}

void ShaderProgram::setDirty(bool state)
{
    _dirty = state;
}

bool ShaderProgram::isDirty() const
{
    return _dirty;
}

void ShaderProgram::onShaderModified(Shader* shader)
{
    addShader(shader);
}

} 