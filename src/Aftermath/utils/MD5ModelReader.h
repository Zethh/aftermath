#pragma once

#include <Aftermath/core/Referenced.h>
#include <Aftermath/utils/ResourceReader.h>

namespace am {
	class SkinnedModel;
class MD5ModelReader: public ResourceReader
{
	
    AFTERMATH_META(MD5ModelReader)
public:
	MD5ModelReader();

	Resource* read(const std::string& fileName, Resource* existingResource = NULL);

protected:
    virtual ~MD5ModelReader();

private:
    void internalRead(const std::string& fileName, SkinnedModel* resource);

    void computeBindPoseVertices(SkinnedModel* resource);

	void computeBindPoseNormals(SkinnedModel* resource);

    void computeJointsLocalSpaceNormals(SkinnedModel* resource);
};


} 