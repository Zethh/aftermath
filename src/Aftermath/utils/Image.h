#pragma once

#include <Aftermath/utils/Resource.h>
#include <Aftermath/core/Observer.h>
namespace am {
class Image: public Resource
{
    AFTERMATH_META(Image)
	AFTERMATH_PIMPLE(ImageImpl)
	AFTERMATH_OBSERVABLE(IImageObserver)
public:
	Image();

	virtual ~Image();

	enum Flags
	{
		FLIP_PIXELS_X_AXIS = (1<<0),
		FLIP_PIXELS_Y_AXIS = (1<<1)
	};

    class IImageObserver
	{
        AFTERMATH_META_BASE(Image, IImageObserver)

    public:
		virtual void onImageModified(Image* sender) = 0;
	};

	void setFlags(unsigned int flags);

	unsigned int getFlags() const;

	void setData(unsigned int width, unsigned int height, unsigned int numComponents, unsigned char* pixels);

	unsigned char* getPixels() const;

	unsigned int getWidth() const;

	unsigned int getHeight() const;

	unsigned int getNumComponents() const;

	void flipX();

	void flipY();

private:
	void applyFlags();
};



} 