#include <Aftermath/utils/StringUtils.h>

namespace am {

std::string StringUtils::getFileName(const std::string& path)
{
	size_t pos = path.find_last_of("/");
	std::string str = path.substr(pos+1, path.length()-pos);
	return str;
}

std::string StringUtils::getFileExtension(const std::string& file)
{
	size_t pos = file.find_last_of(".");
	std::string str = file.substr(pos+1, file.length()-pos);
	return str;
}

std::string StringUtils::getDirectory(const std::string& file)
{
	size_t pos = file.find_last_of("/");
	std::string str = file.substr(0, pos+1);
	return str;
}

std::vector<std::string> StringUtils::getLinesContainingWord(const std::string& word, const std::string& text)
{
	std::vector<std::string> lines;
    if(text.length() > 0)
    {
        size_t start = 0, end = 0;
        while(start < text.length())
        {
            start = text.find(word, start);
            if(start < text.length())
            {
                end = text.find("\n", start);
                if(end < text.length())
                {
                    std::string str = text.substr(start, end-start);
                    lines.push_back(str);
                    start = end+1;
                }
            }
        }

//        unsigned int start = 0, end = 0;
//        while(start != text.npos)
//        {
//            start = text.find(word, start);
//            if(start != text.npos)
//            {
//                end = text.find("\n", start);
//                if(end != text.npos)
//                {
//                    std::string str = text.substr(start, end-start);
//                    lines.push_back(str);
//                    start = end+1;
//                }
//            }
//        }
    }

	return lines;
}


hash StringUtils::stringToHash(const std::string& str)
{
	return generateHash((void*)str.c_str(), str.length());
}

hash StringUtils::generateHash(void* key, unsigned int length)
{
	const hash FNV_PRIME_64 = 1099511628211;
	const hash FNV_OFFSET_64 = 14695981039346656037;
	unsigned char* p = static_cast<unsigned char*>(key);
	hash hash = FNV_OFFSET_64;
	if(!p)
		return 0;

	for(unsigned int i = 0; i < length; ++i)
	{
		hash = (hash * FNV_PRIME_64) ^ p[i];
	}

	return hash;

}

std::wstring StringUtils::stringToWString(const std::string& str)
{
    std::wstring t;
	size_t iSize = str.size();
	t.resize(iSize);

	for (size_t i = 0; i < iSize; i++)
		t[i] = (wchar_t)str[i];

	return t;
}

std::string StringUtils::wstringToString(const std::wstring& wstr)
{
    std::string t;
	size_t iSize = wstr.size();
	t.resize(iSize);

	for (size_t i = 0; i < iSize; i++)
		t[i] = (char)wstr[i];

	return t;
}

std::string StringUtils::toForwardSlash(const std::string& str)
{
	static std::string a("\\");
	static std::string b("/");
	std::string res = str;
	std::size_t found = res.find(a);
	while (found != std::string::npos)
	{
		res.replace(found, 1, b);
		found = res.find(a);
	}
	return res;
}




}