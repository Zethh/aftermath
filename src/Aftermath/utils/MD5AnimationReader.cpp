#include "MD5AnimationReader.h"
#include <Aftermath/utils/SkinnedAnimation.h>
#include <Aftermath/utils/SkinnedModel.h>
#include <fstream>
#include <sstream>
#include <Aftermath/math/Quaternion.h>
#include <Aftermath/math/Vector3.h>
#include <Aftermath/utils/StringUtils.h>

#include <functional>
#include <thread>
#include <assert.h>

namespace am {

#define IGNORELINE ignore(std::numeric_limits<std::streamsize>::max(), '\n')

struct BaseFrame
{
    Vector3f position;
    Quaternionf rotation;
};

struct FrameData
{
	int id;
	std::vector<float> data;
};

struct JointInfo
{
	std::string name;
	int parentId;
	int flags;
	int startIndex;
};
namespace animutils
{
    /*
Calculates the w component of a quaternion.
*/
void calcQuatW(Quaternionf& q)
{
	float t = 1.0f - (q.x * q.x) - (q.y * q.y) - (q.z * q.z);
		if(t < 0.0f)
			q.w = 0.0f;
		else
            q.w = -std::sqrt(t);
}



// Removes quotes from a string.
void removeQuotes(std::string& s) 
{
	s.erase(std::remove(s.begin(), s.end(), '\"'), s.end());
}
}

MD5AnimationReader::MD5AnimationReader()
{
    _supportedExtensions.push_back("md5anim");
}

MD5AnimationReader::~MD5AnimationReader()
{
}

Resource* MD5AnimationReader::read(const std::string& fileName, Resource* existingResource)
{
    SkinnedAnimation* resource = dynamic_cast<SkinnedAnimation*>(existingResource);
    if(!resource)
        resource = new SkinnedAnimation();

    if(resource->getStatus() != Resource::Loading)
    {
        resource->setStatus(Resource::Loading);
		std::thread t(std::bind(&MD5AnimationReader::internalRead, this, fileName, resource));
		t.detach();
    }
    return resource;

}

void MD5AnimationReader::internalRead(const std::string& fileName, SkinnedAnimation* resource)
{
    std::string buffer, junk;
    unsigned int uiBuffer;
    float fBuffer;

    std::ifstream filebuff(fileName.c_str());
	assert(filebuff.is_open() && "Unable to open md5anim file");
	std::stringstream file;
	file << filebuff.rdbuf();
	filebuff.close();

    SkinnedAnimation::KeyFrames& keyFrames = resource->getKeyFrames();
    std::vector<BaseFrame> baseFrames;
    std::vector<JointInfo> jointInfos;
    std::vector<FrameData> frameData;
    unsigned int frameRate;
    //float frameDuration;
    float animationDuration;
    float animationTime;
    unsigned int numFrames;
    unsigned int numJoints;
    unsigned int numAnimatedComponents;
	//Animation animation;
	file >> buffer;
	while(!file.eof())
	{
		if(buffer == "MD5Version")
		{
			int version;
			file >> version;
			assert(version == 10 && "MD5anim file not of version 10");
		}
		if(buffer == "commandline")
			file.IGNORELINE;

		else if(buffer == "numFrames")
		{
            file >> numFrames;
			keyFrames.resize(numFrames);
            frameData.resize(numFrames);
			file.IGNORELINE;
		}

		else if(buffer == "numJoints")
		{
			file >> numJoints;
			jointInfos.resize(numJoints);
			baseFrames.resize(numJoints);
			//animation.currentSkeleton.joints.resize(animation.numJoints);
			for(unsigned int i = 0; i < keyFrames.size(); ++i)
				keyFrames[i].joints.resize(numJoints);
			
			file.IGNORELINE;
		}

		else if(buffer == "frameRate")
		{
			file >> frameRate;
			file.IGNORELINE;
		}

		else if(buffer == "numAnimatedComponents")
		{
			file >> numAnimatedComponents;
			for(unsigned int i = 0; i < numFrames; ++i)
			{
				frameData[i].data.resize(numAnimatedComponents);
			}
			file.IGNORELINE;
		}

		else if(buffer == "hierarchy")
		{
			file >> junk;
			for(unsigned int i = 0; i < numJoints; ++i)
			{
				file >> jointInfos[i].name
					 >> jointInfos[i].parentId 
					 >> jointInfos[i].flags
					 >> jointInfos[i].startIndex;
				file.IGNORELINE;
				animutils::removeQuotes(jointInfos[i].name);
			}
			file >> junk;
		}

		else if(buffer == "bounds")
		{
			file >> junk;
			file.IGNORELINE;
			for(unsigned int i = 0; i < numFrames; ++i)
			{
				file >> junk
					 >> junk >> junk >> junk
					 >> junk >> junk
					 >> junk >> junk >> junk;
				file.IGNORELINE;
			}
			file >> junk;
			file.IGNORELINE;

		}

		else if(buffer == "baseframe")
		{
			file >> junk;
			file.IGNORELINE;
			for(unsigned int i = 0; i < numJoints; ++i)
			{
				file >> junk
					 >> baseFrames[i].position.x >> baseFrames[i].position.y >> baseFrames[i].position.z
					 >> junk >> junk
					 >> baseFrames[i].rotation.x >> baseFrames[i].rotation.y >> baseFrames[i].rotation.z;
				file.IGNORELINE;
			}
			file >> junk;
			file.IGNORELINE;
		}

		else if(buffer == "frame")
		{
			for(unsigned int i = 0; i < numFrames; ++i)
			{
				file >> frameData[i].id >> junk;
				file.IGNORELINE;
				for(unsigned int j = 0; j < numAnimatedComponents; ++j)
				{
					file >> frameData[i].data[j];
				}
				file >> junk;
				file.IGNORELINE;
				file >> buffer;
			}
		}
		file >> buffer;
	}




    //frameJoints contains all the joints for a given frame. after we have
	//iterated through all the joints for a frame, then this frame will be 
	//added to our animations set of framejoints (animatedFrameJoints).
    std::vector<SkinnedModel::Joint> frameJoints(numJoints);

	////animatedFrameJoints contains all the frames for a certain animation as
	////well as all the joints for a given frame.
	//anim.skeletons.resize(anim.numFrames);

	//Iterate through all the frames and then calculate the joints for each frame.
    
	for(unsigned int f = 0; f < keyFrames.size(); ++f)
	{
		//Iterate through each joint of frame f.
		for(unsigned int i = 0; i < keyFrames[f].joints.size(); ++i)
		{
			//Create a temporary AnimatedFrameJoint.
			//Populate the temporary AnimatedFrameJoint with the position, orientation and parent id
			//of baseframe joint [i].
            SkinnedModel::Joint joint;
			joint.position = baseFrames[i].position;
			joint.rotation = baseFrames[i].rotation;
			joint.parentId = jointInfos[i].parentId;
			unsigned int j = 0;


			/*
			Each joint has a flag variable, this is of type int. The first three bits of this int( starting from right) is
			the position vector and the next three bits are the rotation quaternion.

			TODO - Im not entirely sure how this bit works. So need to look into it. it's based off the information sheet at:
			http://tfc.duke.free.fr/coding/md5-specs-en.html

			"flags variable description: starting from the right, the frist three bits are for the position vector and the next three for the orientation quaternion.
			If a bit is set, then you have to replace the corresponding (x, y, z) component by a value from the frame's data. Which value? This is given by the startIndex. 
			You begin at the startIndex in the frame's data array and increment the position each time you have to replace a value to a component."
			*/
			if(jointInfos[i].flags & 1) //pos x
				joint.position.x = frameData[f].data[jointInfos[i].startIndex + j++];
			if(jointInfos[i].flags & 2) //pos y
				joint.position.y = frameData[f].data[jointInfos[i].startIndex + j++];
			if(jointInfos[i].flags & 4) //pos z
				joint.position.z = frameData[f].data[jointInfos[i].startIndex + j++];
			if(jointInfos[i].flags & 8) //orient x
				joint.rotation.x = frameData[f].data[jointInfos[i].startIndex + j++];
			if(jointInfos[i].flags & 16) //orient y
				joint.rotation.y = frameData[f].data[jointInfos[i].startIndex + j++];
			if(jointInfos[i].flags & 32) //orient z
				joint.rotation.z = frameData[f].data[jointInfos[i].startIndex + j++];

			//Calculate the w component of the joint orientation.
			animutils::calcQuatW(joint.rotation);

			//If the animated joint has a parent, u must transform the animated joint by its parents
			//orientation quaternion and add the results to the parents position.
			if(joint.parentId >= 0)
			{
				SkinnedModel::Joint& parentJoint = frameJoints[joint.parentId];
                Vector3f rotPos = parentJoint.rotation * joint.position;
				joint.position = parentJoint.position + rotPos;
				joint.rotation = parentJoint.rotation * joint.rotation;
				joint.rotation = normalize(joint.rotation);

			}
			//Add this animated joint to this frame.
			frameJoints[i] = joint;		
		}
		//Once all joints for a frame have been calculated, add the frame to the animations set of frames.
		keyFrames[f].joints = frameJoints;
	}

    //frameRate = 30;
    //float frameDuration = 1.0f/(float)frameRate;
    //resource->setFrameDuration(frameDuration);
    //resource->setFrameRate(frameRate);
    //resource->setAnimationDuration(frameDuration * (float)numFrames);
    resource->setAnimationDuration((float)numFrames / (float)frameRate);

    resource->setStatus(Resource::Loaded);
//    return resource;
}


} 