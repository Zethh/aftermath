#pragma once

#include <Aftermath/utils/ResourceReader.h>
#include <Aftermath/utils/Resource.h>

namespace am {

class ImageReader : public ResourceReader
{
	AFTERMATH_META(ImageReader)

public:
	ImageReader();

	virtual ~ImageReader();

	Resource* read(const std::string& fileName, Resource* existingResource = NULL);
};


} 
