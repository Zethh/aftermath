#include "ShaderReader.h"
#include <fstream>

#include <Aftermath/utils/StringUtils.h>
#include <Aftermath/core/Log.h>
#include <Aftermath/utils/Shader.h>

namespace am {

ShaderReader::ShaderReader()
{
	_supportedExtensions.push_back("vert");
	_supportedExtensions.push_back("verthlsl");
	_supportedExtensions.push_back("vs");
	_supportedExtensions.push_back("vp");

	_supportedExtensions.push_back("frag");
	_supportedExtensions.push_back("fraghlsl");
	_supportedExtensions.push_back("fs");
	_supportedExtensions.push_back("fp");

	_supportedExtensions.push_back("geom");
	_supportedExtensions.push_back("geomhlsl");
	_supportedExtensions.push_back("gs");
	_supportedExtensions.push_back("gp");
}

Resource* ShaderReader::read(const std::string& fileName, Resource* existingResource)
{
	Shader* resource = dynamic_cast<Shader*>(existingResource);
	if(!resource)
	{
		resource = new Shader();
		//resource->setResourceReader(this);
	}
	else
    {
        LOG_WARNING("Using existing resource!%d\n", 0);
    }

	std::ifstream file(fileName.c_str(), std::ios::in | std::ios::binary | std::ios::ate);
	if(file.is_open())
	{
		std::streamoff size = file.tellg();
		char* data = new char[size+1];
		file.seekg(0, std::ios::beg);
		file.read(data, size);
		data[size] = 0;
		std::string s;
		s.assign(data);
		delete [] data;
		resource->setFileName(fileName);
		std::string ext = StringUtils::getFileExtension(fileName);


		if (ext.compare("vert") == 0 || ext.compare("verthlsl") == 0 || ext.compare("vs") == 0 || ext.compare("vp") == 0)
            resource->setType(Shader::Vertex);
		else if (ext.compare("frag") == 0 || ext.compare("fraghlsl") == 0 || ext.compare("fs") == 0 || ext.compare("fp") == 0)
			resource->setType(Shader::Fragment);
		else if (ext.compare("geom") == 0 || ext.compare("geomhlsl") == 0 ||ext.compare("gs") == 0 || ext.compare("gp") == 0)
			resource->setType(Shader::Geometry);

		//resource->setResourceReader(this);
		resource->setSource(s);
	}	
	return resource;
}

} 