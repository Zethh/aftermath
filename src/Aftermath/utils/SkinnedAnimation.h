#pragma once

#include <Aftermath/core/Referenced.h>
#include <Aftermath/utils/Resource.h>
#include <Aftermath/math/Quaternion.h>
#include <Aftermath/math/Vector3.h>
#include <vector>
#include <Aftermath/render/Buffer.h>

#include <Aftermath/utils/SkinnedModel.h>
namespace am {

    class SkinnedAnimation: public Resource
    {
        AFTERMATH_META(SkinnedAnimation)
    public:
        SkinnedAnimation() { }

        //void setFrameDuration(float duration) { _frameDuration = duration; }

        void setAnimationDuration(float duration) { _animationDuration = duration; }

        //float getFrameDuration() const { return _frameDuration;}

        float getAnimationDuration() const { return _animationDuration; }

        /*void setFrameRate(unsigned int fps) { _frameRate = fps; }

        unsigned int getFrameRate() const { return _frameRate; }*/

        typedef std::vector<SkinnedModel::Skeleton> KeyFrames;

        void addKeyFrame(SkinnedModel::Skeleton keyFrame) { _keyFrames.push_back(keyFrame); }
        
        KeyFrames& getKeyFrames() { return _keyFrames; }

    protected:
        ~SkinnedAnimation() { }

    private:
        float _frameDuration;
        float _animationDuration;
        float _animationTime;
        unsigned int _numFrames;
        unsigned int _numJoints;
        unsigned int _frameRate;
        unsigned int _numAnimatedComponents;
        KeyFrames _keyFrames;
        



    };


}