#pragma once
#include <string>
#include <vector>
#include <algorithm>

#include <Aftermath/core/ResourceManager.h>
#include <Aftermath/core/Referenced.h>



namespace am {
	class Resource;
class ResourceReader: public Referenced
{
    AFTERMATH_META(ResourceReader)
public:
	typedef std::vector<std::string> SupportedExtensions;

	ResourceReader() { }

	virtual ~ResourceReader() { }

	virtual Resource* read(const std::string& fileName, Resource* existingResource = nullptr) = 0;

	virtual bool isSupportedExtension(const std::string& extension)
	{
		SupportedExtensions::iterator itr = std::find(_supportedExtensions.begin(), _supportedExtensions.end(), extension);
		
		if(itr != _supportedExtensions.end())
			return true;

		return false;
	}

protected:
	SupportedExtensions _supportedExtensions;
};

}