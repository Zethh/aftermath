#pragma once

#include <Aftermath/core/Referenced.h>

namespace am {

	class ResourceReader;

class Resource: public Referenced
{
    AFTERMATH_META(Resource)
	AFTERMATH_PIMPLE(ResourceImpl)
public:
	Resource();

    enum Status
    {
        Loading = 0,
        Loaded,
        Unloading,
        Unloaded
    };

    void setStatus(Status s);

    Status getStatus() const;

	void setFileName(const std::string& filename);

	const std::string& getFileName() const;

	void setResourceReader(ResourceReader* reader);

	bool reload();

protected:
	virtual ~Resource();
};


} 