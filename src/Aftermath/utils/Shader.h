#pragma once

#include <Aftermath/core/Referenced.h>
#include <Aftermath/utils/Resource.h>
#include <Aftermath/core/Observer.h>

namespace am {

class Shader: public Resource
{
    AFTERMATH_META(Shader)
	AFTERMATH_PIMPLE(ShaderImpl)
	AFTERMATH_OBSERVABLE(IShaderObserver)
public:
	Shader();

	virtual ~Shader();

	enum Type
	{
		Vertex,
		Fragment,
		Geometry
	};

    class IShaderObserver
	{
		AFTERMATH_META(Shader)
	public:
		virtual void onShaderModified(Shader* sender) = 0;
	};

	void setSource(const std::string& source);

	std::string getSource() const;

	void setType(const Type& type);

	Type getType() const;

	typedef void* Handle;

	void setId(Handle id);

	Handle getId() const;

};



} 