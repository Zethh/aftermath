#pragma once

#include <Aftermath/core/Referenced.h>
#include <Aftermath/utils/ResourceReader.h>
#include <Aftermath/render/Effect.h>
#include <Aftermath/xml/XMLNode.h>

namespace am {

class EffectReader: public ResourceReader
{
    AFTERMATH_META(EffectReader)
		
public:
	EffectReader();

	virtual ~EffectReader();

	void setGroupId(const std::string& groupId);

	Resource* read(const std::string& filename, Resource* existingResosource = NULL);

	void parseGroup(const XMLNode& node, Effect* effect);

	void parseTechnique(const XMLNode& node, Effect::Technique* technique);

private:
	std::string _groupId;
};


} 