#pragma once

#include <Aftermath/core/Referenced.h>
#include <Aftermath/utils/Resource.h>
#include <Aftermath/math/Quaternion.h>
#include <Aftermath/math/Vector3.h>
#include <vector>
#include <Aftermath/render/Buffer.h>

namespace am {

    class SkinnedModel: public Resource
    {
        AFTERMATH_META(SkinnedModel)
    public:
        SkinnedModel() { }

        struct Joint
        {
            std::string name;
            int parentId;
            Vector3f position;
            Quaternionf rotation;
        };

        struct Skeleton
        {
            std::vector<Joint> joints;
        };

        struct Weight
        {
            Weight()
            : position(0.0f)
            , normal(0.0f)
            {
            }
	        int jointIndex;
	        float bias;
	        Vector3f position;
	        Vector3f normal;
        };

        struct Vertex
        {
	        unsigned int numWeights;
            std::vector<Weight> weights;
        };

        struct Mesh
        {
	        unsigned int numVertices;
	        unsigned int numTriangles;
	        unsigned int numWeights;
	        std::vector<Vertex> vertices;

            DynamicFloat3 positions;
            DynamicFloat3 normals;
            StaticFloat2 texCoords;
            StaticUInt indices;
            
        };

        typedef std::vector<SkinnedModel::Mesh> Meshes;

        Skeleton& getSkeleton() { return _skeleton; }

        Meshes& getMeshes() { return _meshes; }

        size_t numMeshes() const { return _meshes.size(); }

        size_t numJoints() const { return _skeleton.joints.size(); }

        void addMesh(Mesh m) { _meshes.push_back(m);}

        void addJoint(Joint j) { _skeleton.joints.push_back(j); }

    protected:
        ~SkinnedModel() { }

        

        Skeleton _skeleton;
        Meshes _meshes;

    };


}