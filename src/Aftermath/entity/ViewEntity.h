#pragma once
#include <Aftermath/entity/SpatialEntity.h>
namespace am
{
	class ViewEntity: public SpatialEntity
	{
		AFTERMATH_META(ViewEntity)
	public:
		__forceinline void setProjectionMatrix(const Matrix4f& matrix) { _projection = matrix; }
		__forceinline Matrix4f getViewMatrix() const { return inverse(getTransform().getWorldMatrix()); }
		__forceinline Matrix4f getProjectionMatrix() const { return _projection; }
		__forceinline void setRenderTarget(IRenderTarget* rt) { _renderTarget = rt; }
		__forceinline IRenderTarget* getRenderTarget() { return _renderTarget; }
		__forceinline const IRenderTarget* getRenderTarget() const { return _renderTarget; }
		__forceinline void setRenderTechnique(const std::string& technique) { _technique = technique; }
		__forceinline const std::string& getRenderTechnique() const  { return _technique; }
	private:
		Matrix4f _projection;
		IRenderTarget::Ptr _renderTarget = nullptr;
		std::string _technique = "Undefined";
	};
}