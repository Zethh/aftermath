#pragma once
#include <Aftermath/entity/Entity.h>
#include <Aftermath/math/Transform.h>
#include <Aftermath/core/Defines.h>
#include <Aftermath/entity/Blueprint.h>
namespace am
{
	enum SpatialFlags
	{
		SpatialFlag_Renderable = 1 << 0
	};

	class SpatialEntity : public Entity
	{
		AFTERMATH_META(SpatialEntity)
	public:
		__forceinline void setTransform(const Transform& transform) { _transform = transform; }
		__forceinline void setSpatialFlags(u32 flags) { _flags = flags; }

		__forceinline const Transform& getTransform() const { return _transform; }
		__forceinline Transform& getTransform() { return _transform; }
		__forceinline Blueprint& getBlueprint() { return _blueprint; }
		__forceinline const Blueprint& getBlueprint() const { return _blueprint; }

		__forceinline bool isRenderable() const { return ((_flags & SpatialFlag_Renderable) == SpatialFlag_Renderable); }

	private:
		Transform _transform;
		Blueprint _blueprint;
		u32 _flags = SpatialFlag_Renderable;
	};
}