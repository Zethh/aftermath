#pragma once
#include <Aftermath/core/Referenced.h>
namespace am
{
	typedef unsigned int EntityId;

	class Entity : public Referenced
	{
		AFTERMATH_META(Entity)
	public:
		__forceinline void setEntityId(EntityId id) { _id = id; }

		__forceinline EntityId getEntityId() const { return _id; }

		__forceinline void setDebugName(const char* name) { _debugName = name; }

		__forceinline const char* getDebugName() const { return _debugName; }

	private:
		EntityId _id = ~0u;
		const char* _debugName = nullptr;

	};
}