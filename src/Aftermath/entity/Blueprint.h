#pragma once
#include <Aftermath/render/Renderer.h>
#include <Aftermath/render/Mesh.h>
#include <Aftermath/render/Effect.h>
#include <Aftermath/render/Uniform.h>
namespace am
{
	class Blueprint
	{
	public:
		typedef std::vector<Mesh> Meshes;
		typedef std::vector<ITexture::Ptr> Textures;

		__forceinline void addMesh(Mesh&& mesh) { _meshes.push_back(mesh); }
		__forceinline void addTexture(ITexture* texture) { _textures.push_back(texture); }
		__forceinline void setEffect(Effect* effect) { _effect = effect; }

		__forceinline Meshes& getMeshes() { return _meshes; }
		__forceinline const Meshes& getMeshes() const { return _meshes; }
		__forceinline Textures& getTextures() { return _textures; }
		__forceinline const Textures& getTextures() const { return _textures; }
		__forceinline UniformBuffer& getUniforms() { return _uniforms; }
		__forceinline const UniformBuffer& getUniforms() const { return _uniforms; }
		__forceinline const ShaderProgram* getShaderProgram(const char* technique) const { return _effect->getTechnique(technique)->getProgram(); }
		__forceinline ShaderProgram* getShaderProgram(const char* technique) { return _effect->getTechnique(technique)->getProgram(); }

	private:
		Meshes _meshes;
		Textures _textures;
		Effect::Ptr _effect;
		UniformBuffer _uniforms;
	};
}