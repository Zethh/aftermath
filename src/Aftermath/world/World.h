#pragma once
#include <Aftermath/core/Referenced.h>
#include <Aftermath/entity/Entity.h>
#include <Aftermath/entity/ViewEntity.h>
#include <Aftermath/world/VisualEnvironment.h>
#include <Aftermath/math/Frustum.h>
#include <vector>

namespace am
{
	class World : public Referenced
	{
		AFTERMATH_META(World)
	public:
		World();
		typedef std::vector<SpatialEntity::Ptr> SpatialEntities;
		typedef std::vector<ViewEntity::Ptr> ViewEntities;

		void addEntity(SpatialEntity* entity);
		void addEntity(ViewEntity* entity);

		SpatialEntities cull(const Frustum& frustum, u32 flags = 0);

		__forceinline ViewEntities& getViewEntities() { return _viewEntities; }
		__forceinline const ViewEntities& getViewEntities() const { return _viewEntities; }
		__forceinline VisualEnvironment& getVisualEnvironment() { return _visualEnvironment; }
		__forceinline const VisualEnvironment& getVisualEnvironment() const { return _visualEnvironment; }
	private:
		SpatialEntities _spatialEntities;
		ViewEntities _viewEntities;
		VisualEnvironment _visualEnvironment;


	};
}