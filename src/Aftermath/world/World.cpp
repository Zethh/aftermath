#include "World.h"

namespace am
{
	World::World()
	{

	}

	void World::addEntity(SpatialEntity* entity)
	{
		_spatialEntities.push_back(entity);
	}

	void World::addEntity(ViewEntity* entity)
	{
		_viewEntities.push_back(entity);
		_spatialEntities.push_back(entity);
	}

	World::SpatialEntities World::cull(const Frustum& frustum, u32 flags)
	{
		SpatialEntities result;
		for (auto& e : _spatialEntities)
		{
			if (e->isRenderable())
				result.push_back(e);
		}
		return std::move(result);
	}

}