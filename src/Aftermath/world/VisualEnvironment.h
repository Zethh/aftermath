#pragma once
#include <Aftermath/core/Referenced.h>
#include <Aftermath/math/Vector3.h>
namespace am
{
	class VisualEnvironment
	{
	public:
		__forceinline void setSkyColor(const am::Vector3f& color) { _skyColor = color; }
		__forceinline const am::Vector3f& getSkyColor() const { return _skyColor; }
		__forceinline void setSunDirection(const am::Vector3f& direction) { _sunDirection = direction; }
		__forceinline void setSunColor(const am::Vector3f& color) { _sunColor = color; }
		__forceinline const am::Vector3f& getSunDirection() const { return _sunDirection; }
		__forceinline const am::Vector3f& getSunColor() const { return _sunColor; }

		void update(float dt);

	private:
		am::Vector3f _skyColor = am::Vector3f(0.2f, 0.2f, 0.3f);
		am::Vector3f _sunDirection = am::Vector3f(1.0f, 0.0f, 0.0f);
		am::Vector3f _sunColor = am::Vector3(1.0f, 0.5f, 0.2f);
		bool _sunEnabled;
	};
}