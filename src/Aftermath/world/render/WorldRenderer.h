#pragma once
//#include <Aftermath/world/World.h>
#include <Aftermath/core/Referenced.h>
#include <Aftermath/math/Matrix4.h>
#include <Aftermath/math/Vector3.h>
#include <Aftermath/render/Renderer.h>
#include <Aftermath/render/Uniform.h>
namespace am
{
	class World;
	class ViewEntity;
	class WorldRenderer: public Referenced
	{
		AFTERMATH_META(WorldRenderer)
	public:
		WorldRenderer() {}

		ITexture* render(World* world, ViewEntity* viewEntity);

	private:
		struct NormalMappingDesc
		{
			am::Matrix4f view;
			am::Matrix4f projection;
			am::Vector3f lightDirection;
			am::Vector3f lightColor;
			am::ITexture* diffuse;
			am::ITexture* normal;
			am::ITexture* specular;
			am::ITexture* depth;
			am::ITexture* result;
		};

		void renderNormalMapping(NormalMappingDesc& desc);
	};
}