#include "WorldRenderer.h"
#include <Aftermath/math/Frustum.h>
#include <Aftermath/render/Window.h>
#include <Aftermath/core/FileSystem.h>
#include <Aftermath/entity/ViewEntity.h>
#include <Aftermath/world/World.h>
#include <sstream>
namespace am
{
	ITexture* WorldRenderer::render(World* world, ViewEntity* viewEntity)
	{
		WorldRenderer::NormalMappingDesc _nmDesc;
		_nmDesc.view = viewEntity->getViewMatrix();
		_nmDesc.projection = viewEntity->getProjectionMatrix();
		_nmDesc.lightDirection = world->getVisualEnvironment().getSunDirection();
		_nmDesc.lightColor = world->getVisualEnvironment().getSunColor();
		_nmDesc.diffuse = viewEntity->getRenderTarget()->getTarget(am::RenderTargetSlot_Color0);
		_nmDesc.normal = viewEntity->getRenderTarget()->getTarget(am::RenderTargetSlot_Color1);
		_nmDesc.specular = viewEntity->getRenderTarget()->getTarget(am::RenderTargetSlot_Color2);
		_nmDesc.depth = viewEntity->getRenderTarget()->getTarget(am::RenderTargetSlot_Color3);
		renderNormalMapping(_nmDesc);

		return _nmDesc.result;
	}

	void WorldRenderer::renderNormalMapping(NormalMappingDesc& desc)
	{
		static am::Mesh fullScreenQuad = am::Mesh::createFullScreenQuad();
		static am::Effect* effect = am::ResourceManager::load<am::Effect>(am::FileSystem::getFilePath("effects/DirectionalLightNormalMapping/DirectionalLightNormalMapping.fx"));
		static am::UniformBuffer uniforms;
		static const char* technique = "DirectionalLightNormalMapping";
		static am::ShaderProgram* program = effect->getTechnique(technique)->getProgram();

		am::TFrustum<float> frustum = am::createFrustum(desc.projection, am::Matrix4f(1.0f));
		am::Vector3f points[8];
		frustum.getFrustumCorners(points);
		am::Matrix4f farClip(am::Vector4f(points[4], 0.0),
			am::Vector4f(points[5], 0.0),
			am::Vector4f(points[6], 0.0),
			am::Vector4f(points[7], 0.0));


		uniforms.set("u_lightDirection", desc.lightDirection);
		uniforms.set("u_lightColor", desc.lightColor);
		uniforms.set("u_cameraView", desc.view);
		uniforms.set("u_gBufferColor", desc.diffuse);
		uniforms.set("u_gBufferNormal", desc.normal);
		uniforms.set("u_gBufferDepth", desc.depth);
		uniforms.set("u_gBufferSpecular", desc.specular);
		uniforms.set("u_frustumCorners", farClip);


		int width = desc.diffuse->getWidth();
		int height = desc.diffuse->getHeight();
		am::TextureDesc tDesc = desc.diffuse->getDesc();
		desc.result = g_renderer->createTexture(tDesc);

		am::RenderTargetDesc rtDesc;
		rtDesc.width = width;
		rtDesc.height = height;
		rtDesc.useHardwareDepth = true;
		rtDesc.slots[am::RenderTargetSlot_Color0] = desc.result;
		am::IRenderTarget::Ptr renderTarget = g_renderer->createRenderTarget(rtDesc);

		g_renderer->setRenderTarget(renderTarget);
		g_renderer->setCullFace(am::CullFace_Back);
		g_renderer->setViewport({ 0, 0, width, height });
		g_renderer->setClearFlag(am::ClearFlag_All, { 0.0f, 0.0f, 0.0f, 1.0f }, 1.0f, 0.0f);
		g_renderer->setShaderProgram(program);
		g_renderer->setUniforms(&uniforms);
		g_renderer->renderMesh(&fullScreenQuad);
		g_renderer->setUniforms(nullptr);
		g_renderer->setShaderProgram(nullptr);
		g_renderer->setRenderTarget(nullptr);
	}
}