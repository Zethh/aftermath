#pragma once

#include <Aftermath/core/Referenced.h>
#include <Aftermath/world/World.h>
#include <Aftermath/render/Window.h>
namespace am
{
	class Game: public Referenced
	{
		AFTERMATH_META(Game)
	public:
		void initialise();

		void shutdown();

		void run();

		__forceinline World* getWorld() { return _world; }

		__forceinline const World* getWorld() const { return _world; }

	private:
		void update();

		void render();

		virtual void onInitialise() = 0;

		virtual void onShutdown() = 0;

		virtual void onUpdate() = 0;

	private:
		World::Ptr _world = nullptr;
		bool _isRunning = false;
	};
}