#include "OpaqueRenderer.h"
#include <Aftermath/world/World.h>
#include <Aftermath/entity/ViewEntity.h>
#include <sstream>
namespace am
{
	void OpaqueRenderer::render(World* world, ViewEntity* viewEntity)
	{
		auto viewFrustum = createFrustum(viewEntity->getProjectionMatrix(), viewEntity->getViewMatrix());
		World::SpatialEntities visibleEntities = world->cull(viewFrustum, SpatialFlag_Renderable);

		// Sort entities on distance to camera
		World::SpatialEntities sortedEntities;
		sortedEntities.reserve(visibleEntities.size());
		for (auto e : visibleEntities)
		{
			sortedEntities.push_back(e);
		}

		for (auto e : sortedEntities)
		{
			auto& transform = e->getTransform();
			auto& g = e->getBlueprint();
			auto& gu = g.getUniforms();
			gu.set("modelview", viewEntity->getViewMatrix() * transform.getLocalMatrix());
			gu.set("model", transform.getLocalMatrix());
			gu.set("view", viewEntity->getViewMatrix());
			gu.set("projection", viewEntity->getProjectionMatrix());
			gu.set("transposeInvModelView", am::transpose(am::inverse(viewEntity->getViewMatrix() * transform.getLocalMatrix())));
			auto& textures = g.getTextures();

			for (size_t i = 0; i < textures.size(); ++i)
			{
				std::stringstream ss;
				ss << "textureUnit" << i;
				gu.set(ss.str(), textures[i]);
			}

			auto* sp = g.getShaderProgram("GBuffer");
			g_renderer->setShaderProgram(sp);
			g_renderer->setUniforms(&gu);
			auto& meshes = g.getMeshes();
			for (auto& m : meshes)
			{
				g_renderer->renderMesh(&m);
			}
			g_renderer->setUniforms(nullptr);
			g_renderer->setShaderProgram(nullptr);
		}
	}
}