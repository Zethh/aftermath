#include "GameRenderer.h"
#include <Aftermath/render/Renderer.h>
#include <algorithm>
#include <assert.h>
#ifdef AM_DX11
#include <Aftermath/render/DX11/DxRenderer.h>
#endif

#ifdef AM_GL
#include <Aftermath/render/OpenGL/GlRenderer.h>
#endif

#include <Aftermath/world/World.h>
#include <Aftermath/render/Window.h>
#include <Aftermath/core/FileSystem.h>


namespace am
{
	IRenderer* g_renderer;
	GameRenderer* g_gameRenderer;
	GameRenderer::GameRenderer()
	{
		_opaqueRenderer = new OpaqueRenderer();
		_worldRenderer = new WorldRenderer();
	}

	GameRenderer::~GameRenderer()
	{
		_opaqueRenderer = nullptr;
		_worldRenderer = nullptr;
	}

	void GameRenderer::initialise()
	{
		assert(g_renderer == nullptr);
		
#ifdef AM_DX11
		if (g_renderer == nullptr)
		{
			g_renderer = new DxRenderer();
		}
#endif

#ifdef AM_GL
		if (g_renderer == nullptr)
		{
			g_renderer = new GlRenderer();
		}
#endif

		assert(g_renderer != nullptr);
	}

	void GameRenderer::shutdown()
	{
		delete g_renderer;
		g_renderer = nullptr;
	}

	void GameRenderer::render(World* world)
	{
		g_renderer->startFrame();
		
		g_renderer->setRenderTarget(nullptr);
		g_renderer->setClearFlag(ClearFlag_All, { 1.2f, 0.2f, 0.2f, 1.0f }, 1.0f, 0.0f);
		g_renderer->setCullFace(am::CullFace_Back);

		const World::ViewEntities& viewEntities = world->getViewEntities();
		for (auto& viewEntity : viewEntities)
		{
			assert(viewEntity->getRenderTarget() != nullptr);
			auto& desc = g_renderer->getWindow()->getDesc();
			g_renderer->setViewport({ 0, 0, desc.width, desc.height });
			g_renderer->setRenderTarget(viewEntity->getRenderTarget());
			g_renderer->setClearFlag(ClearFlag_All, { 1.2f, 0.2f, 0.2f, 1.0f }, 1.0f, 0.0f);

			_opaqueRenderer->render(world, viewEntity);
			ITexture::Ptr finalTexture = _worldRenderer->render(world, viewEntity);
			presentToFramebuffer(finalTexture, 0, 0, desc.width, desc.height);
		}

		
		
		g_renderer->endFrame();
	}

	void GameRenderer::presentToFramebuffer(ITexture* texture, int x, int y, int width, int height)
	{
		g_renderer->setRenderTarget(nullptr);
		g_renderer->setCullFace(am::CullFace_Back);

		static am::Mesh fullScreenQuad = am::Mesh::createFullScreenQuad();
		static am::Effect* effect = am::ResourceManager::load<am::Effect>(am::FileSystem::getFilePath("effects/FullScreenQuad/FullScreenQuad.fx"));
		am::ShaderProgram* sp = effect->getTechnique("GBuffer")->getProgram();
		auto& desc = g_renderer->getWindow()->getDesc();
		g_renderer->setViewport({ 0, 0, desc.width, desc.height });
		am::UniformBuffer fbuniforms;
		fbuniforms.set("textureUnit0", texture);
		g_renderer->setShaderProgram(sp);
		g_renderer->setUniforms(&fbuniforms);
		g_renderer->renderMesh(&fullScreenQuad);
		g_renderer->setUniforms(nullptr);
		g_renderer->setShaderProgram(nullptr);
	}

}