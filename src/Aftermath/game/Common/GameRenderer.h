#pragma once

#include <Aftermath/core/Referenced.h>
#include <Aftermath/game/Common/OpaqueRenderer.h>
#include <Aftermath/world/render/WorldRenderer.h>
#include <vector>
namespace am
{
	class GameRenderer: public Referenced
	{
		AFTERMATH_META(GameRenderer)
	public:
		GameRenderer();

		void initialise();

		void shutdown();

		void render(World* world);

	protected:
		virtual ~GameRenderer();

	private:
		void presentToFramebuffer(ITexture* texture, int x, int y, int width, int height);
	private:
		OpaqueRenderer::Ptr _opaqueRenderer;
		WorldRenderer::Ptr _worldRenderer;
	};

	extern GameRenderer* g_gameRenderer;
}
