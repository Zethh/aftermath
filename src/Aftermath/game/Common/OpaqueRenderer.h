#pragma once
#include <Aftermath/core/Referenced.h>
namespace am
{
	class World;
	class ViewEntity;
	class OpaqueRenderer : public Referenced
	{
		AFTERMATH_META(OpaqueRenderer)
	public:
		void render(World* world, ViewEntity* viewEntity);
	};
}