#include "Game.h"
#include <Aftermath/render/Renderer.h>
#include <assert.h>
#include <Aftermath/core/ResourceManager.h>
#include <Aftermath/game/Common/GameRenderer.h>
namespace am
{
	void Game::initialise()
	{
		_world = new World();
		g_gameRenderer = new GameRenderer();
		g_gameRenderer->initialise();
		onInitialise();
	}

	void Game::shutdown()
	{
		onShutdown();
	}

	void Game::run()
	{
		_isRunning = true;
		while (_isRunning)
		{
			auto events = am::Window::pollEvents();
			for (auto e : events)
			{
				switch (e.type)
				{
				case WindowEvent::Event_Close:
					_isRunning = false;
					break;
				}
			}

			update();
			render();
		}
	}

	void Game::update()
	{
		ResourceManager::update();
		onUpdate();
	}

	void Game::render()
	{
		g_gameRenderer->render(_world);
	}

}
