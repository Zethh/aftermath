project "Engine"
	kind "StaticLib"
	language "C++"

	files {"**.h", "**.cpp", "**.c"}

	configuration "Debug"
        defines { "DEBUG" }
        flags { "Symbols" }
 
    configuration "Release"
        defines { "NDEBUG" }
        flags { "Optimize" }  
