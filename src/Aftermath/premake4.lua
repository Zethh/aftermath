project "Engine"
	kind "StaticLib"
	language "C++"

	files {"**.h", "**.cpp"}

	configuration "Debug"
        defines { "DEBUG" }
        flags { "Symbols" }
 
    configuration "Release"
        defines { "NDEBUG" }
        flags { "Optimize" }  
