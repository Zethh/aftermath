#pragma once
#include <Aftermath/core/Referenced.h>
#include <string>

namespace am {

class FolderWatcher: public Referenced
{
    AFTERMATH_META(FolderWatcher)
	AFTERMATH_PIMPLE(FolderWatcherImpl)
public:

	FolderWatcher(const std::string& path);

	virtual ~FolderWatcher();

	bool popModifiedFile(std::string& file);

	const std::string& getFolderPath() const;
};

}