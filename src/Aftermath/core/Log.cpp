#include "Log.h"
#include <iostream>
#ifdef AM_PLATFORM_WINDOWS
#include <Windows.h>
#endif

#include <mutex>	
namespace am {

class LogPrivate
{
public:
    LogPrivate()
	{
#if defined AM_PLATFORM_WINDOWS
        _index = 0;
		_handleStdin = GetStdHandle(STD_INPUT_HANDLE);
		_handleStdout = GetStdHandle(STD_OUTPUT_HANDLE);
		GetConsoleScreenBufferInfo(_handleStdout, &_rootCsbi);
#endif
	}

	void bindOutputAttribute(unsigned int flag)
	{
#if defined AM_PLATFORM_WINDOWS
		SetConsoleTextAttribute( _handleStdout, flag);
		#endif
	}

	void unbindOutputAttributes()
	{
#if defined AM_PLATFORM_WINDOWS
		FlushConsoleInputBuffer(_handleStdin);
		SetConsoleTextAttribute(_handleStdout, _rootCsbi.wAttributes);
		#endif
	}
#if defined AM_PLATFORM_WINDOWS
	void* _handleStdin;
	void* _handleStdout;
	unsigned short _index;
	CONSOLE_SCREEN_BUFFER_INFO _rootCsbi;
	std::mutex _printMutex;
#endif
};

Log::Log():
	_p(new LogPrivate())
{
}

Log::~Log()
{
	delete _p;
}

void Log::print(unsigned int flag, const char* format, ...)
{
#if defined AM_PLATFORM_WINDOWS
	va_list arg;
	int done;
	_crt_va_start(arg, format);
	_p->bindOutputAttribute(flag);
	done = vfprintf(stdout, format, arg);
	_p->unbindOutputAttributes();
	_crt_va_end(arg);

#endif

}

void Log::print(const char* format, ...)
{
#if defined AM_PLATFORM_WINDOWS
	va_list arg;
	int done;
	_crt_va_start(arg, format);
	done = vfprintf(stdout, format, arg);
	_crt_va_end(arg);

#endif

}

void Log::sPrint(unsigned int flag, const char* format, ...)
{
#if defined AM_PLATFORM_WINDOWS
	HANDLE _handleStdin = GetStdHandle(STD_INPUT_HANDLE);
	HANDLE _handleStdout = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_SCREEN_BUFFER_INFO _rootCsbi;

	GetConsoleScreenBufferInfo(_handleStdout, &_rootCsbi);
	SetConsoleTextAttribute( _handleStdout, flag);
	va_list arg;
	_crt_va_start(arg, format);
	vfprintf(stdout, format, arg);
	_crt_va_end(arg);

	FlushConsoleInputBuffer(_handleStdin);
	SetConsoleTextAttribute(_handleStdout, _rootCsbi.wAttributes);
#endif
}

void Log::sPrint(const char* format, ...)
{
#if defined AM_PLATFORM_WINDOWS
	static std::mutex printMutex;
	printMutex.lock();
	HANDLE _handleStdin = GetStdHandle(STD_INPUT_HANDLE);
	HANDLE _handleStdout = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_SCREEN_BUFFER_INFO _rootCsbi = {};

	va_list arg;
	_crt_va_start(arg, format);
	vfprintf(stdout, format, arg);
	_crt_va_end(arg);

	FlushConsoleInputBuffer(_handleStdin);
	SetConsoleTextAttribute(_handleStdout, _rootCsbi.wAttributes);

	printMutex.unlock();
#endif
}

}