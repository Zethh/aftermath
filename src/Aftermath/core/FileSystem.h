#pragma once

#include <string>
#include <Aftermath/core/Observer.h>
#include <Aftermath/core/FolderWatcher.h>

namespace am {

class FileSystem
{
public:
	static bool mountDirectory(const std::string& directory, bool watchDirectory = false);

	static bool unmountDirectory(const std::string& directory);

	static void unmountDirectories();

	static bool isMounted(const std::string& directory);

	static std::string getFilePath(const std::string& relativePath, bool *success = NULL);

	struct FileDesc
	{
		std::string folderpath;
		std::string filename;
	};

	static std::vector<FileDesc> getModiedFiles();
};



} 