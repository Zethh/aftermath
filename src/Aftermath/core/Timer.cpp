#include "Timer.h"

#if defined AM_PLATFORM_WINDOWS
#include <Windows.h>
#endif
#include <iostream>

namespace am {

Timer::Timer():
    _prevTime(0),
    _currTime(0),
    _deltaTime(-1.0),
    _secondsPerCount(0.0),
	_averageFps(0.0),
    _fpsCalculationInterval(0.1),
    _sumDeltaTime(0.0),
    _frameCount(0)
	
{
#if defined AM_PLATFORM_WINDOWS
      QueryPerformanceCounter((LARGE_INTEGER*)&_counterOffset);
      QueryPerformanceFrequency((LARGE_INTEGER*)&_counterFrequency); //get counts per sec
      _secondsPerCount = 1.0 / (double)_counterFrequency;
#endif
}

void Timer::tick()
{
#if defined AM_PLATFORM_WINDOWS
    QueryPerformanceCounter((LARGE_INTEGER*)&_currTime);
    _currTime -= _counterOffset;
	_deltaTime = (_currTime - _prevTime) / (double)_counterFrequency;
    _prevTime = _currTime;
	++_frameCount;
	_sumDeltaTime += _deltaTime;

	if(_sumDeltaTime >= _fpsCalculationInterval)
	{
		_averageFps = (1.0 / _fpsCalculationInterval) * (double)_frameCount;
		_sumDeltaTime -= _fpsCalculationInterval;
		_frameCount = 0;
		//std::cout << "Fps: " << _averageFps << std::endl;
	}
#endif

}


} 