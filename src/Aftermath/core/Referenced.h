#pragma once

#include <iostream>
#include <Aftermath/core/Meta.h>
namespace am {

	class Referenced
	{
        AFTERMATH_META_BASE(am, Referenced)

        public:
			Referenced(): _refCount(0) { }

			inline Referenced& operator= (const Referenced&)
			{
				return *this; 
			}

			inline int ref() const
			{
				return ++_refCount;
			}

			inline void unref() const
			{
				--_refCount;
				if (_refCount <= 0)
				{
					delete this;
				}
			}

			inline int referenceCount() const
			{
				return _refCount; 
			}
		protected:

			virtual ~Referenced() {  }

			mutable int _refCount;


	};



	template <typename T>
	class ref_ptr
	{
	public:
		typedef T Type;

		ref_ptr() : _ptr(0) { }

		ref_ptr(T *ptr) : _ptr(ptr) { if (_ptr) _ptr->ref(); }

		ref_ptr(const ref_ptr &rp) : _ptr(rp._ptr) { if (_ptr) _ptr->ref(); }

		~ref_ptr() { if (_ptr) _ptr->unref();  _ptr = 0; }

		ref_ptr &operator = (const ref_ptr &rp)
		{
			assign(rp);
			return *this;
		}

		template <class T2>
		ref_ptr &operator = (const ref_ptr<T2> &rp)
		{
			assign(rp);
			return *this;
		}

		ref_ptr &operator = (T *ptr)
		{
			if (_ptr == ptr) return *this;

			T *tmp_ptr = _ptr;
			_ptr = ptr;

			if (_ptr) _ptr->ref();
			if (tmp_ptr) tmp_ptr->unref();

			return *this;
		}

		operator T *() const { return _ptr; }

		T &operator*() const { return *_ptr; }

		T *operator->() const { return _ptr; }

		T *get() const { return _ptr; }

		bool operator!() const { return _ptr == 0; }

		bool valid() const { return _ptr != 0; }

		void swap(ref_ptr &rp)
		{
			T *tmp = _ptr;
			_ptr = rp._ptr;
			rp._ptr = tmp;
		}

	private:
		template <class T2>
		void assign(const ref_ptr<T2> &rp)
		{
			if (_ptr == rp._ptr) return;

			T* tmp_ptr = _ptr;
			_ptr = rp._ptr;

			if (_ptr) _ptr->ref();
			if (tmp_ptr) tmp_ptr->unref();
		}

		T *_ptr;
	};

}