#pragma once

namespace am {

class Log
{
public:
	Log();

	~Log();

	static void sPrint(unsigned int flag, const char* format, ...);

	static void sPrint(const char* format, ...);

	void print(unsigned int flag, const char* format, ...);

	void print(const char* format, ...);

	enum Color
	{
		F_BLUE = 1<<0,
		F_GREEN = 1<<1,
		F_RED = 1<<2,
		F_INTENSITY = 1<<3,
		B_BLUE = 1<<4,
		B_GREEN = 1<<5,
		B_RED = 1<<6,
		B_INTENSITY = 1<<7

	};

private:

	class LogPrivate* _p;
};

}

#ifdef AM_PLATFORM_WINDOWS
#define LOG_SUCCESS(format, ...) { Log::sPrint(Log::F_GREEN | Log::F_INTENSITY , format, __VA_ARGS__); }
#define LOG_WARNING(format, ...) { Log::sPrint(Log::F_RED | Log::F_GREEN | Log::F_INTENSITY , format, __VA_ARGS__); }
#define LOG_ERROR(format, ...) { Log::sPrint(Log::F_RED | Log::F_INTENSITY , format, __VA_ARGS__); }
#elif defined __unix
#include <cstdio>
#define LOG_SUCCESS(format, ...) { printf(format, __VA_ARGS__); }
#define LOG_WARNING(format, ...) { printf(format, __VA_ARGS__); }
#define LOG_ERROR(format, ...) { printf(format, __VA_ARGS__); }
#elif defined __APPLE__
#define LOG_SUCCESS(format, ...) { Log::sPrint(Log::F_GREEN | Log::F_INTENSITY , format, __VA_ARGS__); }
#define LOG_WARNING(format, ...) { Log::sPrint(Log::F_RED | Log::F_GREEN | Log::F_INTENSITY , format, __VA_ARGS__); }
#define LOG_ERROR(format, ...) { Log::sPrint(Log::F_RED | Log::F_INTENSITY , format, __VA_ARGS__); }

#endif
