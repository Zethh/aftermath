#pragma once
#include <string>
namespace am {
	class ResourceReader;
	class Resource;
	class FileSystem;
class ResourceManager
{
public:
	static void registerResourceReader(ResourceReader* reader);

	static void update();

	template <typename T>
	static T* load(const std::string& fileName)
	{
        return static_cast<T*>(internalLoad(fileName));
	}

private:
    static Resource* internalLoad(const std::string& fileName);

	ResourceManager();
};

} 