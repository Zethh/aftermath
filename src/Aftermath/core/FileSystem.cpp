#include "FileSystem.h"
#include <fstream>
#include <mutex>
#include <algorithm>
#include <Aftermath/utils/StringUtils.h>

typedef std::map<std::string, am::FolderWatcher::Ptr> WatcherThreads;
typedef std::vector<std::string> Directories;
std::mutex _mutex;
Directories _directories;
WatcherThreads _watcherThreads;

static Directories::iterator getMounted(const std::string& directory)
{
	return std::find(_directories.begin(), _directories.end(), directory);
}

static bool validFile(const std::string& path)
{
	bool valid = false;
	FILE* file = fopen(path.c_str(), "rb");
	if (file)
	{
		valid = true;
		fclose(file);
	}
	return valid;
}

namespace am {



	bool FileSystem::mountDirectory(const std::string& directory, bool watchDirectory)
	{
		bool mounted = false;
		_mutex.lock();
		Directories::iterator itr = getMounted(directory);
		if (itr == _directories.end())
		{
			_directories.push_back(directory);
			if (watchDirectory)
				_watcherThreads[directory] = new FolderWatcher(directory);
			mounted = true;
		}
		_mutex.unlock();
		return mounted;
	}

	bool FileSystem::unmountDirectory(const std::string& directory)
	{
		bool unmounted = false;
		_mutex.lock();
		Directories::iterator itr = getMounted(directory);
		if (itr != _directories.end())
		{
			_directories.erase(itr);
			_watcherThreads.erase(_watcherThreads.find(directory));
			unmounted = true;
		}
		_mutex.unlock();
		return unmounted;
	}

	void FileSystem::unmountDirectories()
	{
		_mutex.lock();
		_directories.clear();
		_watcherThreads.clear();
		_mutex.unlock();
	}

	bool FileSystem::isMounted(const std::string& directory)
	{
		bool mounted = false;
		_mutex.lock();
		Directories::iterator itr = getMounted(directory);
		if (itr != _directories.end())
		{
			mounted = true;
		}
		_mutex.unlock();
		return mounted;
	}

	std::string FileSystem::getFilePath(const std::string& relativePath, bool *success)
	{
		std::string result;
		_mutex.lock();

		// If the relative path exists. Cancel and return relative path
		if (validFile(relativePath))
		{
			result = relativePath;
			if (success)
				*success = true;
			_mutex.unlock();
			return result;
		}


		if (success)
			*success = false;
		
		for (size_t i = 0; i < _directories.size(); ++i)
		{
			std::string tmp;
			
			int pos = _directories[i].find_last_of("/");
			if (pos == (_directories[i].length() - 1))
				tmp = _directories[i] + relativePath;
			else
				tmp = _directories[i] + "/" + relativePath;
			
			if (validFile(tmp))
			{
				result = tmp;
				if (success)
					*success = true;
				break;
			}
		}
		_mutex.unlock();
		return result;
	}

	std::vector<FileSystem::FileDesc> FileSystem::getModiedFiles()
	{
		std::vector<FileSystem::FileDesc> result;
		std::string a("\\");
		std::string b("/");

		_mutex.lock();
		for (WatcherThreads::iterator itr = _watcherThreads.begin(); itr != _watcherThreads.end(); ++itr)
		{
			std::string file = "";
			while (itr->second->popModifiedFile(file))
			{
				FileSystem::FileDesc fd;
				fd.folderpath = itr->second->getFolderPath().c_str();
				fd.filename = StringUtils::toForwardSlash(file);
				result.push_back(fd);
			}

		}
		_mutex.unlock();

		return std::move(result);
	}
}