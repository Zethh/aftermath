#pragma once

//#define AFTERMATH_META(namespaces, className, ...)\
//    CLASSPOINTER(className)


#define AFTERMATH_META(className)\
	CLASSPOINTER(className)

//\
//    TYPEINFO(namespaces, className, __VA_ARGS__)

#define AFTERMATH_META_BASE(namespaces, className)

//\
 //   TYPEINFO_BASE(namespaces, className)

#define CLASSPOINTER(className) \
    public:\
    typedef am::ref_ptr<className> Ptr;

//#define TYPEINFO(namespaces, className, ...) \
//    public:\
//        static TypeInfo* getStaticTypeInfo() \
//        { \
//            static TypeInfo* _typeInfo = NULL;\
//            if(!_typeInfo)\
//            {\
//                _typeInfo = new TypeInfo(#className, #namespaces "::" #className);\
//                _typeInfo->inherit<void, __VA_ARGS__>();\
//            }\
//            return _typeInfo;\
//        } \
//        \
//        virtual TypeInfo* getTypeInfo()\
//        {\
//            return getStaticTypeInfo();\
//        }
//
//#define TYPEINFO_BASE(namespaces, className) \
//    public:\
//        static TypeInfo* getStaticTypeInfo() \
//        { \
//            static TypeInfo* _typeInfo = NULL;\
//            if(!_typeInfo)\
//            {\
//                _typeInfo = new TypeInfo(#className, #namespaces "::" #className);\
//            }\
//            return _typeInfo;\
//        } \
//        \
//        virtual TypeInfo* getTypeInfo()\
//        {\
//            return getStaticTypeInfo();\
//        }


#define AFTERMATH_PIMPLE(className) \
	private:\
	class className* _p; 


#define AFTERMATH_DECLARE1(namespace1, className) \
    namespace namespace1 { class className; }

#define AFTERMATH_DECLARE2(namespace1, namespace2, className) \
    namespace namespace1 { namespace namespace2 { class className; }}

#define AFTERMATH_DECLARE3(namespace1, namespace2, namespace3, className) \
    namespace namespace1 { namespace namespace2{ namespace namespace3{ class className; }}}

#define AFTERMATH_TEMPLATE_DECLARE1(namespace1, className) \
    namespace namespace1 { template <typename T> class className; }

#define AFTERMATH_TEMPLATE_DECLARE2(namespace1, namespace2, className) \
    namespace namespace1 { namespace namespace2 { template <typename T> class className; }}

#define AFTERMATH_TEMPLATE_DECLARE3(namespace1, namespace2, namespace3, className) \
    namespace namespace1 { namespace namespace2{ namespace namespace3{ template <typename T> class className; }}}
