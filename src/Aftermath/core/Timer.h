#pragma once


#if defined AM_PLATFORM_WINDOWS
#include <Windows.h>
#endif
#include <Aftermath/core/Defines.h>
#include <Aftermath/core/Referenced.h>

namespace am {

class Timer: public Referenced
{
    AFTERMATH_META(Timer)
public:
    Timer();

    enum DIVISOR
    {
        SECONDS = 1,
        MILLISECONDS = 1000,
        MICROSECONDS = 1000000,
        NANOSECONDS = 1000000000
    };

    void setDivisor(DIVISOR d = SECONDS) { _divisor = d; }

	void setFpsCalculationInterval(double interval) { _fpsCalculationInterval = interval; }
    
	double getDeltaTime() { return _deltaTime; }

	double getDeltaTimeInFormat() { return _deltaTime*_divisor; }

    double getFps() { return _averageFps; }

    void tick();


    uint64 _counterOffset; //Time when Timer was created
    uint64 _counterFrequency; //Frequency used
    uint64 _prevTime;
    uint64 _currTime;
    double _deltaTime; //Difference between end and start frame time
    double _secondsPerCount;
	double _averageFps;
	double _fpsCalculationInterval;
	double _sumDeltaTime;
    unsigned int _frameCount; //Holds the current frame number
    DIVISOR _divisor;



};
}