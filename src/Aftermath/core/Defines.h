#pragma once

#ifdef __unix
#include <sys/types.h>
#endif

namespace am {

#ifdef __unix
typedef int64_t uint64;
#elif defined AM_PLATFORM_WINDOWS
typedef unsigned long long uint64;
typedef long long int64;
#else
typedef uint64_t uint64;
#endif

typedef char			s8;		// �128 to 127
typedef short			s16;	// �32,768 to 32,767
typedef int				s32;	// �2,147,483,648 to 2,147,483,647
typedef int64			s64;	// �9,223,372,036,854,775,808 to 9,223,372,036,854,775,807

typedef unsigned char	u8;		// 0 to 255
typedef unsigned short	u16;	// 0 to 65,535
typedef unsigned int	u32;	// 0 to 4,294,967,295
typedef uint64			u64;	// 0 to 18,446,744,073,709,551,615


typedef float			d32;
typedef double			d64;

typedef uint64 hash;

}


