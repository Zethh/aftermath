#include "FolderWatcher.h"
#include <vector>
#include <algorithm>
#include <list>
#include <atomic>
#ifdef AM_PLATFORM_WINDOWS
#include <Windows.h>
#elif defined __unix
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/inotify.h>
#include <sys/types.h>
#endif

#include <functional>
#include <thread>
#include <mutex>

namespace am {

class FolderWatcherImpl
{
public:

	void watcherThread();

	void pushModifiedFile(const std::string& file);

	bool popModifiedFile(std::string& file);

	std::mutex _mutex;
	std::string _path;
	unsigned long _status;
	void* _handle;
	unsigned int _id;
	std::atomic<bool> _active;
	std::thread _thread;


	std::list<std::string> _modifiedFiles;
};

FolderWatcher::FolderWatcher(const std::string& path)
	: _p(new FolderWatcherImpl())
{
	_p->_path = path;
	_p->_thread = std::thread(std::bind(&FolderWatcherImpl::watcherThread, _p));

}

FolderWatcher::~FolderWatcher()
{
	_p->_active = false;
	//_p->_thread.interrupt();
	_p->_thread.join();
	delete _p;
}

void FolderWatcherImpl::watcherThread()
{
#ifdef AM_PLATFORM_WINDOWS
	FILE_NOTIFY_INFORMATION fniBuffer[64];
	unsigned long bytesReturned = 0;
	//_p->_active = true;
	_active = true;
	_handle = CreateFile(_path.c_str(), FILE_LIST_DIRECTORY, FILE_SHARE_WRITE | FILE_SHARE_READ | FILE_SHARE_DELETE, NULL, OPEN_EXISTING, FILE_FLAG_BACKUP_SEMANTICS, NULL);
	if(_handle == INVALID_HANDLE_VALUE)
	{
        std::cerr << "Unable to open folder handle for: " << _path.c_str() << std::endl;
	}else
	{
        std::cerr << "Watching folder: " << _path.c_str() << std::endl;
	}

	unsigned int windowsUglyFix = 0;
	while(ReadDirectoryChangesW(_handle, &fniBuffer, sizeof(fniBuffer), TRUE, FILE_NOTIFY_CHANGE_LAST_WRITE, &bytesReturned, NULL, NULL) && _active)
	{
		char *buff = (char *)&fniBuffer[0];

		std::string str = "";
        bool dataAvailable = true;
		while(dataAvailable)
		{
			FILE_NOTIFY_INFORMATION &fni = (FILE_NOTIFY_INFORMATION &)*buff; 
			if(!fni.NextEntryOffset)
			{
				char*name = (char*)&fni.FileName[0];
				for(unsigned int i = 0; i < fni.FileNameLength; ++i)
				{
					str += name[i];
					i++;
				}
				dataAvailable = false;
			}
			if(dataAvailable)
			    buff += fni.NextEntryOffset;
		}

		if(++windowsUglyFix == 2)
		{
			if(fniBuffer->Action == FILE_ACTION_MODIFIED)
			{
				std::cout << "\nFile has been modified: " << str.c_str() << std::endl;
				pushModifiedFile(str);
			}
			windowsUglyFix = 0;
		}
		//Sleep(500);
	}
#elif defined __unix


#define EVENT_SIZE  ( sizeof (struct inotify_event) )
#define BUF_LEN     ( 1024 * ( EVENT_SIZE + 16 ) )

  int length, i = 0;
  int fd;
  int wd;
  char buffer[BUF_LEN];

  fd = inotify_init();

  if ( fd < 0 ) {
    perror( "inotify_init" );
  }

  std::cout << "Watching folder: " << _path.c_str() << std::endl;
  wd = inotify_add_watch( fd, _path.c_str(),
                         IN_MODIFY);
  while(1)
  {
      i = 0;
      std::cout << "Initiating new read" << std::endl;
      length = read( fd, buffer, BUF_LEN );
      std::cout << "Found data" << std::endl;

      if ( length < 0 ) {
        perror( "read" );
      }

      while ( i < length ) {
        struct inotify_event *event = ( struct inotify_event * ) &buffer[ i ];
        if ( event->len ) {
            if ( event->mask & IN_MODIFY )
            {
                if ( event->mask & IN_ISDIR )
                {
                    printf( "The directory %s was modified.\n", event->name );
                }
                else
                {
                    printf( "The file %s was modified.\n", event->name );
                    pushModifiedFile(event->name);
                }
            }
        }
        i += EVENT_SIZE + event->len;
      }
  }

  std::cout << "Killing folder watcher thread: " << _path.c_str() << std::endl;
  ( void ) inotify_rm_watch( fd, wd );
  ( void ) close( fd );

#endif
}

void FolderWatcherImpl::pushModifiedFile(const std::string& file)
{
	_mutex.lock();
	std::list<std::string>::iterator itr = std::find(_modifiedFiles.begin(), _modifiedFiles.end(), file);
	if(itr == _modifiedFiles.end())
	{
		_modifiedFiles.push_back(file);
		std::cout << "Num stashed changes: " << _modifiedFiles.size() << std::endl;
	}
	_mutex.unlock();
}

bool FolderWatcher::popModifiedFile(std::string& file)
{
	return _p->popModifiedFile(file);
}

bool FolderWatcherImpl::popModifiedFile(std::string& file)
{
	bool pop = false;
	_mutex.lock();
	if(_modifiedFiles.size() > 0)
	{
		file = _modifiedFiles.front();
		_modifiedFiles.pop_front();
		pop = true;
	}
	_mutex.unlock();
	return pop;
}

const std::string& FolderWatcher::getFolderPath() const
{
	return _p->_path;
}

}