#pragma once


#include <vector>
#include <map>

#define AFTERMATH_NOTIFY_OBSERVERS(IObserver, method, ...) \
	for (unsigned _index = 0; _index < (vec_##IObserver).size(); ++_index)\
	((vec_##IObserver)[_index])->method(__VA_ARGS__)

#define AFTERMATH_OBSERVABLE(IObserver)\
public: \
class IObserver;\
	void addObserver(IObserver* observer)\
	{\
	if(!isObserving(observer))\
	(vec_##IObserver).push_back(observer);\
	}\
	\
	void removeObserver(IObserver* observer)\
	{\
	for(unsigned int i = 0; i < (vec_##IObserver).size(); ++i)\
		{\
		if(((vec_##IObserver)[i]) == observer)\
		(vec_##IObserver).erase((vec_##IObserver).begin() + i);\
		}\
	}\
	\
	bool isObserving(IObserver* observer)\
	{\
	for(unsigned int i = 0; i < (vec_##IObserver).size(); ++i)\
		{\
		if(((vec_##IObserver)[i]) == observer)\
		return true;\
		}\
		return false;\
	}\
private: \
	std::vector<IObserver*> vec_##IObserver; \
public:


