#include "ResourceManager.h"
#include <string>
#include <map>
#include <algorithm>
#include <vector>
#include <iostream>
#include <Aftermath/core/Log.h>
#include <Aftermath/core/FileSystem.h>
#include <Aftermath/utils/ResourceReader.h>
#include <Aftermath/utils/StringUtils.h>
#include <Aftermath/utils/Resource.h>

namespace am {

class ResourceManagerImpl
{
public:
	typedef std::map<std::string, Resource::Ptr> ResourceMap;
	typedef std::vector<ResourceReader::Ptr> ResourceReaderMap;


	 ResourceMap _resources;
	 ResourceReaderMap _readers;
};

static ResourceManagerImpl* _p = new ResourceManagerImpl();

void ResourceManager::registerResourceReader(ResourceReader* reader)
{
	ResourceManagerImpl::ResourceReaderMap::iterator itr = std::find(_p->_readers.begin(), _p->_readers.end(), reader);
	if(itr == _p->_readers.end())
	{
		_p->_readers.push_back(reader);
	}
}

void ResourceManager::update()
{
	std::vector<FileSystem::FileDesc> result = FileSystem::getModiedFiles();
	for (auto fd : result)
	{
		std::string file = fd.folderpath + std::string("/") + fd.filename;
		for (auto res : _p->_resources)
		{
			if (res.first.compare(file) == 0)
			{
				std::cerr << "Reloading resource: " << file.c_str() << std::endl;
				res.second->reload();
			}
		}
	}
}

Resource* ResourceManager::internalLoad(const std::string& fileName)
{
	Resource::Ptr resource = NULL;
	for(ResourceManagerImpl::ResourceMap::iterator itr = _p->_resources.begin(); itr != _p->_resources.end(); ++itr)
	{
		if(itr->first.compare(fileName) == 0)
		{
			resource = itr->second;
			break;
		}
	}
	if(!resource)
	{
		std::string extension = StringUtils::getFileExtension(fileName);
		for(unsigned int i = 0; i < _p->_readers.size(); ++i)
		{
			if(_p->_readers[i]->isSupportedExtension(extension))
			{
				resource = _p->_readers[i]->read(fileName);
				resource->setResourceReader(_p->_readers[i]);
				resource->setFileName(fileName);
				_p->_resources[fileName] = resource;
				break;
			}
		}
	}

	return resource;
}

}