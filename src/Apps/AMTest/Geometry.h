#pragma once

#include "Node.h"

#include <Aftermath/ECS/Entity.h>
#include <Aftermath/ECS/components/Drawable.h>
#include <Aftermath/ECS/components/RenderInfo.h>

class Geometry: public Node
{
public:
	Geometry(aftermath::ecs::Entity* entity)
        : Node(entity)
	{


		_drawable = new aftermath::ecs::Drawable();
		_drawable->setName("Drawable");
		
		_renderInfo = new aftermath::ecs::RenderInfo();
		_renderInfo->setName("RenderInfo");

		
		_entity->addComponent(_drawable.get());
		_entity->addComponent(_renderInfo.get());

	}
	
	aftermath::ecs::Drawable::Ptr _drawable;
	aftermath::ecs::RenderInfo::Ptr _renderInfo;
	

};
