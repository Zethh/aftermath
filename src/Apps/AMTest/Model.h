#pragma once
#include "Geometry.h"
#include <Aftermath/ECS/Entity.h>
#include <fstream>
#include <Aftermath/render/SkinnedModel.h>
#include <assert.h>
#include <sstream>

class Model: public Geometry
{
    AFTERMATH_META(, Model, Geometry);
public:
    Model(aftermath::ecs::Entity* entity, const std::string& filename)
        : Geometry(entity)
    {
        //_mesh = core::loadModel(filename);
        _mesh = loadRawModel(filename);
        init();
    }

	static aftermath::render::Mesh* loadRawModel(const std::string& filename)
	{
		std::string buffer;
		std::ifstream filebuff;
		filebuff.open(filename.c_str());
		assert(filebuff.is_open() && "Unable to open file");
		std::stringstream file;
		file << filebuff.rdbuf();
		filebuff.close();
		file >> buffer;

		aftermath::render::Mesh* mesh = new aftermath::render::Mesh();
		aftermath::render::Vector3fBuffer* vertices = new aftermath::render::Vector3fBuffer();
		aftermath::render::Vector3fBuffer* normals = new aftermath::render::Vector3fBuffer();
		aftermath::render::UIntBuffer* indices = new aftermath::render::UIntBuffer();

		while (!file.eof())
		{
			if (buffer.compare("numVertices") == 0)
			{
				int count = 0;
				file >> count;
				for (size_t i = 0; i < count; ++i)
				{
					aftermath::math::Vector3f v;
					file >> v.x >> v.y >> v.z;
					//              mesh->pushVertex(v);
					vertices->push_back(v);
					//              mesh->pushNormal(v);
					//              normals->push_back(v);
				}
			}

			else if (buffer.compare("numNormals") == 0)
			{
				int count = 0;
				file >> count;
				for (size_t i = 0; i < count; ++i)
				{
					aftermath::math::Vector3f v;
					file >> v.x >> v.y >> v.z;
					//              mesh->pushNormal(v);
					normals->push_back(v);
					//                vertices->push_back(v);
				}
			}

			else if (buffer.compare("numIndices") == 0)
			{
				int count = 0;
				file >> count;
				for (size_t i = 0; i < count; ++i)
				{
					unsigned int index;
					file >> index;
					//mesh->pushIndex(index);
					indices->push_back(index);
				}
			}

			file >> buffer;

		}

		mesh->setBuffer(aftermath::render::Mesh::Position, vertices);
		mesh->setBuffer(aftermath::render::Mesh::Normal, normals);
		mesh->addIndexBuffer(aftermath::render::Mesh::Triangles, indices);
		return mesh;
	}


    void init()
    {
        _drawable->addMesh(_mesh.get());
    }

    aftermath::render::Mesh::Ptr _mesh;
};


class MD5Model: public Geometry
{
    AFTERMATH_META(, MD5Model, Geometry);
public:
    MD5Model(aftermath::ecs::Entity* entity) //, aftermath::render::SkinnedModel * model)
        : Geometry(entity)
    {

    }
};
