#pragma once
#include "Geometry.h"

#pragma once
#include "Geometry.h"




class Cube: public Geometry
{
    AFTERMATH_META(, Cube, Geometry);
public:
    Cube(aftermath::ecs::Entity* entity, float size)
        : Geometry(entity)
        , _mesh(new aftermath::render::Mesh())
    {
        init(aftermath::math::Vector3f(-size), aftermath::math::Vector3f(size));
    }

    Cube(aftermath::ecs::Entity* entity, float width, float height, float depth)
        : Geometry(entity)
        , _mesh(new aftermath::render::Mesh())
    {
        
        
        
        init(aftermath::math::Vector3f(-width, -height, -depth), aftermath::math::Vector3f(width, height, depth));
    }

    Cube(aftermath::ecs::Entity* entity, const aftermath::math::Vector3f& min, const aftermath::math::Vector3f& max)
        : Geometry(entity)
        , _mesh(new aftermath::render::Mesh())
    {
        
        
        
        init(min, max);
    }

    //void init(float minValue, float maxValue)
    void init(const aftermath::math::Vector3f& min, const aftermath::math::Vector3f& max)
    {
        unsigned int ind[] =	{   0, 1, 2, 
                                0, 2, 3,
                                0, 3, 7,
                                0, 7, 4,
                                4, 7, 6,
                                4, 6, 5,
                                5, 6, 2,
                                5, 2, 1,
                                3, 2, 6,
                                3, 6, 7,
                                0, 4, 5,
                                0, 5, 1
							};



        _indices.insert(ind, 36);

        _vertices.push_back(aftermath::math::Vector3f(min.x,min.y,max.z));
	    _vertices.push_back(aftermath::math::Vector3f(max.x,min.y,max.z)); //1
	    _vertices.push_back(aftermath::math::Vector3f(max.x,max.y,max.z)); //2
	    _vertices.push_back(aftermath::math::Vector3f(min.x,max.y,max.z)); //3
	    _vertices.push_back(aftermath::math::Vector3f(min.x,min.y,min.z)); //4
	    _vertices.push_back(aftermath::math::Vector3f(max.x,min.y,min.z)); //5
	    _vertices.push_back(aftermath::math::Vector3f(max.x,max.y,min.z)); //6
	    _vertices.push_back(aftermath::math::Vector3f(min.x,max.y,min.z)); //7

		_mesh->setBuffer(aftermath::render::Mesh::Position, &_vertices);
		_mesh->setBuffer(aftermath::render::Mesh::Normal, &_vertices);
		_mesh->addIndexBuffer(aftermath::render::Mesh::Triangles, &_indices);
        _drawable->addMesh(_mesh.get());
    }

    aftermath::render::Mesh::Ptr _mesh;
    aftermath::render::UIntBuffer _indices;
    aftermath::render::Vector3fBuffer _vertices;
    aftermath::render::Vector2uBuffer _uvs;
};
