#pragma comment(linker, "/subsystem:\"console\" /entry:\"WinMainCRTStartup\"")

#include <string>
unsigned int iphoneWidth = 320;
unsigned int iphoneHeight = 480;
unsigned int ipadWidth = 768;
unsigned int ipadHeight = 1024;

#include "Camera.h"
#include "Plane.h"
#include "Application.h"

#include <Aftermath/external/tinyxml/TinyXml2.h>
#include <Aftermath/core/ResourceManager.h>
#include <Aftermath/core/ResourceManager.h>
#include <Aftermath/core/Kernel.h>
#include <Aftermath/core/Module.h>
#include <Aftermath/ECS/components/Component.h>
#include <Aftermath/ECS/Entity.h>
#include <Aftermath/core/FolderManager.h>
#include <Aftermath/utils/StringUtils.h>
#include <Aftermath/utils/ShaderReader.h>
#include <Aftermath/utils/Resource.h>
#include <Aftermath/utils/Shader.h>
#include <Aftermath/render/ShaderProgram.h>
#include <Aftermath/render/gl/OGLShaderProgram.h>
#include <Aftermath/utils/EffectReader.h>
#include <Aftermath/render/Effect.h>
#include <Aftermath/ECS/systems/SceneSystem.h>
#include <Aftermath/ECS/systems/CameraSystem.h>
#include <Aftermath/core/Timer.h>
#include <Aftermath/ECS/components/RenderBin.h>
#include <Aftermath/ECS/components/Drawable.h>
#include <Aftermath/ECS/components/Camera.h>
#include <Aftermath/render/gl/OGLContentBuilder.h>
#include <Aftermath/render/ContentBuilder.h>
#include <Aftermath/ECS/components/Transform.h>
#include <Aftermath/math/Matrix4.h>
#include <Aftermath/math/Vector2.h>
#include <Aftermath/math/Vector3.h>
#include <Aftermath/math/Vector4.h>
#include <Aftermath/core/ViewManager.h>
#include <Aftermath/render/Viewport.h>
#include <Aftermath/utils/ImageReader.h>
#include <Aftermath/render/Uniform.h>
#include "Aftermath/core/Timer.h"
#include <Aftermath/ECS/systems/TransformSystem.h>
#include <Aftermath/math/Quaternion.h>
#include <Aftermath/utils/MD5ModelReader.h>
#include <Aftermath/render/SkinnedModel.h>
#include <Aftermath/utils/MD5AnimationReader.h>
#include <Aftermath/ECS/systems/SkinnedModelSystem.h>

#include <Aftermath/render/gl/OGLView.h>
#include <thread>

int main()
{
    aftermath::core::Timer::Ptr timer = new aftermath::core::Timer();
    timer->setDivisor(aftermath::core::Timer::SECONDS);

    timer->tick();

	aftermath::core::Kernel::Ptr kernel = new aftermath::core::Kernel();
    aftermath::render::ContentBuilder::Ptr contentBuilder = new aftermath::render::gl::OGLContentBuilder();
    kernel->setContentBuilder(contentBuilder.get());

    aftermath::ecs::EntitySystem::Ptr entitySystem = new aftermath::ecs::EntitySystem();
    kernel->addModule(entitySystem.get());

    aftermath::ecs::SkinnedModelSystem* skinSystem = new aftermath::ecs::SkinnedModelSystem();
    entitySystem->registerSystem(skinSystem);
    kernel->addModule(skinSystem);
    aftermath::ecs::SceneSystem* sceneSystem = new aftermath::ecs::SceneSystem();
    entitySystem->registerSystem(sceneSystem);
    kernel->addModule(sceneSystem);
    

    #if defined WIN32
	std::string restest = "C:/Users/C/Documents/aftermath/resources";
	std::string configfile = "C:/Users/C/Documents/aftermath/src/Apps/AMTest/Config.xml";
#elif defined __unix
    std::string configfile = "/home/Chris/Documents/Aftermath/src/Apps/AMTest/Config.xml";
    std::string restest = "/home/Chris/Documents/Aftermath/resources";
#elif defined __MAC_NA
    std::string configfile = "/Users/CMac/Documents/fa2/src/viewer/ShadowMapping";
    std::string restest = "/Users/CMac/Documents/fa2/restest";
#endif
    	aftermath::render::View::Settings settings;

	tinyxml2::XMLDocument config;
    int error = config.LoadFile(configfile.c_str());
	tinyxml2::XMLElement* windowElement = config.FirstChildElement("window");
	settings.windowWidth = std::atoi(windowElement->FirstChildElement("width")->GetText());
	settings.windowHeight = std::atoi(windowElement->FirstChildElement("height")->GetText());
	settings.xOffset = std::atoi(windowElement->FirstChildElement("xoffset")->GetText());
	settings.yOffset = std::atoi(windowElement->FirstChildElement("yoffset")->GetText());
	settings.screenNumber = std::atoi(windowElement->FirstChildElement("screennumber")->GetText());
	settings.versionMajor = std::atoi(windowElement->FirstChildElement("glversionmajor")->GetText());
	settings.versionMinor = std::atoi(windowElement->FirstChildElement("glversionminor")->GetText());
	settings.windowTitle = std::string(windowElement->FirstChildElement("title")->GetText());
	settings.clampWindowToCenter = std::atoi(windowElement->FirstChildElement("clamptocenter")->GetText());
	settings.fullScreen = std::atoi(windowElement->FirstChildElement("fullscreen")->GetText());
	settings.vsync = std::atoi(windowElement->FirstChildElement("vsync")->GetText());
	settings.resizeable = std::atoi(windowElement->FirstChildElement("resizeable")->GetText());
	settings.borderless = std::atoi(windowElement->FirstChildElement("borderless")->GetText());
	
    aftermath::core::ViewManager::Ptr viewManager = new aftermath::core::ViewManager();
    //aftermath::render::View* view = new aftermath::render::gl::SDLView(settings);
	aftermath::render::View* view = new aftermath::render::gl::OGLView(settings);
    view->setId("1");
	aftermath::render::View* view2 = new aftermath::render::gl::OGLView(settings);
	view2->setId("2");
	//view->shareContext(view2);

    aftermath::render::Viewport::Ptr viewport = new aftermath::render::Viewport(); //gl::OGLViewport();
	viewport->setId("1");
	viewport->setPosition(0, 0);
    viewport->setSize(1280, 720);
    view->addViewport(viewport.get());
	view2->addViewport(viewport.get());
//
//    core::Viewport::Ptr viewport2 = new core::OGLViewport();
//	viewport2->setId("2");
//	viewport2->setPosition(0, 0);
//    viewport2->setSize(1280, 720);
//    view->addViewport(viewport2.get());
//
    aftermath::ecs::CameraSystem::Ptr cameraSystem = new aftermath::ecs::CameraSystem();
    entitySystem->registerSystem(cameraSystem.get());
    kernel->addModule(cameraSystem.get());

    aftermath::ecs::TransformSystem::Ptr transformSystem = new aftermath::ecs::TransformSystem();
    entitySystem->registerSystem(transformSystem.get());
    kernel->addModule(transformSystem.get());

    std::cout << "Num: " << aftermath::core::Kernel::getStaticTypeInfo()->getFullClassName().c_str() << std::endl;
	
	aftermath::core::ResourceManager resourceManager;
	kernel->addModule(viewManager.get());
	kernel->addModule(&resourceManager);
	kernel->addModule(contentBuilder.get());
	kernel->setContentBuilder(contentBuilder.get());


	viewManager->addView(view);
    viewManager->addView(view2);

	aftermath::core::FileSystem::Ptr fileSystem = new aftermath::core::FileSystem();
	fileSystem->mountDirectory(restest);
	kernel->addModule(fileSystem.get());

    tinyxml2::XMLElement* folderWatcherElement = config.FirstChildElement("folderwatchers");//->FirstChildElement("folder");
    tinyxml2::XMLElement* folderElement = folderWatcherElement->FirstChildElement("folder");
    while(folderElement)
    {
        fileSystem->mountDirectory(std::string(folderElement->GetText()));

        if(folderElement->NextSibling() == NULL)
            break;

        folderElement = folderElement->NextSibling()->ToElement();
    }

	#if defined __unix
    fileSystem->mountDirectory("/home/Chris/Documents/Aftermath/resources");
//		folderManager->mountFolder("../../../restest");
	#endif


	resourceManager.enableAutomaticResourceUpdates(fileSystem.get());

		aftermath::utils::ShaderReader::Ptr shaderReader = new aftermath::utils::ShaderReader();
	aftermath::utils::EffectReader::Ptr effectReader = new aftermath::utils::EffectReader();
    	/*aftermath::render::gl::SDLImageReader::Ptr sdlImageReader = new aftermath::render::gl::SDLImageReader();*/
	aftermath::utils::ImageReader::Ptr imageReader = new aftermath::utils::ImageReader();
        aftermath::utils::MD5ModelReader::Ptr md5modelReader = new aftermath::utils::MD5ModelReader();
        aftermath::utils::MD5AnimationReader::Ptr md5animationReader = new aftermath::utils::MD5AnimationReader();
	resourceManager.registerResourceReader(shaderReader.get());
	resourceManager.registerResourceReader(effectReader.get());
	resourceManager.registerResourceReader(imageReader.get());
    resourceManager.registerResourceReader(md5modelReader.get());
    resourceManager.registerResourceReader(md5animationReader.get());

    #if defined WIN32
    effectReader->setGroupId("OpenGLDeferred");
#elif defined __unix
    effectReader->setGroupId("LinuxOpenGLDeferred");
#elif defined __MAC_NA
    effectReader->setGroupId("LinuxOpenGLDeferred");
#endif

        Application::Ptr app = new Application();
    kernel->addModule(app.get());
    kernel->initializeModules();


    timer->tick();
    std::cerr << "Initialization took: " << timer->getDeltaTimeInFormat() << " seconds" << std::endl;

	while(view->isRunning())
	{
		kernel->frame();
	}
	kernel->terminate();

	//SDL_Quit();
	delete view;
	return 0;
};

#ifdef WIN32
int WinMain(__in HINSTANCE hInstance, __in_opt HINSTANCE hPrevInstance, __in_opt LPSTR lpCmdLine, __in int nShowCmd)
{
	LPSTR str = GetCommandLine();
	main();
}
#endif