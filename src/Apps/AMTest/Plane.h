#pragma once
#include "Geometry.h"

class Plane: public Geometry
{
    AFTERMATH_META(, Plane, Geometry);
public:
    Plane(aftermath::ecs::Entity* entity, float size)
        : Geometry(entity)
        , _mesh(new aftermath::render::Mesh())
    {
        unsigned int ind[] = { 0, 1, 2, 0, 2, 3};
        _indices.insert(ind, 6);

        _vertices.push_back(aftermath::math::Vector3f(0.0f, 0.0f, 0.0f));
        _vertices.push_back(aftermath::math::Vector3f(size, 0.0f, 0.0f));
        _vertices.push_back(aftermath::math::Vector3f(size, size, 0.0f));
        _vertices.push_back(aftermath::math::Vector3f(0.0f, size, 0.0f));

        _uvs.push_back(aftermath::math::Vector2u(0, 0));
        _uvs.push_back(aftermath::math::Vector2u(1, 0));
        _uvs.push_back(aftermath::math::Vector2u(1, 1));
        _uvs.push_back(aftermath::math::Vector2u(0, 1));

		_mesh->setBuffer(aftermath::render::Mesh::Position, &_vertices);
		_mesh->setBuffer(aftermath::render::Mesh::Normal, &_vertices);
		_mesh->setBuffer(aftermath::render::Mesh::TexCoord, &_uvs);
		_mesh->addIndexBuffer(aftermath::render::Mesh::Triangles, &_indices);
        _drawable->addMesh(_mesh.get());

    }



    aftermath::render::Mesh::Ptr _mesh;
    aftermath::render::UIntBuffer _indices;
    aftermath::render::Vector3fBuffer _vertices;
    aftermath::render::Vector2uBuffer _uvs;

};

class FullScreenQuad: public Geometry
{
    AFTERMATH_META(, FullScreenQuad, Geometry);
public:
    FullScreenQuad(aftermath::ecs::Entity* entity)
        : Geometry(entity)
        , _mesh(new aftermath::render::Mesh())
    {
        unsigned int ind[] = { 0, 1, 2, 0, 2, 3};
        _indices.insert(ind, 6);

        _vertices.push_back(aftermath::math::Vector3f(-1, -1, 0));
        _vertices.push_back(aftermath::math::Vector3f( 1, -1, 0));
        _vertices.push_back(aftermath::math::Vector3f( 1,  1, 0));
        _vertices.push_back(aftermath::math::Vector3f(-1,  1, 0));

		_mesh->setBuffer(aftermath::render::Mesh::Position, &_vertices);
		_mesh->addIndexBuffer(aftermath::render::Mesh::Triangles, &_indices);

        _drawable->addMesh(_mesh.get());
    }



    aftermath::render::Mesh::Ptr _mesh;
    aftermath::render::UIntBuffer _indices;
    aftermath::render::Vector3fBuffer _vertices;
    aftermath::render::Vector2uBuffer _uvs;

};

class HeightMap: public Geometry
{
    AFTERMATH_META(, HeightMap, Geometry);
public:
    HeightMap(aftermath::ecs::Entity* entity, unsigned int width, unsigned int height)
        : Geometry(entity)
        , _mesh(new aftermath::render::Mesh())
    {
       _width = width;
       _height = height;
        _vertices.resize(width * height);


    for(int y = 0; y < height; ++y)
    {
        for(int x = 0; x < width; ++x)
        {
            float pointHeight = std::rand() % 3;
            float pointHeightDec = std::rand() % 1;
            pointHeightDec = pointHeightDec/100;
            pointHeight = pointHeight + pointHeightDec;

            pointHeight = 0.0f;
            _vertices[x+y*width] = aftermath::math::Vector3f(x, pointHeight, -y );
        }
    }

    loadNormals();
    boxFilter();


    _indices.resize(((_width-1) * (_height-1))*6);
    int index = 0;
    for(unsigned int z = 0; z < _height-1; ++z)
    {
        for(unsigned int x = 0; x < _width-1; ++x)
        {
            
            int vertexIndex = x + z*_height;
            _indices[index++] = vertexIndex + _width+1; //x+1, z+1
            _indices[index++] = vertexIndex; //x,z
            _indices[index++] = vertexIndex + _width;  //x, z+1
			
			
            _indices[index++] = vertexIndex + _width+1; //x+1, z+1
            _indices[index++] = vertexIndex +1; //x+1, z
            _indices[index++] = vertexIndex; //x,z
        }
    }



	_mesh->setBuffer(aftermath::render::Mesh::Position, &_vertices);
	_mesh->setBuffer(aftermath::render::Mesh::Normal, &_normals);
	//_mesh->setBuffer(aftermath::render::Mesh::TexCoord, &_uvs);
	_mesh->addIndexBuffer(aftermath::render::Mesh::Triangles, &_indices);

    /*_mesh->setIndexBuffer(&_indices);

    _mesh->getAttributeBuffer().set("position", &_vertices);
    _mesh->getAttributeBuffer().set("normal", &_normals);
    _mesh->getAttributeBuffer().set("uv", &_uvs);

    _mesh->addPrimitiveSet(new aftermath::render::gl::OGLPrimitiveSet());*/
    _drawable->addMesh(_mesh.get());
    }

    void loadNormals()
    {
        _normals.resize(_width*_height,aftermath::math::Vector3f(0.0f));
	    unsigned int patterns[6] = {0, 2, 3, 1, 0, 3 };
	    int v[4];

	    for(unsigned int z = 0; z < _height-1; ++z)
	    {
		    for(unsigned int x = 0; x < _width-1; ++x)
		    {
			    v[0] = x + z*_width;
			    v[1] = (x+1) + z*_width;
			    v[2] = x + (z+1)*_width;
			    v[3] = (x+1) + (z+1)*_width;


			    for(unsigned int i = 0; i < 6; i+=3)
			    {
				    aftermath::math::Vector3f p1(_vertices[v[patterns[i]]]);
				    aftermath::math::Vector3f p2(_vertices[v[patterns[i+1]]]);
				    aftermath::math::Vector3f p3(_vertices[v[patterns[i+2]]]);	

				    aftermath::math::Vector3f v1(p3-p1);
				    aftermath::math::Vector3f v2(p2-p1);	
				    aftermath::math::Vector3f tmp(aftermath::math::cross(v1,v2));

				    _normals[v[patterns[i]]]+=tmp;
				    _normals[v[patterns[i+1]]]+=tmp;
				    _normals[v[patterns[i+2]]]+=tmp;
			    }

		    }
	    }

	    for(unsigned int i = 0 ; i < _width*_height; i++)
	    {
		    _normals[i] = aftermath::math::normalize(_normals[i]);
	    }
    }

    void boxFilter()
    {
	unsigned int area = _width*_height;
	unsigned int patterns[9];
	std::vector<aftermath::math::Vector3f> res(area);
    //unsigned int width = _heightDB->_data._width;
    //unsigned int height = _heightDB->_data._height;
	for(unsigned int z = 0; z < _width; ++z)
	{
		for(unsigned int x = 0; x < _height; ++x)
		{
			//To perform box filtering we create a 3x3 grid around our current position and get the height values.
			//Then we set the height at the current pos equal to the average height of the grid.
			float sum(0.0f), numCells(1.0f);
			
			//Swap these two to speed up performance since point 2x2 will always be accounted for.
			patterns[0] = (x + z*_width); //2x2
			patterns[4] = (x-1) + (z-1) * _width; //1x1 //will always happen
            
			patterns[1] = (x +(z-1)*_width); //1x2
			patterns[2] = ((x+1) +(z-1)*_width); //1x3
			patterns[3] = (x-1) + z*_width; //2x1
			
			patterns[5] = (x+1) + z*_width; //2x3
			patterns[6] = (x-1) +(z+1)*_width; //3x1
			patterns[7] = x + (z + 1) * _width; //3x2
			patterns[8] = (x+1) + (z + 1) * _width; //3x3
            
			
			sum += _vertices[patterns[0]].y;
			for(unsigned int i = 1; i < 9; ++i)
			{
				if(patterns[i] >= 0 && patterns[i] < area)
				{
					sum += _vertices[patterns[i]].y;
					++numCells;
				}
			}
            
			res[x+z*_width] = aftermath::math::Vector3f(x,sum/numCells,z);
            
		}
	}

		for(unsigned int z = 0; z < _width; ++z)
		{
			for(unsigned int x = 0; x < _height; ++x)
			{
				_vertices[x+z*_width].z *=-1.0f;
			}
		}

    
    
}



    aftermath::render::Mesh::Ptr _mesh;
    aftermath::render::UIntBuffer _indices;
    aftermath::render::Vector3fBuffer _vertices;
    aftermath::render::Vector3fBuffer _normals;
    aftermath::render::Vector2uBuffer _uvs;
    unsigned int _width;
    unsigned int _height;

};


