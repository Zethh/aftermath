////////////////////////////////////////////
////									////
////	This file was created by:		////
////	Christoffer Pettersson			////
////	B00191431						////
////									////
////////////////////////////////////////////



#include "Controller.h"


//Sets the state of a given button to either true or false.
void Controller::SetState(int button, bool state)
{
	bMapItr = bMap.begin();
	bMapItr = bMap.find(button);
	if(bMapItr != bMap.end())
		bMapItr->second.state = state;
}

//Returns the state of an event.
bool Controller::GetButtonState(int action)
{
	it = mappings.begin();
	it = mappings.find(action);
	if(it != mappings.end())
	{
		bMapItr = bMap.begin();
		bMapItr = bMap.find(it->second);
		if(bMapItr->second.isSingleHit)
		{
			if(bMapItr->second.state == true)
			{
				bMapItr->second.state = false;
				return true;
			}
		}else
			return bMapItr->second.state;
		return false;
	}
	return false;
}

//Maps a button to an event. This is for multi press events such as moving the player according to
//keypresses.
void Controller::MapButton(int action, int button)
{
	it = mappings.begin();
	it = mappings.find(action);
	if(it != mappings.end())
	{
		std::cout << "This button is already in use." << std::endl;
	}
	else
	{
		mappings.insert(std::make_pair(action,button));	

		Button tmpButton;
		tmpButton.isSingleHit = false;
		tmpButton.state = false;
		bMap.insert(std::make_pair(button, tmpButton));
	}
}

//Maps a button to an event. This is for single hit events such as when the pause button is pressed.
void Controller::MapButtonSingleHit(int action, int button)
{
	it = mappings.begin();
	it = mappings.find(action);
	if(it != mappings.end())
	{
		std::cout << "This button is already in use." << std::endl;
	}
	else
	{
		mappings.insert(std::make_pair(action,button));	
		Button tmpButton;
		tmpButton.isSingleHit = true;
		tmpButton.state = false;
		bMap.insert(std::make_pair(button, tmpButton));
	}
}