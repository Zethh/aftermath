#pragma once

#include <Aftermath/ECS/components/Transform.h>
#include <Aftermath/core/Referenced.h>
#include <Aftermath/ECS/Entity.h>

class Node: public aftermath::core::Referenced
{
    AFTERMATH_META(, Node, aftermath::core::Referenced);
public:
    Node(aftermath::ecs::Entity* entity) 
    {
        _entity = entity;
        _transformation = new aftermath::ecs::Transform();
		_transformation->setName("Transformation");
        _entity->addComponent(_transformation.get());
    }

    void setParent(aftermath::ecs::Transform* parent)
    {
        _transformation->setParent(parent);
    }

    aftermath::ecs::Transform* getTransformation() const
    {
        return _transformation.get();
    }

//protected:
    aftermath::ecs::Transform::Ptr _transformation;
    aftermath::ecs::Entity::Ptr _entity;
};
