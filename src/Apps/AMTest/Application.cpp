#include "Application.h"
#include <Aftermath/core/Kernel.h>
#include <Aftermath/core/ResourceManager.h>
#include <Aftermath/core/ViewManager.h>
//#include <Aftermath/render/gl/SDLView.h>
#include <Aftermath/render/gl/OGLView.h>
#include <Aftermath/render/State.h>
#include <Aftermath/render/SkinnedModel.h>
#include <Aftermath/render/SkinnedAnimation.h>
#include <Aftermath/ECS/components/SkinnedModel.h>
Application::Application()
{
    registerEvent(this, &Application::eventInit, "init");
    registerEvent(this, &Application::eventTick, "tick");

}

void Application::eventInit(void* data)
{
    aftermath::core::Kernel::Ptr kernel = getKernel();
    _resourceManager = kernel->findModule<aftermath::core::ResourceManager>();
    _viewManager = kernel->findModule<aftermath::core::ViewManager>();
    _contentBuilder = kernel->getContentBuilder();
    _system = kernel->findModule<aftermath::ecs::EntitySystem>();

    /*_view = static_cast<aftermath::render::gl::SDLView*>(_viewManager->findViewByName("1"));*/
	_view = static_cast<aftermath::render::gl::OGLView*>(_viewManager->findViewByName("1"));
    if(_view.valid())
        _view->addObserver(this);

    #if defined WIN32
	std::string restest = "C:/Users/C/Documents/aftermath/resources/";
    std::string configfile = "C:/Users/C/Documents/aftermath/src/Apps/AMTest/Config.xml";
    //std::string restest = "C:/Users/C/Documents/aftermath/resources";
#elif defined __unix
    std::string configfile = "/home/Chris/Documents/Aftermath/src/Apps/AMTest/Config.xml";
    std::string restest = "/home/Chris/Documents/Aftermath/resources/";
#elif defined __MAC_NA
    std::string configfile = "/Users/CMac/Documents/fa2/src/viewer/SDLViewer";
    std::string restest = "/Users/CMac/Documents/fa2/restest";
#endif
//
    aftermath::render::Effect::Ptr effect = _resourceManager->load<aftermath::render::Effect>(restest + std::string("effects/BoundingBox.fx"));
    aftermath::render::Effect::Ptr effectDirectionalLight = _resourceManager->load<aftermath::render::Effect>(restest + std::string("effects/DirectionalLight.fx"));
    aftermath::render::Effect::Ptr effectGaussianBlur = _resourceManager->load<aftermath::render::Effect>(restest + std::string("effects/GaussianBlur.fx"));
    aftermath::render::Effect::Ptr effectGBuffer = _resourceManager->load<aftermath::render::Effect>(restest + std::string("effects/GBuffer.fx"));
    aftermath::render::Effect::Ptr effectFullScreenQuad = _resourceManager->load<aftermath::render::Effect>(restest + std::string("effects/FullScreenQuad.fx"));
    aftermath::render::Effect::Ptr effectGodRays = _resourceManager->load<aftermath::render::Effect>(restest + std::string("effects/GodRays.fx"));


//////FBO SETUP//////
        // COLOR TEXTURE

    aftermath::ecs::Camera::Ptr cam = new aftermath::ecs::Camera();
    cam->setClearColor(aftermath::math::Vector4f(0.0f, 0.0f, 0.1f, 1.0));
    cam->setClearFlags(aftermath::ecs::Camera::ClearAll);
    cam->setClippingPlanes(aftermath::math::Vector2f(0.1f, 1000.0f));
    cam->setFOV(aftermath::math::Vector2f(1.0f, 45.0f));
    cam->setProjectionType(aftermath::ecs::Camera::Perspective);

    aftermath::render::Texture::Ptr cameraColor = _contentBuilder->createTexture();
    cameraColor->setWidth(1280);
    cameraColor->setHeight(720);
    cameraColor->setPixelFormat(aftermath::render::PixelFormat::RGBA);
    cameraColor->setWrapMode(aftermath::render::Texture::ClampToEdge, aftermath::render::Texture::ClampToEdge, aftermath::render::Texture::ClampToEdge);
    cameraColor->setFilterMode(aftermath::render::Texture::Linear, aftermath::render::Texture::Linear);

    aftermath::render::Texture::Ptr cameraNormal = _contentBuilder->createTexture();
    cameraNormal->setWidth(1280);
    cameraNormal->setHeight(720);
    cameraNormal->setPixelFormat(aftermath::render::PixelFormat::RGBA);
    cameraNormal->setWrapMode(aftermath::render::Texture::ClampToEdge, aftermath::render::Texture::ClampToEdge, aftermath::render::Texture::ClampToEdge);
    cameraNormal->setFilterMode(aftermath::render::Texture::Linear, aftermath::render::Texture::Linear);

    aftermath::render::Texture::Ptr cameraDepth = _contentBuilder->createTexture();
    cameraDepth->setWidth(1280);
    cameraDepth->setHeight(720);
    cameraDepth->setPixelFormat(aftermath::render::PixelFormat::RG32F);
    cameraDepth->setWrapMode(aftermath::render::Texture::ClampToEdge, aftermath::render::Texture::ClampToEdge, aftermath::render::Texture::ClampToEdge);
    cameraDepth->setFilterMode(aftermath::render::Texture::Linear, aftermath::render::Texture::Linear);

    aftermath::render::Texture::Ptr cameraOcclusion = _contentBuilder->createTexture();
    cameraOcclusion->setWidth(1280);
    cameraOcclusion->setHeight(720);
    cameraOcclusion->setPixelFormat(aftermath::render::PixelFormat::RGB);
    cameraOcclusion->setWrapMode(aftermath::render::Texture::ClampToEdge, aftermath::render::Texture::ClampToEdge, aftermath::render::Texture::ClampToEdge);
    cameraOcclusion->setFilterMode(aftermath::render::Texture::Linear, aftermath::render::Texture::Linear);

    aftermath::render::RenderTarget::Ptr cameraGBuffer = _contentBuilder->createRenderTarget();
    cameraGBuffer->attachTarget(aftermath::render::RenderTarget::Color0, cameraColor.get());
    cameraGBuffer->attachTarget(aftermath::render::RenderTarget::Color1, cameraNormal.get());
    cameraGBuffer->attachTarget(aftermath::render::RenderTarget::Color2, cameraDepth.get());
    cameraGBuffer->attachTarget(aftermath::render::RenderTarget::Color3, cameraOcclusion.get());
    cameraGBuffer->useHardwareDepthAttachment(true);


    //// SUN
    aftermath::render::Texture::Ptr sunColor = _contentBuilder->createTexture();
    sunColor->setWidth(1280);
    sunColor->setHeight(720);
    sunColor->setPixelFormat(aftermath::render::PixelFormat::RGBA);
    sunColor->setWrapMode(aftermath::render::Texture::ClampToEdge, aftermath::render::Texture::ClampToEdge, aftermath::render::Texture::ClampToEdge);
    sunColor->setFilterMode(aftermath::render::Texture::Linear, aftermath::render::Texture::Linear);

    aftermath::render::Texture::Ptr sunNormal = _contentBuilder->createTexture();
    sunNormal->setWidth(1280);
    sunNormal->setHeight(720);
    sunNormal->setPixelFormat(aftermath::render::PixelFormat::RGBA);
    sunNormal->setWrapMode(aftermath::render::Texture::ClampToEdge, aftermath::render::Texture::ClampToEdge, aftermath::render::Texture::ClampToEdge);
    sunNormal->setFilterMode(aftermath::render::Texture::Linear, aftermath::render::Texture::Linear);

    aftermath::render::Texture::Ptr sunDepth = _contentBuilder->createTexture();
    sunDepth->setWidth(1280);
    sunDepth->setHeight(720);
    sunDepth->setPixelFormat(aftermath::render::PixelFormat::RG32F);
    sunDepth->setWrapMode(aftermath::render::Texture::ClampToEdge, aftermath::render::Texture::ClampToEdge, aftermath::render::Texture::ClampToEdge);
    sunDepth->setFilterMode(aftermath::render::Texture::Linear, aftermath::render::Texture::Linear);

    aftermath::render::RenderTarget::Ptr sunGBuffer = _contentBuilder->createRenderTarget();
    sunGBuffer->attachTarget(aftermath::render::RenderTarget::Color0, sunColor.get());
    sunGBuffer->attachTarget(aftermath::render::RenderTarget::Color1, sunNormal.get());
    sunGBuffer->attachTarget(aftermath::render::RenderTarget::Color2, sunDepth.get());
    sunGBuffer->useHardwareDepthAttachment(true);

    //// Directional Light
    aftermath::render::Texture::Ptr directionalLightColor = _contentBuilder->createTexture();
    directionalLightColor->setWidth(1280);
    directionalLightColor->setHeight(720);
    directionalLightColor->setPixelFormat(aftermath::render::PixelFormat::RGBA);
    directionalLightColor->setWrapMode(aftermath::render::Texture::ClampToEdge, aftermath::render::Texture::ClampToEdge, aftermath::render::Texture::ClampToEdge);
    directionalLightColor->setFilterMode(aftermath::render::Texture::Linear, aftermath::render::Texture::Linear);

    aftermath::render::RenderTarget::Ptr directionalLightGBuffer = _contentBuilder->createRenderTarget();
    directionalLightGBuffer->attachTarget(aftermath::render::RenderTarget::Color0, directionalLightColor.get());


    //// Gaussian Blur H
    aftermath::render::Texture::Ptr gaussianColorH = _contentBuilder->createTexture();
    gaussianColorH->setWidth(1280);
    gaussianColorH->setHeight(720);
    gaussianColorH->setPixelFormat(aftermath::render::PixelFormat::RG32F);
    gaussianColorH->setWrapMode(aftermath::render::Texture::ClampToEdge, aftermath::render::Texture::ClampToEdge, aftermath::render::Texture::ClampToEdge);
    gaussianColorH->setFilterMode(aftermath::render::Texture::Linear, aftermath::render::Texture::Linear);

    aftermath::render::RenderTarget::Ptr gaussianGBufferH = _contentBuilder->createRenderTarget();
    gaussianGBufferH->attachTarget(aftermath::render::RenderTarget::Color0, gaussianColorH.get());

    //// Gaussian Blur V
    aftermath::render::Texture::Ptr gaussianColorV = _contentBuilder->createTexture();
    gaussianColorV->setWidth(1280);
    gaussianColorV->setHeight(720);
    gaussianColorV->setPixelFormat(aftermath::render::PixelFormat::RG32F);
    gaussianColorV->setWrapMode(aftermath::render::Texture::ClampToEdge, aftermath::render::Texture::ClampToEdge, aftermath::render::Texture::ClampToEdge);
    gaussianColorV->setFilterMode(aftermath::render::Texture::Linear, aftermath::render::Texture::Linear);

    aftermath::render::RenderTarget::Ptr gaussianGBufferV = _contentBuilder->createRenderTarget();
    gaussianGBufferV->attachTarget(aftermath::render::RenderTarget::Color0, gaussianColorV.get());


    //// God Rays
    aftermath::render::Texture::Ptr godRaysColor = _contentBuilder->createTexture();
    godRaysColor->setWidth(1280);
    godRaysColor->setHeight(720);
    godRaysColor->setPixelFormat(aftermath::render::PixelFormat::RGBA);
    godRaysColor->setWrapMode(aftermath::render::Texture::ClampToEdge, aftermath::render::Texture::ClampToEdge, aftermath::render::Texture::ClampToEdge);
    godRaysColor->setFilterMode(aftermath::render::Texture::Linear, aftermath::render::Texture::Linear);

    aftermath::render::RenderTarget::Ptr godRaysGBuffer = _contentBuilder->createRenderTarget();
    godRaysGBuffer->attachTarget(aftermath::render::RenderTarget::Color0, godRaysColor.get());



    aftermath::render::Texture::Ptr pointLightColor = _contentBuilder->createTexture();
    pointLightColor->setWidth(1280);
    pointLightColor->setHeight(720);
    pointLightColor->setPixelFormat(aftermath::render::PixelFormat::RGBA);
    pointLightColor->setWrapMode(aftermath::render::Texture::ClampToEdge, aftermath::render::Texture::ClampToEdge, aftermath::render::Texture::ClampToEdge);
    pointLightColor->setFilterMode(aftermath::render::Texture::Linear, aftermath::render::Texture::Linear);

    aftermath::render::RenderTarget::Ptr pointLightGBuffer = _contentBuilder->createRenderTarget();
    pointLightGBuffer->attachTarget(aftermath::render::RenderTarget::Color0, pointLightColor.get());



//////CAMERA SETUP////////////

    aftermath::ecs::Entity::Ptr sceneEntity = _system->createEntity();
    _deferredBin = new aftermath::ecs::RenderBin();
    _deferredBin->setName("MainDeferred");
    _deferredBin->setId(aftermath::ecs::RenderBin::Geometry);
    sceneEntity->addComponent(_deferredBin.get());

    aftermath::ecs::Entity::Ptr blurEntity = _system->createEntity();
    aftermath::ecs::RenderBin::Ptr blurBinH = new aftermath::ecs::RenderBin();
    blurBinH->setName("BlurBinH");
    blurBinH->setId(aftermath::ecs::RenderBin::ImageEffect);
    blurEntity->addComponent(blurBinH.get());

    aftermath::ecs::RenderBin::Ptr blurBinV = new aftermath::ecs::RenderBin();
    blurBinV->setName("BlurBinV");
    blurBinV->setId(aftermath::ecs::RenderBin::ImageEffect+1);
    blurEntity->addComponent(blurBinV.get());

    aftermath::ecs::RenderBin::Ptr godRaysBin = new aftermath::ecs::RenderBin();
    godRaysBin->setName("GodRaysBin");
    godRaysBin->setId(aftermath::ecs::RenderBin::Geometry + 3);
    sceneEntity->addComponent(godRaysBin.get());

    aftermath::ecs::RenderBin::Ptr lightBin = new aftermath::ecs::RenderBin();
    lightBin->setName("LightBin");
    lightBin->setId(aftermath::ecs::RenderBin::Transparent);
    sceneEntity->addComponent(lightBin.get());


   
    _finalBin = new aftermath::ecs::RenderBin();
    _finalBin->setName("MainFinal");
    _finalBin->setId(aftermath::ecs::RenderBin::Overlay);
    sceneEntity->addComponent(_finalBin.get());


   /* aftermath::math::Matrix4f matrix3(1.0f);
    matrix3 = aftermath::math::translate(matrix3, aftermath::math::Vector3f(-50.0f, 40.0f, 32.0f));
    matrix3 = aftermath::math::rotate(matrix3, -45.0f, aftermath::math::Vector3f(1.0f, 0.0f, 0.0f));*/

    //_cube = new Cube(_system->createEntity(), 8, 8, 4);
    //_cube->_transformation->setMatrix(matrix3);
    //_cube->_renderInfo->setTechniqueName("GBuffer");
    //_cube->_renderInfo->setEffect(effectGBuffer.get());
    //_cube->setParent(_deferredBin.get());



    _model = new Model(_system->createEntity(), restest + std::string("/models/rawPerson.txt"));
    {
        _model->_transformation->setPosition(aftermath::math::Vector3f(-50.0f, 0.0f, 0.0f));
        _model->_transformation->setScale(aftermath::math::Vector3f(25.0f, 25.0f, 25.0f));
        _model->_transformation->setRotation(aftermath::math::Vector3f(0.0f, 120.0f, 0.0f));

        _model->_renderInfo->setTechniqueName("GBuffer");
        _model->_renderInfo->setEffect(effectGBuffer.get());
        _model->setParent(_deferredBin.get());
    }


    


    _md5Model = new MD5Model(_system->createEntity());
    {
        _md5Model->_transformation->setRotation(aftermath::math::Quaternionf(aftermath::math::Vector3f(90.0f, 275.0f, 0.0f)));
        _md5Model->_transformation->setScale(aftermath::math::Vector3f(0.5f, 0.5f, 0.5f));

        _md5Model->_transformation->setParent(_deferredBin.get());
        _md5Model->_renderInfo->setTechniqueName("GBuffer");
        _md5Model->_renderInfo->setEffect(effectGBuffer.get());

        aftermath::ecs::SkinnedModel::Ptr skinnedModelComponent = new aftermath::ecs::SkinnedModel();
        skinnedModelComponent->_modelName = restest + "/models/boblampclean.md5mesh";
        skinnedModelComponent->_animationFiles.push_back(restest + "/models/boblampclean.md5anim");
        skinnedModelComponent->_animationIndex = 0;
        skinnedModelComponent->setDirty();
        _md5Model->_entity->addComponent(skinnedModelComponent.get());
    }

    //    _md5Model2 = new MD5Model(_system->createEntity());
    //{
    //    _md5Model2->_transformation->setPosition(aftermath::math::Vector3f(20.0f, 0.0f, 2.0f));
    //    _md5Model2->_transformation->setRotation(aftermath::math::Quaternionf(aftermath::math::Vector3f(90.0f, 275.0f, 0.0f)));
    //    _md5Model2->_transformation->setScale(aftermath::math::Vector3f(0.5f, 0.5f, 0.5f));

    //    _md5Model2->_transformation->setParent(_deferredBin.get());
    //    _md5Model2->_renderInfo->setTechniqueName("GBuffer");
    //    _md5Model2->_renderInfo->setEffect(effectGBuffer.get());

    //    aftermath::ecs::SkinnedModel::Ptr skinnedModelComponent = new aftermath::ecs::SkinnedModel();
    //    skinnedModelComponent->_modelName = restest + "models/boblampclean.md5mesh";
    //    skinnedModelComponent->_animationFiles.push_back(restest + "models/boblampclean.md5anim");
    //    skinnedModelComponent->_animationIndex = 0;
    //    skinnedModelComponent->setDirty();
    //    _md5Model2->_entity->addComponent(skinnedModelComponent.get());
    //}

    

    

    _heightMap = new HeightMap(_system->createEntity(), 512, 512);
    {
        _heightMap->_transformation->setPosition(aftermath::math::Vector3f(-256.0f, 0.0f, -256.0f));
        _heightMap->_renderInfo->setTechniqueName("GBuffer");
        _heightMap->_renderInfo->setEffect(effectGBuffer.get());
        _heightMap->setParent(_deferredBin.get());
    }


    _playerModel = new Cube(_system->createEntity(), 8.0f);
    {
        _playerModel->_transformation->setPosition(aftermath::math::Vector3f(0.0f, 0.0f, 256.0f));
        aftermath::math::Matrix4f pcm = aftermath::math::lookAt(_playerModel->_transformation->getPosition(), _model->_transformation->getPosition(), aftermath::math::Vector3f(0.0f, 1.0f, 0.0f));
        //_playerModel->_transformation->setRotation(aftermath::math::Quaternionf(pcm));

        _playerModel->_renderInfo->setTechniqueName("GBuffer");
        _playerModel->_renderInfo->setEffect(effectGBuffer.get());
        _playerModel->setParent(_deferredBin.get());
    }

    aftermath::core::ViewManager::Ptr viewManager = getKernel()->findModule<aftermath::core::ViewManager>();
    if(!viewManager.valid())
        std::cerr << "RenderManager not valid in cameraProcessor%d\n" << std::endl;

    aftermath::render::Viewport::Ptr viewport = viewManager->findViewportByName("1");

	_playerCamera = new Camera(_system->createEntity());
    {
        _playerCamera->_transformation->setPosition(aftermath::math::Vector3f(0.0f, 20.0f, 0.0f));

        _playerCamera->setParent(_playerModel->_transformation.get());

        _playerCamera->_cameraComponent->setFOV(aftermath::math::Vector2f(1.0f, 45.0f));
        _playerCamera->_cameraComponent->setClippingPlanes(aftermath::math::Vector2f(0.1f, 2000.0f));
        _playerCamera->_cameraComponent->addRenderBinId(_deferredBin->getId());
        _playerCamera->_cameraComponent->setRenderTarget(cameraGBuffer.get());
        _playerCamera->_cameraComponent->setRenderType(aftermath::ecs::Camera::Deferred);
        _playerCamera->_cameraComponent->setProjectionType(aftermath::ecs::Camera::Perspective);
        _playerCamera->_cameraComponent->setViewport(viewport.get());
        _playerCamera->_cameraComponent->setClearColor(aftermath::math::Vector4f(0.3, 0.3, 0.3, 1.0));
    }




    //////////// SUN ///////////////
        //aftermath::math::Matrix4f sunCameraMatrix(1.0f);
        
    //sunCameraMatrix = aftermath::math::translate(sunCameraMatrix, );
    _sunCamera = new Camera(_system->createEntity());
    {
        _sunCamera->_transformation->setPosition(aftermath::math::Vector3f(100.0f, 100.0f, 0.0f));
        _sunCamera->setParent(_deferredBin.get());
        _sunCamera->_cameraComponent->setFOV(aftermath::math::Vector2f(1.0f, 45.0f));
        _sunCamera->_cameraComponent->setClippingPlanes(aftermath::math::Vector2f(0.1f, 2000.0f));
        _sunCamera->_cameraComponent->addRenderBinId(_deferredBin->getId());
        _sunCamera->_cameraComponent->setRenderTarget(sunGBuffer.get());
        _sunCamera->_cameraComponent->setRenderType(aftermath::ecs::Camera::Deferred);
        _sunCamera->_cameraComponent->setProjectionType(aftermath::ecs::Camera::Perspective);
        _sunCamera->_cameraComponent->setViewport(viewport.get());
        _sunCamera->_cameraComponent->setClearColor(aftermath::math::Vector4f(0.3, 0.3, 0.3, 1.0));
    }


    ////////////////////////////////////////////


    /////////////// BLUR SHADOW MAP//////////////
    _GaussianBlurHorizontalQuad = new FullScreenQuad(_system->createEntity());
    _GaussianBlurHorizontalQuad->_renderInfo->setTechniqueName("GaussianBlurHorizontal");
    _GaussianBlurHorizontalQuad->_renderInfo->setEffect(effectGaussianBlur.get());
    _GaussianBlurHorizontalQuad->setParent(blurBinH.get());
    _GaussianBlurHorizontalQuad->_renderInfo->getUniforms().set("u_shadowMap", sunDepth.get());
    _GaussianBlurHorizontalQuad->_renderInfo->getUniforms().set("u_shadowMapDimensions", aftermath::math::Vector2f((float)sunColor->getWidth(), (float)sunColor->getHeight()));

    _gaussianBlurHorizontalCamera = new Camera(_system->createEntity());
    _gaussianBlurHorizontalCamera->setParent(_GaussianBlurHorizontalQuad->_transformation.get());
    _gaussianBlurHorizontalCamera->_cameraComponent->setClippingPlanes(aftermath::math::Vector2f(1.0f, 2000.0f));
    _gaussianBlurHorizontalCamera->_cameraComponent->addRenderBinId(blurBinH->getId());
    _gaussianBlurHorizontalCamera->_cameraComponent->setRenderTarget(gaussianGBufferH.get());
    _gaussianBlurHorizontalCamera->_cameraComponent->setRenderType(aftermath::ecs::Camera::Deferred);
    _gaussianBlurHorizontalCamera->_cameraComponent->setProjectionType(aftermath::ecs::Camera::Orthographic);
    _gaussianBlurHorizontalCamera->_cameraComponent->setViewport(viewport.get()); ///////////////////////////////////////////  <-  WHAT IF X IS NOT 0
    _gaussianBlurHorizontalCamera->_cameraComponent->setClearColor(aftermath::math::Vector4f(0.3, 0.3, 0.3, 1.0));



    _GaussianBlurVerticalQuad = new FullScreenQuad(_system->createEntity());
    _GaussianBlurVerticalQuad->_renderInfo->setTechniqueName("GaussianBlurVertical");
    _GaussianBlurVerticalQuad->_renderInfo->setEffect(effectGaussianBlur.get());
    _GaussianBlurVerticalQuad->setParent(blurBinV.get());
    _GaussianBlurVerticalQuad->_renderInfo->getUniforms().set("u_shadowMap", gaussianColorH.get());
    _GaussianBlurVerticalQuad->_renderInfo->getUniforms().set("u_shadowMapDimensions", aftermath::math::Vector2f((float)sunColor->getWidth(), (float)sunColor->getHeight()));
    
    _gaussianBlurVerticalCamera = new Camera(_system->createEntity());
    _gaussianBlurVerticalCamera->setParent(_GaussianBlurVerticalQuad->_transformation.get());
    _gaussianBlurVerticalCamera->_cameraComponent->setClippingPlanes(aftermath::math::Vector2f(1.0f, 2000.0f));
    _gaussianBlurVerticalCamera->_cameraComponent->addRenderBinId(blurBinV->getId());
    _gaussianBlurVerticalCamera->_cameraComponent->setRenderTarget(gaussianGBufferV.get());
    _gaussianBlurVerticalCamera->_cameraComponent->setRenderType(aftermath::ecs::Camera::Deferred);
    _gaussianBlurVerticalCamera->_cameraComponent->setProjectionType(aftermath::ecs::Camera::Orthographic);
    _gaussianBlurVerticalCamera->_cameraComponent->setViewport(viewport.get()); ///////////////////////////////////////////  <-  WHAT IF X IS NOT 0
    _gaussianBlurVerticalCamera->_cameraComponent->setClearColor(aftermath::math::Vector4f(0.3, 0.3, 0.3, 1.0));



//////    ////////////// Directional light ////////////////
    _lightPlane = new FullScreenQuad(_system->createEntity());
    _lightPlane->_renderInfo->setTechniqueName("DirectionalLight");
    _lightPlane->_renderInfo->setEffect(effectDirectionalLight.get());
    _lightPlane->setParent(lightBin.get());

    aftermath::render::UniformBuffer& lightPlaneUniforms = _lightPlane->_renderInfo->getUniforms();
    lightPlaneUniforms.set("u_gBufferColor", cameraColor.get());
    lightPlaneUniforms.set("u_gBufferNormal", cameraNormal.get());
    lightPlaneUniforms.set("u_gBufferDepth", cameraDepth.get());
    lightPlaneUniforms.set("u_sunDepth", gaussianColorV.get());
    lightPlaneUniforms.set("u_lightColorVector", aftermath::math::Vector3f(1.0f));
    lightPlaneUniforms.set("u_lightDirection", aftermath::math::normalize(aftermath::math::extractHeading<float>(_sunCamera->_transformation->getGlobalMatrix())));


    _lightCamera = new Camera(_system->createEntity());
    _lightCamera->setParent(_lightPlane->_transformation.get());
    _lightCamera->_cameraComponent->setClippingPlanes(aftermath::math::Vector2f(1.0f, 2000.0f));
    _lightCamera->_cameraComponent->addRenderBinId(lightBin->getId());
    _lightCamera->_cameraComponent->setRenderTarget(directionalLightGBuffer.get());
    _lightCamera->_cameraComponent->setRenderType(aftermath::ecs::Camera::Deferred);
    _lightCamera->_cameraComponent->setProjectionType(aftermath::ecs::Camera::Orthographic);
    _lightCamera->_cameraComponent->setViewport(viewport.get()); ///////////////////////////////////////////  <-  WHAT IF X IS NOT 0
    _lightCamera->_cameraComponent->setClearColor(aftermath::math::Vector4f(0.3, 0.3, 0.3, 1.0));


    _cachedTextures.push_back(directionalLightColor.get());
    _cachedTextures.push_back(cameraOcclusion.get());
    _cachedTextures.push_back(sunDepth.get());
    _cachedTextures.push_back(sunNormal.get());
    _cachedTextures.push_back(cameraNormal.get());
    _currentCachedTexture = 0;



    ////////////// Final Image ///////////////////////
//    LEFT VIEWPORT
    _finalScreen = new FullScreenQuad(_system->createEntity());
    _finalScreen->_renderInfo->setTechniqueName("FullScreenQuad");
    _finalScreen->_renderInfo->setEffect(effectFullScreenQuad.get());
    _finalScreen->_renderInfo->getUniforms().set("textureUnit0", _cachedTextures[_currentCachedTexture].get());
    _finalScreen->setParent(_finalBin.get());

    _finalCamera = new Camera(_system->createEntity());
    _finalCamera->setParent(_finalScreen->_transformation.get());
    _finalCamera->_cameraComponent->setClippingPlanes(aftermath::math::Vector2f(1.0f, 2000.0f));
    _finalCamera->_cameraComponent->addRenderBinId(_finalBin->getId());
    _finalCamera->_cameraComponent->setRenderType(aftermath::ecs::Camera::Forward);
    _finalCamera->_cameraComponent->setProjectionType(aftermath::ecs::Camera::Orthographic);
    _finalCamera->_cameraComponent->setViewport(viewport.get()); ///////////////////////////////////////////  <-  WHAT IF X IS NOT 0
    _finalCamera->_cameraComponent->setClearColor(aftermath::math::Vector4f(0.3, 0.3, 0.3, 1.0));

    //LOG_SUCCESS("APPLICATION INITIALIZED%d\n", 1);

    _controller.MapButton(move_forward, VK_UP);
    _controller.MapButton(move_left, VK_LEFT);
    _controller.MapButton(move_right, VK_RIGHT);
    _controller.MapButton(move_backward, VK_DOWN);
    /*_sdlController.MapButton(move_rotate_left, SDLK_LEFT);
    _sdlController.MapButton(move_rotate_right, SDLK_RIGHT);
    _sdlController.MapButton(mouse_rotate, SDL_BUTTON_LEFT);
    _sdlController.MapButton(object_forward, SDLK_UP);
    _sdlController.MapButton(object_backward, SDLK_DOWN);
    _sdlController.MapButton(move_up, SDLK_n);
    _sdlController.MapButton(move_down, SDLK_m);*/

}

float approach(float dt, float goal, float current)
{
    float diff = goal - current;

    if(diff > dt)
        return current + dt;
    if(diff < -dt)
        return current - dt;

    return goal;
}

void Application::eventTick(void* data)
{

    //math::Matrix4f matrix = _cube->_transformation->getLocalMatrix();
    //matrix = math::rotate(matrix, 1.0f, math::Vector3f(0.0f, 1.0f, 0.0f));
    //_cube->_transformation->setMatrix(matrix);
    
    float speed = 100.0f;
    float rotateSpeed = 1.0f;
    aftermath::ecs::Transform::Ptr currentTransformation = _playerModel->_transformation;

    aftermath::math::Vector3f targetVelocity(0.0f);
    static aftermath::math::Vector3f velocity(0.0f);

    if(_controller.GetButtonState(move_forward))
    {
        targetVelocity += currentTransformation->getRotation().getForwardVector() * speed;
    }
  
	if (_controller.GetButtonState(move_backward))
    {
        targetVelocity += currentTransformation->getRotation().getForwardVector() * -speed;
    }

	if (_controller.GetButtonState(move_left))
    {
        targetVelocity += currentTransformation->getRotation().getRightVector() * -speed;
    }

	if (_controller.GetButtonState(move_right))
    {
        targetVelocity += currentTransformation->getRotation().getRightVector() * speed;
    }

	if (_controller.GetButtonState(move_rotate_left))
    {
        currentTransformation->setRotation(currentTransformation->getRotation() * aftermath::math::Quaternionf(aftermath::math::Vector3f(0.0f, rotateSpeed, 0.0f)));
    }

	if (_controller.GetButtonState(move_rotate_right))
    {
        currentTransformation->setRotation(currentTransformation->getRotation() * aftermath::math::Quaternionf(aftermath::math::Vector3f(0.0f, -rotateSpeed, 0.0f)));
    }
  
	if (_controller.GetButtonState(move_up))
    {
        targetVelocity += currentTransformation->getRotation().getUpVector() * speed;
    }
  
	if (_controller.GetButtonState(move_down))
    {
        targetVelocity += currentTransformation->getRotation().getUpVector() * -speed;
    }

    
    targetVelocity = aftermath::math::normalize(targetVelocity);
    targetVelocity = targetVelocity * speed;
    float dt = 1001.0/60000.0;
    velocity.x = approach(dt * 40.0, targetVelocity.x, velocity.x);
    velocity.y = approach(dt * 40.0, targetVelocity.y, velocity.y);
    velocity.z = approach(dt * 40.0, targetVelocity.z, velocity.z);

    currentTransformation->setPosition(_playerModel->_transformation->getPosition() + velocity * dt);

  
//    _playerModel->_transformation->setLocalMatrix(aftermath::math::lookAt(_playerModel->_transformation->getPosition(), _model->_transformation->getPosition(), aftermath::math::Vector3f(0.0f, 1.0f, 0.0f)));

    //math::Vector3f vpos = _sunCamera->_transformation->getPosition() + math::normalize(math::extractPitch(_sunCamera->_transformation->getLocalMatrix())) * 1.0f;
    //math::Matrix4f rotation = math::createRotation(_sunCamera->_transformation->getLocalMatrix());
    //math::Matrix4f translation=  math::createTranslation(vpos);
    //_sunCamera->_transformation->setMatrix(translation * rotation); 

    aftermath::math::Matrix4f lookAtMatrix = aftermath::math::lookAt(_sunCamera->getTransformation()->getPosition(), _model->getTransformation()->getPosition(), aftermath::math::Vector3f(0.0f, 1.0f, 0.0f));
    {
        _sunCamera->_transformation->setRotation(aftermath::math::Quaternionf(lookAtMatrix));
    }



     /*   aftermath::math::Quaternionf lookAtRot(lookAtMatrix);
        std::cerr << lookAtRot.getEuler() << std::endl;
        aftermath::math::Matrix4f lookAtTranslation = aftermath::math::createTranslation(lookAtMatrix);
    _sunCamera->_transformation->setRotation(aftermath::math::Quaternionf(lookAtMatrix));
    _sunCamera->_transformation->setLocalMatrix(lookAtTranslation * _sunCamera->_transformation->getRotation().getMatrix());*/
    //_sunCamera->_transformation->setLocalMatrix(aftermath::math::lookAt(_sunCamera->getTransformation()->getPosition(), _model->getTransformation()->getPosition(), aftermath::math::Vector3f(0.0f, 1.0f, 0.0f)));




    aftermath::math::Vector3f lightdir = aftermath::math::normalize(aftermath::math::extractHeading<float>(_sunCamera->_transformation->getGlobalMatrix()));
    aftermath::render::UniformBuffer& lightPlaneUniforms = _lightPlane->_renderInfo->getUniforms();
    lightPlaneUniforms.set("u_lightDirection", lightdir);
    lightPlaneUniforms.set("u_lightView", aftermath::math::inverse(_sunCamera->_transformation->getGlobalMatrix()));
    /*lightPlaneUniforms.set("u_lightProjection", _sunCamera->_cameraComponent->getProjection()->getMatrix());*/
    lightPlaneUniforms.set("u_lightProjection", _sunCamera->_cameraComponent->getProjectionMatrix());
    lightPlaneUniforms.set("u_cameraView", aftermath::math::inverse(_playerCamera->_transformation->getGlobalMatrix()));

    aftermath::math::Vector3f frustumCorners[8];
    aftermath::math::TFrustum<float> frustum = aftermath::math::createFrustum(_playerCamera->_cameraComponent->getProjectionMatrix(), aftermath::math::Matrix4f(1.0f));
    
    frustum.getFrustumCorners(frustumCorners);
    aftermath::math::Matrix4f farClip( aftermath::math::Vector4f(frustumCorners[4], 0.0),
                            aftermath::math::Vector4f(frustumCorners[5], 0.0),
                            aftermath::math::Vector4f(frustumCorners[6], 0.0),
                            aftermath::math::Vector4f(frustumCorners[7], 0.0));
                                
    lightPlaneUniforms.set("u_frustumCorners", farClip);

}

void Application::onViewEvent(aftermath::render::View* view, aftermath::core::ViewEvent* data)
{
        switch(data->getType())
        {
        case aftermath::core::ViewEvent::Undefined:
            break;
        case aftermath::core::ViewEvent::MouseUp:
        std::cerr << "MouseUp event" << std::endl;
        break;

        case aftermath::core::ViewEvent::MouseDown:
            std::cerr << "MouseDown event" << std::endl;
            _currentCachedTexture++;
            if(_currentCachedTexture >= _cachedTextures.size())
                _currentCachedTexture = 0;
            _finalScreen->_renderInfo->getUniforms().set("textureUnit0", _cachedTextures[_currentCachedTexture].get());
            break;

        case aftermath::core::ViewEvent::MouseMove:
            {
            //std::cerr << "MouseMove: " << math::Vector2u(data->dx, data->dy) << std::endl;
            //_sdlController.SetState(
            
            }
        break;

        case aftermath::core::ViewEvent::KeyDown:
            {
                _controller.SetState(data->buttonId, true);
            }
            break;
        case aftermath::core::ViewEvent::KeyUp:
            {
				_controller.SetState(data->buttonId, false);
            }
            break;

        };
}
