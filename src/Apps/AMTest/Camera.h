#pragma once

#include <Aftermath/ECS/Entity.h>
#include <Aftermath/ECS/components/Camera.h>
#include "Node.h"

class Camera: public Node
{
    AFTERMATH_META(, Camera, Node);
public:
	Camera(aftermath::ecs::Entity* entity)
        : Node(entity)
	{
		_cameraComponent = new aftermath::ecs::Camera();
		_cameraComponent->setName("Camera");

		_entity->addComponent(_cameraComponent.get());

	}
	aftermath::ecs::Camera::Ptr _cameraComponent;
};
