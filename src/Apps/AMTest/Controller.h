////////////////////////////////////////////
////									////
////	This file was created by:		////
////	Christoffer Pettersson			////
////	B00191431						////
////									////
////////////////////////////////////////////

#ifndef CONTROLMANAGER_H
#define CONTROLMANAGER_H

#include <map>
#include <vector>
#include <iostream>

/*
Instructions
------------
1. Define handle for an action, e.g const int CAMERA_FORWARD = 0;
2. Map this action with a key on the keyboard, e.g controller->MapButton(CAMERA_FORWARD, SDLK_w);
3. Update the state of a key when a button is pressed, using SDL Events: controller->SetState(sdlEvent.key.keysym.sym, state);
4. Check if a button is active, e.g controller->GetButtonState(CAMERA_FORWARD)
*/

struct Button
{
	bool state;
	bool isSingleHit;
};

class Controller
{
public:
	Controller():numMappings(0){};	
	void MapButton(int action, int button); //Maps a button to a handle. Button will remain active until released.
	void MapButtonSingleHit(int action, int button); //Maps a button to a handle. Button does not remain active while holding down the button.
	bool GetButtonState(int action); //Returns the state of an action.
	void SetState(int button,bool state); //Update the state of a button
	inline int GetCount(){numMappings++; return numMappings-1;} //Used when button values are not assigned using an enum. Returns an index.
protected:
private:
	std::map<int, int> mappings;
	std::map<int, int>::iterator it;
	std::map<int, Button> bMap;
	std::map<int, Button>::iterator bMapItr;
	int numMappings;

};

#endif