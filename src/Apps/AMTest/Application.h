#pragma once

#include "Camera.h"
#include "Plane.h"
#include "Node.h"
#include "Cube.h"

#include "Model.h"



#include <Aftermath/core/Module.h>
#include <Aftermath/ECS/EntitySystem.h>
#include <Aftermath/ECS/components/Transform.h>
#include <Aftermath/ECS/Entity.h>
//#include <Aftermath/render/gl/SDLView.h>
#include <Aftermath/render/gl/OGLView.h>
#include <Aftermath/utils/ShaderReader.h>
#include <Aftermath/render/gl/OGLContentBuilder.h>
#include <Aftermath/core/ViewManager.h>
#include <Aftermath/core/FileSystem.h>
#include <Aftermath/utils/EffectReader.h>
#include <Aftermath/utils/ShaderReader.h>
#include <Aftermath/render/gl/OGLTexture.h>
#include <Aftermath/ECS/components/RenderBin.h>
#include "Controller.h"

class Application: public aftermath::core::Module, public aftermath::render::View::IViewObserver
{
    AFTERMATH_META(, Application, aftermath::core::Module);
public:
    Application();

private:
    enum controlls
    {
        move_forward = 0,
        move_backward,
        move_left,
        move_right,
        move_up,
        move_down,
        move_rotate_left,
        move_rotate_right,
        mouse_rotate,
        object_forward,
        object_backward,
        object_rotate_left,
        object_rotate_right
    };
    void eventInit(void* data);

    void eventTick(void* data);

    void onViewEvent(aftermath::render::View* view, aftermath::core::ViewEvent* data);

    virtual void onViewEvent(aftermath::render::View* view, aftermath::core::Event* data) { }

    aftermath::ecs::EntitySystem::Ptr _system;
    Node::Ptr _rootNode;
    HeightMap::Ptr _heightMap;
    /*FullScreenQuad::Ptr _finalPlane;
    FullScreenQuad::Ptr _sunFinalPlane;*/
    Plane::Ptr _finalPlane;
    Plane::Ptr _sunFinalPlane;

    Cube::Ptr _floor;

    Camera::Ptr _playerCamera;
    Cube::Ptr _playerModel;
    FullScreenQuad::Ptr _lightPlane;
    FullScreenQuad::Ptr _finalScreen;

    FullScreenQuad::Ptr _GaussianBlurHorizontalQuad;
    FullScreenQuad::Ptr _GaussianBlurVerticalQuad;
    Camera::Ptr _gaussianBlurHorizontalCamera;
    Camera::Ptr _gaussianBlurVerticalCamera;

    FullScreenQuad::Ptr _godRaysQuad;
    Camera::Ptr _godRaysCamera;

    Camera::Ptr _lightCamera;

    Camera::Ptr _sunCamera;
    Camera::Ptr _sunFinalCamera;
    Camera::Ptr _finalCamera;
    
    Cube::Ptr _cube;
    Model::Ptr _model;
    MD5Model::Ptr _md5Model;
    MD5Model::Ptr _md5Model2;
    //Cube::Ptr _rigidBody;
    
    aftermath::core::ResourceManager::Ptr _resourceManager;
    aftermath::core::ViewManager::Ptr _viewManager;
    aftermath::render::ContentBuilder::Ptr _contentBuilder;
    aftermath::core::FileSystem::Ptr _folderManager;

    //aftermath::render::gl::SDLView::Ptr _view;
	aftermath::render::gl::OGLView::Ptr _view;
    
    aftermath::render::Viewport::Ptr _viewport;

    Controller _controller;


//    aftermath::ecs::Scene::Ptr _mainScene;
    aftermath::ecs::RenderBin::Ptr _deferredBin;
    aftermath::ecs::RenderBin::Ptr _forwardBin;
    aftermath::ecs::RenderBin::Ptr _finalBin;

    std::vector<aftermath::render::Texture::Ptr> _cachedTextures;
    size_t _currentCachedTexture;

};
