#pragma once
#include "Application.h"
#include "Components.h"
#include "GBuffer.h"
#include <Aftermath/render/Window.h>
#include <Aftermath/render/RenderTarget.h>
#include <Aftermath/math/Vector2.h>
#include <iostream>
#include <vector>
#include <Aftermath/render/RenderDevice.h>
#include <Aftermath/render/Effect.h>
#include <Aftermath/render/Uniform.h>
#include <Aftermath/render/Mesh.h>




class ExampleGame : public Application
{
public:
	ExampleGame();

private:
	void mainLoop(float dt);

private:
	am::Window _mainWindow;

	enum CameraSlot
	{
		CameraSlot_Main,
		CameraSlot_Sun,
		CameraSlot_Count
	};

	std::vector<CameraComponent> _cameras;
	Framebuffer _framebuffer;
	am::Vector2u _mainWindowSize;

	const int _numGameObjects = 3;
	std::vector<GameObject> _gameObjects;
	std::vector<TransformComponent> _transformComponents;
	std::vector<GraphicsComponent> _graphicsComponents;
	std::vector<IInputComponent*> _inputComponents;

};