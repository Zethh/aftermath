#include "ExampleGame.h"
#include <Aftermath/render/RenderDevice.h>
#include <Aftermath/core/FileSystem.h>
#include <Aftermath/core/ResourceManager.h>
#include <Aftermath/utils/EffectReader.h>
#include <Aftermath/utils/ShaderReader.h>
#include <Aftermath/utils/ImageReader.h>

ExampleGame::ExampleGame()
{
	am::FileSystem::mountDirectory("C:/Users/C/Documents/aftermath/resources", true);
	am::EffectReader* effectReader = new am::EffectReader();
	effectReader->setGroupId("OpenGLDeferred");
	am::ResourceManager::registerResourceReader(effectReader);
	am::ResourceManager::registerResourceReader(new am::ShaderReader());
	am::ResourceManager::registerResourceReader(new am::ImageReader());

	_mainWindowSize = am::Vector2u(1280, 720);

	am::Window::Desc desc;
	desc.x = 0;
	desc.y = 0;
	desc.width = 1280;
	desc.height = 720;
	desc.title = "ExampleGame";
	_mainWindow.create(desc);
	

	_mainWindow.setGraphicsContext(am::RenderDevice::createGraphicsContext(&_mainWindow));
	am::RenderDevice::bindGraphicsContext(_mainWindow.getGraphicsContext());

	_cameras.resize(CameraSlot_Count);

	_cameras[CameraSlot_Main]._gbuffer.resize(_mainWindowSize.x, _mainWindowSize.y);
	_cameras[CameraSlot_Main]._renderTarget.attachTarget(am::RenderTargetSlot::Diffuse, &_cameras[CameraSlot_Main]._gbuffer._slots[GBuffer::Slot_Color]);
	_cameras[CameraSlot_Main]._renderTarget.attachTarget(am::RenderTargetSlot::Normal, &_cameras[CameraSlot_Main]._gbuffer._slots[GBuffer::Slot_Normal]);
	_cameras[CameraSlot_Main]._renderTarget.attachTarget(am::RenderTargetSlot::LinearDepth, &_cameras[CameraSlot_Main]._gbuffer._slots[GBuffer::Slot_Depth]);
	_cameras[CameraSlot_Main]._renderTarget.attachTarget(am::RenderTargetSlot::Specular, &_cameras[CameraSlot_Main]._gbuffer._slots[GBuffer::Slot_Specular]);
	_cameras[CameraSlot_Main]._renderTarget.useHardwareDepthAttachment(true);
	_cameras[CameraSlot_Main]._renderTarget.setSize(_mainWindowSize.x, _mainWindowSize.y);
	_cameras[CameraSlot_Main]._transform._localMatrix = am::lookAt(am::Vector3f(0.0, 1.5f, -1.5f), am::Vector3f(0.0f, 0.0f, 1.0f), am::Vector3f(0.0f, 1.0f, 0.0f));
	_cameras[CameraSlot_Main]._projection = am::createPerspectiveMatrix(45.0f, (float)_mainWindowSize.x / (float)_mainWindowSize.y, 0.1f, 1000.0f);
	_cameras[CameraSlot_Main]._effect = am::ResourceManager::load<am::Effect>(am::FileSystem::getFilePath("effects/GBuffer.fx"));
	_cameras[CameraSlot_Main]._cullFace = am::RenderDevice::CullFace_Back;


	_cameras[CameraSlot_Sun]._gbuffer.resize(_mainWindowSize.x, _mainWindowSize.y);
	_cameras[CameraSlot_Sun]._renderTarget.attachTarget(am::RenderTargetSlot::Diffuse, &_cameras[CameraSlot_Sun]._gbuffer._slots[GBuffer::Slot_Color]);
	_cameras[CameraSlot_Sun]._renderTarget.attachTarget(am::RenderTargetSlot::Normal, &_cameras[CameraSlot_Sun]._gbuffer._slots[GBuffer::Slot_Normal]);
	_cameras[CameraSlot_Sun]._renderTarget.attachTarget(am::RenderTargetSlot::LinearDepth, &_cameras[CameraSlot_Sun]._gbuffer._slots[GBuffer::Slot_Depth]);
	_cameras[CameraSlot_Sun]._renderTarget.attachTarget(am::RenderTargetSlot::Specular, &_cameras[CameraSlot_Sun]._gbuffer._slots[GBuffer::Slot_Specular]);
	_cameras[CameraSlot_Sun]._renderTarget.useHardwareDepthAttachment(true);
	_cameras[CameraSlot_Sun]._renderTarget.setSize(_mainWindowSize.x, _mainWindowSize.y);
	_cameras[CameraSlot_Sun]._transform._localMatrix = am::lookAt(am::Vector3f(-8.0f, 10.0f, -8.0f), am::Vector3f(0.0f, 0.0f, 0.0f), am::Vector3f(0.0f, 1.0f, 0.0f));
	_cameras[CameraSlot_Sun]._projection = am::createPerspectiveMatrix(45.0f, (float)_mainWindowSize.x / (float)_mainWindowSize.y, 0.1f, 1000.0f);
	//_cameras[CameraSlot_Sun]._projection = am::createOrthographicMatrix(-1.0f, 1.0f, -1.0f, 1.0f, 0.1f, 1000.0f);
	_cameras[CameraSlot_Sun]._effect = am::ResourceManager::load<am::Effect>(am::FileSystem::getFilePath("effects/GBuffer.fx"));
	_cameras[CameraSlot_Sun]._cullFace = am::RenderDevice::CullFace_Back;



	_framebuffer._effect = am::ResourceManager::load<am::Effect>(am::FileSystem::getFilePath("effects/FullScreenQuad.fx"));
	
	_gameObjects.resize(_numGameObjects);
	_transformComponents.resize(_numGameObjects);
	_graphicsComponents.resize(_numGameObjects);
	_inputComponents.resize(_numGameObjects, new StaticInputComponent());
	
	for (size_t i = 0; i < _numGameObjects; ++i)
	{
		GameObject& gameObject = _gameObjects[i];
		gameObject._graphics = &_graphicsComponents[i];
		gameObject._transform = &_transformComponents[i];
		gameObject._input = _inputComponents[i];
	}


	{
		GameObject& gameObject = _gameObjects[0];
		gameObject._input = new DummyInputComponent();
		_inputComponents[0] = new DummyInputComponent();
		gameObject._graphics->_meshes.push_back(loadAma(am::FileSystem::getFilePath("models/character/character.ama").c_str()));
		gameObject._graphics->_textures.resize(2);
		
		{
			am::Texture& textureDiffuse = gameObject._graphics->_textures[0];
			textureDiffuse.setPixelFormat(am::PixelFormat::RGB);
			textureDiffuse.setWrapMode(am::WrapMode::ClampToEdge, am::WrapMode::ClampToEdge, am::WrapMode::ClampToEdge);
			textureDiffuse.setFilterMode(am::FilterMode::Linear, am::FilterMode::Linear);
			am::Image* imageDiffuse = am::ResourceManager::load<am::Image>(am::FileSystem::getFilePath("models/character/character_dif.jpg"));
			imageDiffuse->flipY();
			textureDiffuse.setImage(imageDiffuse);
		}

		{
			am::Texture& textureNormal = gameObject._graphics->_textures[1];
			textureNormal.setPixelFormat(am::PixelFormat::RGB);
			textureNormal.setWrapMode(am::WrapMode::ClampToEdge, am::WrapMode::ClampToEdge, am::WrapMode::ClampToEdge);
			textureNormal.setFilterMode(am::FilterMode::Linear, am::FilterMode::Linear);
			am::Image* imageNormal = am::ResourceManager::load<am::Image>(am::FileSystem::getFilePath("models/character/character_normal.jpg"));
			imageNormal->flipY();
			textureNormal.setImage(imageNormal);
		}
		gameObject._graphics->_effect = am::ResourceManager::load<am::Effect>(am::FileSystem::getFilePath("effects/Character/GBuffer.fx"));
		gameObject._transform->_localMatrix = am::createTranslation(am::Vector3f(1.0f, 0.0f, 0.0f));
	}

	{
		GameObject& gameObject = _gameObjects[1];
		int width = 14;
		int height = 14;
		//gameObject._graphics->_meshes.push_back(am::Mesh::createCube(10, 1, 10));
		gameObject._graphics->_meshes.push_back(createHeightmap(width, height));
		gameObject._graphics->_textures.resize(1);
		am::Texture& texture = gameObject._graphics->_textures[0];
		texture.setPixelFormat(am::PixelFormat::RGB);
		texture.setWrapMode(am::WrapMode::ClampToEdge, am::WrapMode::ClampToEdge, am::WrapMode::ClampToEdge);
		texture.setFilterMode(am::FilterMode::Linear, am::FilterMode::Linear);
		texture.setImage(am::ResourceManager::load<am::Image>(am::FileSystem::getFilePath("models/box/metal.bmp")));
		gameObject._graphics->_effect = am::ResourceManager::load<am::Effect>(am::FileSystem::getFilePath("effects/GBuffer.fx"));
		gameObject._transform->_localMatrix = am::createTranslation(am::Vector3f(-(float)width/2.0, 0.0f, (float)height / 2.0));// *am::Quaternionf(am::Vector3f(0.0f, 0.0f, 25.0f)).getMatrix();
	}

	{
		GameObject& gameObject = _gameObjects[2];

		//am::Mesh mesh = loadAma(am::FileSystem::getFilePath("models/Stacy/Stacy.ama").c_str());
		gameObject._graphics->_meshes.push_back(loadAma(am::FileSystem::getFilePath("models/Stacy/Stacy.ama").c_str()));
		gameObject._graphics->_meshes.push_back(am::Mesh());
		am::Mesh* m0 = &gameObject._graphics->_meshes[0];
		//am::Mesh* m0 = &mesh;
		am::Mesh* m1 = &gameObject._graphics->_meshes[1];
		//am::Mesh* m1 = &gameObject._graphics->_meshes[0];
		am::StaticFloat3* m0nbuffer = static_cast<am::StaticFloat3*>(m0->getVertexElement(am::VertexSlot::Normal));
		auto& m0nvector = m0nbuffer->getVector();
		am::StaticFloat3* m0vbuffer = static_cast<am::StaticFloat3*>(m0->getVertexElement(am::VertexSlot::Position));
		auto& m0vvector = m0vbuffer->getVector();

		am::StaticFloat3* m1vbuffer = new am::StaticFloat3();
		auto& m1vvector = m1vbuffer->getVector();
		am::StaticUInt* indices = new am::StaticUInt();
		int count = 0;

		for (size_t i = 0; i < m0vbuffer->getVector().size(); ++i)
		{
			am::Vector3f a = m0vvector[i];
			am::Vector3f b = a + (m0nvector[i] * 0.2f);
			m1vvector.push_back(a);
			m1vvector.push_back(b);
			indices->getVector().push_back(count++);
			indices->getVector().push_back(count++);
		}

		m1->set(am::VertexSlot::Position, m1vbuffer);
		m1->addIndexBuffer(indices, am::PrimitiveSet::Lines);
		

		gameObject._graphics->_textures.resize(3);

		{
			am::Texture& texture = gameObject._graphics->_textures[0];
			texture.setPixelFormat(am::PixelFormat::RGBA);
			texture.setWrapMode(am::WrapMode::ClampToEdge, am::WrapMode::ClampToEdge, am::WrapMode::ClampToEdge);
			texture.setFilterMode(am::FilterMode::Linear, am::FilterMode::Linear);
			am::Image* image = am::ResourceManager::load<am::Image>(am::FileSystem::getFilePath("models/Stacy/Stacy_dif.tga"));
			image->flipY();
			texture.setImage(image);
		}
		{
			am::Texture& textureNormal = gameObject._graphics->_textures[1];
			textureNormal.setPixelFormat(am::PixelFormat::RGBA);
			textureNormal.setWrapMode(am::WrapMode::ClampToEdge, am::WrapMode::ClampToEdge, am::WrapMode::ClampToEdge);
			textureNormal.setFilterMode(am::FilterMode::Linear, am::FilterMode::Linear);
			am::Image* imageNormal = am::ResourceManager::load<am::Image>(am::FileSystem::getFilePath("models/Stacy/Stacy_normal.tga"));
			imageNormal->flipY();
			textureNormal.setImage(imageNormal);
		}
		{
			am::Texture& textureSpecular = gameObject._graphics->_textures[2];
			textureSpecular.setPixelFormat(am::PixelFormat::RGBA);
			textureSpecular.setWrapMode(am::WrapMode::ClampToEdge, am::WrapMode::ClampToEdge, am::WrapMode::ClampToEdge);
			textureSpecular.setFilterMode(am::FilterMode::Linear, am::FilterMode::Linear);
			am::Image* imageSpecular = am::ResourceManager::load<am::Image>(am::FileSystem::getFilePath("models/Stacy/Stacy_spec.tga"));
			imageSpecular->flipY();
			textureSpecular.setImage(imageSpecular);
		}

		gameObject._graphics->_effect = am::ResourceManager::load<am::Effect>(am::FileSystem::getFilePath("effects/Stacy/GBuffer.fx"));

		am::Matrix4f rotation = am::Quaternionf(am::Vector3f(-90.0f, 180.0f, 0.0f)).getMatrix();
		am::Matrix4f scale = am::createScale(am::Vector3f(0.1f, 0.1f, 0.1f));

		gameObject._transform->_localMatrix = am::createTranslation(am::Vector3f(0.0f, 0.0f, 0.0f)) * rotation * scale;
	}

}

void ExampleGame::mainLoop(float dt)
{
	static float angleY = 0.0f;
	static float angleX = 0.0f;
	static bool mouseLeft = false;
	static bool mouseRight = false;
	am::RenderDevice::bindGraphicsContext(_mainWindow.getGraphicsContext());
	//_mainWindow.tick();



	auto events = am::Window::pollEvents();
	for (auto e : events)
	{
		switch (e.type)
		{
		case WindowEvent::Event_Close:
			shutdown();
			break;
		case WindowEvent::Event_MouseMove:
			std::cerr << e.x << ", " << e.y << std::endl;
			break;
		case WindowEvent::Event_MouseDown:
			std::cerr << "mbuttonDown: " << e.mouseButton << std::endl;
			if (e.mouseButton == MouseButton_Left)
				mouseLeft = true;
			else if (e.mouseButton == MouseButton_Right)
				mouseRight = true;
			else if (e.mouseButton == MouseButton_Middle)
			{
				angleY = 0.0f;
				angleX = 0.0f;
			}
			break;
		case WindowEvent::Event_MouseUp:
			std::cerr << "mbuttonUp: " << e.mouseButton << std::endl;
			if (e.mouseButton == MouseButton_Left)
				mouseLeft = false;
			else if (e.mouseButton == MouseButton_Right)
				mouseRight = false;
			break;
		case WindowEvent::Event_MouseWheel:
			std::cerr << "wheel: " << e.wheelDelta << std::endl;
			float sign = 1.0f;
			if (e.wheelDelta < 0.0f)
				sign = -sign;
			angleX += 2.0f * sign;
		}
	}

	float speed = 2.0f;
	if (mouseLeft)
		angleY += speed;
	else if (mouseRight)
		angleY -= speed;

	
	am::Matrix4f defaultRotation = am::Quaternionf(am::Vector3f(-90.0f, 180.0f, 0.0f)).getMatrix();
	am::Matrix4f deltaRotation = am::Quaternionf(am::Vector3f(angleX, angleY, 0.0f)).getMatrix();
	//angle += 0.1;
	if (angleY > 360.0f)
		angleY -= 360.0f;
	else if (angleY < -360.0f)
		angleY += 360.0f;
	am::Matrix4f scale = am::createScale(am::Vector3f(0.1f, 0.1f, 0.1f));
	am::Matrix4f translation = am::createTranslation(am::Vector3f(0.0f, 0.0f, 0.0f));

	am::Matrix4f transform = translation * deltaRotation  * defaultRotation* scale;
	_gameObjects[2]._transform->_localMatrix = transform;


	/*am::Matrix4f newMatrix = _cameras[CameraSlot_Main]._transform._localMatrix;
	am::Vector3f right = am::cross(am::extractHeading(newMatrix), am::Vector3f(0.0f, 1.0f, 0.0));
	right = am::normalize(right);
	float speed = -0.05;
	am::Vector3f newPos = am::extractPosition(newMatrix) + speed * right;
	_cameras[CameraSlot_Main]._transform._localMatrix = am::lookAt(newPos, am::Vector3f(0.0f), am::Vector3f(0.0f, 1.0f, 0.0f));*/
	
	for (size_t i = 0; i < _numGameObjects; ++i)
		_inputComponents[i]->update(_gameObjects[i]);

	// Update GBuffer
	for (size_t i = 0; i < CameraSlot_Count; ++i)
	{
		CameraComponent& camera = _cameras[i];
		camera.update();

		const am::Matrix4f viewMatrix = camera._view;
		const am::Matrix4f projectionMatrix = camera._projection;
		//Populate GBuffer Start
		for (size_t i = 0; i < _numGameObjects; ++i)
		{
			GameObject& gameObject = _gameObjects[i];
			gameObject._graphics->update(&gameObject, viewMatrix, projectionMatrix, "GBuffer");
		}
		//Populate GBuffer End

		am::RenderDevice::unbindShaderProgramUniforms(camera._shaderProgram);
		am::RenderDevice::unbindShaderProgram();
		am::RenderDevice::unbindRenderTarget();
	}
	
	static am::Texture lightTexture;
	//directionalLighting(_cameras[CameraSlot_Main]._view, _cameras[CameraSlot_Main]._projection, am::Vector3f(0.0, 3.0f, 1.0f), am::Vector3f(1.0f, 1.0f, 1.0f), _cameras[CameraSlot_Main]._gbuffer._slots[GBuffer::Slot_Color], _cameras[CameraSlot_Main]._gbuffer._slots[GBuffer::Slot_Normal], _cameras[CameraSlot_Main]._gbuffer._slots[GBuffer::Slot_Depth], lightTexture);
	directionalLightingNormalMapping(_cameras[CameraSlot_Main]._view, _cameras[CameraSlot_Main]._projection, am::Vector3f(0.0, 3.0f, 1.0f), am::Vector3f(1.0f, 1.0f, 1.0f), _cameras[CameraSlot_Main]._gbuffer._slots[GBuffer::Slot_Color], _cameras[CameraSlot_Main]._gbuffer._slots[GBuffer::Slot_Normal], _cameras[CameraSlot_Main]._gbuffer._slots[GBuffer::Slot_Specular], _cameras[CameraSlot_Main]._gbuffer._slots[GBuffer::Slot_Depth], lightTexture);

	static am::Texture vsmTexture;
	varianceShadowMapping(_cameras[CameraSlot_Main]._view, _cameras[CameraSlot_Main]._projection, _cameras[CameraSlot_Sun]._view, _cameras[CameraSlot_Sun]._projection, _cameras[CameraSlot_Sun]._gbuffer._slots[GBuffer::Slot_Depth], lightTexture, _cameras[CameraSlot_Main]._gbuffer._slots[GBuffer::Slot_Depth], vsmTexture);
	
	static am::Texture fxaaTexture;
	fxaa(vsmTexture, fxaaTexture);

	am::Texture& finalTexture = fxaaTexture;
	//am::Texture& finalTexture = lightTexture;
	//am::Texture& finalTexture = _cameras[CameraSlot_Main]._gbuffer._slots[GBuffer::Slot_Normal];

	// Draw final image to framebuffer
	_framebuffer._size = _mainWindowSize;
	_framebuffer.update(&finalTexture);

	am::RenderDevice::swapBuffers(_mainWindow.getGraphicsContext());
}