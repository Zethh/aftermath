#pragma once
#include <Aftermath/render/RenderDevice.h>
#include <Aftermath/render/Effect.h>
#include <Aftermath/render/Uniform.h>
#include <Aftermath/render/Mesh.h>
#include <Aftermath/render/RenderTarget.h>
#include <Aftermath/math/Vector2.h>
#include <Aftermath/math/Vector4.h>
#include <Aftermath/core/Timer.h>
#include "GBuffer.h"
#include <fstream>
#include <sstream>
#include <vector>
#include <Aftermath/render/Texture.h>
#include <Aftermath/core/FileSystem.h>
#include <Aftermath/math/Quaternion.h>
#include <Aftermath/math/Frustum.h>


struct TransformComponent;
struct GraphicsComponent;
struct IInputComponent;

struct GameObject
{
	TransformComponent* _transform;
	GraphicsComponent* _graphics;
	IInputComponent* _input;
};

struct TransformComponent
{
	am::Matrix4f _localMatrix;
};

struct IInputComponent
{
	virtual void update(const GameObject& owner) = 0;
};

struct DummyInputComponent: public IInputComponent
{
	void update(const GameObject& owner)
	{
		
		am::Quaternionf q(rotation, am::Vector3f(0.0, 1.0, 0.0));
		rotation += 1.0f;
		if (rotation > 360.0f)
			rotation -= 360.0f;
		owner._transform->_localMatrix = am::createTranslation(am::extractPosition(owner._transform->_localMatrix)) *  q.getMatrix();
	}

	float rotation = 0;
};

struct StaticInputComponent : public IInputComponent
{
	void update(const GameObject& owner)
	{
	}

	float rotation = 0;
};


struct GraphicsComponent
{
	void update(const GameObject* owner, const am::Matrix4f& viewMatrix, const am::Matrix4f& projectionMatrix, std::string technique)
	{
		am::Matrix4f modelview = viewMatrix * owner->_transform->_localMatrix;
		_uniforms.set("view", viewMatrix);
		_uniforms.set("projection", projectionMatrix);
		_uniforms.set("modelview", modelview);
		_uniforms.set("model", owner->_transform->_localMatrix);
		for (size_t i = 0; i < _textures.size(); ++i)
		{
			std::stringstream ss;
			ss << "textureUnit" << i;
			_uniforms.set(ss.str(), &_textures[i]);
		}

		am::ShaderProgram* shaderProgram = _effect->getTechnique(technique)->getProgram();
		am::RenderDevice::bindShaderProgram(shaderProgram);
		am::RenderDevice::bindShaderProgramUniforms(shaderProgram, _uniforms);

		for (size_t i = 0; i < _meshes.size(); ++i)
		{
			am::RenderDevice::renderMesh(&_meshes[i]);
		}

		am::RenderDevice::unbindShaderProgramUniforms(shaderProgram);
		am::RenderDevice::unbindShaderProgram();
	}

	std::vector<am::Mesh> _meshes;
	std::vector<am::Texture> _textures;
	am::UniformBuffer _uniforms;
	am::Effect::Ptr _effect;
};


struct CameraComponent
{
	void update()
	{
		am::RenderDevice::bindRenderTarget(&_renderTarget);
		am::RenderDevice::enableDepthTesting();
		am::RenderDevice::enableCullFace(_cullFace);
		am::RenderDevice::setViewport(0, 0, _renderTarget.getWidth(), _renderTarget.getHeight());
		am::RenderDevice::setClearColor(0.2f, 0.1f, 0.3f, 1.0f);
		am::RenderDevice::clearBuffers(am::RenderDevice::ClearBuffer_Color | am::RenderDevice::ClearBuffer_Depth);
		_shaderProgram = _effect->getTechnique("GBuffer")->getProgram();
		am::RenderDevice::bindShaderProgram(_shaderProgram);
		_view = am::inverse(_transform._localMatrix);
		_uniforms.set("view", _view);
		_uniforms.set("projection", _projection);
		am::RenderDevice::bindShaderProgramUniforms(_shaderProgram, _uniforms);
	}

	am::RenderTarget _renderTarget;
	GBuffer _gbuffer;
	TransformComponent _transform;
	am::Matrix4f _projection;
	am::Matrix4f _view;
	am::Effect::Ptr _effect;
	am::UniformBuffer _uniforms;
	am::ShaderProgram* _shaderProgram;
	am::RenderDevice::CullFace _cullFace;
};

struct Framebuffer
{
	Framebuffer()
	{
		_mesh = am::Mesh::createFullScreenQuad();
	}

	void update(am::Texture* finalTexture)
	{
		am::RenderDevice::enableDepthTesting();
		am::RenderDevice::enableCullFace(am::RenderDevice::CullFace_Back);
		am::RenderDevice::setViewport(0, 0, _size.x, _size.y);
		am::RenderDevice::setClearColor(1.0, 0.0, 0.0, 1.0);
		am::RenderDevice::clearBuffers(am::RenderDevice::ClearBuffer_Color | am::RenderDevice::ClearBuffer_Depth);

		_shaderProgram = _effect->getTechnique("FullScreenQuad")->getProgram();
		am::RenderDevice::bindShaderProgram(_shaderProgram);
		_uniforms.set("textureUnit0", finalTexture);
		am::RenderDevice::bindShaderProgramUniforms(_shaderProgram, _uniforms);
		am::RenderDevice::renderMesh(&_mesh);
		am::RenderDevice::unbindShaderProgramUniforms(_shaderProgram);
		am::RenderDevice::unbindShaderProgram();
	}

	am::Mesh _mesh;
	am::Effect::Ptr _effect;
	am::ShaderProgram* _shaderProgram;
	am::Vector2u _size;
	am::UniformBuffer _uniforms;
};

static void gaussianBlur(am::Texture& texture)
{
	static am::Mesh fullScreenQuad = am::Mesh::createFullScreenQuad();
	static am::Effect* effect = am::ResourceManager::load<am::Effect>(am::FileSystem::getFilePath("effects/GaussianBlur.fx"));
	static am::UniformBuffer _uniforms;
	static const char* horizontalTechnique = "GaussianBlurHorizontal";
	static const char* verticalTechnique = "GaussianBlurVertical";
	static am::ShaderProgram* horizontalProgram = effect->getTechnique(horizontalTechnique)->getProgram();
	static am::ShaderProgram* verticalProgram = effect->getTechnique(verticalTechnique)->getProgram();

	int width = texture.getWidth();
	int height = texture.getHeight();

	am::Texture tmp;
	tmp.cloneProperties(&texture);
	_uniforms.set("u_shadowMapDimensions", am::Vector2f(width, height));
	am::RenderTarget renderTarget;
	renderTarget.setSize(width, height);
	renderTarget.useHardwareDepthAttachment(true);



	renderTarget.attachTarget(am::RenderTargetSlot::Diffuse, &tmp);
	_uniforms.set("u_shadowMap", &texture);
	am::RenderDevice::bindRenderTarget(&renderTarget);
	am::RenderDevice::enableDepthTesting();
	am::RenderDevice::enableCullFace(am::RenderDevice::CullFace_Back);
	am::RenderDevice::setViewport(0, 0, width, height);
	am::RenderDevice::setClearColor(1.0, 0.0, 0.0, 1.0);
	am::RenderDevice::clearBuffers(am::RenderDevice::ClearBuffer_Color | am::RenderDevice::ClearBuffer_Depth);

	am::RenderDevice::bindShaderProgram(horizontalProgram);
	am::RenderDevice::bindShaderProgramUniforms(horizontalProgram, _uniforms);
	am::RenderDevice::renderMesh(&fullScreenQuad);
	am::RenderDevice::unbindShaderProgramUniforms(horizontalProgram);
	am::RenderDevice::unbindShaderProgram();
	am::RenderDevice::unbindRenderTarget();


	renderTarget.attachTarget(am::RenderTargetSlot::Diffuse, &texture);
	_uniforms.set("u_shadowMap", &tmp);
	am::RenderDevice::bindRenderTarget(&renderTarget);
	am::RenderDevice::enableDepthTesting();
	am::RenderDevice::enableCullFace(am::RenderDevice::CullFace_Back);
	am::RenderDevice::setViewport(0, 0, width, height);
	am::RenderDevice::setClearColor(1.0, 0.0, 0.0, 1.0);
	am::RenderDevice::clearBuffers(am::RenderDevice::ClearBuffer_Color | am::RenderDevice::ClearBuffer_Depth);

	am::RenderDevice::bindShaderProgram(verticalProgram);
	am::RenderDevice::bindShaderProgramUniforms(verticalProgram, _uniforms);
	am::RenderDevice::renderMesh(&fullScreenQuad);
	am::RenderDevice::unbindShaderProgramUniforms(verticalProgram);
	am::RenderDevice::unbindShaderProgram();
	am::RenderDevice::unbindRenderTarget();

}

static void gaussianBlur(am::Texture& input, am::Texture& output)
{
	static am::Mesh fullScreenQuad = am::Mesh::createFullScreenQuad();
	static am::Effect* effect = am::ResourceManager::load<am::Effect>(am::FileSystem::getFilePath("effects/GaussianBlur.fx"));
	static am::UniformBuffer _uniforms;
	static const char* horizontalTechnique = "GaussianBlurHorizontal";
	static const char* verticalTechnique = "GaussianBlurVertical";
	static am::ShaderProgram* horizontalProgram = effect->getTechnique(horizontalTechnique)->getProgram();
	static am::ShaderProgram* verticalProgram = effect->getTechnique(verticalTechnique)->getProgram();

	int width = input.getWidth();
	int height = input.getHeight();

	am::Texture tmp;
	tmp.cloneProperties(&input);
	output.cloneProperties(&input);
	_uniforms.set("u_shadowMapDimensions", am::Vector2f(width, height));
	am::RenderTarget renderTarget;
	renderTarget.setSize(width, height);
	renderTarget.useHardwareDepthAttachment(true);
	
	

	renderTarget.attachTarget(am::RenderTargetSlot::Diffuse, &tmp);
	_uniforms.set("u_shadowMap", &input);
	am::RenderDevice::bindRenderTarget(&renderTarget);
	am::RenderDevice::enableDepthTesting();
	am::RenderDevice::enableCullFace(am::RenderDevice::CullFace_Back);
	am::RenderDevice::setViewport(0, 0, width, height);
	am::RenderDevice::setClearColor(1.0, 0.0, 0.0, 1.0);
	am::RenderDevice::clearBuffers(am::RenderDevice::ClearBuffer_Color | am::RenderDevice::ClearBuffer_Depth);

	am::RenderDevice::bindShaderProgram(horizontalProgram);
	am::RenderDevice::bindShaderProgramUniforms(horizontalProgram, _uniforms);
	am::RenderDevice::renderMesh(&fullScreenQuad);
	am::RenderDevice::unbindShaderProgramUniforms(horizontalProgram);
	am::RenderDevice::unbindShaderProgram();
	am::RenderDevice::unbindRenderTarget();


	renderTarget.attachTarget(am::RenderTargetSlot::Diffuse, &output);
	_uniforms.set("u_shadowMap", &tmp);
	am::RenderDevice::bindRenderTarget(&renderTarget);
	am::RenderDevice::enableDepthTesting();
	am::RenderDevice::enableCullFace(am::RenderDevice::CullFace_Back);
	am::RenderDevice::setViewport(0, 0, width, height);
	am::RenderDevice::setClearColor(1.0, 0.0, 0.0, 1.0);
	am::RenderDevice::clearBuffers(am::RenderDevice::ClearBuffer_Color | am::RenderDevice::ClearBuffer_Depth);

	am::RenderDevice::bindShaderProgram(verticalProgram);
	am::RenderDevice::bindShaderProgramUniforms(verticalProgram, _uniforms);
	am::RenderDevice::renderMesh(&fullScreenQuad);
	am::RenderDevice::unbindShaderProgramUniforms(verticalProgram);
	am::RenderDevice::unbindShaderProgram();
	am::RenderDevice::unbindRenderTarget();

}

static void directionalLighting(const am::Matrix4f& view, const am::Matrix4f& projection, const am::Vector3& lightDirection, const am::Vector3& lightColor, am::Texture& colorMap, am::Texture& normalMap, am::Texture& depthMap, am::Texture& output)
{
	static am::Mesh fullScreenQuad = am::Mesh::createFullScreenQuad();
	static am::Effect* effect = am::ResourceManager::load<am::Effect>(am::FileSystem::getFilePath("effects/DirectionalLight/DirectionalLight.fx"));
	static am::UniformBuffer uniforms;
	static const char* technique = "DirectionalLight";
	static am::ShaderProgram* program = effect->getTechnique(technique)->getProgram();

	am::TFrustum<float> frustum = am::createFrustum(projection, am::Matrix4f(1.0f));
	am::Vector3f points[8];
	frustum.getFrustumCorners(points);
	am::Matrix4f farClip(am::Vector4f(points[4], 0.0),
						 am::Vector4f(points[5], 0.0),
						 am::Vector4f(points[6], 0.0),
						 am::Vector4f(points[7], 0.0));

	
	uniforms.set("u_lightDirection", lightDirection);
	uniforms.set("u_lightColor", lightColor);
	uniforms.set("u_cameraView", view);
	uniforms.set("u_gBufferColor", &colorMap);
	uniforms.set("u_gBufferNormal", &normalMap);
	uniforms.set("u_gBufferDepth", &depthMap);
	uniforms.set("u_frustumCorners", farClip);
	

	int width = colorMap.getWidth();
	int height = colorMap.getHeight();
	output.cloneProperties(&colorMap);
	am::RenderTarget renderTarget;
	renderTarget.setSize(width, height);
	renderTarget.useHardwareDepthAttachment(true);

	renderTarget.attachTarget(am::RenderTargetSlot::Diffuse, &output);
	am::RenderDevice::bindRenderTarget(&renderTarget);
	am::RenderDevice::enableDepthTesting();
	am::RenderDevice::enableCullFace(am::RenderDevice::CullFace_Back);
	am::RenderDevice::setViewport(0, 0, width, height);
	am::RenderDevice::setClearColor(1.0, 0.0, 0.0, 1.0);
	am::RenderDevice::clearBuffers(am::RenderDevice::ClearBuffer_Color | am::RenderDevice::ClearBuffer_Depth);

	am::RenderDevice::bindShaderProgram(program);
	am::RenderDevice::bindShaderProgramUniforms(program, uniforms);
	am::RenderDevice::renderMesh(&fullScreenQuad);
	am::RenderDevice::unbindShaderProgramUniforms(program);
	am::RenderDevice::unbindShaderProgram();
	am::RenderDevice::unbindRenderTarget();
}

static void directionalLightingNormalMapping(const am::Matrix4f& view, const am::Matrix4f& projection, const am::Vector3& lightDirection, const am::Vector3& lightColor, am::Texture& colorMap, am::Texture& normalMap, am::Texture& specularMap, am::Texture& depthMap, am::Texture& output)
{
	static am::Mesh fullScreenQuad = am::Mesh::createFullScreenQuad();
	static am::Effect* effect = am::ResourceManager::load<am::Effect>(am::FileSystem::getFilePath("effects/DirectionalLightNormalMapping/DirectionalLightNormalMapping.fx"));
	static am::UniformBuffer uniforms;
	static const char* technique = "DirectionalLightNormalMapping";
	static am::ShaderProgram* program = effect->getTechnique(technique)->getProgram();

	am::TFrustum<float> frustum = am::createFrustum(projection, am::Matrix4f(1.0f));
	am::Vector3f points[8];
	frustum.getFrustumCorners(points);
	am::Matrix4f farClip(am::Vector4f(points[4], 0.0),
		am::Vector4f(points[5], 0.0),
		am::Vector4f(points[6], 0.0),
		am::Vector4f(points[7], 0.0));


	uniforms.set("u_lightDirection", lightDirection);
	uniforms.set("u_lightColor", lightColor);
	uniforms.set("u_cameraView", view);
	uniforms.set("u_gBufferColor", &colorMap);
	uniforms.set("u_gBufferNormal", &normalMap);
	uniforms.set("u_gBufferDepth", &depthMap);
	uniforms.set("u_gBufferSpecular", &specularMap);
	uniforms.set("u_frustumCorners", farClip);


	int width = colorMap.getWidth();
	int height = colorMap.getHeight();
	output.cloneProperties(&colorMap);
	am::RenderTarget renderTarget;
	renderTarget.setSize(width, height);
	renderTarget.useHardwareDepthAttachment(true);

	renderTarget.attachTarget(am::RenderTargetSlot::Diffuse, &output);
	am::RenderDevice::bindRenderTarget(&renderTarget);
	am::RenderDevice::enableDepthTesting();
	am::RenderDevice::enableCullFace(am::RenderDevice::CullFace_Back);
	am::RenderDevice::setViewport(0, 0, width, height);
	am::RenderDevice::setClearColor(1.0, 0.0, 0.0, 1.0);
	am::RenderDevice::clearBuffers(am::RenderDevice::ClearBuffer_Color | am::RenderDevice::ClearBuffer_Depth);

	am::RenderDevice::bindShaderProgram(program);
	am::RenderDevice::bindShaderProgramUniforms(program, uniforms);
	am::RenderDevice::renderMesh(&fullScreenQuad);
	am::RenderDevice::unbindShaderProgramUniforms(program);
	am::RenderDevice::unbindShaderProgram();
	am::RenderDevice::unbindRenderTarget();
}

static void varianceShadowMapping(am::Matrix4f& view, const am::Matrix4f& projection, const am::Matrix4f& lightView, const am::Matrix4f& lightProjection, am::Texture& lightDepthMap, am::Texture& colorMap, am::Texture& depthMap, am::Texture& output)
{
	gaussianBlur(lightDepthMap);
	static am::Mesh fullScreenQuad = am::Mesh::createFullScreenQuad();
	static am::Effect* effect = am::ResourceManager::load<am::Effect>(am::FileSystem::getFilePath("effects/VarianceShadowMapping/VarianceShadowMapping.fx"));
	static am::UniformBuffer uniforms;
	static const char* technique = "VarianceShadowMapping";
	static am::ShaderProgram* program = effect->getTechnique(technique)->getProgram();

	am::TFrustum<float> frustum = am::createFrustum(projection, am::Matrix4f(1.0f));
	am::Vector3f points[8];
	frustum.getFrustumCorners(points);
	am::Matrix4f farClip(am::Vector4f(points[4], 0.0),
		am::Vector4f(points[5], 0.0),
		am::Vector4f(points[6], 0.0),
		am::Vector4f(points[7], 0.0));


	const static am::Matrix4f bias(	am::Vector4f(0.5f, 0.0f, 0.0f, 0.0f),
									am::Vector4f(0.0f, 0.5f, 0.0f, 0.0f),
									am::Vector4f(0.0f, 0.0f, 0.5f, 0.0f),
									am::Vector4f(0.5f, 0.5f, 0.5f, 1.0f));

	uniforms.set("u_cameraToLightView", lightView * inverse(view));
	uniforms.set("u_shadowMatrix", bias * lightProjection * lightView * inverse(view));
	uniforms.set("u_gBufferColor", &colorMap);
	uniforms.set("u_gBufferDepth", &depthMap);
	uniforms.set("u_frustumCorners", farClip);
	uniforms.set("u_shadowMap", &lightDepthMap);

	int width = colorMap.getWidth();
	int height = colorMap.getHeight();
	output.cloneProperties(&colorMap);
	am::RenderTarget renderTarget;
	renderTarget.setSize(width, height);
	renderTarget.useHardwareDepthAttachment(true);

	renderTarget.attachTarget(am::RenderTargetSlot::Diffuse, &output);
	am::RenderDevice::bindRenderTarget(&renderTarget);
	am::RenderDevice::enableDepthTesting();
	am::RenderDevice::enableCullFace(am::RenderDevice::CullFace_Back);
	am::RenderDevice::setViewport(0, 0, width, height);
	am::RenderDevice::setClearColor(1.0, 0.0, 0.0, 1.0);
	am::RenderDevice::clearBuffers(am::RenderDevice::ClearBuffer_Color | am::RenderDevice::ClearBuffer_Depth);

	am::RenderDevice::bindShaderProgram(program);
	am::RenderDevice::bindShaderProgramUniforms(program, uniforms);
	am::RenderDevice::renderMesh(&fullScreenQuad);
	am::RenderDevice::unbindShaderProgramUniforms(program);
	am::RenderDevice::unbindShaderProgram();
	am::RenderDevice::unbindRenderTarget();
}

static void fxaa(am::Texture& texture, am::Texture& output)
{
	static am::Mesh fullScreenQuad = am::Mesh::createFullScreenQuad();
	static am::Effect* effect = am::ResourceManager::load<am::Effect>(am::FileSystem::getFilePath("effects/FXAA/FXAA.fx"));
	static am::UniformBuffer _uniforms;
	static const char* technique = "FXAA";
	static am::ShaderProgram* program = effect->getTechnique(technique)->getProgram();

	int width = texture.getWidth();
	int height = texture.getHeight();

	output.cloneProperties(&texture);
	am::RenderTarget renderTarget;
	renderTarget.setSize(width, height);
	renderTarget.useHardwareDepthAttachment(true);



	renderTarget.attachTarget(am::RenderTargetSlot::Diffuse, &output);
	_uniforms.set("textureSampler", &texture);
	_uniforms.set("frameBufSize", am::Vector2f(width, height));
	am::RenderDevice::bindRenderTarget(&renderTarget);
	am::RenderDevice::enableDepthTesting();
	am::RenderDevice::enableCullFace(am::RenderDevice::CullFace_Back);
	am::RenderDevice::setViewport(0, 0, width, height);
	am::RenderDevice::setClearColor(1.0, 0.0, 0.0, 1.0);
	am::RenderDevice::clearBuffers(am::RenderDevice::ClearBuffer_Color | am::RenderDevice::ClearBuffer_Depth);

	am::RenderDevice::bindShaderProgram(program);
	am::RenderDevice::bindShaderProgramUniforms(program, _uniforms);
	am::RenderDevice::renderMesh(&fullScreenQuad);
	am::RenderDevice::unbindShaderProgramUniforms(program);
	am::RenderDevice::unbindShaderProgram();
	am::RenderDevice::unbindRenderTarget();
}

static am::Mesh loadAma(const char* str)
{
	am::StaticFloat3* vertexBuffer = new am::StaticFloat3();
	am::StaticFloat3* normalBuffer = new am::StaticFloat3();
	am::StaticFloat3* tangentBuffer = new am::StaticFloat3();
	am::StaticFloat3* bitangentBuffer = new am::StaticFloat3();
	am::StaticFloat2* uvBuffer = new am::StaticFloat2();
	am::StaticUInt* indexBuffer = new am::StaticUInt();
	am::Timer t;
	t.tick();
	std::ifstream filebuff(str);
	if (filebuff.is_open())
	{
		std::string buffer;
		std::stringstream file;
		file << filebuff.rdbuf();
		filebuff.close();
		unsigned int numVertices;
		unsigned int numNormals;
		unsigned int numIndices;
		unsigned int numUV;
		file >> numVertices >> numNormals >> numUV >> numIndices;

		for (size_t i = 0; i < numVertices; ++i)
		{
			float x, y, z;
			file >> x >> y >> z;
			vertexBuffer->getVector().push_back(am::Vector3f(x, y, z));
		}

		for (size_t i = 0; i < numNormals; ++i)
		{
			float x, y, z;
			file >> x >> y >> z;
			normalBuffer->getVector().push_back(am::Vector3f(x, y, z));
		}

		for (size_t i = 0; i < numUV; ++i)
		{
			float u, v;
			file >> u >> v;
			uvBuffer->getVector().push_back(am::Vector2f(u, v));

		}

		for (size_t i = 0; i < numIndices; ++i)
		{
			float x;
			file >> x;
			indexBuffer->getVector().push_back(x);
		}



	}

	const auto& vertices = vertexBuffer->getVector();
	const auto& normals = normalBuffer->getVector();
	const auto& uvs = uvBuffer->getVector();
	const auto& indices = indexBuffer->getVector();
	std::vector<am::Vector3f>& tangents = tangentBuffer->getVector();
	std::vector<am::Vector3f>& bitangents = bitangentBuffer->getVector();
	tangents.resize(vertices.size());
	bitangents.resize(vertices.size());

	for (int i = 0; i < indices.size(); i+=3)
	{
		int index0 = indices[i + 0];
		int index1 = indices[i + 1];
		int index2 = indices[i + 2];
		const am::Vector3f& v0 = vertices[index0];
		const am::Vector3f& v1 = vertices[index1];
		const am::Vector3f& v2 = vertices[index2];

		const am::Vector2f& uv0 = uvs[index0];
		const am::Vector2f& uv1 = uvs[index1];
		const am::Vector2f& uv2 = uvs[index2];

		am::Vector3f deltaPos1 = v1 - v0;
		am::Vector3f deltaPos2 = v2 - v0;

		am::Vector2f deltaUV1 = uv1 - uv0;
		am::Vector2f deltaUV2 = uv2 - uv0;

		float r = 1.0f / (deltaUV1.x * deltaUV2.y - deltaUV1.y * deltaUV2.x);
		am::Vector3f tangent = (deltaPos1 * deltaUV2.y - deltaPos2 * deltaUV1.y) * r;
		am::Vector3f bitangent = (deltaPos2 * deltaUV1.x - deltaPos1 * deltaUV2.x) * r;
		
		tangents[index0] += tangent;
		tangents[index1] += tangent;
		tangents[index2] += tangent;

		bitangents[index0] += bitangent;
		bitangents[index1] += bitangent;
		bitangents[index2] += bitangent;
	}


	t.tick();
	std::cerr << "Model loading time: " << t.getDeltaTime() * 1000.0 << "ms" << std::endl;

	am::Mesh mymesh;
	mymesh.set(am::VertexSlot::Position, vertexBuffer);
	mymesh.set(am::VertexSlot::Normal, normalBuffer);
	mymesh.set(am::VertexSlot::TexCoord, uvBuffer);
	mymesh.set(am::VertexSlot::Tangent, tangentBuffer);
	mymesh.set(am::VertexSlot::BiTangent, bitangentBuffer);
	mymesh.addIndexBuffer(indexBuffer);
	return std::move(mymesh);
}





static inline double findnoise(int xv)
{
	int x = (xv << 13) ^ xv;
	return (double)(1.0 - ((x * (x * x * 15731 + 789221) + 1376312589) & 0x7fffffff) / 1073741824.0);
}

static inline double findnoise2(double x, double y)
{
	int n = (int)x + (int)y * 57;
	n = (n << 13) ^ n;
	int nn = (n*(n*n * 60493 + 19990303) + 1376312589) & 0x7fffffff;
	return 1.0 - ((double)nn / 1073741824.0);
}

static inline double interpolate(double a, double b, double x)
{
	double ft = x * 3.1415927;
	double f = (1.0 - cos(ft))* 0.5;
	return a*(1.0 - f) + b*f;
}

static double noise(double x, double y)
{
	double floorx = (double)((int)x);//This is kinda a cheap way to floor a double integer.
	double floory = (double)((int)y);
	double s, t, u, v;//Integer declaration
	s = findnoise2(floorx, floory);
	t = findnoise2(floorx + 1, floory);
	u = findnoise2(floorx, floory + 1);//Get the surrounding pixels to calculate the transition.
	v = findnoise2(floorx + 1, floory + 1);
	double int1 = interpolate(s, t, x - floorx);//Interpolate between the values.
	double int2 = interpolate(u, v, x - floorx);//Here we use x-floorx, to get 1st dimension. Don't mind the x-floorx thingie, it's part of the cosine formula.
	return interpolate(int1, int2, y - floory);//Here we use y-floory, to get the 2nd dimension.
}

static void perlinNoise(am::Vector3f* vertices, int count, int width, int height, double zoom = 175.0, double persistance = 0.5, int octaves = 18)
{
	for (int y = 0; y < height; ++y)
	{
		for (int x = 0; x < width; ++x)
		{
			double getNoise = 0.0;
			for (int a = 0; a < octaves - 1; ++a)
			{
				double frequency = pow(2, a);
				double amplitude = pow(persistance, a);
				getNoise += noise(((double)x)*frequency / zoom, ((double)y) / zoom*frequency)*amplitude;
			}
			int color = (int)((getNoise * 128.0) + 128.0);
			if (color > 255)
				color = 255;
			if (color < 0)
				color = 0;

			vertices[x + y*width] = am::Vector3f(x, color, -y);
		}
	}
}

static int perlinNoise(int x, int y, double zoom = 175.0, double persistance = 0.5, int octaves = 18)
{
	double getNoise = 0.0;
	for (int a = 0; a < octaves - 1; ++a)
	{
		double frequency = pow(2, a);
		double amplitude = pow(persistance, a);
		getNoise += noise(((double)x)*frequency / zoom, ((double)y) / zoom*frequency)*amplitude;
	}

	int color = (int)((getNoise * 128.0) + 128.0);
	if (color > 255)
		color = 255;
	if (color < 0)
		color = 0;

	return color;
}


static void calculateHeightmapNormals(am::Vector3f* vertices, unsigned int width, unsigned int height, am::Vector3f* normals)
{
	unsigned int patterns[6] = { 0, 2, 3, 1, 0, 3 };
	int v[4];

	for (unsigned int z = 0; z < height - 1; ++z)
	{
		for (unsigned int x = 0; x < width - 1; ++x)
		{
			v[0] = x + z*width;
			v[1] = (x + 1) + z*width;
			v[2] = x + (z + 1)*width;
			v[3] = (x + 1) + (z + 1)*width;


			for (unsigned int i = 0; i < 6; i += 3)
			{
				am::Vector3f p1(vertices[v[patterns[i]]]);
				am::Vector3f p2(vertices[v[patterns[i + 1]]]);
				am::Vector3f p3(vertices[v[patterns[i + 2]]]);

				am::Vector3f v1(p3 - p1);
				am::Vector3f v2(p2 - p1);
				am::Vector3f tmp(am::cross(v1, v2));

				normals[v[patterns[i]]] += tmp;
				normals[v[patterns[i + 1]]] += tmp;
				normals[v[patterns[i + 2]]] += tmp;
			}

		}
	}

	for (unsigned int i = 0; i < width*height; i++)
	{
		normals[i] = am::normalize(normals[i]);
	}
}

static am::Mesh createHeightmap(unsigned int width, unsigned int height)
{
	am::StaticFloat3* vertices = new am::StaticFloat3();
	am::StaticFloat3* normals = new am::StaticFloat3();
	am::StaticUInt* indices = new am::StaticUInt();
	am::Mesh mesh;
	vertices->getVector().resize(width * height);
	normals->getVector().resize(width * height, am::Vector3f(0.0f));
	for (int y = 0; y < height; ++y)
	{
		for (int x = 0; x < width; ++x)
		{
			float pointHeight = std::rand() % 3;
			float pointHeightDec = std::rand() % 1;
			pointHeightDec = pointHeightDec / 100;
			pointHeight = pointHeight + pointHeightDec;

			pointHeight = 0.0f;
			vertices->getVector()[x + y*width] = am::Vector3f(x, pointHeight, -y);
		}
	}


	//perlinNoise(vertices->getVector().data(), vertices->getVector().size(), width, height);

	calculateHeightmapNormals(vertices->getVector().data(), width, height, normals->getVector().data());
		//boxFilter();


		indices->getVector().resize(((width - 1) * (height - 1)) * 6);
		int index = 0;
		for (unsigned int z = 0; z < height - 1; ++z)
		{
			for (unsigned int x = 0; x < width - 1; ++x)
			{

				int vertexIndex = x + z*height;

				indices->getVector()[index++] = vertexIndex; //x,z
				indices->getVector()[index++] = vertexIndex + 1; //x+1, z
				indices->getVector()[index++] = vertexIndex + width + 1; //x+1, z+1

				indices->getVector()[index++] = vertexIndex; //x,z
				indices->getVector()[index++] = vertexIndex + width + 1; //x+1, z+1
				indices->getVector()[index++] = vertexIndex + width;  //x, z+1

				//indices->getVector()[index++] = vertexIndex + width + 1; //x+1, z+1
				//indices->getVector()[index++] = vertexIndex; //x,z
				//indices->getVector()[index++] = vertexIndex + width;  //x, z+1


				//indices->getVector()[index++] = vertexIndex + width + 1; //x+1, z+1
				//indices->getVector()[index++] = vertexIndex + 1; //x+1, z
				//indices->getVector()[index++] = vertexIndex; //x,z
			}
		}



		mesh.set(am::VertexSlot::Position, vertices);
		mesh.set(am::VertexSlot::Normal, normals);
		mesh.addIndexBuffer(indices);

		return std::move(mesh);
	}








	//void boxFilter()
	//{
	//	unsigned int area = _width*_height;
	//	unsigned int patterns[9];
	//	std::vector<aftermath::math::Vector3f> res(area);
	//	//unsigned int width = _heightDB->_data._width;
	//	//unsigned int height = _heightDB->_data._height;
	//	for (unsigned int z = 0; z < _width; ++z)
	//	{
	//		for (unsigned int x = 0; x < _height; ++x)
	//		{
	//			//To perform box filtering we create a 3x3 grid around our current position and get the height values.
	//			//Then we set the height at the current pos equal to the average height of the grid.
	//			float sum(0.0f), numCells(1.0f);

	//			//Swap these two to speed up performance since point 2x2 will always be accounted for.
	//			patterns[0] = (x + z*_width); //2x2
	//			patterns[4] = (x - 1) + (z - 1) * _width; //1x1 //will always happen

	//			patterns[1] = (x + (z - 1)*_width); //1x2
	//			patterns[2] = ((x + 1) + (z - 1)*_width); //1x3
	//			patterns[3] = (x - 1) + z*_width; //2x1

	//			patterns[5] = (x + 1) + z*_width; //2x3
	//			patterns[6] = (x - 1) + (z + 1)*_width; //3x1
	//			patterns[7] = x + (z + 1) * _width; //3x2
	//			patterns[8] = (x + 1) + (z + 1) * _width; //3x3


	//			sum += _vertices[patterns[0]].y;
	//			for (unsigned int i = 1; i < 9; ++i)
	//			{
	//				if (patterns[i] >= 0 && patterns[i] < area)
	//				{
	//					sum += _vertices[patterns[i]].y;
	//					++numCells;
	//				}
	//			}

	//			res[x + z*_width] = aftermath::math::Vector3f(x, sum / numCells, z);

	//		}
	//	}

	//	for (unsigned int z = 0; z < _width; ++z)
	//	{
	//		for (unsigned int x = 0; x < _height; ++x)
	//		{
	//			_vertices[x + z*_width].z *= -1.0f;
	//		}
	//	}



	//}



