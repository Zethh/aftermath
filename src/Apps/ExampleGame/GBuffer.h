#pragma once
#include <Aftermath/render/Texture.h>
#include <Aftermath/render/RenderEnums.h>
struct GBuffer
{

	enum Slot
	{
		Slot_Color,
		Slot_Normal,
		Slot_Depth,
		Slot_Occlusion,
		Slot_Specular,
		Slot_Count
	};

	GBuffer()
	{
		_slots[Slot_Color].setPixelFormat(am::PixelFormat::RGBA);
		_slots[Slot_Color].setWrapMode(am::WrapMode::ClampToEdge, am::WrapMode::ClampToEdge, am::WrapMode::ClampToEdge);
		_slots[Slot_Color].setFilterMode(am::FilterMode::Linear, am::FilterMode::Linear);

		_slots[Slot_Normal].setPixelFormat(am::PixelFormat::RGBA);
		_slots[Slot_Normal].setWrapMode(am::WrapMode::ClampToEdge, am::WrapMode::ClampToEdge, am::WrapMode::ClampToEdge);
		_slots[Slot_Normal].setFilterMode(am::FilterMode::Linear, am::FilterMode::Linear);

		_slots[Slot_Depth].setPixelFormat(am::PixelFormat::RG32F);
		_slots[Slot_Depth].setWrapMode(am::WrapMode::ClampToEdge, am::WrapMode::ClampToEdge, am::WrapMode::ClampToEdge);
		_slots[Slot_Depth].setFilterMode(am::FilterMode::Linear, am::FilterMode::Linear);

		_slots[Slot_Specular].setPixelFormat(am::PixelFormat::RGBA);
		_slots[Slot_Specular].setWrapMode(am::WrapMode::ClampToEdge, am::WrapMode::ClampToEdge, am::WrapMode::ClampToEdge);
		_slots[Slot_Specular].setFilterMode(am::FilterMode::Linear, am::FilterMode::Linear);

	}

	void resize(unsigned int width, unsigned int height)
	{
		for (size_t i = 0; i < Slot_Count; ++i)
		{
			_slots[i].setWidth(width);
			_slots[i].setHeight(height);
		}
	}

	am::Texture _slots[Slot_Count];
};

