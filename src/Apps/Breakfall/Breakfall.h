#include <Aftermath/game/Common/Game.h>

namespace bf
{
	class BreakFall: public am::Game
	{
		AFTERMATH_META(BreakFall)
	public:
		BreakFall();

	private:
		virtual void onInitialise() override;

		virtual void onShutdown() override;

		virtual void onUpdate() override;

		//void init();

		//void deinit();
	};
}