#include "Breakfall.h"

int main(int argc, char** argv)
{
	{
		bf::BreakFall::Ptr breakFall = new bf::BreakFall();
		breakFall->initialise();
		breakFall->run();
		breakFall->shutdown();
	}
}