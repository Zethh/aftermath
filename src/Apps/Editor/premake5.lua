local _libName = "Editor"

project (_libName)
	kind "WindowedApp"
	language "C++"
	links {"Engine"}
	files {"**.h", "**.cpp"}
	includedirs {("c:/Users/C/Documents/aftermath/src")}


	local _vpath = AM_APP_ROOT .. _libName .. "/*"
	vpaths { ["*"] = _vpath}


	configuration "Debug"
        defines { "DEBUG" }
        flags { "Symbols" }
    configuration {}
 
    configuration "Release"
        defines { "NDEBUG" }
        flags { "Optimize" }  
    configuration {}
