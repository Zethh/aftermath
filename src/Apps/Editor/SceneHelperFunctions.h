#pragma once
#include <Aftermath/render/Mesh.h>
#include <Aftermath/world/World.h>
namespace editor
{
	class World;
	class Scene;
	class SceneHelperFunctions
	{
	public:
		static void loadLevel1(am::World* scene);

		static am::Mesh loadAma(const char* filename);
	};

}