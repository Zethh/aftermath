#pragma once

#include <Aftermath/game/Common/Game.h>

namespace editor
{
	class Application: public am::Game
	{
		AFTERMATH_META(Application)
	public:
		Application();
	private:
		virtual void onInitialise() override;

		virtual void onShutdown() override;

		virtual void onUpdate() override;
	};
}