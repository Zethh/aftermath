#ifdef AM_PLATFORM_WINDOWS
#pragma comment(linker, "/subsystem:\"console\" /entry:\"WinMainCRTStartup\"")
#endif

#include "Application.h"

using namespace editor;

int main()
{
	Application app;
	app.initialise();
	app.run();
	app.shutdown();
	return 0;
}