#include "Application.h"
#include "SceneHelperFunctions.h"
#include <Aftermath/game/Common/GameRenderer.h>
#include <Aftermath/world/render/WorldRenderer.h>
#include <Aftermath/render/Window.h>
#include <Aftermath/world/World.h>
namespace editor
{
	Application::Application()
	{
	}

	void Application::onInitialise()
	{

		am::Window::Desc desc;
		desc.x = 0;
		desc.y = 0;
		desc.width = 640;
		desc.height = 480;
		desc.title = "Editor";
		am::g_renderer->initWindow(am::Window::create(desc));

		SceneHelperFunctions::loadLevel1(getWorld());
	}

	void Application::onShutdown()
	{
	}

	void Application::onUpdate()
	{
	}

}