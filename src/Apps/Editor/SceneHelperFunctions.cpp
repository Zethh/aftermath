#include "SceneHelperFunctions.h"
#include <Aftermath/core/FileSystem.h>
#include <Aftermath/utils/EffectReader.h>
#include <Aftermath/utils/ShaderReader.h>
#include <Aftermath/utils/ImageReader.h>
#include <Aftermath/math/Ray.h>
#include <Aftermath/math/Sphere.h>
#include <Aftermath/math/Plane.h>
#include <Aftermath/render/Renderer.h>
#include <Aftermath/utils/Image.h>
#include <fstream>
#include <sstream>
#include <Aftermath/world/World.h>

namespace editor
{
	void SceneHelperFunctions::loadLevel1(am::World* scene)
	{
		am::IRenderer* renderer = am::g_renderer;
		am::FileSystem::mountDirectory("C:/Users/C/Documents/aftermath/resources", false);
		am::EffectReader* effectReader = new am::EffectReader();
		effectReader->setGroupId(renderer->getName());
		am::ResourceManager::registerResourceReader(effectReader);
		am::ResourceManager::registerResourceReader(new am::ShaderReader());
		am::ResourceManager::registerResourceReader(new am::ImageReader());

		{
			am::SpatialEntity::Ptr entity = new am::SpatialEntity();

			am::Matrix4f rotation = am::Quaternionf(am::Vector3f(-90.0f, 0.0f, 0.0f)).getMatrix();
			am::Matrix4f scale = am::createScale(am::Vector3f(0.1f, 0.1f, 0.1f));
			auto& transform = entity->getTransform();
			transform.setMatrix(am::createTranslation(am::Vector3f(0.0f, 0.0f, 0.0f)) * rotation * scale);

			auto& blueprint = entity->getBlueprint();
			//geometry.setEffect(am::ResourceManager::load<am::Effect>(am::FileSystem::getFilePath("effects/Stacy/GBuffer.fx")));
			blueprint.setEffect(am::ResourceManager::load<am::Effect>(am::FileSystem::getFilePath("effects/GBuffer/GBuffer.fx")));
			blueprint.addMesh(loadAma(am::FileSystem::getFilePath("models/Stacy/Stacy.ama").c_str()));
			
			const char* images[] = { "models/Stacy/Stacy_dif.tga", "models/Stacy/Stacy_normal.tga", "models/Stacy/Stacy_spec.tga" };
			for (int i = 0; i < 3; ++i)
			{
				am::TextureDesc desc;
				desc.pixelFormat = am::PixelFormat_RGBA;
				desc.s = desc.r = desc.t = am::WrapMode_ClampToEdge;
				desc.min = desc.mag = am::FilterMode_Linear;
				desc.data = am::ResourceManager::load<am::Image>(am::FileSystem::getFilePath(images[i]));
				desc.data->flipY();
				blueprint.addTexture(renderer->createTexture(desc));
			}

			scene->addEntity(entity);
		}

		{
			am::RenderTargetDesc rtDesc;
			rtDesc.width = 640;
			rtDesc.height = 480;
			rtDesc.useHardwareDepth = true;
			{
				am::TextureDesc desc;
				desc.pixelFormat = am::PixelFormat_RGBA;
				desc.s = desc.r = desc.t = am::WrapMode_ClampToEdge;
				desc.min = desc.mag = am::FilterMode_Linear;
				desc.width = 640;
				desc.height = 480;
				rtDesc.slots[am::RenderTargetSlot_Color0] = renderer->createTexture(desc);
			}

			{
				am::TextureDesc desc;
				desc.pixelFormat = am::PixelFormat_RGBA;
				desc.s = desc.r = desc.t = am::WrapMode_ClampToEdge;
				desc.min = desc.mag = am::FilterMode_Linear;
				desc.width = 640;
				desc.height = 480;
				rtDesc.slots[am::RenderTargetSlot_Color1] = renderer->createTexture(desc);
			}

			{
				am::TextureDesc desc;
				desc.pixelFormat = am::PixelFormat_RG32F;
				desc.s = desc.r = desc.t = am::WrapMode_ClampToEdge;
				desc.min = desc.mag = am::FilterMode_Linear;
				desc.width = 640;
				desc.height = 480;
				rtDesc.slots[am::RenderTargetSlot_Color3] = renderer->createTexture(desc);
			}

			{
				am::TextureDesc desc;
				desc.pixelFormat = am::PixelFormat_RGBA;
				desc.s = desc.r = desc.t = am::WrapMode_ClampToEdge;
				desc.min = desc.mag = am::FilterMode_Linear;
				desc.width = 640;
				desc.height = 480;
				rtDesc.slots[am::RenderTargetSlot_Color2] = renderer->createTexture(desc);
			}
			auto* renderTarget = renderer->createRenderTarget(rtDesc);
			am::ViewEntity::Ptr camera = new am::ViewEntity();
			camera->setRenderTarget(renderTarget);
			camera->setProjectionMatrix(am::createPerspectiveMatrix(45.0f, (float)640.0 / (float)480.0f, 0.1f, 1000.0f));
			camera->setRenderTechnique("GBuffer");

			auto& blueprint = camera->getBlueprint();
			blueprint.addMesh(am::Mesh::createCube(0.5f, 0.5f, 0.5f));

			auto& transform = camera->getTransform();

			transform.setMatrix(am::lookAt(am::Vector3f(0.0, 1.5f, -1.5f), am::Vector3f(0.0f, 0.0f, 1.0f), am::Vector3f(0.0f, 1.0f, 0.0f)));
			//transform.setLocalMatrix(am::createTranslation(am::Vector3f(0.0f, 0.5f, 1.5f)));

			scene->addEntity(camera);
			camera->setSpatialFlags(0);
		}
	}

	am::Mesh SceneHelperFunctions::loadAma(const char* str)
	{
		am::StaticFloat3* vertexBuffer = new am::StaticFloat3();
		am::StaticFloat3* normalBuffer = new am::StaticFloat3();
		am::StaticFloat3* tangentBuffer = new am::StaticFloat3();
		am::StaticFloat3* bitangentBuffer = new am::StaticFloat3();
		am::StaticFloat2* uvBuffer = new am::StaticFloat2();
		am::StaticUInt* indexBuffer = new am::StaticUInt();
		std::ifstream filebuff(str);
		if (filebuff.is_open())
		{
			std::string buffer;
			std::stringstream file;
			file << filebuff.rdbuf();
			filebuff.close();
			unsigned int numVertices;
			unsigned int numNormals;
			unsigned int numIndices;
			unsigned int numUV;
			file >> numVertices >> numNormals >> numUV >> numIndices;

			for (size_t i = 0; i < numVertices; ++i)
			{
				float x, y, z;
				file >> x >> y >> z;
				vertexBuffer->getVector().push_back(am::Vector3f(x, y, z));
			}

			for (size_t i = 0; i < numNormals; ++i)
			{
				float x, y, z;
				file >> x >> y >> z;
				normalBuffer->getVector().push_back(am::Vector3f(x, y, z));
			}

			for (size_t i = 0; i < numUV; ++i)
			{
				float u, v;
				file >> u >> v;
				uvBuffer->getVector().push_back(am::Vector2f(u, v));

			}

			for (size_t i = 0; i < numIndices; ++i)
			{
				int x;
				file >> x;
				indexBuffer->getVector().push_back(x);
			}



		}

		const auto& vertices = vertexBuffer->getVector();
		const auto& normals = normalBuffer->getVector();
		const auto& uvs = uvBuffer->getVector();
		const auto& indices = indexBuffer->getVector();
		auto& tangents = tangentBuffer->getVector();
		auto& bitangents = bitangentBuffer->getVector();
		tangents.resize(vertices.size());
		bitangents.resize(vertices.size());

		for (int i = 0; i < indices.size(); i += 3)
		{
			int index0 = indices[i + 0];
			int index1 = indices[i + 1];
			int index2 = indices[i + 2];
			const am::Vector3f& v0 = vertices[index0];
			const am::Vector3f& v1 = vertices[index1];
			const am::Vector3f& v2 = vertices[index2];

			const am::Vector2f& uv0 = uvs[index0];
			const am::Vector2f& uv1 = uvs[index1];
			const am::Vector2f& uv2 = uvs[index2];

			am::Vector3f deltaPos1 = v1 - v0;
			am::Vector3f deltaPos2 = v2 - v0;

			am::Vector2f deltaUV1 = uv1 - uv0;
			am::Vector2f deltaUV2 = uv2 - uv0;

			float r = 1.0f / (deltaUV1.x * deltaUV2.y - deltaUV1.y * deltaUV2.x);
			am::Vector3f tangent = (deltaPos1 * deltaUV2.y - deltaPos2 * deltaUV1.y) * r;
			am::Vector3f bitangent = (deltaPos2 * deltaUV1.x - deltaPos1 * deltaUV2.x) * r;

			tangents[index0] += tangent;
			tangents[index1] += tangent;
			tangents[index2] += tangent;

			bitangents[index0] += bitangent;
			bitangents[index1] += bitangent;
			bitangents[index2] += bitangent;
		}


		am::Mesh mymesh;
		mymesh.set(am::Vertex_Position, vertexBuffer);
		mymesh.set(am::Vertex_Normal, normalBuffer);
		mymesh.set(am::Vertex_TexCoord, uvBuffer);
		mymesh.set(am::Vertex_Tangent, tangentBuffer);
		mymesh.set(am::Vertex_BiTangent, bitangentBuffer);
		mymesh.addIndexBuffer(indexBuffer);
		return std::move(mymesh);
	}


}