#pragma once

class Application
{
public:
	Application();

	void init();

	int run();

	void shutdown();

private:
	virtual void mainLoop(float dt) = 0;

	bool _isRunning;
};