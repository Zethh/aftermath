#include "ExampleGame.h"
#include <Aftermath/render/Renderer.h>
#include <Aftermath/core/FileSystem.h>
#include <Aftermath/core/ResourceManager.h>
#include <Aftermath/utils/EffectReader.h>
#include <Aftermath/utils/ShaderReader.h>
#include <Aftermath/utils/ImageReader.h>
#include <Aftermath/render/OpenGL/GlRenderer.h>
#include <Aftermath/render/DX11/DxRenderer.h>
#include <Aftermath/render/Uniform.h>
#include <Aftermath/render/RenderEnums.h>
#include <Aftermath/math/Quaternion.h>
#include <Aftermath/math/Matrix4.h>
ExampleGame::ExampleGame()
{
	_renderer = new am::DxRenderer();
	am::FileSystem::mountDirectory("C:/Users/C/Documents/aftermath/resources");
	am::EffectReader* effectReader = new am::EffectReader();
	effectReader->setGroupId(_renderer->getName());
	am::ResourceManager::registerResourceReader(effectReader);
	am::ResourceManager::registerResourceReader(new am::ShaderReader());
	am::ResourceManager::registerResourceReader(new am::ImageReader());

	am::Window::Desc desc;
	desc.x = 0;
	desc.y = 0;
	desc.width = 1280;
	desc.height = 720;
	desc.title = _renderer->getName();
	_mainWindow.create(desc);
	
	_renderer->initWindow(&_mainWindow);

	am::ITexture::Desc tdesc;
	tdesc.data = am::ResourceManager::load<am::Image>(am::FileSystem::getFilePath("models/Stacy/Stacy_dif.tga"));
	tdesc.data->flipY();
	tdesc.pixelFormat = am::PixelFormat::RGBA;
	tdesc.min = tdesc.mag = am::FilterMode::Linear;
	tdesc.s = tdesc.r = tdesc.t = am::WrapMode::ClampToEdge;
	_texture = _renderer->createTexture(tdesc);

	_uniforms.set("textureUnit0", _texture);

	outputs[0].effect = am::ResourceManager::load<am::Effect>(am::FileSystem::getFilePath("effects/Debug/Debug.fx"));
	am::StaticFloat4 *vertices2 = new am::StaticFloat4{ { 0.0f, 0.5f, 0.5f, 1.0f }, { 0.5f, -0.5f, 0.5f, 1.0f }, { -0.5f, -0.5f, 0.5f, 1.0f } };
	am::StaticFloat4 *colors2 = new am::StaticFloat4{ { 1.0f, 0.0f, 0.0f, 1.0f }, { 0.0f, 1.0f, 0.0f, 1.0f }, { 0.0f, 0.0f, 1.0f, 1.0f } };
	am::StaticUInt *indices = new am::StaticUInt{ 2, 1, 0 };
	outputs[0].mesh.set(am::VertexSlot::Position, vertices2);
	outputs[0].mesh.set(am::VertexSlot::Color, colors2);
	outputs[0].mesh.addIndexBuffer(indices);
	outputs[0].uniforms.set("u_mvp", am::Matrix4f(1.0f));
	outputs[0].uniforms.set("u_color", am::Vector4f(0.2f, 0.2f, 0.3f, 1.0f));
	outputs[0].uniforms.set("u_useUniformColor", 1);

	//// Debug effect
	/*_effect = am::ResourceManager::load<am::Effect>(am::FileSystem::getFilePath("effects/Debug/Debug.fx"));
	am::StaticFloat4 *vertices2 = new am::StaticFloat4{ { 0.0f, 0.5f, 0.5f, 1.0f }, { 0.5f, -0.5f, 0.5f, 1.0f }, { -0.5f, -0.5f, 0.5f, 1.0f } };
	am::StaticFloat4 *colors2 = new am::StaticFloat4{ { 1.0f, 0.0f, 0.0f, 1.0f }, { 0.0f, 1.0f, 0.0f, 1.0f }, { 0.0f, 0.0f, 1.0f, 1.0f } };
	am::StaticUInt *indices = new am::StaticUInt{ 2, 1, 0 };
	_mesh.set(am::VertexSlot::Position, vertices2);
	_mesh.set(am::VertexSlot::Color, colors2);
	_mesh.addIndexBuffer(indices);
	_uniforms.set("u_mvp", am::Matrix4f(1.0f));
	_uniforms.set("u_color", am::Vector4f(0.2f, 0.2f, 0.3f, 1.0f));
	_uniforms.set("u_useUniformColor", 1);*/

	// FullScreenQuad effect
	/*_effect = am::ResourceManager::load<am::Effect>(am::FileSystem::getFilePath("effects/FullScreenQuad/FullScreenQuad.fx"));
	_mesh = am::Mesh::createFullScreenQuad();
	*/

	outputs[1].effect = am::ResourceManager::load<am::Effect>(am::FileSystem::getFilePath("effects/FullScreenQuad/FullScreenQuad.fx"));
	outputs[1].mesh = am::Mesh::createFullScreenQuad();
	outputs[1].uniforms.set("textureUnit0", _texture);
	

	// 3D cube
	/*_effect = am::ResourceManager::load<am::Effect>(am::FileSystem::getFilePath("effects/Debug2/Debug.fx"));*/
	gbuffer.effect = am::ResourceManager::load<am::Effect>(am::FileSystem::getFilePath("effects/GBuffer/GBuffer.fx"));
	gbuffer.mesh = am::Mesh::createCube(0.5f, 0.5f, 0.5f);

	am::Matrix4f rotation = am::Quaternionf(am::Vector3f(30.0f, 0.0f, 0.0f)).getMatrix();
	am::Matrix4f scale = am::createScale(am::Vector3f(1.0f, 1.0f, 1.0f));

	am::Matrix4f modelMatrix = rotation * scale;
	am::Matrix4f viewMatrix = am::inverse(am::createTranslation(am::Vector3f(0.0f, 0.0f, 10.0f)));
	am::Matrix4f projectionMatrix = am::createPerspectiveMatrix(45.0f, (float)desc.width / (float)desc.height, 0.1f, 1000.0f);
	gbuffer.uniforms.set("red", am::Vector4f{ 1.0f, 0.0f, 0.0f, 0.0f });
	gbuffer.uniforms.set("model", modelMatrix);
	gbuffer.uniforms.set("view", viewMatrix);
	gbuffer.uniforms.set("projection", projectionMatrix);
	gbuffer.uniforms.set("mvp", projectionMatrix * viewMatrix * modelMatrix);
	gbuffer.uniforms.set("textureUnit0", _texture);


	am::ITexture::Desc diffuseDesc;
	diffuseDesc.width = desc.width;
	diffuseDesc.height = desc.height;
	diffuseDesc.pixelFormat = am::PixelFormat::RGBA;
	diffuseDesc.min = diffuseDesc.mag = am::FilterMode::Linear;
	diffuseDesc.s = diffuseDesc.r = diffuseDesc.t = am::WrapMode::ClampToEdge;
	diffuseTexture = _renderer->createTexture(diffuseDesc);

	am::ITexture::Desc normalDesc;
	normalDesc.width = desc.width;
	normalDesc.height = desc.height;
	normalDesc.pixelFormat = am::PixelFormat::RGBA;
	normalDesc.min = diffuseDesc.mag = am::FilterMode::Linear;
	normalDesc.s = diffuseDesc.r = diffuseDesc.t = am::WrapMode::ClampToEdge;
	normalTexture = _renderer->createTexture(normalDesc);

	am::IRenderTarget::Desc rtDesc;
	rtDesc.height = desc.height;
	rtDesc.width = desc.width;
	rtDesc.useHardwareDepth = true;
	rtDesc.slots[am::RenderTargetSlot::Diffuse] = diffuseTexture;
	rtDesc.slots[am::RenderTargetSlot::Normal] = normalTexture;
	_renderTarget = _renderer->createRenderTarget(rtDesc);

	_renderTargetEffect = am::ResourceManager::load<am::Effect>(am::FileSystem::getFilePath("effects/FullScreenQuad/FullScreenQuad.fx"));
	_renderTargetMesh = am::Mesh::createFullScreenQuad();
	_renderTargetUniforms.set("textureUnit0", diffuseTexture);

	outputs[2].effect = am::ResourceManager::load<am::Effect>(am::FileSystem::getFilePath("effects/FullScreenQuad/FullScreenQuad.fx"));
	outputs[2].mesh = am::Mesh::createFullScreenQuad();
	outputs[2].uniforms.set("textureUnit0", normalTexture);

	outputs[3].effect = am::ResourceManager::load<am::Effect>(am::FileSystem::getFilePath("effects/FullScreenQuad/FullScreenQuad.fx"));
	outputs[3].mesh = am::Mesh::createFullScreenQuad();
	outputs[3].uniforms.set("textureUnit0", diffuseTexture);

	outputs[4] = gbuffer;
}


void ExampleGame::mainLoop(float dt)
{
	_renderer->startFrame();
	auto events = am::Window::pollEvents();
	for (auto e : events)
	{
		switch (e.type)
		{
		case WindowEvent::Event_Close:
			shutdown();
			break;
		case WindowEvent::Event_MouseMove:
			//std::cerr << e.x << ", " << e.y << std::endl;
			break;
		case WindowEvent::Event_MouseDown:
			std::cerr << "mbuttonDown: " << e.mouseButton << std::endl;
			break;
		case WindowEvent::Event_MouseUp:
			std::cerr << "mbuttonUp: " << e.mouseButton << std::endl;
			break;
		case WindowEvent::Event_MouseWheel:
			std::cerr << "wheel: " << e.wheelDelta << std::endl;
		}
	}

	_renderer->setRenderTarget(_renderTarget);
	{
		am::Viewport vp{ 0, 0, 1280, 720 };
		_renderer->setViewport(vp);
		_renderer->setClearFlag(am::ClearFlag::All, { 0.1f, 0.1f, 0.1f, 1.0f }, 1.0f, 0.0f);
		_renderer->setCullFace(am::CullFace::Back);
		am::ShaderProgram* program = gbuffer.effect->getTechnique("GBuffer")->getProgram();
		_renderer->setShaderProgram(program);
		_renderer->setUniforms(&gbuffer.uniforms);
		_renderer->renderMesh(&gbuffer.mesh);
		_renderer->setUniforms(nullptr);
		_renderer->setShaderProgram(nullptr);
	}

	


	_renderer->setRenderTarget(nullptr);
	{
		am::Viewport vp{ 0, 0, 1280, 720 };
		_renderer->setViewport(vp);
		_renderer->setClearFlag(am::ClearFlag::All, { 0.0f, 0.0f, 0.0f, 1.0f }, 1.0f, 0.0f);
		_renderer->setCullFace(am::CullFace::Back);
		am::ShaderProgram* program = _renderTargetEffect->getTechnique("GBuffer")->getProgram();
		_renderer->setShaderProgram(program);
		_renderer->setUniforms(&_renderTargetUniforms);
		_renderer->renderMesh(&_renderTargetMesh);
		_renderer->setUniforms(nullptr);
		_renderer->setShaderProgram(nullptr);
	}

	//_renderer->setRenderTarget(nullptr);
	_renderer->setClearFlag(am::ClearFlag::Depth, { 0.1f, 0.1f, 0.1f, 1.0f }, 1.0f, 0.0f);
	//_renderer->setCullFace(am::CullFace::Back);
	unsigned int textureSize = 1280 / 5;
	for (size_t i = 0; i < 5; ++i)
	{

		am::Viewport vp{ i * textureSize, 720 - textureSize, textureSize, textureSize };
		_renderer->setViewport(vp);
		am::ShaderProgram* program = outputs[i].effect->getTechnique("GBuffer")->getProgram();
		_renderer->setShaderProgram(program);
		_renderer->setUniforms(&outputs[i].uniforms);
		_renderer->renderMesh(&outputs[i].mesh);
		_renderer->setUniforms(nullptr);
		_renderer->setShaderProgram(nullptr);
	}
	
	_renderer->endFrame();
}