#ifdef WIN32
#pragma comment(linker, "/subsystem:\"console\" /entry:\"WinMainCRTStartup\"")
#endif

#include "Application.h"

#include <iostream>


#include <time.h>
#include "ExampleGame.h"

int main()
{
	ExampleGame game;
	game.init();
	int ret = game.run();
	game.shutdown();

	return ret;
};