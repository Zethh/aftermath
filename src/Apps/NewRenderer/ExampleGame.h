#pragma once
#include "Application.h"
#include <Aftermath/render/Window.h>
#include <Aftermath/render/RenderTarget.h>
#include <Aftermath/math/Vector2.h>
#include <iostream>
#include <vector>
#include <Aftermath/render/Renderer.h>
#include <Aftermath/render/Effect.h>
#include <Aftermath/render/Uniform.h>
#include <Aftermath/render/Mesh.h>
#include <Aftermath/render/Texture.h>
#include <Aftermath/render/RenderTarget.h>

class ExampleGame : public Application
{
public:
	ExampleGame();

private:
	void mainLoop(float dt);

private:

	struct Batch
	{
		am::Effect::Ptr effect;
		am::Mesh mesh;
		am::UniformBuffer uniforms;
	};

	Batch gbuffer;
	Batch outputs[5];

	am::Window _mainWindow;
	am::Effect::Ptr _effect;
	am::Mesh _mesh;
	
	am::IRenderer::Ptr _renderer;
	am::UniformBuffer _uniforms;
	am::ITexture::Ptr _texture;

	am::Effect::Ptr _renderTargetEffect;
	am::IRenderTarget::Ptr _renderTarget;
	am::UniformBuffer _renderTargetUniforms;
	am::Mesh _renderTargetMesh;
	am::ITexture::Ptr diffuseTexture;
	am::ITexture::Ptr normalTexture;
};