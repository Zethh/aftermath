#include "Application.h"
#include <Aftermath/core/Timer.h>
#include <Aftermath/core/ResourceManager.h>

Application::Application()
: _isRunning(false)
{

}

void Application::init()
{

}

int Application::run()
{
	am::Timer t;
	_isRunning = true; 
	while (_isRunning)
	{
		t.tick();
		mainLoop(t.getDeltaTime());
		am::ResourceManager::update();
	}

	return 0;
}

void Application::shutdown()
{
	_isRunning = false;
}