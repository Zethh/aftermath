#version 330 core
uniform mat4 view;
uniform mat4 model;
uniform mat4 projection;
uniform mat4 modelview;

layout (location = 0) in vec3 position;
layout (location = 1) in vec2 uv;
layout (location = 2) in vec3 normal;

out vec2 out_uv;
out vec3 out_normal;
out vec4 out_test;
out vec4 out_position;

void main(void) 
{
	out_normal =  (transpose(inverse(modelview)) * vec4(normal, 0.0)).xyz; //transform local space normals to view space
	out_position = (modelview * vec4(position.xyz, 1.0));
	out_uv = uv;
	gl_Position = projection * view * model * vec4(position, 1.0);
	
	
}