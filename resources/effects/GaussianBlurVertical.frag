#version 330

uniform sampler2D u_shadowMap;
uniform vec2 u_shadowMapDimensions;

in vec2 texCoord0;
in vec2 fragCoord;

layout (location = 0) out vec4 finalColor;



void main()
{
	
	float offset[3] = float[]( 0.0, 1.3846153846, 3.2307692308 );
	float weight[3] = float[]( 0.2270270270, 0.3162162162, 0.0702702703 );
    finalColor = texture2D( u_shadowMap, vec2(fragCoord)/u_shadowMapDimensions ) * weight[0];

	vec2 multiplier = vec2(0.0, 1.0);
	
    for (int i=1; i<3; i++) 
	{
		finalColor += texture2D( u_shadowMap, ( fragCoord + offset[i] * multiplier )/u_shadowMapDimensions ) * weight[i];
		finalColor += texture2D( u_shadowMap, ( fragCoord - offset[i] * multiplier )/u_shadowMapDimensions ) * weight[i];
    }
}