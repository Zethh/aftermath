#version 330

uniform sampler2D textureUnit0;


in vec2 texCoord;
//out vec4 finalColor;
layout (location = 0) out vec4 finalColor;

void main(void) 
{
	finalColor = texture2D(textureUnit0, texCoord);

	//finalColor = vec4(0.0, 0.0, 0.0, 1.0);
	//finalColor.r = texture2D(textureUnit0, texCoord);

}