////Frag
#version 330

uniform int colorUniform;
uniform sampler2D textureUnit0;
uniform sampler2D textureUnit1;

precision mediump float;
in vec3 out_normal;
in vec4 out_test;
in vec4 out_position;
in vec2 out_uv;

layout (location = 0) out vec4 Albedo;
layout (location = 1) out vec4 Normal;
layout (location = 2) out vec4 Specular;
layout (location = 3) out vec2 Depth;

void main(void) 
{
//	Albedo = vec4(1.0, 1.0, 1.0, 1.0);
	Albedo = texture2D(textureUnit0, out_uv);
	Normal = texture2D(textureUnit1, out_uv);
	Normal = vec4(normalize(out_normal) * 0.5 + 0.5, 1.0);

	float depth = length(out_position);
	//float depth = out_position.z / out_position.w;

	float moment1 = depth;
	float moment2 = depth * depth;
	
	// Adjusting moments (this is sort of bias per pixel) using partial derivative
	float dx = dFdx(depth);
	float dy = dFdy(depth);
	moment2 += 0.25*(dx*dx+dy*dy) ;

	Depth.r = moment1;
	Depth.g = moment2;

	//Depth.r = length(out_position); 
		
	// ADD THIS TO DepthMap !
	//float depth = length(out_position); 
	//float dx = dFdx(depth);
	//float dy = dFdy(depth); 
	////gl_FragData[2].g = depth * depth + 0.25 * (dx * dx + dy * dy);
	//Depth.g = depth * depth + 0.25 * (dx * dx + dy * dy);*/
	
	float occluded = 0.0;
	if(depth == 0.0)
		occluded = 1.0;
	 
}
