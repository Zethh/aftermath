#version 330 core

uniform mat4 view;
uniform mat4 model;
uniform mat4 projection;
uniform mat4 modelview;
uniform mat4 transposeInvModelView;

layout (location = 0) in vec3 position;
layout (location = 1) in vec2 texCoord;
layout (location = 2) in vec3 normal;
layout (location = 3) in vec3 tangent;
layout (location = 4) in vec3 bitangent;

out vec2 out_texCoord;
out vec3 out_normal;
out vec3 out_tangent;
out vec3 out_bitangent;
out vec4 out_position;
out mat3 out_TBN;

void main(void)
{
	out_normal =  (transposeInvModelView * vec4(normal, 0.0)).xyz; //transform local space normals to view space
	out_tangent =  (transposeInvModelView * vec4(tangent, 0.0)).xyz; //transform local space tangent to view space
	out_bitangent =  (transposeInvModelView * vec4(bitangent, 0.0)).xyz; //transform local space bitangent to view space
	out_position = (modelview * vec4(position.xyz, 1.0));
	out_texCoord = texCoord;
	gl_Position = projection * view * model * vec4(position, 1.0);

	out_TBN = transpose(mat3(out_tangent, out_bitangent, out_normal));
}