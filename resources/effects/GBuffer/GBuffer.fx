<effect>
	<group id ="OpenGLDeferred">
		<technique id="GBuffer">
			<vertexshader>effects/GBuffer/GBuffer.vert</vertexshader>
			<fragmentshader>effects/GBuffer/GBuffer.frag</fragmentshader>
		</technique>	
	</group>	
	<group id ="DirectXDeferred">
		<technique id="GBuffer">
			<vertexshader>effects/GBuffer/GBuffer.verthlsl</vertexshader>
			<fragmentshader>effects/GBuffer/GBuffer.fraghlsl</fragmentshader>
		</technique>	
	</group>
</effect>