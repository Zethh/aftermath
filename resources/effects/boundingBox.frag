#version 130
precision mediump float;

uniform sampler2D textureUnit0;
uniform sampler2D textureUnit1;
in vec3 ex_colour;
in vec2 ex_uv;

void main(void) 
{
	gl_FragColor = vec4(ex_colour, 1.0); //texture2D(textureUnit0, ex_uv);
	//gl_FragColor.r = texture2D(textureUnit0, ex_uv)/1000.0f;

}
