<effect>
	<group id ="OpenGLDeferred">
		<technique id="GBuffer">
			<vertexshader>effects/Debug/Debug.vert</vertexshader>
			<fragmentshader>effects/Debug/Debug.frag</fragmentshader>
		</technique>	
	</group>	
	<group id ="DirectXDeferred">
		<technique id="GBuffer">
			<vertexshader>effects/Debug/Debug.verthlsl</vertexshader>
			<fragmentshader>effects/Debug/Debug.fraghlsl</fragmentshader>
		</technique>	
	</group>
</effect>