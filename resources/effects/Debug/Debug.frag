#version 330

in vec4 out_color;
layout (location = 0) out vec4 finalColor;
void main(void)
{
	finalColor = out_color;
}