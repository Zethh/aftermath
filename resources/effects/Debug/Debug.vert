#version 330 core

uniform mat4 u_mvp;
uniform vec4 u_color;
uniform int u_useUniformColor;

layout (location = 0) in vec3 position;
layout (location = 5) in vec4 color;

out vec4 out_color;
void main(void)
{
	if(u_useUniformColor == 1)
	{
		out_color = u_color;
	}
	else
	{
		out_color = color;
	}

	gl_Position = u_mvp * vec4(position, 1.0);
}