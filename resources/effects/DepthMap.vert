#version 150 core
uniform mat4 view;
uniform mat4 model;
uniform mat4 projection;
uniform mat4 modelview;

in vec3 position;
in vec3 normal;
in vec2 uv;

out vec3 out_normal;
out vec4 out_test;
out vec4 out_position;

void main(void) 
{
	out_normal =  (model * vec4(normal, 0.0)).xyz; //transform local space normals to world space
	
	out_test = model[0];
	
  
	out_position = (modelview * vec4(position.xyz, 1.0));
	gl_Position = projection * view * model * vec4(position, 1.0);
	
	
}