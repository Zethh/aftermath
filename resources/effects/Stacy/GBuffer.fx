<effect>
	<group id ="OpenGLDeferred">
		<technique id="GBuffer">
			<vertexshader>effects/Stacy/GBuffer.vert</vertexshader>
			<fragmentshader>effects/Stacy/GBuffer.frag</fragmentshader>
		</technique>	
	</group>	
	<group id ="DirectXDeferred">
		<technique id="GBuffer">
			<vertexshader>effects/GBuffer.verthlsl</vertexshader>
			<fragmentshader>effects/GBuffer.fraghlsl</fragmentshader>
		</technique>	
	</group>
</effect>