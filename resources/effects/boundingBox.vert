#version 130

uniform mat4 modelview;
uniform mat4 projection;
uniform mat4 model;
uniform mat4 view;

in vec3 position;
in vec3 normal;
in vec2 uv;


out vec3 ex_colour;
out vec2 ex_uv;

// multiply each vertex positison by the MVP matrix
void main(void) 
{
/*
	vec4 vertexPosition = vec4(position, 1.0);
	gl_Position = vertexPosition;
	
	ex_uv = vertexPosition.xy * 0.5 + 0.5;

*/

	// vertex into eye coordinates
//	vec4 vertexPosition = modelview* vec4(position,1.0);
	vec4 vertexPosition = view * model * vec4(position,1.0);
	gl_Position = projection* vertexPosition;

	ex_uv = uv;

	
	ex_colour = normal;
}
