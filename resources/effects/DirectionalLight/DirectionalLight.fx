<effect>
	<group id ="OpenGLDeferred">
		<technique id="DirectionalLight">
			<vertexshader>effects/DirectionalLight/DirectionalLight.vert</vertexshader>
			<fragmentshader>effects/DirectionalLight/DirectionalLight.frag</fragmentshader>
		</technique>
	
	</group>
	
</effect>