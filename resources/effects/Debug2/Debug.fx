<effect>
	<group id ="OpenGLDeferred">
		<technique id="GBuffer">
			<vertexshader>effects/Debug2/Debug.vert</vertexshader>
			<fragmentshader>effects/Debug2/Debug.frag</fragmentshader>
		</technique>	
	</group>	
	<group id ="DirectXDeferred">
		<technique id="GBuffer">
			<vertexshader>effects/Debug2/Debug.verthlsl</vertexshader>
			<fragmentshader>effects/Debug2/Debug.fraghlsl</fragmentshader>
		</technique>	
	</group>
</effect>