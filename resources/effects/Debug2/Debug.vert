#version 330 core

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

layout (location = 0) in vec3 position;
layout (location = 1) in vec2 texCoord;
layout (location = 2) in vec3 normal;

out vec4 out_color;
out vec2 out_texCoord;
void main(void)
{
	out_color = vec4(normal, 1.0);
	out_texCoord = texCoord;
	vec4 pos = vec4(position, 1.0);
	gl_Position = projection * view * model * pos;
}