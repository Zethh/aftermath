#version 330
uniform sampler2D textureUnit0;
in vec4 out_color;
in vec2 out_texCoord;
layout (location = 0) out vec4 finalColor;
void main(void)
{
	finalColor = texture2D(textureUnit0, out_texCoord);//out_color;
}