#version 330
uniform sampler2D u_gBufferColor;
uniform sampler2D u_gBufferNormal;
uniform sampler2D u_gBufferDepth;
uniform sampler2D u_gBufferSpecular;
uniform vec3 u_lightColor;
uniform mat4 u_frustumCorners;

uniform mat4 u_lightView;
uniform mat4 u_lightProjection;

in vec2 texCoord0;
in vec2 texCoord1;
in vec3 direction;

layout (location = 0) out vec4 finalColor;

void main(void)
{
	//find linear depth
	float depth = texture2D(u_gBufferDepth, texCoord0).r;
	if(depth == 0.2)
		discard;
	
	
	//Bilinear interpolation to find the weighted 3D point in our frustum
	vec3 P =  u_frustumCorners[0].xyz * texCoord1.x * texCoord0.y //D
			+ u_frustumCorners[1].xyz * texCoord0.x * texCoord0.y //C
			+ u_frustumCorners[2].xyz * texCoord0.x * texCoord1.y //B
			+ u_frustumCorners[3].xyz * texCoord1.x * texCoord1.y; //A
			

	vec4 diffuseColor = texture(u_gBufferColor, texCoord0);
	vec4 ambientColor = vec4(0.1, 0.1, 0.1, 1.0) * diffuseColor;
	vec4 materialSpecular = texture(u_gBufferSpecular, texCoord0);

	vec4 normalColor = vec4(texture(u_gBufferNormal, texCoord0).xyz, 1.0);
	normalColor = normalColor * 2.0 - 1.0;
	normalColor = normalize(normalColor);

	float materialShininess = 128.0;

	vec3 viewSpacePosition = normalize(P) * depth;
	vec3 viewSpaceNormal = texture(u_gBufferNormal, texCoord0).xyz; // [0, 1]
	viewSpaceNormal = viewSpaceNormal * 2.0 - 1.0; // [-1, 1]
	vec3 eye = -viewSpacePosition;
	vec4 spec = vec4(0.0);
	vec3 n = normalize(viewSpaceNormal);
	vec3 e = normalize(eye);
	vec3 ldir = direction;

	float intensity = max(dot(n, ldir), 0.1);
	if (intensity > 0.0)
	{
		vec3 h = normalize(ldir + e);
		float intSpec = max(dot(h, n), 0.0);
		spec = materialSpecular * pow(intSpec, materialShininess);
	}

	finalColor = max(intensity * vec4(u_lightColor, 1.0) * diffuseColor + spec, ambientColor);
}