<effect>
	<group id ="OpenGLDeferred">
		<technique id="DirectionalLightNormalMapping">
			<vertexshader>effects/DirectionalLightNormalMapping/DirectionalLightNormalMapping.vert</vertexshader>
			<fragmentshader>effects/DirectionalLightNormalMapping/DirectionalLightNormalMapping.frag</fragmentshader>
		</technique>
	</group>

	<group id ="DirectXDeferred">
		<technique id="DirectionalLightNormalMapping">
			<vertexshader>effects/DirectionalLightNormalMapping/DirectionalLightNormalMapping.verthlsl</vertexshader>
			<fragmentshader>effects/DirectionalLightNormalMapping/DirectionalLightNormalMapping.fraghlsl</fragmentshader>
		</technique>
	</group>
	
</effect>