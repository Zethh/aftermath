#version 330 core

uniform vec2 u_shadowMapDimensions;


layout (location = 0) in vec3 position;
out vec2 texCoord0;
out vec2 fragCoord;

void main()
{
	vec4 vertexPosition = vec4(position, 1.0);
	gl_Position = vertexPosition;
	
	fragCoord = vec2(0.0) + u_shadowMapDimensions * (1.0 + gl_Position.xy / gl_Position.w)/2.0;
	texCoord0 = vertexPosition.xy * 0.5 + 0.5;

}