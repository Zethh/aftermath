<effect>
	<group id ="OpenGLDeferred">
		<technique id="GaussianBlurVertical">
			<vertexshader>effects/GaussianBlurVertical.vert</vertexshader>
			<fragmentshader>effects/GaussianBlurVertical.frag</fragmentshader>
		</technique>
		
		<technique id="GaussianBlurHorizontal">
			<vertexshader>effects/GaussianBlurHorizontal.vert</vertexshader>
			<fragmentshader>effects/GaussianBlurHorizontal.frag</fragmentshader>
		</technique>
	</group>
	
</effect>