#version 330
uniform vec3 u_lightDirection;

in vec3 position;
out vec2 texCoord0;
out vec2 texCoord1;

void main()
{
	vec4 vertexPosition = vec4(position, 1.0);
	gl_Position = vertexPosition;
	texCoord0 = vertexPosition.xy * 0.5 + 0.5;
	texCoord1 = vec2(1, 1) - texCoord0;
}