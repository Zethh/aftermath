#version 330
uniform sampler2D u_gBufferColor;
uniform sampler2D u_gBufferDepth;
uniform sampler2D u_shadowMap;
uniform mat4 u_frustumCorners;
uniform mat4 u_cameraToLightView;
uniform mat4 u_shadowMatrix;

in vec2 texCoord0;
in vec2 texCoord1;

layout (location = 0) out vec4 finalColor;

float chebyshevUpperBound(vec2 moments, float t)
{
	if (t < moments.x)
		return 1.0;

	float minVariance = 0.1;

	//One tailed inequality valid if t > Moments.x
	float p = float(t <= moments.x);
	
	//Compute Variance
	float variance = moments.y - (moments.x * moments.x);
	variance = max(variance, minVariance);

	//Compute probalistic upper bound
	float d = t - moments.x;

	float pMax = variance / (variance + d*d);

	return max(p, pMax);

}


void main(void)
{

	//find linear depth
	float depth = texture2D(u_gBufferDepth, texCoord0).r;
//	if(depth == 0.0)
//		discard;
	
	//Bilinear interpolation to find the weighted 3D point in our frustum
	vec3 farClipRay = u_frustumCorners[0].xyz * texCoord1.x * texCoord0.y //D
					+ u_frustumCorners[1].xyz * texCoord0.x * texCoord0.y //C
					+ u_frustumCorners[2].xyz * texCoord0.x * texCoord1.y //B
					+ u_frustumCorners[3].xyz * texCoord1.x * texCoord1.y; //A
			

	vec3 viewPosition = normalize(farClipRay) * depth;

	vec3 lightPosition = vec3(u_cameraToLightView * vec4(viewPosition, 1));
	vec4 shadowCoord = u_shadowMatrix * vec4(viewPosition, 1);
	shadowCoord.xyz /= shadowCoord.w;
	vec2 moments = texture(u_shadowMap, shadowCoord.xy).xy;

	float shadow = 1.0;
	if (shadowCoord.x >= 0 && shadowCoord.x < 1 && shadowCoord.y >= 0 && shadowCoord.y < 1)
		shadow = chebyshevUpperBound(moments, length(lightPosition));

	finalColor = texture(u_gBufferColor, texCoord0) * shadow;
	
}

// http://gamedev.stackexchange.com/questions/62753/shadow-mapping-and-linear-depth
// https://developer.nvidia.com/sites/default/files/akamai/gamedev/docs/ShadowMaps_CEDEC_E.pdf?download=1