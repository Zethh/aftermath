#version 330

uniform sampler2D u_cameraOcclusion;
uniform sampler2D u_cameraColor;

uniform sampler2D u_gBufferDepth;
uniform sampler2D u_sunDepth;
uniform vec3 u_lightDirection;
uniform mat4 u_frustumCorners;
uniform mat4 u_cameraView;

uniform mat4 u_lightView;
uniform mat4 u_lightProjection;

in vec2 texCoord0;
in vec2 texCoord1;
out vec4 finalColor;
in vec2 texCoord;



void main(void) 
{
	float depth = texture2D(u_gBufferDepth, texCoord0).r;
	//if(depth == 0.0)
	//	discard;
		
	//Bilinear interpolation to find the weighted 3D point in our frustum
	vec3 P =  u_frustumCorners[0].xyz * texCoord1.x * texCoord0.y //D
			+ u_frustumCorners[1].xyz * texCoord0.x * texCoord0.y //C
			+ u_frustumCorners[2].xyz * texCoord0.x * texCoord1.y //B
			+ u_frustumCorners[3].xyz * texCoord1.x * texCoord1.y; //A

	vec3 viewSpacePosition = normalize(P) * depth;
	
	vec3 correctedLightPos = vec3(100.0, 100.0, 0.0) - (-viewSpacePosition);
	
	
	
	
	
	mat4 cameraViewToLightView = u_lightView * inverse(u_cameraView);
	mat4 cameraViewToLightViewProjection = u_lightProjection * cameraViewToLightView;			
	vec4 lightProjectedPosition = cameraViewToLightViewProjection * vec4(viewSpacePosition, 1.0);
	lightProjectedPosition.xyz /= lightProjectedPosition.w;		
	vec2 lightTexCoord = lightProjectedPosition.xy * 0.5 + 0.5;
 

	vec2 lightPixel = vec2(0.0) + vec2(1280.0, 720.0) * (1.0 + lightProjectedPosition.xy / lightProjectedPosition.w)/2.0;	
	
	float weight = 0.9; // 0 - 1
	float decay = 0.9; // 0 - 1
	float density = 0.9; // 0 - 1
	int numSamples = 100;
	float exposure = 1.0; // 0 - 10
	vec2 deltaTexCoord = texCoord0 - lightTexCoord;
	deltaTexCoord *= 1.0 / float(numSamples) * density;
	float illuminationDecay = 1.0;
	
	vec2 sampleTexCoord = texCoord0;
	
	
	for(int i = 0; i < numSamples; ++i)
	{
		sampleTexCoord -= deltaTexCoord;
		vec4 sample = texture2D(u_cameraOcclusion, sampleTexCoord);
		sample *= illuminationDecay * weight;
		finalColor += sample;
		illuminationDecay *= decay;
	}
	
	finalColor *= exposure;

			
			
			
			
	//finalColor = texture2D(u_cameraColor, texCoord);

}