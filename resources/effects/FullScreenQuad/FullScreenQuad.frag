#version 330

uniform sampler2D textureUnit0;

in vec2 texCoord;

layout (location = 0) out vec4 finalColor;

void main(void) 
{
	finalColor = texture2D(textureUnit0, texCoord);
}