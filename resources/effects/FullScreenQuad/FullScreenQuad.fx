<effect>
	<group id ="OpenGLDeferred">
		<technique id="GBuffer">
			<vertexshader>effects/FullScreenQuad/FullScreenQuad.vert</vertexshader>
			<fragmentshader>effects/FullScreenQuad/FullScreenQuad.frag</fragmentshader>
		</technique>	
	</group>	
	<group id ="DirectXDeferred">
		<technique id="GBuffer">
			<vertexshader>effects/FullScreenQuad/FullScreenQuad.verthlsl</vertexshader>
			<fragmentshader>effects/FullScreenQuad/FullScreenQuad.fraghlsl</fragmentshader>
		</technique>	
	</group>
</effect>