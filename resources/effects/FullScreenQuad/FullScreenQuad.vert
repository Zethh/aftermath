#version 330

layout (location = 0) in vec3 position;

out vec2 texCoord;

void main()
{
	vec4 vertexPosition = vec4(position, 1.0);
	gl_Position = vertexPosition;
	texCoord = vertexPosition.xy * 0.5 + 0.5;

}