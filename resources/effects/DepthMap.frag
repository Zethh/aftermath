////Frag
#version 150 core

uniform int colorUniform;


precision mediump float;
in vec3 out_normal;
in vec4 out_test;
in vec4 out_position;
void main(void) 
{
	gl_FragData[0].r = length(out_position)/10000.0; 

}
