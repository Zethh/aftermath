<effect>
	<group id ="OpenGLDeferred">
		<technique id="GBuffer">
			<vertexshader>effects/Character/GBuffer.vert</vertexshader>
			<fragmentshader>effects/Character/GBuffer.frag</fragmentshader>
		</technique>	
	</group>	
	<group id ="DirectXDeferred">
		<technique id="GBuffer">
			<vertexshader>effects/GBuffer.verthlsl</vertexshader>
			<fragmentshader>effects/GBuffer.fraghlsl</fragmentshader>
		</technique>	
	</group>
</effect>