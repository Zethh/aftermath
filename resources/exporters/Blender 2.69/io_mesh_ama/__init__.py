bl_info = {
    "name":         "Aftermath Ascii",
    "author":       "Christoffer Pettersson",
    "blender":      (2,6,2),
    "version":      (0,0,1),
    "location":     "File > Import-Export",
    "description":  "Export custom data format",
    "category":     "Import-Export"
}

import mathutils
import bpy
from bpy_extras.io_utils import ExportHelper
import threading
import time
import struct
SZ_SHORT = 2
SZ_INT = 4
SZ_FLOAT = 4

##############################    
def uv_key(uv):
    return round(uv[0], 6), round(uv[1], 6)
##############################

##############################
class Array(object):
    __slots__ = "values", "size"

    def __init__(self):
        self.values = []
        self.size = SZ_SHORT

    # add an item:
    def add(self, item):
        self.values.append(item)
        self.size += item.get_size()

    def get_size(self):
        return self.size

    def validate(self):
        return len(self.values) <= 65535

    def write(self, file):
        _3ds_ushort(len(self.values)).write(file)
        for value in self.values:
            value.write(file)

    # To not overwhelm the output in a dump, a _3ds_array only
    # outputs the number of items, not all of the actual items.
    def __str__(self):
        return '(%d items)' % len(self.values)
##############################

##############################
class Vector3f(object):
    __slots__ = "x", "y", "z"

    def __init__(self, point):
        self.x, self.y, self.z = point

    def get_size(self):
        return 3 * SZ_FLOAT

    def write(self, file):
        file.write(struct.pack('<3f', self.x, self.y, self.z))

    def __str__(self):
        return '(%f, %f, %f)' % (self.x, self.y, self.z)
##############################

##############################    
class Vector2f(object):
    __slots__ = ("uv", )

    def __init__(self, point):
        self.uv = point

    def get_size(self):
        return 2 * SZ_FLOAT

    def write(self, file):
        data = struct.pack('<2f', self.uv[0], self.uv[1])
        file.write(data)

    def __str__(self):
        return '(%g, %g)' % self.uv
##############################

##############################    
class Triangle(object):
    """Class representing a triangle.

    Used when converting faces to triangles"""

    __slots__ = "vertex_index", "mat", "image", "faceuvs", "offset"

    def __init__(self, vindex=(0, 0, 0), mat=None, image=None, faceuvs=None):
        self.vertex_index = vindex
        self.mat = mat
        self.image = image
        self.faceuvs = faceuvs
        self.offset = [0, 0, 0]  # offset indices
##############################        

##############################        
def extract_triangles(mesh):
    """Extract triangles from a mesh.

    If the mesh contains quads, they will be split into triangles."""
    tri_list = []
    do_uv = bool(mesh.tessface_uv_textures)

    img = None
    for i, face in enumerate(mesh.tessfaces):
        f_v = face.vertices

        uf = mesh.tessface_uv_textures.active.data[i] if do_uv else None

        if do_uv:
            f_uv = uf.uv
            img = uf.image if uf else None
            if img is not None:
                img = img.name

        # if f_v[3] == 0:
        if len(f_v) == 3:
            new_tri = Triangle((f_v[0], f_v[1], f_v[2]), face.material_index, img)
            if (do_uv):
                new_tri.faceuvs = uv_key(f_uv[0]), uv_key(f_uv[1]), uv_key(f_uv[2])
            tri_list.append(new_tri)

        else:  # it's a quad
            new_tri = Triangle((f_v[0], f_v[1], f_v[2]), face.material_index, img)
            new_tri_2 = Triangle((f_v[0], f_v[2], f_v[3]), face.material_index, img)

            if (do_uv):
                new_tri.faceuvs = uv_key(f_uv[0]), uv_key(f_uv[1]), uv_key(f_uv[2])
                new_tri_2.faceuvs = uv_key(f_uv[0]), uv_key(f_uv[2]), uv_key(f_uv[3])

            tri_list.append(new_tri)
            tri_list.append(new_tri_2)

    return tri_list
##############################

##############################
def remove_face_uv(verts, tri_list):
    """Remove face UV coordinates from a list of triangles.

    Since 3ds files only support one pair of uv coordinates for each vertex, face uv coordinates
    need to be converted to vertex uv coordinates. That means that vertices need to be duplicated when
    there are multiple uv coordinates per vertex."""

    # initialize a list of UniqueLists, one per vertex:
    #uv_list = [UniqueList() for i in xrange(len(verts))]
    unique_uvs = [{} for i in range(len(verts))]

    # for each face uv coordinate, add it to the UniqueList of the vertex
    for tri in tri_list:
        for i in range(3):
            # store the index into the UniqueList for future reference:
            # offset.append(uv_list[tri.vertex_index[i]].add(_3ds_point_uv(tri.faceuvs[i])))

            context_uv_vert = unique_uvs[tri.vertex_index[i]]
            uvkey = tri.faceuvs[i]

            offset_index__uv_3ds = context_uv_vert.get(uvkey)

            if not offset_index__uv_3ds:
                offset_index__uv_3ds = context_uv_vert[uvkey] = len(context_uv_vert), Vector2f(uvkey)

            tri.offset[i] = offset_index__uv_3ds[0]

    # At this point, each vertex has a UniqueList containing every uv coordinate that is associated with it
    # only once.

    # Now we need to duplicate every vertex as many times as it has uv coordinates and make sure the
    # faces refer to the new face indices:
    vert_index = 0
    vert_array = Array()
    norm_array = Array()
    uv_array = Array()
    index_list = []
    for i, vert in enumerate(verts):
        index_list.append(vert_index)
        pt = Vector3f(vert.co)  # reuse, should be ok
        norm = Vector3f(vert.normal)
        uvmap = [None] * len(unique_uvs[i])
        for ii, uv_3ds in unique_uvs[i].values():
            # add a vertex duplicate to the vertex_array for every uv associated with this vertex:
            vert_array.add(pt)
            norm_array.add(norm);
            # add the uv coordinate to the uv array:
            # This for loop does not give uv's ordered by ii, so we create a new map
            # and add the uv's later
            # uv_array.add(uv_3ds)
            uvmap[ii] = uv_3ds

        # Add the uv's in the correct order
        for uv_3ds in uvmap:
            # add the uv coordinate to the uv array:
            uv_array.add(uv_3ds)

        vert_index += len(unique_uvs[i])

    # Make sure the triangle vertex indices now refer to the new vertex list:
    for tri in tri_list:
        for i in range(3):
            tri.offset[i] += index_list[tri.vertex_index[i]]
        tri.vertex_index = tri.offset

    return vert_array, norm_array, uv_array, tri_list
##############################

        

              
    

##############################     
class ExportMyFormat(bpy.types.Operator, ExportHelper):
    bl_idname       = "aftermathascii.ama";
    bl_label        = "Aftermath Ascii";
    bl_options      = {'PRESET'};
    
    filename_ext    = ".ama";
    

    
    def execute(self, context):
        bpy.ops.object.mode_set(mode='OBJECT');
        
        # set default return state
        result = {'FINISHED'};
        
        #check if the selected object contains meshes
        ob = context.object;
        if not ob or ob.type != 'MESH':
            raise NameError("Cannot Export: object %s is not a mesh" % ob);
            
        print("Processing mesh: " + ob.name);
        scene = context.scene;
        mesh = ob.to_mesh(scene, apply_modifiers=True, settings='PREVIEW', calc_tessface=True, calc_undeformed=False);
        
        # Export mesh information
        file = open(self.filepath, 'w');
      
            
        triangles = extract_triangles(mesh);
        vertices, normals, texcoords, triangles = remove_face_uv(mesh.vertices, triangles);
        
        file.write('%d %d %d %d \n' % (len(vertices.values), len(normals.values), len(texcoords.values), len(triangles) * 3));
        for v in vertices.values:
            file.write('%f %f %f ' % (v.x, v.y, v.z));
          
        file.write("\n");  
        for n in normals.values:
            file.write('%f %f %f ' % (n.x, n.y, n.z));
           
        file.write("\n"); 
        for u in texcoords.values:
            file.write('%f %f ' % (u.uv[0], u.uv[1]));

        file.write("\n");             
        for t in triangles:
            file.write('%d %d %d ' % (t.vertex_index[0], t.vertex_index[1], t.vertex_index[2]));
        
        print('\n\n')
        numWeights = 0;
        for vertex in mesh.vertices:
            numWeights += len(vertex.groups);
        
        
        
        print('Total number of joints: %d ' % len(ob.vertex_groups));
        for joint in (ob.vertex_groups):
            print("joint " + ('%d:' % joint.index) + joint.name);
            
        
        print('Total number of weights: %d ' % numWeights);
        print('Total number of vertices: %d ' % len(mesh.vertices));
        for vertex in mesh.vertices:
            print("vertex " + ('%d ' % vertex.index));
            for weight in vertex.groups:
                joint = ob.vertex_groups[weight.group];
                print(' jointName: %s jointID: %d Bias: %f ' % (joint.name, joint.index, weight.weight));
        
        
        
        """
        print("Joint Hierarchy");
        for i in range(len(ob.vertex_groups)):
            indent = "";
            for numIndents in range(i):
                indent += " "
            print(indent + ('%d: ' % i) + ob.vertex_groups[i].name);
        
        numWeights = 0;
        for vertex in mesh.vertices:
            numWeights += len(vertex.groups);
        
        print('Total number of weights: %d ' % numWeights);
            
        for vertex in mesh.vertices:
            print('vertex: %d - num weights: %d' % (vertex.index, len(vertex.groups)));
            sum = 0;
            for group in vertex.groups:
                print (ob.vertex_groups[group.group].name)
                print(group.group);
                print('\tweight: %f' % (group.weight))
                sum += group.weight;
            print('\tsum: %f' % sum);
#            for group in vertex.groups:
#                print(vertex, group, group.weight)
        """
            
        file.close();
        return result;
##############################

##############################
def menu_func(self, context):
    self.layout.operator(ExportMyFormat.bl_idname, text="Aftermath Ascii(.ama)");
##############################

##############################
def register():
    bpy.utils.register_module(__name__);
    bpy.types.INFO_MT_file_export.append(menu_func);
##############################

##############################    
def unregister():
    bpy.utils.unregister_module(__name__);
    bpy.types.INFO_MT_file_export.remove(menu_func);
##############################

##############################
if __name__ == "__main__":
    register()
##############################